﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NewBehaviourScript : MonoBehaviour {

	public GameObject Player;
	// Update is called once per frame
	void Update ()
	{
		var PlayerObj = Player.gameObject.transform.GetChild(4).gameObject;
		var PlayerText = PlayerObj.GetComponent<Text>();
		PlayerText.text = PlayerText.text.ToLower ();
	}
}
