﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UNActive : MonoBehaviour {
	public GameObject Maskbtngrp;

	// Update is called once per frame
	void Start ()
	{
		Debug.Log("Dev - Start UNAction");
		Maskbtngrp.gameObject.SetActive (true);
		StartCoroutine(STP(120F));
	}

	private IEnumerator STP(float time)
	{
		yield return new WaitForSeconds(time);
		Maskbtngrp.gameObject.SetActive (false);
	}

}
