﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SkipActive : MonoBehaviour {
	public GameObject Maskbtn;
	public GameObject Skipbtn;
	public GameObject Voice;

	// Update is called once per frame
	void Start ()
	{
		Debug.Log("Dev - Start SkipAction");
		Maskbtn.gameObject.SetActive (false);
		Skipbtn.gameObject.SetActive (false);
		StartCoroutine(Waiting(2F));
	}

	public void ActiveSkip ()
	{
		Maskbtn.gameObject.SetActive (false);
		Skipbtn.gameObject.SetActive (true);
		StartCoroutine(Waiting(4F));
	}

	public void GotoMenu ()
	{
		Voice.gameObject.SetActive (false);
		SceneManager.LoadScene ("Loading");	
	}


	private IEnumerator Waiting(float time)
	{
		yield return new WaitForSeconds(time);
		Maskbtn.gameObject.SetActive (true);
		Skipbtn.gameObject.SetActive (false);
	}

}
