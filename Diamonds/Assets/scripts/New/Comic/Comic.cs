﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Comic : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		Debug.Log("Dev - Start Comic");
		StartCoroutine(GoToMenu(139F));
	}

	private IEnumerator GoToMenu(float time)
	{
		yield return new WaitForSeconds(time);
		SceneManager.LoadScene ("Loading");
	}

	void Update()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}
}
