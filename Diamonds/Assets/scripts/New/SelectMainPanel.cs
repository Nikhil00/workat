﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;

public class SelectMainPanel : MonoBehaviour
{

    public GameObject[] UnLock;
    public GameObject[] Lock;
    public ObscuredLong BestScore;

    // Update is called once per frame
    void Start()
    {
        BestScore = GameData.getInstance().bestScore;

        // //Item01
        // if (ObscuredPrefs.HasKey("score3K") || BestScore >= 3000)
        // {
        //     Lock[1].gameObject.SetActive(false);
        //     UnLock[1].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score3K", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score3K"))
        // {
        //     Lock[1].gameObject.SetActive(true);
        //     UnLock[1].gameObject.SetActive(false);
        // }
        // //Item02
        // if (ObscuredPrefs.HasKey("score15K") || BestScore >= 15000)
        // {
        //     Lock[2].gameObject.SetActive(false);
        //     UnLock[2].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score15K", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score15K"))
        // {
        //     Lock[2].gameObject.SetActive(true);
        //     UnLock[2].gameObject.SetActive(false);
        // }
        // //Item03
        // if (ObscuredPrefs.HasKey("score30K") || BestScore >= 30000)
        // {
        //     Lock[3].gameObject.SetActive(false);
        //     UnLock[3].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score30K", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score30K"))
        // {
        //     Lock[3].gameObject.SetActive(true);
        //     UnLock[3].gameObject.SetActive(false);
        // }
        // //Item04
        // if (ObscuredPrefs.HasKey("score60K") || BestScore >= 60000)
        // {
        //     Lock[4].gameObject.SetActive(false);
        //     UnLock[4].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score60K", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score60K"))
        // {
        //     Lock[4].gameObject.SetActive(true);
        //     UnLock[4].gameObject.SetActive(false);
        // }
        // //Item05
        // if (ObscuredPrefs.HasKey("score100K") || BestScore >= 100000)
        // {
        //     Lock[5].gameObject.SetActive(false);
        //     UnLock[5].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score100K", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score100K"))
        // {
        //     Lock[5].gameObject.SetActive(true);
        //     UnLock[5].gameObject.SetActive(false);
        // }
        // //Item06
        // if (ObscuredPrefs.HasKey("score150K") || BestScore >= 150000)
        // {
        //     Lock[6].gameObject.SetActive(false);
        //     UnLock[6].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score150K", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score150K"))
        // {
        //     Lock[6].gameObject.SetActive(true);
        //     UnLock[6].gameObject.SetActive(false);
        // }
        // //Item07
        // if (ObscuredPrefs.HasKey("score300K") || BestScore >= 300000)
        // {
        //     Lock[7].gameObject.SetActive(false);
        //     UnLock[7].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score300K", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score300K"))
        // {
        //     Lock[7].gameObject.SetActive(true);
        //     UnLock[7].gameObject.SetActive(false);
        // }
        // //Item08
        // if (ObscuredPrefs.HasKey("score400K") || BestScore >= 400000)
        // {
        //     Lock[8].gameObject.SetActive(false);
        //     UnLock[8].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score400K", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score400K"))
        // {
        //     Lock[8].gameObject.SetActive(true);
        //     UnLock[8].gameObject.SetActive(false);
        // }
        // //Item09
        // if (ObscuredPrefs.HasKey("score600K") || BestScore >= 600000)
        // {
        //     Lock[9].gameObject.SetActive(false);
        //     UnLock[9].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score600K", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score600K"))
        // {
        //     Lock[9].gameObject.SetActive(true);
        //     UnLock[9].gameObject.SetActive(false);
        // }
        // //Item10
        // if (ObscuredPrefs.HasKey("score1T") || BestScore >= 1000000)
        // {
        //     Lock[10].gameObject.SetActive(false);
        //     UnLock[10].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score1T", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score1T"))
        // {
        //     Lock[10].gameObject.SetActive(true);
        //     UnLock[10].gameObject.SetActive(false);
        // }
        // //Item11
        // if (ObscuredPrefs.HasKey("score1.5T") || BestScore >= 1500000)
        // {
        //     Lock[11].gameObject.SetActive(false);
        //     UnLock[11].gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score1.5T", 1);
        // }

        // if (!ObscuredPrefs.HasKey("score1.5T"))
        // {
        //     Lock[11].gameObject.SetActive(true);
        //     UnLock[11].gameObject.SetActive(false);
        // }
        //testing unlock all levels 
        BestScore = 1500001;
		GameData.getInstance().cScore = BestScore;
        for (int i = 0; i < Lock.Length; i++)
        {
            Lock[i].gameObject.SetActive(false);
            UnLock[i].gameObject.SetActive(true);
        }
    }
}
