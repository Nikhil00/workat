﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {
	public GameObject FinalFade;

	void Start ()
	{
		Debug.Log("Dev - Start LevelLoader");
		FinalFade.gameObject.SetActive (false);
		StartCoroutine(Waiting(5F));
		StartCoroutine(Waiting2(6F));
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}
		
	private IEnumerator Waiting(float time)
	{
		yield return new WaitForSeconds(time);
		FinalFade.gameObject.SetActive (true);
	}

	private IEnumerator Waiting2(float time)
	{
		yield return new WaitForSeconds(time);
		SceneManager.LoadScene ("startscene");	
	}

	// Nik - Code Change || 04-04
	// void Update()
	// {
	// 	Screen.sleepTimeout = SleepTimeout.NeverSleep;
	// }
}
