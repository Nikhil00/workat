﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;

public class PaperRank : MonoBehaviour {
	public GameObject[] Ranks;

	void Update ()
	{
		if (ObscuredPrefs.HasKey ("RANK00")) {
			Ranks [11].gameObject.SetActive (true);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK01")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (true);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK02")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (true);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK03")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (true);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK04")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (true);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK05")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (true);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK06")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (true);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK07")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (true);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK08")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (true);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK09")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (true);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK10")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (true);
			Ranks [10].gameObject.SetActive (false);
		}

		if (ObscuredPrefs.HasKey ("RANK11")) {
			Ranks [11].gameObject.SetActive (false);
			Ranks [0].gameObject.SetActive (false);
			Ranks [1].gameObject.SetActive (false);
			Ranks [2].gameObject.SetActive (false);
			Ranks [3].gameObject.SetActive (false);
			Ranks [4].gameObject.SetActive (false);
			Ranks [5].gameObject.SetActive (false);
			Ranks [6].gameObject.SetActive (false);
			Ranks [7].gameObject.SetActive (false);
			Ranks [8].gameObject.SetActive (false);
			Ranks [9].gameObject.SetActive (false);
			Ranks [10].gameObject.SetActive (true);
		}
	}

}
