﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;

public class LevelUp : MonoBehaviour
{
    public ObscuredLong CurrentScore;
    public ObscuredLong BestScore;
    public GameObject Black;
    public GameObject WinPanel;
    public GameObject DiamondsPanel;
    public GameObject[] Level;
    public GameObject[] Ranks;
    public GameObject[] About;
    public BoxCollider2D[] Items;

    public GameObject CS;

    Text lb_coin;

    // Update is called once per frame
    void Update()
    {
        // lb_coin = GameObject.Find("money").GetComponentInChildren<Text>();
        // lb_coin.text = GameData.getInstance().cCoin.ToString();

        for (int i = 0; i < Level.Length; i++)
        {
            if (ObscuredPrefs.HasKey("RANK0"+ i) || ObscuredPrefs.HasKey("RANK"+ i))
            {
                if (i == 0)
                {
                    Level[11].gameObject.SetActive(true);
                    Ranks[11].gameObject.SetActive(true);
                    About[11].gameObject.SetActive(true);
                }
                else// (i == 1)
                {
                    Level[i - 1].gameObject.SetActive(true);
                    Ranks[i - 1].gameObject.SetActive(true);
                    About[i - 1].gameObject.SetActive(true);
                }
                break;
            }
        }
#region Old Code
        // if (ObscuredPrefs.HasKey("RANK00"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(true);
        //     CS.gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(true);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(true);
        // }

        // if (ObscuredPrefs.HasKey("RANK01"))
        // {
        //     Level[0].gameObject.SetActive(true);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(true);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(true);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK02"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(true);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);
        //     //CS.gameObject.SetActive (false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(true);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(true);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK03"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(true);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);
        //     CS.gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(true);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(true);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK04"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(true);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);
        //     CS.gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(true);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(true);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK05"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(true);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);
        //     CS.gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(true);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(true);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK06"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(true);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);
        //     CS.gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(true);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(true);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK07"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(true);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);
        //     CS.gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(true);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(true);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK08"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(true);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);
        //     CS.gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(true);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(true);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK09"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(true);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);
        //     CS.gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(true);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(true);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK10"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(true);
        //     Level[10].gameObject.SetActive(false);
        //     Level[11].gameObject.SetActive(false);
        //     CS.gameObject.SetActive(false);

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(true);
        //     Ranks[10].gameObject.SetActive(false);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(true);
        //     About[10].gameObject.SetActive(false);
        //     About[11].gameObject.SetActive(false);
        // }

        // if (ObscuredPrefs.HasKey("RANK11"))
        // {
        //     Level[0].gameObject.SetActive(false);
        //     Level[1].gameObject.SetActive(false);
        //     Level[2].gameObject.SetActive(false);
        //     Level[3].gameObject.SetActive(false);
        //     Level[4].gameObject.SetActive(false);
        //     Level[5].gameObject.SetActive(false);
        //     Level[6].gameObject.SetActive(false);
        //     Level[7].gameObject.SetActive(false);
        //     Level[8].gameObject.SetActive(false);
        //     Level[9].gameObject.SetActive(false);
        //     Level[10].gameObject.SetActive(true);
        //     Level[11].gameObject.SetActive(false);

        //     if (ObscuredPrefs.HasKey("RANKCS")) //&& ObscuredPrefs.HasKey ("CS"))
        //     {
        //         Level[10].gameObject.SetActive(false);
        //         CS.gameObject.SetActive(true);
        //     }

        //     else
        //     {
        //         Level[10].gameObject.SetActive(true);
        //         CS.gameObject.SetActive(false);
        //     }

        //     Ranks[11].gameObject.SetActive(false);
        //     Ranks[0].gameObject.SetActive(false);
        //     Ranks[1].gameObject.SetActive(false);
        //     Ranks[2].gameObject.SetActive(false);
        //     Ranks[3].gameObject.SetActive(false);
        //     Ranks[4].gameObject.SetActive(false);
        //     Ranks[5].gameObject.SetActive(false);
        //     Ranks[6].gameObject.SetActive(false);
        //     Ranks[7].gameObject.SetActive(false);
        //     Ranks[8].gameObject.SetActive(false);
        //     Ranks[9].gameObject.SetActive(false);
        //     Ranks[10].gameObject.SetActive(true);

        //     About[0].gameObject.SetActive(false);
        //     About[1].gameObject.SetActive(false);
        //     About[2].gameObject.SetActive(false);
        //     About[3].gameObject.SetActive(false);
        //     About[4].gameObject.SetActive(false);
        //     About[5].gameObject.SetActive(false);
        //     About[6].gameObject.SetActive(false);
        //     About[7].gameObject.SetActive(false);
        //     About[8].gameObject.SetActive(false);
        //     About[9].gameObject.SetActive(false);
        //     About[10].gameObject.SetActive(true);
        //     About[11].gameObject.SetActive(false);
        // }
#endregion
        
        
        CurrentScore = GameData.getInstance().cScore;
        BestScore = GameData.getInstance().bestScore;

#region Non Use Code Level Completeed

        //level00
        // if (CurrentScore >= 0 && !ObscuredPrefs.HasKey("score0K") && CurrentScore >= BestScore)
        // {
        //     ObscuredPrefs.SetInt("score0K", 1);
        //     ObscuredPrefs.SetInt("RANK00", 1);
        // }

        // //level01
        // // if (CurrentScore >= 3000 && !ObscuredPrefs.HasKey("score3K") && CurrentScore >= BestScore)
        // if (GameData.getInstance().cLevel == 1)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score3K", 1);
        //     ObscuredPrefs.DeleteKey("RANK00");
        //     ObscuredPrefs.SetInt("RANK01", 1);
        //     GameData.getInstance().cCoin += 100;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level02
        // // if (CurrentScore >= 15000 && !ObscuredPrefs.HasKey("score15K") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 2)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score15K", 1);
        //     ObscuredPrefs.DeleteKey("RANK01");
        //     ObscuredPrefs.SetInt("RANK02", 1);
        //     GameData.getInstance().cCoin += 100;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level03
        // // if (CurrentScore >= 30000 && !ObscuredPrefs.HasKey("score30K") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 3)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score30K", 1);
        //     ObscuredPrefs.DeleteKey("RANK02");
        //     ObscuredPrefs.SetInt("RANK03", 1);
        //     GameData.getInstance().cCoin += 100;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level04
        // // if (CurrentScore >= 60000 && !ObscuredPrefs.HasKey("score60K") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 4)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score60K", 1);
        //     ObscuredPrefs.DeleteKey("RANK03");
        //     ObscuredPrefs.SetInt("RANK04", 1);
        //     GameData.getInstance().cCoin += 200;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level05
        // // if (CurrentScore >= 100000 && !ObscuredPrefs.HasKey("score100K") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 5)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score100K", 1);
        //     ObscuredPrefs.DeleteKey("RANK04");
        //     ObscuredPrefs.SetInt("RANK05", 1);
        //     GameData.getInstance().cCoin += 200;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level06
        // // if (CurrentScore >= 150000 && !ObscuredPrefs.HasKey("score150K") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 6)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score150K", 1);
        //     ObscuredPrefs.DeleteKey("RANK05");
        //     ObscuredPrefs.SetInt("RANK06", 1);
        //     GameData.getInstance().cCoin += 200;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level07
        // // if (CurrentScore >= 300000 && !ObscuredPrefs.HasKey("score300K") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 7)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score300K", 1);
        //     ObscuredPrefs.DeleteKey("RANK06");
        //     ObscuredPrefs.SetInt("RANK07", 1);
        //     GameData.getInstance().cCoin += 200;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level08
        // // if (CurrentScore >= 400000 && !ObscuredPrefs.HasKey("score400K") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 8)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score400K", 1);
        //     ObscuredPrefs.DeleteKey("RANK07");
        //     ObscuredPrefs.SetInt("RANK08", 1);
        //     GameData.getInstance().cCoin += 300;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level09
        // // if (CurrentScore >= 600000 && !ObscuredPrefs.HasKey("score600K") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 9)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score600K", 1);
        //     ObscuredPrefs.DeleteKey("RANK08");
        //     ObscuredPrefs.SetInt("RANK09", 1);
        //     GameData.getInstance().cCoin += 300;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level10
        // // if (CurrentScore >= 1000000 && !ObscuredPrefs.HasKey("score1T") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 10)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score1T", 1);
        //     ObscuredPrefs.DeleteKey("RANK09");
        //     ObscuredPrefs.SetInt("RANK10", 1);
        //     GameData.getInstance().cCoin += 600;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //level11
        // // if (CurrentScore >= 1500000 && !ObscuredPrefs.HasKey("score1.5T") && CurrentScore >= BestScore)
		// if (GameData.getInstance().cLevel == 11)
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("score1.5T", 1);
        //     ObscuredPrefs.DeleteKey("RANK10");
        //     ObscuredPrefs.SetInt("RANK11", 1);
        //     GameData.getInstance().cCoin += 600;
        //     ObscuredPrefs.SetString("coin", GameData.getInstance().cCoin.ToString());
        //     // Nik - Code Change || 07-04
        //     // lb_coin.text = GameData.getInstance().cCoin.ToString();
		// 	GameData.getInstance().cLevel = 0;
        // }
        // //levelCS
        // if (ObscuredPrefs.HasKey("RANKCS") && !ObscuredPrefs.HasKey("scoreCS"))
        // {
        //     GameManager.getInstance().playSfx("finish");
        //     WinPanel.gameObject.SetActive(true);
        //     GameData.getInstance().isLock = 1;
        //     Black.gameObject.SetActive(true);
        //     ObscuredPrefs.SetInt("scoreCS", 1);
        // }
#endregion
    }

    public void HideLevelup()
    {
        if (ObscuredPrefs.HasKey("RANK11") && !ObscuredPrefs.HasKey("RANKCS"))
        {
            Level[0].gameObject.SetActive(false);
            Level[1].gameObject.SetActive(false);
            Level[2].gameObject.SetActive(false);
            Level[3].gameObject.SetActive(false);
            Level[4].gameObject.SetActive(false);
            Level[5].gameObject.SetActive(false);
            Level[6].gameObject.SetActive(false);
            Level[7].gameObject.SetActive(false);
            Level[8].gameObject.SetActive(false);
            Level[9].gameObject.SetActive(false);
            Level[10].gameObject.SetActive(false);
            Level[11].gameObject.SetActive(false);
            CS.gameObject.SetActive(false);

            ObscuredPrefs.SetInt("RANKCS", 1);
            //ObscuredPrefs.SetInt ("CS", 1);
            Black.gameObject.SetActive(false);
            GameData.getInstance().isLock = 0;
            WinPanel.gameObject.SetActive(false);
        }
        else
        {
            Level[0].gameObject.SetActive(false);
            Level[1].gameObject.SetActive(false);
            Level[2].gameObject.SetActive(false);
            Level[3].gameObject.SetActive(false);
            Level[4].gameObject.SetActive(false);
            Level[5].gameObject.SetActive(false);
            Level[6].gameObject.SetActive(false);
            Level[7].gameObject.SetActive(false);
            Level[8].gameObject.SetActive(false);
            Level[9].gameObject.SetActive(false);
            Level[10].gameObject.SetActive(false);
            Level[11].gameObject.SetActive(false);
            CS.gameObject.SetActive(false);

            ObscuredPrefs.DeleteKey("RANKCS");
            //ObscuredPrefs.DeleteKey ("CS");
            Black.gameObject.SetActive(false);
            GameData.getInstance().isLock = 0;
            WinPanel.gameObject.SetActive(false);
        }
    }

    public void ShowWinPanel()
    {
        GameManager.getInstance().playSfx("levelup");
        WinPanel.gameObject.SetActive(true);
        GameData.getInstance().isLock = 1;
        Black.gameObject.SetActive(true);
    }

    public void ShowDiamondsPanel()
    {
        GameManager.getInstance().playSfx("levelup");
        DiamondsPanel.gameObject.SetActive(true);
        GameData.getInstance().isLock = 1;
        Black.gameObject.SetActive(true);
    }


    public void HideDiamondsPanel()
    {
        Level[0].gameObject.SetActive(false);
        Level[1].gameObject.SetActive(false);
        Level[2].gameObject.SetActive(false);
        Level[3].gameObject.SetActive(false);
        Level[4].gameObject.SetActive(false);
        Level[5].gameObject.SetActive(false);
        Level[6].gameObject.SetActive(false);
        Level[7].gameObject.SetActive(false);
        Level[8].gameObject.SetActive(false);
        Level[9].gameObject.SetActive(false);
        Level[10].gameObject.SetActive(false);
        Level[11].gameObject.SetActive(false);
        CS.gameObject.SetActive(false);

        Black.gameObject.SetActive(false);
        GameData.getInstance().isLock = 0;
        DiamondsPanel.gameObject.SetActive(false);
    }
}
