﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopController : MonoBehaviour {
	public GameObject CoinShoppanel;
	public GameObject ItemShoppanel;
	public GameObject Videopanel;
	public GameObject LevelUppanel;
	public GameObject Diamondspanel;
	public BoxCollider2D[] Items;

	// Update is called once per frame
	void Update ()
	{
		if(CoinShoppanel.gameObject.activeSelf)
		{
			ItemShoppanel.gameObject.SetActive (false);
			Videopanel.gameObject.SetActive (false);
			LevelUppanel.gameObject.SetActive (false);
			Items [0].enabled = false;
			Items [1].enabled = false;
			Items [2].enabled = false;
		}

		else if(ItemShoppanel.gameObject.activeSelf)
		{
			CoinShoppanel.gameObject.SetActive (false);
			Videopanel.gameObject.SetActive (false);
			LevelUppanel.gameObject.SetActive (false);
			Items [0].enabled = false;
			Items [1].enabled = false;
			Items [2].enabled = false;
		}

		else if(Videopanel.gameObject.activeSelf)
		{
			CoinShoppanel.gameObject.SetActive (false);
			ItemShoppanel.gameObject.SetActive (false);
			LevelUppanel.gameObject.SetActive (false);
			Items [0].enabled = false;
			Items [1].enabled = false;
			Items [2].enabled = false;
		}
			
		else if (LevelUppanel.gameObject.activeSelf) {
			CoinShoppanel.gameObject.SetActive (false);
			ItemShoppanel.gameObject.SetActive (false);
			Videopanel.gameObject.SetActive (false);
			Items [0].enabled = false;
			Items [1].enabled = false;
			Items [2].enabled = false;
		} 


		else if (Diamondspanel.gameObject.activeSelf) {
			CoinShoppanel.gameObject.SetActive (false);
			ItemShoppanel.gameObject.SetActive (false);
			Videopanel.gameObject.SetActive (false);
			Items [0].enabled = false;
			Items [1].enabled = false;
			Items [2].enabled = false;
		} 

		else
		{
			Items [0].enabled = true;
			Items [1].enabled = true;
			Items [2].enabled = true;
		}
	}
}
