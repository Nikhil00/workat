﻿using UnityEngine;
using System.Collections;
// using SmartLocalization;
using UnityEngine.UI;
public class PanelBuyCoin : MonoBehaviour {

	// Use this for initialization
	GameObject scrollpanel;
	string lang = "";
	public GameObject btnBuyCoinClose;
	public GameObject panelBuyAlert;
	void Start () {
		Debug.Log("Dev - Start PanelBuyCoin");
		GameManager.getInstance ().initLocalize ();
		lang = "en";//LanguageManager.Instance.GetTextValue("GAME_TIPCONTEXT");
//		scrollpanel = GetComponent<dfScrollPanel> ();
		for (int i = 0; i<7; i++) {
			Transform t = transform.Find(i.ToString());
			Text lbDetail = t.Find("lbDetail").GetComponent<Text>();
			Text lbPrice = t.Find("lbPrice").GetComponent<Text>();


			lbDetail.text = getString("detail",i);
			lbPrice.text = getString("lbPrice",i);
		}
//		btnBuyCoinClose = btnBuyCoinClose.GetComponentInChildren<Text>();
		btnBuyCoinClose.GetComponentInChildren<Text>().text = "Cancel";//LanguageManager.Instance.GetTextValue("GAME_CANCEL");

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	int[] nCoin = {0,0,0,0,0,0,0};
	string[] priceCN = {""};
	string[] priceEN = {"9"};
	string[] offEN = {"",""};
	string[] offCN = {"",""};
	string getString(string type,int no){
		string tstring = "";
		if (lang == "en") {
			switch (type) {
			case "detail":
				tstring = "Get " + nCoin[no] + " " + "coins";
				break;
			case "lbPrice":
				tstring = offEN[no] + "  " + priceEN[no] + " " ;
				break;
				
			}
		} else {
			switch (type) {
			case "detail":
				tstring = "获得" + nCoin[no] + " " + "金币";
				break;
			case "lbPrice":
				tstring = offCN[no] + "  " + priceCN[no] + " " ;
				break;
				
			}	
		}
		return tstring ;
		
	}

//	public void OnClick( dfControl control, dfMouseEventArgs mouseEvent )
//	{
//		// Add event handler code here
//		switch (control.name) {
//			case "btnBuyCoin":
//			GameManager.getInstance().playSfx("select");
////#if !UNITY_WP8
////			panelBuyAlert.IsVisible = true;
////			StopCoroutine("hideWait");
////			StartCoroutine("hideWait");
////#endif
//			int tindex = int.Parse(control.transform.parent.name);
//			print(tindex);
//			GameManager.getInstance().buy(tindex);
//			break;
//		}
//	}

	IEnumerator hideWait(){
		yield return new WaitForSeconds (30);
		panelBuyAlert.SetActive(false);
	}


	public void OnClick2(GameObject g){
		switch (g.name) {
		case "btnBuyCoin":
			GameManager.getInstance().playSfx("select");
			//#if !UNITY_WP8
			//			panelBuyAlert.IsVisible = true;
			//			StopCoroutine("hideWait");
			//			StartCoroutine("hideWait");
			//#endif
			int tindex = int.Parse(g.transform.parent.name);
			print(tindex);
			GameManager.getInstance().buy(tindex);
			break;
		case "btnBuyCoinClose":
			GameData.getInstance().main.buttonHandler(g);
			break;
		}
	}
}
