﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Util : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	
	public static List<string> ListRandom(List<string> myList) 
	{ 

		List<string> newList = new List<string>(); 
		int index = 0;
		string temp = ""; 
		for (int i = 0; i < myList.Count; i++) 
		{
// #if UNITY_EDITOR
        	index = Random.Range(0, myList.Count-1); 
// #elif UNITY_ANDROID || UNITY_IOS
// 			index = SkillzCrossPlatform.Random.Range(0, myList.Count-1); 
// #endif
			
			if (index != i)
			{ temp = myList[i]; 
				myList[i] = myList[index]; 
				myList[index] = temp; 
			}
		}
		return myList;
	}
	public static List<int> ListRandomInt(List<int> myList) 
	{ 
		
		List<int> newList = new List<int>(); 
		int index = 0;
		int temp = 0; 
		for (int i = 0; i < myList.Count; i++) 
		{
// #if UNITY_EDITOR
        	index = Random.Range(0, myList.Count-1); 
// #elif UNITY_ANDROID || UNITY_IOS
// 			index = SkillzCrossPlatform.Random.Range(0, myList.Count-1); 
// #endif
			
			if (index != i)
			{ temp = myList[i]; 
				myList[i] = myList[index]; 
				myList[index] = temp; 
			}
		}
		return myList;
	}
}
