﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;
using System.Collections.Generic;
using UnityEngine.UI;
public class PanelScoreBoard : MonoBehaviour {

	// Use this for initialization
	GameObject myPanel;
	string lang = "";
	[HideInInspector]
	public Text title,basicScore,totalStep,itemUsed,totalScore;
	Text btnAgain;
	void Start () {

//		refreshView ();
	}

	void OnEnable(){
		GameManager.getInstance ().initLocalize ();
		lang = "en";//LanguageManager.Instance.GetTextValue("GAME_TIPCONTEXT");
		//		myPanel = GameObject.Find("")
		myPanel = gameObject;

		basicScore = transform.Find("basicScore").GetComponent<Text> ();
		totalStep = transform.Find ("totalStep").GetComponent<Text> ();
		totalScore = transform.Find ("totalScore").GetComponent<Text> ();

		btnAgain = transform.Find ("btnAgain").GetComponentInChildren<Text> ();
		initView ();
	}

	void initView(){
		jlist = new List<GameObject> ();
		for (int i = 1; i<= TouchGame.maxN; i++) {
			Transform tj = transform.Find("sj"+i);
			jlist.Add(tj.gameObject);
			jlblist.Add(tj.transform.Find("Label").GetComponent<Text>());

		}


		lb11 = transform.Find ("sj11").transform.Find("Label").GetComponent<Text> ();
		lb12 = transform.Find ("sj12").transform.Find("Label").GetComponent<Text> ();

	}

	public List<GameObject> jlist;
	public List<Text> jlblist;
	public List<long> njlist;
	Text lb11,lb12;
	public void refreshView(){
		basicScore.text = ""+GameData.getInstance ().cScore;//LanguageManager.Instance.GetTextValue ("SCORE_BASIC") + GameData.getInstance ().cScore;
		//totalStep.text = "Total Steps: " + GameData.getInstance ().gameStep;//LanguageManager.Instance.GetTextValue ("SCORE_STEP") + GameData.getInstance ().gameStep;

		ObscuredLong nScore = GameData.getInstance ().cScore;
		//long nScore = GameData.getInstance ().cScore + GameData.getInstance ().itemUsed * 1000 + GameData.getInstance ().gameStep * 10;


		btnAgain.text = "SUBMIT SCORE";//LanguageManager.Instance.GetTextValue ("SCORE_AGAIN");
		njlist = new List<long> ();

		njlist.Add (GameData.getInstance ().nj1);
		njlist.Add (GameData.getInstance ().nj2);
		njlist.Add (GameData.getInstance ().nj3);
		njlist.Add (GameData.getInstance ().nj4);
		njlist.Add (GameData.getInstance ().nj5);
		njlist.Add (GameData.getInstance ().nj6);
		njlist.Add (GameData.getInstance ().nj7);
		njlist.Add (GameData.getInstance ().nj8);
		njlist.Add (GameData.getInstance ().nj9);
		njlist.Add (GameData.getInstance ().nj10);
//		njlist.Add (GameData.getInstance ().nj11);
//		njlist.Add (GameData.getInstance ().nj12);

		 


		ObscuredLong coinBonus = 0;
		for (int i = 0; i<TouchGame.maxN; i++) {

			jlblist[i].text = " X "+ njlist[i];	
			if(njlist[i] == 0 && (i+1 > GameData.getInstance().bestJew)){
				jlist[i].GetComponent<Image>().color = Color.black;
				jlist[i].transform.Find("lb?").gameObject.SetActive(true);
			}else{
				jlist[i].GetComponent<Image>().color = Color.white;
				jlist[i].transform.Find("lb?").gameObject.SetActive(false);
				if(i >= 5){
					nScore += i*1;

				}
				int tinc = (i - 4)*1;
				if(tinc  > 0){
					coinBonus += tinc;
				}else{
					coinBonus += 0;
				}


			}
		}

		lb11.text = "X" + GameData.getInstance ().nj200;
		lb12.text = "X" + GameData.getInstance ().nj201;

		coinBonus += (int)nScore / 300;
		GameData.getInstance ().cCoin += coinBonus;
		ObscuredPrefs.SetString ("coin", GameData.getInstance ().cCoin.ToString());

		totalScore.text = "Total Score: " + nScore;//LanguageManager.Instance.GetTextValue ("SCORE_TOTAL") + nScore;

		if (GameData.getInstance ().bestScore <= nScore) {
			GameData.getInstance ().bestScore = nScore;
			ObscuredPrefs.SetString("bestScore",GameData.getInstance().bestScore.ToString());
		}
	}
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick(GameObject g)
	{

		// Add event handler code here
		switch (g.name) {
		case "btnAgain":
			// Application.LoadLevel(Application.loadedLevelName);	
// #if UNITY_EDITOR
        SceneManager.LoadScene(0);
// #elif UNITY_ANDROID || UNITY_IOS
// 		SkillzCrossPlatform.ReportFinalScore((int)GameData.getInstance ().cScore);
// #endif
			// Application.LoadLevel(0);

			break;
		}
	}

}
