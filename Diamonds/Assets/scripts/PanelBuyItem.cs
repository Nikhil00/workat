﻿using UnityEngine;
using System.Collections;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;
using UnityEngine.UI;
public class PanelBuyItem : MonoBehaviour {

	// Use this for initialization
//	dfSlicedSprite myPanel;
	TouchGame touchGame;
	public GameObject CoinShop;
	public GameObject Black;
	string lang = "";
	void Start () {
		Debug.Log("Dev - Start PanelByItem");
		GameManager.getInstance ().initLocalize ();
		lang = "en";//LanguageManager.Instance.GetTextValue("GAME_TIPCONTEXT");
//		myPanel = GetComponent<dfSlicedSprite> ();
		touchGame = GameObject.Find ("gridbg").GetComponent<TouchGame> ();

		initView ();
	}

	void OnEnable(){


	}
	Text lbName,lbCost,lbDetail;
	void initView(){
		lbName = transform.Find ("lbName").gameObject.GetComponent<Text>();
	
		lbCost = transform.Find("coin").Find("lbCost").gameObject.GetComponentInChildren<Text>();
		btnBuy = transform.Find ("btnMoneyBuy").gameObject;
		btnYes = transform.Find ("btnYes").gameObject;
		btnNo = transform.Find ("btnNo").gameObject;
		if(btnBuy)btnBuy.GetComponentInChildren<Text>().text = "SHOP";//LanguageManager.Instance.GetTextValue("GAME_BUYCOIN");
		btnYes.GetComponentInChildren<Text>().text = "USE";//LanguageManager.Instance.GetTextValue("GAME_YES");
		btnNo.GetComponentInChildren<Text>().text = "BACK";//LanguageManager.Instance.GetTextValue("GAME_NO");
		lbDetail = transform.Find("lbDetail").GetComponent<Text>();

	}
	GameObject btnBuy,btnYes,btnNo;
	ObscuredInt allmatchcost = 200,bombcost = 75,shovecost = 100;

	public void refreshInfo(string itemName){
		ObscuredInt tcost = 0;
		initView ();
		switch (itemName) {
		case "allfit":
			lbName.text = "All Match";//LanguageManager.Instance.GetTextValue("ITEM_ALLMATCH");
			tcost = allmatchcost;
			break;
		case "bomb":
			lbName.text = "Bomb";//LanguageManager.Instance.GetTextValue("ITEM_BOMB");
			tcost = bombcost;
			break;
		case "shove":
			lbName.text = "Shove";//LanguageManager.Instance.GetTextValue("ITEM_SHOVE");
			tcost = shovecost;
			break;
		}
		if (GameData.getInstance ().cCoin >= tcost) {
			btnBuy.SetActive (false);// = false;	
			btnYes.SetActive(true);// = true;
		} else {
			//not enough coin
			btnBuy.SetActive(true);// = true;		
			btnYes.SetActive(false);// = false;
		}
		lbCost.text = "-" + tcost.ToString ();
		refreshDetail(itemName);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick( GameObject g )
	{
		

		if (GameData.getInstance ().isWin || GameData.getInstance ().isFail)
						return;

		GameManager.getInstance().playSfx("select");
		// Add event handler code here
		switch (g.name) {
		case "btnYes":

			switch (GameData.getInstance ().cBuyItem) {
			case "allfit":
				GameData.getInstance ().nAllFit++;
				ObscuredPrefs.SetInt ("nAllFit", 1);
				GameData.getInstance ().cCoin -= allmatchcost;
				Debug.Log("allfit");
				break;
			case "bomb":
				GameData.getInstance ().nBomb++;
				ObscuredPrefs.SetInt ("nBomb", 1);
				GameData.getInstance ().cCoin -= bombcost;
				Debug.Log("bomb");
				break;
			case "shove":
				GameData.getInstance ().nShove++;
				ObscuredPrefs.SetInt ("nShove", 1);
				GameData.getInstance ().cCoin -= shovecost;
				Debug.Log("shove");
				break;
			
			}
	
			touchGame.itemBought (GameData.getInstance ().cBuyItem);
			GameData.getInstance ().cBuyItem = "";
			gameObject.SetActive (false);
			Black.gameObject.SetActive (false);
			GameData.getInstance().isLock = 0;
			break;
		case "btnNo":
			
			GameData.getInstance().cBuyItem = "";
			gameObject.SetActive (false);
			Black.gameObject.SetActive (false);
			GameData.getInstance().isLock = 0;
			break;
		case "btnMoneyBuy":
			GameManager.getInstance ().playSfx ("shop");
			CoinShop.gameObject.SetActive (true);
			GameData.getInstance().cBuyItem = "";
			gameObject.SetActive (false);
			GameData.getInstance().isLock = 1;
			break;
		}

	}

	void refreshDetail(string itemname){
		switch (itemname) {
		case "allfit":
			if(lang == "TIP_CN"){
				lbDetail.text = "";
			}else{
				lbDetail.text = "ADD MAGIC MAGNET BETWEEN SIMILAR NUMBERS AND\nMATCH THEM EASILY !";
			}
			break;
		case "bomb":
			if(lang == "TIP_CN"){
				lbDetail.text = "";
			}else{
				lbDetail.text = "DESTROY A NUMBER\nIF YOU DON'T NEED IT !";
			}
			break;
		case "shove":
			if(lang == "TIP_CN"){
				lbDetail.text = "";
			}else{
				lbDetail.text = "PICK UP A NUMBER FROM THE BOARD AND ADD IT ANYWHERE YOU NEED !";
			}
			break;
		}
	}

}
