﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;
using UnityEngine.UI;
using DG.Tweening;
//using Umeng;
public class MainScript : MonoBehaviour
{

    // Use this for initialization

    //data
    public ObscuredInt timeCount = 0;

    public void Start()
    {
        GameData.lastWindow = "game";
        initData();
        initView();
        //		refreshView ();
        StartCoroutine("waitAsecond");
        Black.gameObject.SetActive(false);
        fadeOut();
    }

    //	dfCheckbox checkR,checkL;
    //	dfButton btnLeft,btnRight,btnUp,btnDown,btnRewind;
    //	dfLabel lbLevel,lbStep;
    //	dfCheckbox checkWatch;
    //	
    //	dfLabel lbComplete;
    //	dfButton btnContinue;
    //	
    //	dfLabel lbFailed;
    //	dfButton btnRetry;
    //	dfButton btnTitle;
    ObscuredInt clevel;
    //panel
    public GameObject panelBuyCoins;
    public GameObject panelBuyItems;
    public GameObject panelHelp;
    public GameObject panelVideo;

    void initView()
    {
        //		panelBuyCoins = GameObject.Find ("PanelBuyCoinsC").GetComponent<Text> ();
        //		panelBuyItems = GameObject.Find ("PanelBuyItem").GetComponent<dfSlicedSprite> ();
        //		panelHelp = GameObject.Find ("panelHelp").GetComponent<dfPanel> ();

        GameManager.getInstance().setBannerVisible(true);
        GameManager.getInstance().initInterestitial();
        GameManager.getInstance().initLocalize();



        //		lbLevel = GameObject.Find("lbLevel").GetComponent<dfLabel>();
        //		//		lbStep = GameObject.Find("lbStep").GetComponent<dfLabel>();
        //		string levelname = Application.loadedLevelName;
        //		clevel = int.Parse(levelname.Substring (5, levelname.Length-5));
        //		lbLevel.Text = levelText (clevel);//LanguageManager.Instance.GetTextValue("GAME_LEVELNUM") + (GameData.getInstance().cLevel+1);

        //		

        //		lbComplete = GameObject.Find("lbComplete").GetComponent<dfLabel>();
        //		btnContinue = GameObject.Find("btnContinue").GetComponent<dfButton>();
        //		lbComplete.Text =  LanguageManager.Instance.GetTextValue("GAME_WINTITLE");
        //		btnContinue.Text =  LanguageManager.Instance.GetTextValue("CONTINUE");
        //		lbFailed = GameObject.Find ("lbFailTititle").GetComponent<dfLabel> ();
        //		btnRetry = GameObject.Find ("btnRestart").GetComponent<dfButton> ();
        //		btnTitle = GameObject.Find ("btnTitle").GetComponent<dfButton> ();
        //		lbFailed.Text = LanguageManager.Instance.GetTextValue("GAME_FAILTITLE");
        //		btnRetry.Text = LanguageManager.Instance.GetTextValue("BTN_RESTART");
        //		btnTitle.Text = LanguageManager.Instance.GetTextValue("BTN_TITLE");

        //		GameObject tipText = GameObject.Find("tipText");
        //		dfLabel tipLabel = tipText.GetComponent<dfLabel> ();
        //		switch (Application.loadedLevelName) {
        //		case "level3":
        //			tipLabel.Text = LanguageManager.Instance.GetTextValue("TIP_C3");//
        //			break;
        //		case "level7":
        //			tipLabel.Text = LanguageManager.Instance.GetTextValue("TIP_C7");
        //			break;
        //		}
        //		dfPanel tipWindow = GameObject.Find("panelHelp").GetComponent<dfPanel>();
        //		tipWindow.IsVisible = true;//!tipWindow.IsVisible;

        //		GameManager.getInstance ().stopLoopMusic ();



    }



    IEnumerator waitAsecond()
    {
        yield return new WaitForSeconds(1);

        //		if (timeCount > 0) {
        if (GameData.getInstance().isWin == false)
        {
            timeCount++;
            StartCoroutine("waitAsecond");
        }
        //		}
    }

    // Update is called once per frame
    ObscuredInt un = 0;

    public void refreshView()
    {
        //		GameObject.Find ("btnTip").GetComponentInChildren<dfLabel> ().Text = GameData.getInstance ().tipRemain.ToString();
        ////		print (GameData.getInstance ().tipRemain.ToString ());
        //		GameObject.Find ("lb_level").GetComponent<dfLabel> ().Text = "LEVEL " + (GameData.getInstance ().cLevel+1);
        //		if (GameData.getInstance ().cLevel > 0) {
        //			GameObject.Find ("lb_ins").GetComponent<dfLabel> ().IsVisible = false;
        //		}
    }


    GameObject nodeOrigin;
    GameObject linkLine;
    //	public Vector3 list;
    //	dfPanel dfpanel_;
    //	List<dfControl> nodes;

    public string[] lvAnswerData;
    string[] lvLinkData;
    GameObject gameContainer;
    void initData()
    {
        GameData.isInGame = true;
        GameData.getInstance().resetData();
        //		GameData.getInstance ().maing = GameObject.Find ("gameContainer");//.GetComponent<GameMain> ();
        GameData.getInstance().main = this;//GameObject.Find ("PanelMain").GetComponent<MainScript> ();


        Time.timeScale = 1;
        gameContainer = GameObject.Find("gameContainer");
        //		MadLevel.currentLevelName = "level (" + (GameData.getInstance ().cLevel+1) + ")";
        //		GA.StartLevel (Application.loadedLevelName);

        //print (Application.loadedLevelName);
    }

    void clearGame()
    {
        //		foreach(dfControl tnode in nodes){
        //			dfpanel_.RemoveControl(tnode);
        //			dfpanel_.RemoveAllEventHandlers();
        //			DestroyImmediate(tnode.gameObject);
        //		}
        //		nodes.Clear ();

        //		GameObject.Find ("btnTip").GetComponent<dfButton> ().Enable ();
    }

    //handler event
    //	public void OnRetryClick( dfControl control, dfMouseEventArgs mouseEvent )
    //	{
    //		// Add event handler code here
    //		GameManager.getInstance ().playSfx ("click");
    //		clearGame ();
    //		initData ();
    //	}

    GameObject panelWin;
    WinPanel winpanel;
    //	dfLabel lbTime;
    //	dfLabel lbUndo;
    //	dfLabel lbScore;
    public void gameWin(bool directWin = false)
    {
        if (GameData.getInstance().isFail)
            return;
        //fire win event
        //		gameWinEvent ();

        GameManager.getInstance().playSfx("applaud");
        //		if (GameData.getInstance ().cLevel % 5 == 0 && GameData.getInstance ().cLevel > 0) {
        //			musicScript.showCB();		
        //		}

        //		GA.FinishLevel(Application.loadedLevelName);
        GameData.getInstance().isWin = true;
        //		int threeStar = (int)(lvLinkData.Length/2/1.2f)+2;//one line per second
        //		int twoStar = threeStar + 5;//
        //		int oneStar = threeStar + 20;//
        //		
        //		print (GameData.getInstance ().cLevel);
        if (GameData.getInstance().cLevel < 10)
        {
            if (GameData.getInstance().cLevel % 2 == 0 && GameData.getInstance().cLevel > 0)
                GameManager.getInstance().showInterestitial();
        }
        else
        {
            GameManager.getInstance().showInterestitial();
        }



        GameObject[] tpanels = GameObject.FindGameObjectsWithTag("game");
        foreach (GameObject tPanel in tpanels)
        {
            //			tPanel.GetComponent<dfPanel>().IsVisible = false;		
        }
        GameObject panelWin = GameObject.Find("PanelWin");

        //		dfTweenVector3 dfwin = panelWin.GetComponent<dfTweenVector3> ();
        if (!directWin)
        {
            //			dfwin.Play ();
        }
        disableAll();


        //		lbTime = GameObject.Find ("lbTimeUse").GetComponent<dfLabel> ();
        //		lbScore = GameObject.Find ("lbScore").GetComponent<dfLabel> ();


        //		lbTime.Text = LanguageManager.Instance.GetTextValue("GAME_TIMEUSED") +": "+ timeCount;


        //		print (myStep + myUndo +"  "+minStep);
        ObscuredInt tScore = 600 - timeCount;//600 - timeCount - (myStep + myUndo - minStep)*2;
        if (tScore < 0)
            tScore = 0;
        //		lbScore.Text = LanguageManager.Instance.GetTextValue("GAME_SCORE") +": " + tScore;

        ObscuredInt tbestScore = 300;//GameData.getInstance ().getLevelBestScore ();
        ObscuredInt nStar = 0;
        if (timeCount <= tbestScore)
        {
            nStar = 3;
        }
        else if (timeCount <= tbestScore + 5)
        {
            nStar = 2;
        }
        else if (timeCount <= tbestScore + 10)
        {
            nStar = 1;
        }
        else
        {
            nStar = 0;
        }





        for (int i = 1; i <= 3; i++)
        {
            GameObject tstar = GameObject.Find("star_" + i);
            //			if(i <= nStar){
            //				tstar.GetComponent<dfSprite>().IsVisible = true;
            //			}else{
            //				tstar.GetComponent<dfSprite>().IsVisible = false;
            //			}
        }

        //save
        ObscuredInt saveLevel = 0;

        if (GameData.getInstance().cLevel < GameData.totalLevel - 1)
        {
            saveLevel = GameData.getInstance().cLevel + 1;
            //			btnContinue.Text =  LanguageManager.Instance.GetTextValue("CONTINUE");	
        }
        else
        {
            //			btnContinue.name = "btnTitle";
            //			btnContinue.Text =  LanguageManager.Instance.GetTextValue("BTN_TITLE");	
        }
        //		print (GameData.getInstance ().levelPassed);
        // Nik - Code Change || 07-04
        if (GameData.getInstance().levelPassed < saveLevel)
        // if (GameData.getInstance().levelPassed != saveLevel)
        {
            print("saving..");
            // Nik - Code Change || 07-04
            ObscuredPrefs.SetInt("levelPassed", saveLevel);
            GameData.getInstance().levelPassed = saveLevel;


            //			print (MadLevel.currentLevelName + "name");
            //			MadLevelProfile.SetCompleted(MadLevel.currentLevelName, true);
            //UniRate.Instance.LogEvent(true);
        }


        //save score
        int cLvScore = ObscuredPrefs.GetInt("levelScore_" + GameData.getInstance().cLevel, 0);
        //		print (cLvScore + "_" + timeCount);
        if (cLvScore < tScore)
        {
            ObscuredPrefs.SetInt("levelScore_" + GameData.getInstance().cLevel, tScore);
            //			print (tScore+"tallscore");
            //save to GameData instantlly
            //			print(GameData.getInstance().lvStar.Count+"    "+GameData.getInstance().cLevel);
            if (GameData.getInstance().lvStar.Count == 0) return;
            GameData.getInstance().lvStar[GameData.getInstance().cLevel] = nStar;
            //			print ("save new score"+cLvScore+"_"+timeCount);


            //submitscore
            int tallScore = 0;
            for (int i = 0; i < GameData.totalLevel; i++)
            {
                int tlvScore = ObscuredPrefs.GetInt("levelScore_" + i.ToString(), 0);
                tallScore += tlvScore;

            }

            GameData.getInstance().bestScore = tallScore;

        }

        //check star
        ObscuredInt cLvStar = ObscuredPrefs.GetInt("levelStar_" + GameData.getInstance().cLevel, 0);
        //		print ("getstar"+cLvStar+"   "+nStar);
        if (cLvStar < nStar)
        {

            ObscuredPrefs.SetInt("levelStar_" + GameData.getInstance().cLevel, nStar);
            for (int i = 1; i <= nStar; i++)
            {
                //				MadLevelProfile.SetLevelBoolean (MadLevel.currentLevelName, "star_" + i, true);
            }
        }

        if (directWin)
        {//do continue button automatically
            if (GameData.getInstance().cLevel < GameData.totalLevel - 1)
            {
                GameData.getInstance().cLevel++;
            }

            if (Application.loadedLevelName == "level" + GameData.totalLevel)
            {

                SceneManager.LoadScene("mainMenu");

            }
            else
            {
                SceneManager.LoadScene("level" + (GameData.getInstance().cLevel + 1));
            }


            GameManager.getInstance().playSfx("click");

        }
    }

    static ObscuredInt nFail = 0;
    public void gameFailed()
    {

        if (GameData.getInstance().isWin || GameData.getInstance().isFail)
            return;


        GameManager.getInstance().playSfx("gamelose");

        GameData.getInstance().isFail = true;
        //		GA.FailLevel(Application.loadedLevelName);
        //		GameData.getInstance ().isWin = true;
        nFail++;
        if (nFail == 4)
        {
            GameManager.getInstance().showInterestitial();
            nFail = 0;
        }
        GameObject[] tpanels = GameObject.FindGameObjectsWithTag("panels");
        foreach (GameObject tPanel in tpanels)
        {
            //			tPanel.GetComponent<dfPanel>().IsVisible = false;		
        }

        GameObject panelWin = GameObject.Find("PanelFail");
        //		dfTweenVector3 dfwin = panelWin.GetComponent<dfTweenVector3> ();
        //		dfwin.Play ();


        disableAll();
    }


    //define a firable event;
    //	public delegate void gameWinDelegate();
    //	public event gameWinDelegate gameWinEvent;

    //	public dfTweenFloat fade2Main;
    //	public dfTweenFloat fade2Level;
    //	public dfTweenFloat fadeInOnly;

    public GameObject mask;
    public GameObject Black;
    public void buttonHandler(GameObject g)
    {

        switch (g.name)
        {
            case "btnMain":
                //			fade2Main.Play();
                GameManager.getInstance().playSfx("click");
                fadeIn("MainMenu");
                break;
            case "btnLevel":
                //			fade2Level.Play();
                fadeIn("LevelMenu");
                GameManager.getInstance().playSfx("click");
                GameData.isInGame = false;
                break;
            case "btnRewind":
            //			if(GameData.getInstance().isWin)return;
            //			GameData.getInstance().maing.undoStep();
            //			GameManager.getInstance ().playSfx ("undo");
            //			break;
            case "btnRestart":
                SceneManager.LoadScene(Application.loadedLevelName);
                
                GameManager.getInstance().playSfx("click");
                break;
            case "btnRetry":
                SceneManager.LoadScene(Application.loadedLevelName);
                GameManager.getInstance().playSfx("click");
                break;
            case "btnContinue":
                if (GameData.getInstance().cLevel < GameData.totalLevel - 1)
                {
                    GameData.getInstance().cLevel++;
                }
                SceneManager.LoadScene("level" + (GameData.getInstance().cLevel + 1));
                GameManager.getInstance().playSfx("click");
                break;
            case "btnAction":
                GameData.getInstance().maing.BroadcastMessage("action");
                break;
            case "btnPauseClose":
                //			dfPanel tipWindow = GameObject.Find("panelPause").GetComponent<dfPanel>();
                //			tipWindow.IsVisible = !tipWindow.IsVisible;
                Time.timeScale = 1;
                break;
            case "btnHelp":
                if (GameData.isTutorial) return;
                //			if(GameData.getInstance().isWin || GameData.getInstance().isFail)return;
                //
                //			panelHelp.IsVisible = true;//!tipWindow1.IsVisible;
                //			panelBuyCoins.IsVisible = false;
                //			panelBuyItems.IsVisible = false;
                //
                //			GameData.getInstance().isLock = 4;//help
                GameData.isTutorial = true;
                SceneManager.LoadScene(Application.loadedLevelName);
                break;
            case "btnTitle":
                //			fade2Main.Play();
                fadeIn("MainMenu");
                GameManager.getInstance().playSfx("click");
                GameData.isInGame = false;
                if (GameData.back2main % 3 == 0)
                {
                    GameManager.getInstance().showInterestitial();
                    GameData.back2main++;
                }
                break;
            case "btnPause":
                Time.timeScale = 0;
                if (GameData.getInstance().isWin || GameData.getInstance().isFail) return;
                //			dfPanel panelPause = GameObject.Find("panelPause").GetComponent<dfPanel>();
                //			dfLabel pauseText = GameObject.Find("pauseText").GetComponent<dfLabel>();
                //			pauseText.Text = LanguageManager.Instance.GetTextValue("GAME_PAUSED");
                //			panelPause.IsVisible = true;
                break;
            case "btnTip":
                //			if(GameData.isTutorial)return;
                //			if(GameData.getInstance().isWin || GameData.getInstance().isFail)return;
                //			GameManager.getInstance ().playSfx ("click");
                //			if(GameData.getInstance().isLock >= 3 )return;//if is buy coin
                //			dfPanel panelAskTip =  GameObject.Find("PanelAskTip").GetComponent<dfPanel>();
                //			PanelShowTip panelAskTip2 =  GameObject.Find("PanelAskTip").GetComponent<PanelShowTip>();
                //			dfPanel panelNoTip = GameObject.Find("PanelNoTip").GetComponent<dfPanel>();
                //			dfPanel panelDisplayTip = GameObject.Find("PanelDisplayTip").GetComponent<dfPanel>();
                //			PanelDisplayTip panelDisplayTip2 = GameObject.Find("PanelDisplayTip").GetComponent<PanelDisplayTip>();
                //			if(panelAskTip.IsVisible || panelNoTip.IsVisible || panelDisplayTip.IsVisible)return;
                //			if(GameData.getInstance().cLvShowedTip == false){ 
                //		
                //			if(Application.loadedLevelName == "level1"){
                //				panelDisplayTip2.showMe(true);
                //				break;
                //			}
                //
                //			if(GameData.getInstance().tipRemain >= 20){
                //				panelAskTip2.showMe(true);
                //			}else{
                //				panelNoTip.GetComponent<PanelNoTip>().showMe(true);// = true;
                //			}
                //			}else{
                //
                //				panelDisplayTip2.showMe(true);
                //			}
                break;
            case "btnMoneyBuy":
                if (GameData.isTutorial)
                    return;
                if (GameData.getInstance().isWin || GameData.getInstance().isFail)
                    return;
                //			panelBuyCoins.IsVisible = true;
                //			panelBuyItems.IsVisible = false;
                //			panelHelp.IsVisible =false;
                panelBuyCoins.SetActive(true);
                panelBuyItems.SetActive(false);
                panelHelp.SetActive(false);

                GameData.getInstance().isLock = 3;
                GameManager.getInstance().playSfx("select");
                break;
            case "btnBuyCoinClose":
                GameManager.getInstance().playSfx("select");
                //			panelBuyCoins.IsVisible =false;
                panelBuyCoins.SetActive(false);

                GameData.getInstance().isLock = 0;
                //			GameObject.Find ("panelBuyAlert").GetComponent<dfPanel> ().IsVisible = false;
                break;
        }
    }



    //	public void loadMainScene(){
    //		//		GameObject.Find("particle").SetActive(false);
    //		//		GameObject.Find ("loading").GetComponent<dfPanel> ().IsVisible = true;
    //		Application.LoadLevel("MainMenu");
    //	}

    //	public void loadLevelScene(){
    //		//		GameObject.Find("particle").SetActive(false);
    //		//		GameObject.Find ("loading").GetComponent<dfPanel> ().IsVisible = true;
    //		Application.LoadLevel("LevelMenu");
    //	}


    public void fadeIn(string levelname)
    {
        //		fadeInOnly.Play ();
        mask.SetActive(true);
        mask.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        mask.GetComponent<Image>().DOColor(new Color(0, 0, 0, 1), 1).OnComplete(() => loadnewlevel(levelname));
    }

    void loadnewlevel(string levelname)
    {
        Application.LoadLevel(levelname);
    }

    public void fadeOut()
    {

        mask.SetActive(true);
        mask.GetComponent<Image>().color = new Color(0, 0, 0, 1);
        mask.GetComponent<Image>().DOColor(new Color(0, 0, 0, 0), 1).OnComplete(() => { mask.SetActive(false); });
    }

    //--------------------
    public void activeUndo(bool isactive)
    {
        //		if (isactive) {
        //			btnRewind.Enable ();
        //		} else {
        //			btnRewind.Disable();		
        //		}
    }
    ObscuredInt nstep;
    public void setLbStep(ObscuredInt _step)
    {
        //		lbStep.Text = LanguageManager.Instance.GetTextValue("GAME_STEPNUM")+": " + _step;
        nstep = _step;
    }

    void disableAll()
    {
        GameObject tipWindow = GameObject.Find("panelHelp");
        if (tipWindow != null)
        {
            //			tipWindow.GetComponent<dfPanel>().IsVisible = false;		
        }
    }

    public void HideShop()
    {
        Black.gameObject.SetActive(false);
        panelBuyCoins.gameObject.SetActive(false);
        GameData.getInstance().isLock = 0;
    }

    public void ShowShop()
    {
        GameManager.getInstance().playSfx("shop");
        GameData.getInstance().isLock = 1;
        Black.gameObject.SetActive(true);
        panelBuyCoins.gameObject.SetActive(true);
    }


    public void HideVideo()
    {
        Black.gameObject.SetActive(false);
        panelVideo.gameObject.SetActive(false);
        GameData.getInstance().isLock = 0;
    }

    public void ShowVideo()
    {
        // Nik - Code Change || 07-04
        panelVideo.transform.GetChild(3).GetComponent<Text>().text = "You Have " + GameData.getInstance().cCoin.ToString() + " Coins.";
        GameManager.getInstance().playSfx("select");
        GameData.getInstance().isLock = 1;
        Black.gameObject.SetActive(true);
        panelVideo.gameObject.SetActive(true);
    }

}
