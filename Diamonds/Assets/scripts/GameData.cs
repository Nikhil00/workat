﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;

public class GameData  {
	
	// Use this for initialization

	public ObscuredInt nLink = 0;
	public ObscuredInt levelPassed = 0;
	public ObscuredInt cLevel = 0;
	public ObscuredLong cScore = 0;
	public ObscuredLong cCoin = 0;
	public ObscuredLong bestScore = 0;
	public ObscuredInt isSoundOn = 0;
	public ObscuredInt isSfxOn = 0;
	public static bool isTrial;
	public static string lastWindow = "";
	public static bool isInGame = false;
	public static bool isAds = true;
	public static ObscuredInt back2main = 0;

	public ObscuredLong gameScore;//save to leaderoard
	public ObscuredLong gameStep,itemUsed;
	public ObscuredLong nj1, nj2, nj3, nj4, nj5, nj6, nj7, nj8, nj9, nj10; //nj11; //nj12;
	public ObscuredLong nj200, nj201, nj202;

	public ObscuredInt tipRemain = 1;

	public MainScript main;
	public GameObject maing;
	public static ObscuredInt totalLevel = 20;


	//control
	public ObscuredInt controlType = 1;


	//game
	public ObscuredInt nAllFit = 0;
	public ObscuredInt nBomb = 0;
	public ObscuredInt nShove = 0;
	public string cBuyItem = "";
	public GameObject[] njlist;

	public static ObscuredInt mapType = 2;

	public static bool isTutorial = false;

	public ObscuredInt bestJew = 4;


	public static GameData instance;
	public static GameData getInstance(){
		if (instance == null) {
			instance = new GameData();
		}
		return instance;
	}

	public bool isWin = false;
	public bool isFail = false;
	public ObscuredInt isLock = 0;
	public bool canMove =true;
	public string tickStartTime = "0";
	public List<ObscuredInt>lvStar = new List<ObscuredInt>(260);

	public bool cLvShowedTip = false;


	public void resetData(){

		isLock = 0;

		isWin = false;
		isFail = false;

		tipRemain = ObscuredPrefs.GetInt ("tipRemain", 1);
		tickStartTime = ObscuredPrefs.GetString ("tipStart", "0");

		cBuyItem = "";

		cLvShowedTip = false;


		//game
		bestJew = ObscuredPrefs.GetInt("bestjew",4);

		mapType = ObscuredPrefs.GetInt("mapType",2);

		nj1 = long.Parse(ObscuredPrefs.GetString ("nj1", "0"));
		nj2 = long.Parse(ObscuredPrefs.GetString ("nj2", "0"));
		nj3 = long.Parse(ObscuredPrefs.GetString ("nj3", "0"));
		nj4 = long.Parse(ObscuredPrefs.GetString ("nj4", "0"));
		nj5 = long.Parse(ObscuredPrefs.GetString ("nj5", "0"));
		nj6 = long.Parse(ObscuredPrefs.GetString ("nj6", "0"));
		nj7 = long.Parse(ObscuredPrefs.GetString ("nj7", "0"));
		nj8 = long.Parse(ObscuredPrefs.GetString ("nj8", "0"));
		nj9 = long.Parse(ObscuredPrefs.GetString ("nj9", "0"));
		nj10 = long.Parse(ObscuredPrefs.GetString ("nj10", "0"));
		//      nj11 = long.Parse(ObscuredPrefs.GetString ("nj11", "0"));
		//		nj12 = long.Parse(ObscuredPrefs.GetString ("nj12", "0"));

		nj200 = long.Parse(ObscuredPrefs.GetString ("nj200", "0"));
		nj201 = long.Parse(ObscuredPrefs.GetString ("nj201", "0"));
		nj202 = long.Parse(ObscuredPrefs.GetString ("nj202", "0"));
	
		gameStep = long.Parse(ObscuredPrefs.GetString ("gameStep", "0"));
		itemUsed = long.Parse(ObscuredPrefs.GetString ("itemUsed", "0"));


	}


	//-------------------------------variables------------------------

	public void addJewNo(ObscuredInt type){
		if (isTutorial)
						return;
		switch (type) {
		case 1:
			nj1++;
			ObscuredPrefs.SetString ("nj"+type,nj1.ToString());
		
			break;
		case 2:
			nj2++;
			ObscuredPrefs.SetString ("nj"+type,nj2.ToString());
			break;
		case 3:
			nj3++;
			ObscuredPrefs.SetString ("nj"+type,nj3.ToString());
			break;
		case 4:
			nj4++;
			ObscuredPrefs.SetString ("nj"+type,nj4.ToString());
			break;
		case 5:
			nj5++;
			ObscuredPrefs.SetString ("nj"+type,nj5.ToString());
			break;
		case 6:
			nj6++;
			ObscuredPrefs.SetString ("nj"+type,nj6.ToString());
			break;
		case 7:
			nj7++;
			ObscuredPrefs.SetString ("nj"+type,nj7.ToString());
			break;
		case 8:
			nj8++;
			ObscuredPrefs.SetString ("nj"+type,nj8.ToString());
			break;
		case 9:
			nj9++;
			ObscuredPrefs.SetString ("nj"+type,nj9.ToString());
			break;
		case 10:
			nj10++;
			ObscuredPrefs.SetString ("nj"+type,nj10.ToString());
			break;
//		case 11:
//			nj11++;
			//			ObscuredPrefs.SetString ("nj"+type,nj11.ToString());
//			break;
//		case 12:
//			nj12++;
			//			ObscuredPrefs.SetString ("nj"+type,nj12.ToString());
//			break;
		case 200:
			nj200++;
			ObscuredPrefs.SetString ("nj"+type,nj200.ToString());
			break;
		case 201:
			nj201++;
			ObscuredPrefs.SetString ("nj"+type,nj201.ToString());
			break;
		case 202:
			nj202++;
			ObscuredPrefs.SetString ("nj"+type,nj202.ToString());
			break;
		}

		if (type >= bestJew && type <= 12) {
			bestJew = type;
			ObscuredPrefs.SetInt("bestjew",bestJew);
		}
	}

	public void addKill(ObscuredInt type,ObscuredInt num){
		long tnum = long.Parse(ObscuredPrefs.GetString ("monsterkilled_"+type+"_"+mapType,"0"));
		switch (type) {
		case 1001:
			ObscuredPrefs.SetString ("monsterkilled_"+type+"_"+mapType,(tnum+num).ToString());
			break;
		case 1002:
			break;
		}
	}


	public void addStep(){
		gameStep++;
		ObscuredPrefs.SetString("gameStep",gameStep.ToString());


	}
	public void addItemUse(){

		itemUsed++;
		ObscuredPrefs.SetString("itemUsed",itemUsed.ToString());


	}
	public void clearGame(){


		for (int i = 1; i<= TouchGame.maxN; i++) {
			ObscuredPrefs.SetString("nj"+i,"0");
		}
		ObscuredPrefs.DeleteKey("nj200");
		ObscuredPrefs.DeleteKey("nj201");
		ObscuredPrefs.DeleteKey("nj202");

		ObscuredPrefs.DeleteKey ("gameStep");
		ObscuredPrefs.DeleteKey ("itemUsed");
		ObscuredPrefs.DeleteKey ("score");

		string tmap = "";
		ObscuredPrefs.DeleteKey ("jewMap");
		ObscuredPrefs.DeleteKey ("energy");
		Debug.Log("clearing...");

		ObscuredPrefs.DeleteKey ("cpreview");
		ObscuredPrefs.DeleteKey ("cjew");
		ObscuredPrefs.DeleteKey ("ready1");
		ObscuredPrefs.DeleteKey ("ready2");
	
		ObscuredPrefs.Save ();
	}



}
