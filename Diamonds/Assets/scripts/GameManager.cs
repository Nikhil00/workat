﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;

#if UNITY_IOS
//using Umeng;
#endif

public class GameManager{
	private string LOCATION_TOP = "isNTop";

	#if UNITY_ANDROID

	#endif
	// Use this for initialization
	void Start () {
		
	}
	public void initSth(){
		music = GameObject.Find ("music");
		initInterestitial ();
		showInterestitial ();

	
		showBanner();
		
		
		//store
	}
	// Update is called once per frame
	void Update () {
		
	}
	
	
	

	public GameObject getObjectByName(string objname){
		GameObject rtnObj = null;
		foreach (GameObject obj in Object.FindObjectsOfType(typeof(GameObject)))
		{
			if(obj.name == objname){
				rtnObj = obj;
			}
		}
		return rtnObj;
	}
	
	public static GameManager instance;
	public static GameManager getInstance(){
		if(instance == null){
			instance = new GameManager();
			instance.initSth();
		}
		return instance;
	}
	
	
	
	GameObject music;
	public void playMusic(string str,bool isforce = false){

		//do not play the same music again
		// if (!isforce) {
		// 	if (bgMusic != null && musicName == str) {
		// 		Debug.Log("Nik - Return");
		// 		return;
		// 	}
		// }


		if (!music)
		{
			// return;
		}



		AudioSource tmusic = null;

		AudioClip clip = (AudioClip)Resources.Load (str, typeof(AudioClip));
		if (GameData.getInstance ().isSoundOn == 0) {
			if (bgMusic)
				bgMusic.Stop ();

			tmusic = music.GetComponent<musicScript> ().PlayAudioClip (clip,true);
			if (str.Substring (0, 2) == "bg") {
				musicName = str;
				bgMusic = tmusic;

			}
		}



	}






	List<AudioSource> currentSFX = new List<AudioSource>();
	Dictionary<string,int> sfxdic = new Dictionary<string,int>();
	public AudioSource playSfx(string str){
		AudioSource sfxSound = null;

		if (!music)
			return null;
		AudioClip clip = (AudioClip)Resources.Load (str, typeof(AudioClip));
		if (GameData.getInstance ().isSfxOn == 0) {
			sfxSound = music.GetComponent<musicScript> ().PlayAudioClip (clip);
			if (sfxSound != null) {
				if (sfxdic.ContainsKey (str) == false || sfxdic [str] != 1) {
					currentSFX.Add (sfxSound);
					sfxdic [str] = 1;
				}
			}	
		}	

		return sfxSound;


	}


	AudioSource bgMusic = new AudioSource();
	public string musicName = "";
	public void stopBGMusic(){
		if(bgMusic){
			bgMusic.Stop();
			musicName = "";
		}
	}

	public void stopAllSFX(){
		foreach(AudioSource taudio in currentSFX){
			if(taudio!=null)taudio.Stop();
		}
		currentSFX.Clear ();
		sfxdic.Clear ();
	}



	public void stopMusic(string musicName = ""){
		if (music) {
			AudioSource[] as1 = music.GetComponentsInChildren<AudioSource> ();
			foreach (AudioSource tas in as1) {
				if(musicName == ""){
					tas.Stop ();
					break;
				}else{
					if(tas && tas.clip){
						string clipname = (tas.clip.name);
						if(clipname == musicName){
							tas.Stop();


							musicName = "";
							if(sfxdic.ContainsKey(clipname)){
								sfxdic[clipname] = 0;
							}
							break;
						}
					}
				}
			}
		}
	}
	
	//music
	public void toggleSound(){
		GameObject music = GameObject.Find("music") as GameObject;
		//		AudioListener alistener = music.GetComponentInChildren<AudioListener>();
		//		
		//		 
		ObscuredInt soundState  = GameData.getInstance().isSoundOn;
		
		
		
		
	}

	public static bool inited;
	public void init(){
		//get data
		if (inited)
			return;
		
		//		GameManager.getInstance().playMusic("bgmusic");
		int allScore = 0;
		// Nik - Code Change || 07-04
		// for(int i = 0;i<GameData.totalLevel;i++){
		// 	int tScore = ObscuredPrefs.GetInt("levelScore_"+i.ToString(),0);
		// 	allScore += tScore;
		// 	//save star state to gameobject
		// 	ObscuredInt tStar = ObscuredPrefs.GetInt("levelStar_"+i.ToString(),0);
		// 	GameData.getInstance().lvStar.Add(tStar);
		// }
		Debug.Log("bestScore is:"+allScore);
		Debug.Log("<color=black><b>Nik - Level number is :</b></color>" +  ObscuredPrefs.GetInt("levelPassed",0));
		GameData.getInstance ().levelPassed = ObscuredPrefs.GetInt("levelPassed",0);
		Debug.Log ("current passed level = " + GameData.getInstance ().levelPassed);
		
		//for continue,set default to lastest level
		GameData.getInstance ().cLevel = GameData.getInstance ().levelPassed;
		
		
		//		for (int i = 0; i<=GameData.getInstance().levelPassed; i++) {
		//			MadLevelProfile.SetCompleted ("Level "+(i), true);
		//		}
		GameData.getInstance().bestScore = allScore;
		GameData.getInstance().isSoundOn = (int)ObscuredPrefs.GetInt("sound",0);
		GameData.getInstance().isSfxOn = (int)ObscuredPrefs.GetInt("sfx",0);
		GameData.getInstance ().tipRemain = (int)ObscuredPrefs.GetInt ("tipRemain", 0);
		GameData.getInstance ().bestScore = long.Parse(ObscuredPrefs.GetString ("bestScore", "0"));
		GameData.getInstance ().cScore = long.Parse(ObscuredPrefs.GetString ("score", "0"));
		if (GameData.getInstance ().cScore >= GameData.getInstance ().bestScore) {
			GameData.getInstance().bestScore = GameData.getInstance().cScore;		
		}
		//		Debug.Log("soundstate:"+GameData.getInstance().isSoundOn+"sfxstate:"+GameData.getInstance().isSfxOn);
		GameData.isAds = ObscuredPrefs.GetInt ("isAds", 0) == 0 ? true : false;
		
		GameData.getInstance().controlType = ObscuredPrefs.GetInt ("controlType", 1);
		//Nik - Game Center Code Remove || 21-04
		// initGameCenter();
		
		
		initLocalize ();
		
		
		inited = true;
		
	}
	
	public string fontName;
	public void initLocalize(){
		//int localize
//		string systemLanguage = Application.systemLanguage.ToString();
//		//		Debug.Log (systemLanguage);
//		string fulllanguage = "Chinese_zh-CN_zh-TW_zh_Tr_zh_Hans";
//		if (fulllanguage.IndexOf (systemLanguage) != -1) {
//			systemLanguage = "zh-CN";
//		} else {
//			systemLanguage = "en";		
//		}
//		//		LanguageManager.Instance.defaultLanguage =("en");
//		systemLanguage = "en";
//
//		//Check if a language is supported(string = "en" "sv" "es" etc.)
//		bool hasCLanguage = LanguageManager.Instance.IsLanguageSupported(systemLanguage);
//		//Change a language(string = "en" "sv" "es" etc., Make sure the language is supported)
//		if (!hasCLanguage) {
//			LanguageManager.Instance.ChangeLanguage ("en");
//		} else {
//			LanguageManager.Instance.ChangeLanguage (systemLanguage);		
//		}
//		fontName = systemLanguage;
		
	}
	public bool noToggleSound = false;
	//	public void setToggleState(){
	//		//this section will trigger the click itself.So force not play the sound.(if notogglesound is true)
	//		noToggleSound = true;
	//		GameObject checkMusicG = GameObject.Find ("toggleMusic");
	//		if (checkMusicG) {
	//			tk2dUIToggleButton checkMusic =  checkMusicG.GetComponent<tk2dUIToggleButton>();
	//			
	//			
	//			
	//			if(GameData.getInstance().isSoundOn == 0){//music is on
	//				checkMusic.IsOn = true;
	//			}else{
	//				checkMusic.IsOn = false;
	//			}
	//			tk2dUIToggleButton toggleSfx =  GameObject.Find("toggleSFX").gameObject.GetComponent<tk2dUIToggleButton>();
	//			if(GameData.getInstance().isSfxOn == 0){//sfx is on
	//				toggleSfx.IsOn = true;
	//			}else{
	//				toggleSfx.IsOn = false;
	//			}
	//			noToggleSound = false;
	//			
	//			GameObject.Find("toggleMusic").GetComponent<ToggleButtonClick>().refreshCheck();
	//			GameObject.Find("toggleSFX").GetComponent<ToggleButtonClick>().refreshCheck();
	//		}
	//	}
	
	
	//=================================GameCenter======================================
	// public void initGameCenter(){
	// 	Social.localUser.Authenticate(HandleAuthenticated);
	// }
	
	
	private bool isAuthored = false;
	private void HandleAuthenticated(bool success)
	{
		//        Debug.Log("*** HandleAuthenticated: success = " + success);
		if (success) {
			Social.localUser.LoadFriends(HandleFriendsLoaded);
			Social.LoadAchievements(HandleAchievementsLoaded);
			Social.LoadAchievementDescriptions(HandleAchievementDescriptionsLoaded);
			
			
			isAuthored = true;
			
		}
		
		
		
	}
	
	private void HandleFriendsLoaded(bool success)
	{
		//        Debug.Log("*** HandleFriendsLoaded: success = " + success);
		foreach (IUserProfile friend in Social.localUser.friends) {
			//            Debug.Log("*   friend = " + friend.ToString());
		}
	}
	
	private void HandleAchievementsLoaded(IAchievement[] achievements)
	{
		//        Debug.Log("*** HandleAchievementsLoaded");
		foreach (IAchievement achievement in achievements) {
			//            Debug.Log("*   achievement = " + achievement.ToString());
		}
	}
	
	private void HandleAchievementDescriptionsLoaded(IAchievementDescription[] achievementDescriptions)
	{
		//        Debug.Log("*** HandleAchievementDescriptionsLoaded");
		foreach (IAchievementDescription achievementDescription in achievementDescriptions) {
			//            Debug.Log("*   achievementDescription = " + achievementDescription.ToString());
		}
	}
	
	// achievements
	//Nik - Game Center Code Remove || 21-04
	// public void ReportProgress(string achievementId, double progress)
	// {
	// 	if (Social.localUser.authenticated) {
	// 		Social.ReportProgress(achievementId, progress, HandleProgressReported);
	// 	}
	// }
	
	private void HandleProgressReported(bool success)
	{
		//        Debug.Log("*** HandleProgressReported: success = " + success);
	}
	//Nik - Game Center Code Remove || 21-04
	// public void ShowAchievements()
	// {
	// 	if (Social.localUser.authenticated) {
	// 		Social.ShowAchievementsUI();
	// 	}
	// }
	
	// leaderboard
	
	public void ReportScore(string leaderboardId, long score)
	{
		Debug.Log("submitting score to GC...");
	}
	
	public void HandleScoreReported(bool success)
	{
		//        Debug.Log("*** HandleScoreReported: success = " + success);
	}
	//Nik - Game Center Code Remove || 21-04
	// public void ShowLeaderboard()
	// {
	// 	Debug.Log("showLeaderboard");
	// 	if (Social.localUser.authenticated) {
	// 		Social.ShowLeaderboardUI();
	// 	}
	// }
	
	
	//=============================================GameCenter=========================
	
	public void buyFullVersion(){
//		UnityPluginForWindowsPhone.Class1.BuyFullVersion(Const.wp8ID);
	}
	
	ObscuredInt isVis  = 1;
	public void setBannerVisible(bool isVisible){
		#if UNITY_ANDROID
//		try{
//			if(isVisible){
//				jo.Call("setMogoVisibility");
//			}else{
//				jo.Call("setMogoVisibilityGone");
//			}
//		}catch(System.Exception e){
//			
//		}
		#endif
		#if UNITY_IOS
	
		#endif
	}
	//ads
	public void showBanner(){
		if(Application.internetReachability == NetworkReachability.NotReachable){
			return;
		}
		if (GameData.isAds) {

		}
	}
	public bool isCached = false;
	public bool showAfterCache = false;
	public void initInterestitial(){
		if (GameData.isAds) {
//			Chartboost.cacheInterstitial(CBLocation.Default);		
		}
	}
	public void showInterestitial(bool rnd = true){
		if (GameData.isAds) {
//			Chartboost.showInterstitial(CBLocation.Default);
		}
	}
	
	
	bool isInited = false;
	void onInterstitialReadyed(){

		
	}
	

	
	

	

	


	//chartboost events
//	void didFailToLoadInterstitial(CBLocation location, CBImpressionError error) {
//		Debug.Log(string.Format("didFailToLoadInterstitial: {0} at location {1}", error, location));
//	}
//	
//	void didDismissInterstitial(CBLocation location) {
//		Debug.Log("didDismissInterstitial: " + location);
//	}
//	
//	void didCloseInterstitial(CBLocation location) {
//		Debug.Log("didCloseInterstitial: " + location);
//	}
//	
//	void didClickInterstitial(CBLocation location) {
//		Debug.Log("didClickInterstitial: " + location);
//	}
//	
//	void didCacheInterstitial(CBLocation location) {
//		Debug.Log("didCacheInterstitial: " + location);
//	}
//	
//	bool shouldDisplayInterstitial(CBLocation location) {
//		Debug.Log("shouldDisplayInterstitial: " + location);
//		return true;
//	}
//	
//	void didDisplayInterstitial(CBLocation location){
//		Debug.Log("didDisplayInterstitial: " + location);
//	}
//	
//	void didFailToLoadMoreApps(CBLocation location, CBImpressionError error) {
//		Debug.Log(string.Format("didFailToLoadMoreApps: {0} at location: {1}", error, location));
//	}
//	
//	void didDismissMoreApps(CBLocation location) {
//		Debug.Log(string.Format("didDismissMoreApps at location: {0}", location));
//	}
//	
//	void didCloseMoreApps(CBLocation location) {
//		Debug.Log(string.Format("didCloseMoreApps at location: {0}", location));
//	}
//	
//	void didClickMoreApps(CBLocation location) {
//		Debug.Log(string.Format("didClickMoreApps at location: {0}", location));
//	}
//	
//	void didCacheMoreApps(CBLocation location) {
//		Debug.Log(string.Format("didCacheMoreApps at location: {0}", location));
//	}
//	
//	bool shouldDisplayMoreApps(CBLocation location) {
//		Debug.Log(string.Format("shouldDisplayMoreApps at location: {0}", location));
//		return true;
//	}
//	
//	void didDisplayMoreApps(CBLocation location){
//		Debug.Log("didDisplayMoreApps: " + location);
//	}
//	
//	void didFailToRecordClick(CBLocation location, CBImpressionError error) {
//		Debug.Log(string.Format("didFailToRecordClick: {0} at location: {1}", error, location));
//	}
//	
//	void didFailToLoadRewardedVideo(CBLocation location, CBImpressionError error) {
//		Debug.Log(string.Format("didFailToLoadRewardedVideo: {0} at location {1}", error, location));
//	}
//	
//	void didDismissRewardedVideo(CBLocation location) {
//		Debug.Log("didDismissRewardedVideo: " + location);
//	}
//	
//	void didCloseRewardedVideo(CBLocation location) {
//		Debug.Log("didCloseRewardedVideo: " + location);
//	}
//	
//	void didClickRewardedVideo(CBLocation location) {
//		Debug.Log("didClickRewardedVideo: " + location);
//	}
//	
//	void didCacheRewardedVideo(CBLocation location) {
//		Debug.Log("didCacheRewardedVideo: " + location);
//	}
//	
//	bool shouldDisplayRewardedVideo(CBLocation location) {
//		Debug.Log("shouldDisplayRewardedVideo: " + location);
//		return true;
//	}
//	
//	void didCompleteRewardedVideo(CBLocation location, int reward) {
//		Debug.Log(string.Format("didCompleteRewardedVideo: reward {0} at location {1}", reward, location));
//	}
//	
//	void didDisplayRewardedVideo(CBLocation location){
//		Debug.Log("didDisplayRewardedVideo: " + location);
//	}
//	
//	void didCacheInPlay(CBLocation location) {
//		Debug.Log("didCacheInPlay called: "+location);
//	}
//	
//	void didFailToLoadInPlay(CBLocation location, CBImpressionError error) {
//		Debug.Log(string.Format("didFailToLoadInPlay: {0} at location: {1}", error, location));
//	}
//	
//	void didPauseClickForConfirmation() {
//		Debug.Log("didPauseClickForConfirmation called");
//	}
//	
//	void willDisplayVideo(CBLocation location) {
//		Debug.Log("willDisplayVideo: " + location);
//	}





	//----------------------------------------------------
	//test purchanse(always success)
//	void testPurchanse(int index){
//		dfPanel panelBuyAlert = GameObject.Find ("panelBuyAlert").GetComponent<dfPanel> ();
//		switch (index) {
//		case 0:
//			GameData.getInstance().cCoin += 3500;
//			panelBuyAlert.IsVisible = false;
//			break;
//		case 1:
//			GameData.getInstance().cCoin += 7800;
//			panelBuyAlert.IsVisible = false;
//			break;
//		case 2:
//			GameData.getInstance().cCoin += 12000;
//			panelBuyAlert.IsVisible = false;
//			break;
//		case 3:
//			GameData.getInstance().cCoin += 18000;
//			panelBuyAlert.IsVisible = false;
//			break;
//		case 4:
//			GameData.getInstance().cCoin += 25000;
//			panelBuyAlert.IsVisible = false;
//			break;
//		case5:
//			GameData.getInstance().cCoin += 38000;
//			panelBuyAlert.IsVisible = false;
//			break;
//		case 6:
//			GameData.getInstance().cCoin += 100000;
//			panelBuyAlert.IsVisible = false;
//			break;
//		}
//		
//		
	//		ObscuredPrefs.SetString ("coin", GameData.getInstance ().cCoin.ToString());
//		GameObject gridbg = GameObject.Find("gridbg");
//		if (gridbg != null) {
//			gridbg.SendMessage("refreshCoin");		
//		}
	//		ObscuredPrefs.Save ();
//		
//		GameObject panelStore = GameObject.Find("PanelStore");
//		if(panelStore != null){
//			panelStore.GetComponent<GameStore>().refresh();
//		}
//	}

	//change your own id
	public const string CONSUMABLE0 = "farmCombin0";
	public const string CONSUMABLE1 = "farmCombin1";
	public const string CONSUMABLE2 = "farmCombin2";
	public const string CONSUMABLE3 = "farmCombin3";
	public const string CONSUMABLE4 = "farmCombin4";
	public const string CONSUMABLE5 = "farmCombin5";
	public const string CONSUMABLE6 = "farmCombin6";

	public void buy(int index){
		//		Unibiller.initiatePurchase(items[index]);
		switch(index){
		case 0:
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
		case 6:
			break;

		}



	}

	private int selectedItemIndex;
	//private PurchasableItem[] items;

	//	private void onBillerReady(UnibillState state) {
	//		UnityEngine.Debug.Log("onBillerReady:" + state);
	//	}

	/// <summary>
	/// This will be called after a call to Unibiller.restoreTransactions().
	/// </summary>
	private void onTransactionsRestored (bool success) {
		Debug.Log("Transactions restored.");
	}

	/// <summary>
	/// This will be called when a purchase completes.
	/// </summary>
	/// 


	public void purchansedCallback(string id) {


		switch (id) {
		case "pack0":
			GameData.getInstance().cCoin += 0;
		
			break;
		case "pack1":
			GameData.getInstance().cCoin += 0;
		
			break;
		case "pack2":
			GameData.getInstance().cCoin += 0;

			break;
		case "pack3":
			GameData.getInstance().cCoin += 0;
		
			break;
		case "pack4":
			GameData.getInstance().cCoin += 0;

			break;
		case "pack5":
			GameData.getInstance().cCoin += 0;

			break;
		case "pack6":
			GameData.getInstance().cCoin += 0;

			break;
		}


		ObscuredPrefs.SetString ("coin", GameData.getInstance ().cCoin.ToString());
		GameObject gridbg = GameObject.Find("gridbg");
		if (gridbg != null) {
			gridbg.SendMessage("refreshCoin");		
		}
		ObscuredPrefs.Save ();


//		removeAds
		ObscuredPrefs.SetInt ("isAds", 1);
				GameData.isAds = false;
				GameManager.getInstance ().setBannerVisible (false);


	}
}
