﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class LevelMenu : MonoBehaviour
{
    public GameObject StrtFade;
    public GameObject Black;
    // Use this for initialization

    GameObject listItemg;
    void Start()
    {
        StrtFade.gameObject.SetActive(true);
        StartCoroutine(WaitingDis(1F));
        PanelFade.SetActive(false);
        Black.gameObject.SetActive(false);
    }

    void OnEnable()
    {
        GameData.getInstance().resetData();

        GameManager.getInstance().showBanner();
        for (int i = 0; i < 3; i++)
        {
        }


        initView();
    }

    void Update()
    {



    }
    //Text lbTotalScore,lbTotalCoin;

    Text btnContinue, btnStart, btnBack;
    void initView()
    {
        GameManager.getInstance().setBannerVisible(true);
        GameManager.getInstance().initLocalize();

        //lbTotalScore = GameObject.Find ("lbTotalScore").GetComponent<Text> ();
        //lbTotalCoin = GameObject.Find ("lbTotalCoin").GetComponent<Text> ();

        initIndex();

        //lbTotalScore.text = "Best Score:" + GameData.getInstance().bestScore;

        //lbTotalCoin.text = ObscuredPrefs.GetString ("coin", "0");

        GameManager.getInstance().playMusic("bg_music");
    }

    void initIndex()
    {
        GameData.mapType = ObscuredPrefs.GetInt("mapType", 2);
    }
    public GameObject PanelFade;
    public void continueLevel01()
    {

        GameManager.getInstance().playSfx("click");

        PanelFade.SetActive(true);
        PanelFade.GetComponent<Image>().DOFade(1, 1f).OnComplete(() =>
        {

            ObscuredInt tLastLevel = GameData.getInstance().levelPassed;
            if (tLastLevel < GameData.totalLevel)
            {
                GameData.getInstance().cLevel = tLastLevel;
            }
            else
            {
                GameData.getInstance().cLevel = GameData.totalLevel - 1;
            }

            //		ObscuredPrefs.SetInt ("mapType", GameData.mapType);
            fadeIn("level1");
            GameData.mapType = 1;
            GameManager.getInstance().playMusic("bg_music2");

            ObscuredPrefs.SetInt("mapType", GameData.mapType);
        });
    }

    public void MyLevelLoad(int level)
    {
        GameManager.getInstance().playSfx("click");

        PanelFade.SetActive(true);
        PanelFade.GetComponent<Image>().DOFade(1, 1f).OnComplete(() =>
        {

			ObscuredPrefs.DeleteAll();
            ObscuredInt tLastLevel = GameData.getInstance().levelPassed;
            GameData.getInstance().cLevel = level;
            GameData.getInstance().levelPassed = level;
            Debug.Log("current level " + GameData.getInstance().cLevel);
            ObscuredPrefs.DeleteKey("RANK0" + (level - 1));
            ObscuredPrefs.SetInt("RANK0" + level, 1);
            //		ObscuredPrefs.SetInt ("mapType", GameData.mapType);
            fadeIn("level1");
            GameData.mapType = 1;
            GameManager.getInstance().playMusic("bg_music2");

            ObscuredPrefs.SetInt("mapType", GameData.mapType);
        });
    }


    public void backMain()
    {
        //		fade2Main.Play ();
        GameManager.getInstance().playSfx("click");
    }

    public void loadGameScene()
    {
        //		GameObject.Find ("loading").GetComponent<dfPanel> ().IsVisible = true;
        SceneManager.LoadScene("level" + (GameData.getInstance().cLevel + 1));
    }
    public void loadMainScene()
    {
        //		GameObject.Find ("loading").GetComponent<dfPanel> ().IsVisible = true;
        GameManager.getInstance().playSfx("click");
        Black.gameObject.SetActive(true);
        StartCoroutine(WaitingToMenu(1F));
        //		fadeIn("MainMenu");
    }

    public void fadeIn(string levelname)
    {
        //		fadeInOnly.Play ();
        PanelFade.SetActive(true);
        PanelFade.GetComponent<Image>().DOFade(1, 1f).OnComplete(() => { loadnewlevel(levelname); });
    }

    void loadnewlevel(string levelname)
    {
        SceneManager.LoadScene(levelname);
        //Application.LoadLevel(levelname);
    }

    private IEnumerator WaitingDis(float time)
    {
        yield return new WaitForSeconds(time);
        StrtFade.gameObject.SetActive(false);
    }

    private IEnumerator WaitingToMenu(float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene("MainMenu");
    }


}
