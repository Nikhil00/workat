﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.ObscuredTypes;
// using SmartLocalization;

using UnityEngine.UI;
using DG.Tweening;
public class MainMenu : MonoBehaviour {
	public GameObject MusicOn;
	public GameObject MusicOff;
	public GameObject SoundOn;
	public GameObject SoundOff;
	public GameObject Black;
	public GameObject StrtFade;
	public List<GameObject> diamonds = new List<GameObject>();

	public void OnClickedMoreGamesButton()
    {
        Application.OpenURL("https://eclashgames.com/");//https://www.eplaystudios.com
    }


	void Awake()
	{
		if(!PlayerPrefs.HasKey("Diamond"))
		{
				Debug.Log("Player's pref not creted");
			for (int j = 0; j < diamonds.Count; j++)
			{
				// Debug.Log("Dev - String Checked");
				PlayerPrefs.SetInt("Diamond"+(j+1), 0);
				Debug.Log("Dv player name " +  (j+1));
				Debug.Log(PlayerPrefs.GetInt("Diamond"+(j+1)));
			}
				PlayerPrefs.SetInt("Diamond",1);
		}else{
				Debug.Log("Player's pref already  creted");
		}

		if(!PlayerPrefs.HasKey("pausegame"))
		{
				Debug.Log("Player's pref not creted");
				PlayerPrefs.SetInt("pausegame",0);
				PlayerPrefs.Save();
		}else{
				Debug.Log("Player's pref already  creted");
		}

		if(!PlayerPrefs.HasKey("Time"))
		{
				Debug.Log("Player's pref not creted");
				PlayerPrefs.SetInt("Time",0);
				PlayerPrefs.Save();
		}else{
				Debug.Log("Player's pref already  creted");
		}

		if(!PlayerPrefs.HasKey("LevelNum"))
		{
				Debug.Log("Player's pref not creted");
				PlayerPrefs.SetInt("LevelNum",0);
				PlayerPrefs.Save();
		}else{
				Debug.Log("Player's pref already  creted");
		}

		if(!PlayerPrefs.HasKey("BombPower") || !PlayerPrefs.HasKey("shovePower") ||!PlayerPrefs.HasKey("allfitPower"))
		{
				Debug.Log("Player's pref not creted");
				PlayerPrefs.SetInt("BombPower",0);
				PlayerPrefs.SetInt("shovePower",0);
				PlayerPrefs.SetInt("allfitPower",0);
				PlayerPrefs.Save();
		}else{
				Debug.Log("Player's pref already  creted");
		}

		if(!PlayerPrefs.HasKey("Merge3Score") || !PlayerPrefs.HasKey("Merge4Score") || 
			!PlayerPrefs.HasKey("Merge5Score")|| !PlayerPrefs.HasKey("Merge6Score"))
		{
				Debug.Log("Player's pref not creted");
				PlayerPrefs.SetInt("Merge3Score",0);
				PlayerPrefs.SetInt("Merge4Score",0);
				PlayerPrefs.SetInt("Merge5Score",0);
				PlayerPrefs.SetInt("Merge6Score",0);
				PlayerPrefs.Save();
		}else{
				Debug.Log("Player's pref already  creted");
		}
		if(!PlayerPrefs.HasKey("firstPlay"))
		{
				Debug.Log("Player's pref not creted");
				PlayerPrefs.SetInt("firstPlay",0);
				PlayerPrefs.Save();
		}else{
				Debug.Log("Player's pref already  creted");
		}
	}

	// Use this for initialization
	void Start () {
		Debug.Log("Dev - Start MainMenu");
		PlayerPrefs.SetInt("sound",0);
		PlayerPrefs.SetInt("sfx",0);

		StrtFade.gameObject.SetActive (true);
		StartCoroutine(WaitingDis(1F));
		Black.gameObject.SetActive (false);
		GameData.lastWindow = "mainMenu";
		GameManager.getInstance ().init();
		initView ();
#if UNITY_EDITOR
        
#elif UNITY_ANDROID || UNITY_IOS
		GameData.getInstance().clearGame();
		if (PlayerPrefs.GetInt("pausegame") == 1)
		{
			PlayerPrefs.SetInt("pausegame", 0);
			PlayerPrefs.Save();
		}
#endif
		

		for (int i = 0; i < diamonds.Count; i++)
		{
			Debug.Log(PlayerPrefs.GetInt("Diamond"+(i+1)));

			int num = PlayerPrefs.GetInt("Diamond"+(i+1));

			if (num == 0)
			{
				num = 0;
			}else
			{
				num = 1;
			}

			switch (num)
			{
				case 1:
					diamonds[i].transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
					diamonds[i].transform.GetChild(1).gameObject.SetActive(true);
					diamonds[i].transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetInt("Diamond"+(i+1)).ToString();
					break;
				case 0:
					diamonds[i].transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
					diamonds[i].transform.GetChild(1).gameObject.SetActive(false);
					break;
			}
		}


	}
	
//	dfButton lbStart,lbContinue,lbMore,lbReview;
	GameObject gameContainer;
	public static bool showOnStart = false;
	void initView(){
		GameManager.getInstance ().setBannerVisible(false);
		
		GameManager.getInstance ().initLocalize ();
		
		
		
//		lbStart = GameObject.Find ("btnStart").GetComponent<dfButton> ();
////		lbContinue = GameObject.Find ("btnContinue").GetComponent<dfButton> ();
//		lbMore = GameObject.Find ("btnMore").GetComponent<dfButton> ();
//		lbReview = GameObject.Find ("btnReview").GetComponent<dfButton> ();
//		lbStart.Text = LanguageManager.Instance.GetTextValue("GAME_START");
//		lbContinue.Text = LanguageManager.Instance.GetTextValue("CONTINUE");
//		lbMore.Text = LanguageManager.Instance.GetTextValue("MORE_GAME");
//		lbReview.Text = LanguageManager.Instance.GetTextValue("REVIEW");
		
		
		
		string titlepic = "en";//LanguageManager.Instance.GetTextValue("MAIN_TITLE");
		//		print (titlepic + "titlepic");
//		GameObject.Find (titlepic).GetComponent<SpriteRenderer> ().enabled = true;
		
		if (GameData.getInstance ().levelPassed == 0) {
//			lbContinue.Disable();		
		}
		

		//checks
		Toggle tcheckMusic = GameObject.Find("ToggleMusic").gameObject.GetComponent<Toggle>();	
		Toggle tcheckSound = GameObject.Find("ToggleSfx").gameObject.GetComponent<Toggle>();

		tcheckMusic.isOn = GameData.getInstance ().isSoundOn == 1 ? true : false; 
		tcheckSound.isOn = GameData.getInstance ().isSfxOn == 1 ? true : false; 
		
		
		//music
		GameManager.getInstance ().playMusic ("bg_music");
		//sfx
//		GameManager.getInstance ().stopLoopMusic ();

		//only showonce
//		if (!showOnStart) {
//			GameManager.getInstance().showInterestitial ();
//			showOnStart = true;
//		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (GameData.instance.isSoundOn == 1)
		{
			MusicOn.gameObject.SetActive (false);
			MusicOff.gameObject.SetActive (true);
		}

		if (GameData.instance.isSoundOn == 0)
		{
			MusicOn.gameObject.SetActive (true);
			MusicOff.gameObject.SetActive (false);
		}

		if (GameData.instance.isSfxOn == 1)
		{
			SoundOn.gameObject.SetActive (false);
			SoundOff.gameObject.SetActive (true);
		}

		if (GameData.instance.isSfxOn == 0)
		{
			SoundOn.gameObject.SetActive (true);
			SoundOff.gameObject.SetActive (false);
		}
	}
	

	bool locker = false;
	public void OnClick( GameObject g )
	{
		
		if (locker)
			return;
		// Add event handler code here
		switch (g.name) {
		case "btnSetting":
			GameManager.getInstance ().playSfx ("click");
			SceneManager.LoadScene("Setting");
			break;
		case "btnStart":
			locker = true;
			GameManager.getInstance ().playSfx ("click");
			
			//StartCoroutine(loadScene());  
//			fadeIn.Play();
			
			break;
		case "btnContinue":
			GameManager.getInstance ().playSfx ("click");
			locker = true;
			isHitContinue = true;
//			fadeIn.Play();
			break;
		case "btnReview":
			print ("rateme");
			GameManager.getInstance ().playSfx ("click");
			if (Application.platform == RuntimePlatform.WP8Player) {
//				UnityPluginForWindowsPhone.Class1.rateAndReview ();
			} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
				Application.OpenURL ("");
			} else {//andriod
				Application.OpenURL
					("");
			}
			break;
		case "btnMore":
			GameManager.getInstance ().playSfx ("click");
			if (Application.platform == RuntimePlatform.WP8Player) {
//				UnityPluginForWindowsPhone.Class1.moregame ();
				
			} else {
				#if (UNITY_IPHONE || UNITY_ANDROID)
				Application.OpenURL ("");
				#endif
			}
			break;
		case "buttonFull":
			// GameManager.getInstance ().playSfx ("click");
			// GameManager.getInstance ().buyFullVersion ();
			break;	
		case "btnGC":
			// GameManager.getInstance ().playSfx ("click");
			// GameManager.getInstance ().ShowLeaderboard ();
			break;
		case "btnGameTip":
			// GameManager.getInstance ().playSfx ("click");
			// SceneManager.LoadScene("GameTip");
			break;
		case "btnShop":
//			fadeInOnly.Play();
			// StartCoroutine("delay","shop");
			break;
		case "btnQuitYes":
			// Application.Quit();
			break;
		case "btnQuitNo":
//			g.transform.Parent.IsVisible = false;
			break;
		}



		Toggle tcheck = g.GetComponent<Toggle>();
		if (g.name == "ToggleMusic") {
			GameManager.getInstance().playSfx("click");
			if(tcheck.isOn){
				GameManager.getInstance().stopMusic();
				GameData.getInstance().isSoundOn = 1;
				//				print ("open music");
			}else{
				
				GameData.getInstance().isSoundOn = 0;
				GameManager.getInstance().playMusic("bg_music",true);

			}
			PlayerPrefs.SetInt("sound",GameData.getInstance().isSoundOn);
		} else if(g.name == "ToggleSfx"){//sfx
			if(tcheck.isOn){

				GameData.getInstance().isSfxOn = 1;
			}else{
				GameData.getInstance().isSfxOn = 0;
				//on
				GameManager.getInstance().playSfx("click");
			}


			PlayerPrefs.SetInt("sfx",GameData.getInstance().isSfxOn);
		}

	}
	
	
	
	IEnumerator delay(string type){
		float delaytime = 0;
		switch (type) {
		case "shop"	:
			delaytime = 1;
			break;
		}
		yield return new WaitForSeconds (delaytime);
		switch (type) {
		case "shop":
			SceneManager.LoadScene("store");
			break;
		}
	}
	
	


	
	
	
	
	
	
	AsyncOperation async;  
	IEnumerator loadScene()  
	{  
		async = SceneManager.LoadSceneAsync("LevelMenu");  
		// SceneManager.LoadScene
		
		yield return async;  
		
	}  
	bool isHitContinue = false;
	public void loadLevelMenu(){
		GameManager.getInstance ().playSfx ("click");
		Debug.Log("<color=red><b>Star The Game Play Scene</b></color>" + ObscuredPrefs.GetInt ("firstPlay", 0));
		// fadeIn ("LevelMenu");
// #if UNITY_EDITOR
            if (ObscuredPrefs.GetInt ("firstPlay", 0) == 0)
			{
				fadeIn ("Tutorial");
			}
			else
			{
				fadeIn ("level1");
			}
// #elif UNITY_ANDROID || UNITY_IOS
// 			if (ObscuredPrefs.GetInt ("firstPlay", 0) == 0)
// 			{
// 				fadeIn ("Tutorial");
// 			}
// 			else
// 			{
//             	SkillzCrossPlatform.LaunchSkillz(new GameController());
// 			}
//             // SceneManager.LoadScene(1);
// #endif
		
	}

	public GameObject mask;


	public void fadeIn(string levelname){
		//		fadeInOnly.Play ();
		mask.SetActive(true);
		mask.GetComponent<Image> ().color = new Color(0,0,0,0);
		mask.GetComponent<Image> ().DOColor (new Color (0, 0, 0, 1), 1).OnComplete(()=>loadnewlevel(levelname));
	}

	void loadnewlevel(string levelname){
		SceneManager.LoadScene(levelname);
	}

	public void DoQuit()
	{
		Black.gameObject.SetActive (true);
		StartCoroutine(Waiting(1F));
	}

	public void GotoBoards()
	{
		GameManager.getInstance ().playSfx ("click");
		Black.gameObject.SetActive (true);
		StartCoroutine(WaitingBoard(1F));

//		if (PlayerPrefs.HasKey ("Register")){
		//		StartCoroutine(WaitingBoard(1F));
//		} 
//		else{
		//showpopup
//		}
	}

	private IEnumerator Waiting (float time)
	{
		yield return new WaitForSeconds(time);
		// async = SceneManager.LoadSceneAsync("Quit");  
	}
		
	private IEnumerator WaitingBoard (float time)
	{
		yield return new WaitForSeconds(time);
		async = SceneManager.LoadSceneAsync("TopFarmers");  
	}
		
	private IEnumerator WaitingDis (float time)
	{
		yield return new WaitForSeconds(time);
		StrtFade.gameObject.SetActive (false);
	}
}
