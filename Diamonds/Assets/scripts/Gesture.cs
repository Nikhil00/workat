
using UnityEngine;
using System.Collections;

public class Gesture{
	
	/// <summary>
	/// The index of the finger that raise the event (Starts at 0), or -1 for a two fingers gesture.
	/// </summary>
	public int fingerIndex;				
	/// <summary>
	/// The touches count.
	/// </summary>
	public int touchCount;				
	/// <summary>
	/// The start position of the current gesture, or the center position between the two touches for a two fingers gesture.
	/// </summary>
	public Vector2 startPosition;		
	/// <summary>
	/// The current position of the touch that raise the event,  or the center position between the two touches for a two fingers gesture.
	/// </summary>
	public Vector2 position;
	/// <summary>
	/// The position delta since last change.
	/// </summary>
	public Vector2 deltaPosition;		
	/// <summary>
	/// Time since the beginning of the gesture.
	/// </summary>
	public float actionTime;			
	/// <summary>
	/// Amount of time passed since last change.
	/// </summary>
	public float deltaTime;				
	/// <summary>
	/// The siwpe or drag  type ( None, Left, Right, Up, Down, Other => look at EayTouch.SwipeType enumeration).
	/// </summary>

	/// <summary>
	/// The length of the swipe.
	/// </summary>
	public float swipeLength;				
	/// <summary>
	/// The swipe vector direction.
	/// </summary>
	public Vector2 swipeVector;			
	/// <summary>
	/// The pinch length delta since last change.
	/// </summary>
	public float deltaPinch;	
	/// <summary>
	/// The angle of the twist.
	/// </summary>
	public float twistAngle;		
	/// <summary>
	/// The distance between two finger for a two finger gesture.
	/// </summary>
	public float twoFingerDistance;
	/// <summary>
	/// The current picked gameObject under the touch that raise the event.
	/// </summary>
	public GameObject pickObject;	
	/// <summary>
	/// The pick camera.
	/// </summary>
	public Camera pickCamera;

	
	

}

