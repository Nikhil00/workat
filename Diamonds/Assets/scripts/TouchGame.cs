﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;
using DG.Tweening;



[System.Serializable]
public class Nik_BlockList
{
	public string B_Name;
	public Sprite B_Sprite;
}

// Dev - Todo || Select Item Move Code Here
[System.Serializable]
public class Nik_IndexList
{
	// public List<int> WayofItemList = new List<int>();

	public int[,] WayofItemArray = new int[5,5];
}
public class TouchGame : MonoBehaviour {
	
	
	public bool testmode = false;
	public GameObject Partical;

	public GameObject CountPanel;

	public GameObject[] Go321;
	public List<Sprite> DiamondSprite = new List<Sprite>();
	public GameObject WarringPanel;
	public GameObject PausePanel;

	public List<Color> P_Color = new List<Color>();
	public LevelUp L_Up;
	// public GameObject TutorialPanel;
	public GameObject LastObj;


	void Awake()
    {
        // ObscuredPrefs.DeleteAll();

        ObscuredPrefs.DeleteKey("RANK00");
        ObscuredPrefs.DeleteKey("RANK01");
        ObscuredPrefs.DeleteKey("RANK02");
        ObscuredPrefs.DeleteKey("RANK03");
        ObscuredPrefs.DeleteKey("RANK04");
        ObscuredPrefs.DeleteKey("RANK05");
        ObscuredPrefs.DeleteKey("RANK06");
        ObscuredPrefs.DeleteKey("RANK07");
        ObscuredPrefs.DeleteKey("RANK08");
        ObscuredPrefs.DeleteKey("RANK09");
        ObscuredPrefs.DeleteKey("RANK10");
        ObscuredPrefs.DeleteKey("RANK11");
        ObscuredPrefs.DeleteKey("RANKCS");
        ObscuredPrefs.DeleteKey("scoreCS");
        
        
		if (PlayerPrefs.GetInt("pausegame") == 0)
		{
			ObscuredInt tLastLevel = GameData.getInstance().levelPassed;
			int level = 0;
			if (ObscuredPrefs.GetInt ("firstPlay", 0) == 0)
			{}else
			{
// #if UNITY_EDITOR
        		level = Random.Range(1, L_Up.Level.Length);
// #elif UNITY_ANDROID || UNITY_IOS
//         		level = SkillzCrossPlatform.Random.Range(1, L_Up.Level.Length);
// #endif
				
				
				Debug.Log("<color=black><b> Nik - Code Test </b></color>");
			}
			
			ObscuredPrefs.SetInt("levelPassed", level);
			GameData.getInstance().levelPassed = level;

			Debug.Log("<color>Nik - Level Number is </color>" + level + " " + GameData.getInstance().levelPassed);
			GameData.getInstance().cLevel = level;
			GameData.getInstance().levelPassed = level;
			Debug.Log("current level " + GameData.getInstance().cLevel);
			ObscuredPrefs.DeleteKey("RANK0" + (level - 1));
			ObscuredPrefs.SetInt("RANK0" + level, 1);
			//		ObscuredPrefs.SetInt ("mapType", GameData.mapType);
			// fadeIn("level1");
			GameData.mapType = 1;
			GameManager.getInstance().playMusic("bg_music2");

			ObscuredPrefs.SetInt("mapType", GameData.mapType);
		}
		else
		{
			ObscuredInt tLastLevel = GameData.getInstance().levelPassed;
			int level = 0;
			if (ObscuredPrefs.GetInt ("firstPlay", 0) == 0)
			{}else
			{
				level = PlayerPrefs.GetInt("LevelNum");				
			}
			
			ObscuredPrefs.SetInt("levelPassed", level);
			GameData.getInstance().levelPassed = level;

			Debug.Log("<color>Nik - Level Number is </color>" + level + " " + GameData.getInstance().levelPassed);
			GameData.getInstance().cLevel = level;
			GameData.getInstance().levelPassed = level;
			Debug.Log("current level " + GameData.getInstance().cLevel);
			ObscuredPrefs.DeleteKey("RANK0" + (level - 1));
			ObscuredPrefs.SetInt("RANK0" + level, 1);
			//		ObscuredPrefs.SetInt ("mapType", GameData.mapType);
			// fadeIn("level1");
			GameData.mapType = 1;
			GameManager.getInstance().playMusic("bg_music2");

			ObscuredPrefs.SetInt("mapType", GameData.mapType);
		}
    }
	void Start () {
		Debug.Log("Dev - Start TouchGame");

		if (ObscuredPrefs.GetInt ("firstPlay", 0) == 0)
		{
			Dev_Start();
		}
		else
		{
			StartCoroutine(StartDelay());
		}

		// Nik - Code || IntrigratedImage Ref. || 14-04
		// IntrigratedImage
		IntrigratedImage.SetActive(true);
		GoodjobImage = IntrigratedImage.transform.GetChild(0).gameObject;
		GreatImage = IntrigratedImage.transform.GetChild(1).gameObject;
		FantasticImage = IntrigratedImage.transform.GetChild(2).gameObject;
		HurryUpImahe = IntrigratedImage.transform.GetChild(3).gameObject;
		TimeUpImage = IntrigratedImage.transform.GetChild(4).gameObject;
		SecondImage = IntrigratedImage.transform.GetChild(5).gameObject;
		CongratulationsImage = IntrigratedImage.transform.GetChild(6).gameObject;

		// Nik - Code Score || 14-04
		
		MERGE3 = panelScoreBoard.transform.GetChild(8).GetComponent<Text>();
		MERGE4 = panelScoreBoard.transform.GetChild(9).GetComponent<Text>();
		MERGE5 = panelScoreBoard.transform.GetChild(10).GetComponent<Text>();
		MERGE6 = panelScoreBoard.transform.GetChild(11).GetComponent<Text>();
		TimeBonusText = panelScoreBoard.transform.GetChild(12).GetComponent<Text>();

		UnlockDiamondImage = panelScoreBoard.transform.GetChild(13).GetChild(1).gameObject;
	}

	IEnumerator StartDelay()
    {
        CountPanel.SetActive(true);
        // yield return new WaitForSeconds(4.0f);
        yield return new WaitForSeconds(1.0f);
        // GPC.GetComponent<AudioSource>().PlayOneShot(Dev_GUM.GO321Audio[0]);
        // Debug.Log("Nik - 3");
        Go321[0].SetActive(false);
        yield return new WaitForSeconds(1.0f);
        // GPC.GetComponent<AudioSource>().PlayOneShot(Dev_GUM.GO321Audio[1]);
        // Debug.Log("Nik - 2");
        Go321[1].SetActive(false);
        yield return new WaitForSeconds(1.0f);
        // GPC.GetComponent<AudioSource>().PlayOneShot(Dev_GUM.GO321Audio[2]);
        // Debug.Log("Nik - 1");
        Go321[2].SetActive(false);
        yield return new WaitForSeconds(1.0f);
        // GPC.GetComponent<AudioSource>().PlayOneShot(Dev_GUM.GO321Audio[3]);
        // Debug.Log("Nik - GO");
        Go321[3].SetActive(false);
        CountPanel.SetActive(false);
        Dev_Start();

		Debug.Log("<color><b><i>Nik - Is Log</i></b></color>");
		if (PlayerPrefs.GetInt("pausegame") == 1)
		{
			PlayerPrefs.SetInt("pausegame", 0);
			PlayerPrefs.Save();
		}
    }

	void Dev_Start()
	{
		initData ();
		initView ();
		// StartCoroutine("test");

		GenerateNewRingSpawn();

		if ((cJewNo == 4) || (cJewNo == 10) || (cJewNo == 6) || (cJewNo == 200) || (cJewNo == 201))
		{
			// readyJew0.transform.localScale = new Vector3 (.35f, .35f, 1);
		}
		else if ((cJewNo == 101) || (cJewNo == 102) || (cJewNo == 103)) {
			// readyJew0.transform.localScale = new Vector3 (.4f, .4f, 1);
		}
		else
		{
			// readyJew0.transform.localScale = new Vector3 (.35f, .35f, 1);
		}

	}
	ObscuredInt n = 0;

	IEnumerator test(){
		while (testmode) {

			yield return new WaitForSeconds (.01f);

			Gesture g = new Gesture ();
			g.pickObject = gameObject;
// #if UNITY_EDITOR
        	ObscuredInt tposx = (int)Random.Range (0, 500);
			ObscuredInt tposy = (int)Random.Range (100, 800);
// #elif UNITY_ANDROID || UNITY_IOS
// 			ObscuredInt tposx = (int)SkillzCrossPlatform.Random.Range (0, 500);
// 			ObscuredInt tposy = (int)SkillzCrossPlatform.Random.Range (100, 800);
// #endif
			
			
			if (n % 2 == 0) {
				g.position = new Vector3 (tposx, tposy, 0);
			} else {
				g.position = lastTestPos;	
			}
			// Debug.Log("Nik - In call test to On_TouchStart");
			// On_TouchStart (g);
			
			lastTestPos = g.position;
			n++;
		}
	}

	void Update(){
		if (CountPanel.activeInHierarchy == false)
		{
			if ((cJewNo == 4) || (cJewNo == 10) || (cJewNo == 6) || (cJewNo == 200) || (cJewNo == 201))
			{
				// readyJew0.transform.localScale = new Vector3 (.35f, .35f, 1);
			}
			else if ((cJewNo == 101) || (cJewNo == 102) || (cJewNo == 103)) {
				// readyJew0.transform.localScale = new Vector3 (.4f, .4f, 1);
			}
			else
			{
				// readyJew0.transform.localScale = new Vector3 (.35f, .35f, 1);
			}
		}

		if(Input.GetMouseButtonDown(0)){
			Vector2 mousePosition   = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Collider2D hitCollider  = Physics2D.OverlapPoint(mousePosition);

			if (PausePanel.activeInHierarchy == true)
			{
				// Debug.Log("Nik - Pause Panel True");
				return;
			}

			// Debug.Log("mouse pos "+mousePosition.x+" y "+mousePosition.y+" ");    


			if(hitCollider){
//				selectorSprite.transform.position.x = hitCollider.transform.position.x;
//				selectorSprite.transform.position.y = hitCollider.transform.position.y;    
//				Debug.Log("Hit "+hitCollider.transform.name+" x"+hitCollider.transform.position.x+" y "+hitCollider.transform.position.y);    

				switch(hitCollider.name){
				case "gridbg":
						// Debug.Log("Nik - gridbg is clicked");
						Gesture g = new Gesture ();
						g.pickObject = hitCollider.gameObject;
						g.position = Input.mousePosition;
						Dev_On_TouchStart(g);
						// On_TouchStart (g);
						// On_TouchStart (g);
					break;
				case "ready0":
					Debug.Log("Nik - ready0 is clicked 2");
					touchReady (0);
					break;
				case "ready1":
					Debug.Log("Nik - gridbg is clicked 3");
					touchReady (1);
					break;
				case "ready2":
					Debug.Log("Nik - gridbg is clicked 4");
					touchReady (2);
					break;
				case "bomb":
					touchItems ("bomb");
					break;
				case "shove":
					touchItems ("shove");
					Debug.Log("If Shove");
					break;
				case "allfit":
					touchItems ("allfit");
					break;
				}

			}
		}
	}

	public GameObject t_Jewrey;//
	public int TempXX = -1;
	public int TempYY = -1;
	int LastPreviewNo = -1;
	public GameObject[] N_S_Item;
	public List<string> N_S_ItemName = new List<string>();
	public string SpawnItemName;
	public List<int> _ringcolorList;
	public List<GameObject> RingSpawnItem = new List<GameObject>();
	public float xxx = 3.98f, yyy = 3.07f;

	// Only Testing Example
	public bool isOccupied = false;
	public bool haslightsOn = false;
	public Jew north;
	public Jew south;
	public Jew east;
	public Jew west;
	public List<List<int>> mapDataTemp = new List<List<int>>();
	public List<Nik_IndexList> wayFind= new List<Nik_IndexList>();

	public List<int> wayFindinde = new List<int>();
	public List<int> wayFindindexY = new List<int>();

	int findtempX = -1;
	int findtempY = -1;

	public List<string> WayfindIndexList = new List<string>();
	public List<string> Emptymapdata = new List<string>();

	public List<string> upList = new List<string>();

	public List<string> downList = new List<string>();
	public List<string> leftList = new List<string>();
	public List<string> rightList = new List<string>();
	public void FindItemWayLogic(int tempx, int tempY, int X, int Y)
	{
		// X And Y Current Click index (Movement End position Index)

		// TempX And TempY old index (Movement Start position Index)

		Debug.Log("Nik Value IS : " + tempx + " " + tempY + " " + X + " " + Y);

		if(X == tempx)
		{
			findtempX = X;
			Debug.Log("Nikhil - SameValue X");
		}
		else if (X < tempx)
		{
			Debug.Log("<color=blue>Index xx : </color>" + " Current Index "+ X + " < " + tempx + "Right");

			// for (int i = X; i < TempXX; i++)
			// {
			if (tempx -1 != -1)	
				if (X <= tempx)
				{
					Debug.Log("Nikhil - Right");
					findtempX = tempx - 1;
				}
			// }
			
		}
		else if (X > tempx)
		{
			Debug.Log("<color=blue>Index xx : </color>" + " Current Index "+ X + " > " + tempx + "Left");
			
			// for (int i = xx; i > TempXX; i--)
			// {
			if (tempx + 1 != 5)
				if (X >= tempx)
				{
					Debug.Log("Nikhil - Left");
					findtempX = tempx + 1;
				}
			// }
		}
		else
		{
			Debug.LogError("Its a Not Complete");
		}

		if(Y == tempY)
		{
			findtempY = Y;
			Debug.Log("Nikhil - SameValue Y");
		}
		else if (Y < tempY)
		{
			Debug.Log("<color=red>Index yy : </color>" + " Current Index "+ Y + " < " + tempY + " UP");

			// for (int i = yy; i < TempYY; i++)
			// {
			if (tempY + 1 != 5)
				if (Y <= tempY)
				{
					Debug.Log("Nikhil - UP");
					findtempY = Y + 1;
				}
			// }
		}
		else if (Y > tempY)
		{
			Debug.Log("<color=red>Index yy : </color>" + " Current Index "+ Y + " > " + tempY + " Down");

			// for (int i = yy; i > TempYY; i--)
			// {
			if (tempY - 1 != -1)
				if (Y >= tempY)
				{
					Debug.Log("Nikhil - DOWN");
					findtempY = Y - 1;
				}
			// }
		}
		else
		{
			Debug.LogError("Its a Not Complete");
		}

		Debug.Log("Dev - new code log in new value is " + findtempX + " " + findtempY);

		if (findtempX != -1 && findtempY != -1)
		{
			if (mapData[findtempX][findtempY] == 0)
			{
				// wayFind.Add(findtempX,findtempY);

				// wayFind[0]
				Debug.Log("Dev - new code log is wayfind Complete ");

				// mapDataTemp[0][0] = mapDataTemp[findtempX][findtempY];
				if (!WayfindIndexList.Contains(findtempX.ToString()  + findtempY.ToString()))
				{
					WayfindIndexList.Add(findtempX.ToString()  + findtempY.ToString());
					Debug.Log("List In Add A Value");
				}
				

				if (TempXX != findtempX && TempYY != findtempY)
				{
					// FindItemWayLogic(findtempX , findtempY, X, Y);
					Debug.LogError("Is Not the same X And Y not Same Position");
				}
				else if (TempXX == findtempX && TempYY != findtempY)
				{
					// FindItemWayLogic(findtempX , findtempY, X, Y);
					Debug.LogError("Is Not the same X same but Y not Same Position");
				}
				else if (TempXX != findtempX && TempYY == findtempY)
				{
					// FindItemWayLogic(findtempX , findtempY, X, Y);
					Debug.LogError("Is Not the same X not same but Y  Same Position");
				}
				else if (TempXX == findtempX && TempYY == findtempY)
				{
					// FindItemWayLogic(findtempX , findtempY, X, Y);
					Debug.LogError("Is Not the same X And Y Same Position");
					
				}
				else
				{
					Debug.LogError("Is Not the same Position");
				}

				Debug.Log("Dev - new code log mapdataTemp : " + mapDataTemp);
			}
			else
			{
				Debug.Log("Dev - new code log is wayfind Uncomplete by return wayfind");
			}
		}
		else Debug.Log("Dev - new code log is mapadata in negative value is not Wored");
	}

	public void FindItemWayIndex(int xpos, int ypos)
	{
		upList = new List<string>();
		downList = new List<string>();

		// for (int i = 0; i < Emptymapdata.Count; i++)
		// {
			foreach (var item in Emptymapdata)
			{
				int tempXpos = item[0] - 48;
				int tempYpos = item[1] - 48;

				// Debug.Log("List Data : " + );

				if (!Emptymapdata.Contains((tempXpos - 1).ToString() + tempYpos.ToString())) // Only Right List
				{
					Debug.Log("Right side used");
				}
				else
				{
					if (tempXpos == -1)
					{
						Debug.Log("Right side not grid");
					}
					else
					{
						if (!rightList.Contains((tempXpos - 1).ToString() + tempYpos.ToString()))
						{
							
						}
						else
						{
							rightList.Add((tempXpos - 1).ToString() + tempYpos.ToString());
							Debug.Log("RightList Add Data");
						}
					}
					// for (int j = 0; j < Emptymapdata.Count; j++)
					// {
					// 	int listXpos = item[0];
					// 	int listYpos = item[1];

					// 	if (Emptymapdata.Contains((listXpos - 1).ToString() + listYpos.ToString()))
					// 	{
					// 		Debug.Log("RightList Add Data Used");
					// 	}
					// 	else
					// 	{
					// 		rightList.Add((listXpos - 1).ToString() + listYpos.ToString());
					// 		Debug.Log("RightList Add Data");
					// 	}
					// }
				}

				if (!Emptymapdata.Contains((tempXpos + 1).ToString() + tempYpos.ToString())) // Only Left List
				{
					
				}

				if (!Emptymapdata.Contains(tempXpos.ToString() + (tempYpos - 1).ToString())) // Only Up List
				{
					
				}

				if (!Emptymapdata.Contains(tempXpos.ToString() + (tempYpos + 1).ToString())) // Only Down List
				{
					if (tempYpos + 1 == 5)
					{
						// Debug.Log("Down Is not Set");
					}
					else
					{
						
					}
				}

				

				// if (tempXpos == xpos && tempYpos > ypos) 	// Only Down || Left
				// {
				// 	downList.Add(tempXpos.ToString()+tempYpos.ToString());
				// 	Debug.Log("DownList Add");
				// }
				// else if (tempXpos > xpos && tempYpos == ypos) 	// Only Up || right
				// {
				// 	upList.Add(tempXpos.ToString()+tempYpos.ToString());
				// 	Debug.Log("UpList Add");
				// }
				// else
				// {
				// 	Debug.Log("No useed List");
				// }


			}
		// }
	}


	public void Dev_On_TouchStart(Gesture gesture)
	{
		if (TimeUpImage.activeInHierarchy == true)
		{
			return;
		}

		// Verification that the action on the object
		if (GameData.getInstance ().isWin || GameData.getInstance ().isFail)
			return;
		
		if (gesture.pickObject!= null && gesture.pickObject.name == "gridbg")
		{
			if(GameData.getInstance().isLock != 0)return;
			float toffsetX = (gridwidth - .64f)/2+.64f/2;
			float toffsetY = (gridheight - .64f)/2+.64f/2;
			Vector3 a = Camera.main.ScreenToWorldPoint(gesture.position);

			// Debug.Log("Vector3 Value : " + a);
			cameraOffstY = bg.transform.position.y;

			// ObscuredInt xx = Mathf.FloorToInt((a.x-gridwidth*3)/gridwidth)+6;
			// ObscuredInt yy = -1*Mathf.FloorToInt((a.y-gridheight*3f-cameraOffstY)/gridheight);
			ObscuredInt xx = Mathf.FloorToInt((a.x-gridwidth*3.48f)/gridwidth)+6;
			ObscuredInt yy = -1*Mathf.FloorToInt((a.y-gridheight*2.57f-cameraOffstY)/gridheight);

			if(mapData[xx][yy] != 0)
			{
				// Debug.Log("Nik - Is Enter !=0");

				if (GameObject.Find("jew_"+xx+"_"+yy) && LastObj != GameObject.Find("jew_"+xx+"_"+yy) )
				{
					if (LastObj)
					{
						Destroy(t_Jewrey);
						LastPreviewNo = 0;
						Debug.Log("Nik - LastObj Destory");
					}
					Debug.Log("Nik Name Grid " + gesture.pickObject.name + GameObject.Find("jew_"+xx+"_"+yy).name);

					t_Jewrey = GameObject.Find("jew_"+xx+"_"+yy) as GameObject;
					LastObj = t_Jewrey;
					// t_Jewrey.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
					t_Jewrey.transform.DOScale(new Vector3(0.7f, 0.7f, 0.7f), .2f); //original scale

					TempXX = xx;
					TempYY = yy;

					GameObject[] cnjews = GameObject.FindGameObjectsWithTag("jew");

					for (int i = 0; i < cnjews.Length; i++)
					{
						if (cnjews[i] != t_Jewrey)
						{
							cnjews[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
						}
					}
					
					int colorNum;
					string trndNum = LastObj.GetComponent<SpriteRenderer>().sprite.name;
					char c = trndNum[1];
					colorNum = c;
					colorNum -= 48;
					if (colorNum <= 11)
					{
						cPreviewNo = colorNum;
					}
					else
					{
						colorNum = colorNum - 48;
						cPreviewNo = colorNum;
					}

					t_Jewrey = Resources.Load("j" + cPreviewNo + "_" + GameData.mapType) as GameObject;
					t_Jewrey = Instantiate (t_Jewrey, preview.transform.position, Quaternion.identity) as GameObject;
					readyJew0 = t_Jewrey;
					LastPreviewNo = cPreviewNo;
					cJewNo = LastPreviewNo;
					readyJew0.transform.localScale = new Vector3(.35f,.35f,1);
					readyJew0.transform.DOMove(ready0.transform.position,.2f);
					readyJew0.transform.DOScale(new Vector3(.35f,.35f,1), .2f); //original scale

					Emptymapdata = new List<string>();

					for (int i = 0; i < 5; i++)
					{
						for (int j = 0; j < 5; j++)
						{
							if (mapData[i][j] == 0)
							{
								Emptymapdata.Add(i.ToString()+j.ToString());
							}
						}
					}

				}
				else
				{
					
				}
			}
			else
			{
				// Dev - Todo || Select Item Move Code Here

				Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3.98f,-gridheight*yy+gridheight*3.07f+cameraOffstY,0);
				// Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3.48f,-gridheight*yy+gridheight*2.57f+cameraOffstY,0);
				// Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*xxx,-gridheight*yy+gridheight*yyy+cameraOffstY,0);
				// Debug.Log("- Jpos : " + jpos);
					
				if (LastObj)
				{
					// ObscuredInt combined = combineJews();
					// Debug.Log("- XX :" + xx + " - YY : " + yy + " Combined is : " + combined);
					// if(combined > 0)
					// {
					// 	Debug.Log("Nik - It's A combined : " + combined);
					// }
					
#region Spawn And Destroy Code
					// Debug.Log("Nik - Spawn jew_ in 3");
					// GameObject tJewrey = Resources.Load ("j" + cJewNo + "_" + GameData.mapType) as GameObject;
					// cJew = Instantiate(tJewrey,jpos,Quaternion.identity) as GameObject;
					// cJew.transform.parent = container.transform;
					
					// cJew.name = "jew_"+xx+"_"+yy;
					// cJew.transform.position = jpos;
					// cJew.transform.Translate(toffsetX ,toffsetY,0);  
					// cJew.tag = "jew";
					// mapData[xx][yy] = (int)Random.Range(1,4);
#endregion
#region Spawn And Move Code

					#region Map MoveMent code

					// XX And YY Current Click index (Movement End position Index)

					// TempXX And TempYY old index (Movement Start position Index)

					Debug.Log("<color=green>Index : </color>" + " Current Index "+ xx + " " + yy + " Old Index " + TempXX + " : " + TempYY);

					mapDataTemp = new List<List<int>>();
					WayfindIndexList = new List<string>();

					FindItemWayIndex(xx, yy);

					// FindItemWayLogic(TempXX , TempYY, xx, yy);

					// int forTempx = -1;
					// int forTempy = -1;

					int[,] blocks = new int [4,4];

					int tempndexX = -1;
					int tempndexY = -1;
					
					for(int i = 0; i < 4; i++) 
					{
						for(int j = 0; j < 4; j++) 
						{
							// if(i + 1 < 4) blocks[i,j].North = blocks[i +1,j];
							// if(i - 1 >= 0) blocks[i,j].South = blocks[i - 1,j];
							// if(j + 1 < 4) blocks[i,j].East = blocks[i,j + 1];
							// if(j - 1 >= 0) blocks[i,j].West = blocks[i,j - 1];
						}
					}
					//Jew[] blocks;// = new Jew[4,4];

					// for (int i=0; i<5; i++) 
					// {
					// 	for (int j=0; j<5; j++) 
					// 	{
					// 		if (mapData[i][j] == 0)
					// 		{
					// 			if (mapData[xx][yy] == 0)
					// 			{
									
					// 			}
								
					// 		}
					// 	}
					// }

					// for (int i = 0; i<25; i++)
					// {
					// 	int trow = Mathf.FloorToInt(i/5);
					// 	int tcol = i % 5;
					// 	mapDataTemp[trow][tcol] = mapData[trow][tcol];
					// }

					// Debug.Log("MapData : " + mapDataTemp);
					

		// for (int k=0; k<5; k++) 
		// {
		// 	for (int j=0; j<5; j++) 
		// 		{

		for (int k=0; k<5; k++) 
		{
			for (int j=0; j<5; j++) 
				{
					if (mapData[k][j] == 0)
					{
					#region One code Commit
					if (xx < TempXX)
					{
						float moveValuex = -1;
						float moveValuey = -1;
						
						// Debug.Log("<color=blue>Index xx : </color>" + " Current Index "+ xx + " < " + TempXX + "Right");
						for (int i = xx; i < TempXX; i++)
						{
							if (i <= TempXX)
							{
								// Debug.Log("Nikhil - Right");
							}
							moveValuex = (xx-6)*gridwidth+gridwidth*3.98f;
						}
						// float moveValue = (tempPos-6)*gridwidth+gridwidth*3.98f;
						moveValuey = -gridheight*yy+gridheight*3.07f+cameraOffstY;
						LastObj.transform.DOMoveX(moveValuex, 0.2f);
						LastObj.transform.DOMoveY(moveValuey, 0.2f);
						
					}
					else if (xx > TempXX)
					{
						// int tempPos = -1;
						float moveValuex = -1;
						float moveValuey = -1;
						// Vector3 tempPos = new Vector3(0,0,0);
						// Debug.Log("<color=blue>Index xx : </color>" + " Current Index "+ xx + " > " + TempXX + "Left");
						for (int i = xx; i > TempXX; i--)
						{
							if (i >= TempXX)
							{
								// Debug.Log("Nikhil - Left");
							}
							moveValuex = (xx-6)*gridwidth+gridwidth*3.98f;
						}
						// float moveValue = -gridheight*(tempPos-1)+gridheight*3.07f+cameraOffstY;
						// float moveValue;// = (tempPos.x - 6)*gridwidth+gridwidth*3.98f;
						moveValuey = -gridheight*yy+gridheight*3.07f+cameraOffstY;
						LastObj.transform.DOMoveX(moveValuex, 0.2f);
						LastObj.transform.DOMoveY(moveValuey, 0.2f);
						// LastObj.transform.DOMoveX()
					}
					else
					{
						// Debug.LogError("Its a Not Complete" + k + " : " + j);
					}

					if (yy < TempYY)
					{
						float moveValuex = -1;
						float moveValuey = -1;
						// Debug.Log("<color=red>Index yy : </color>" + " Current Index "+ yy + " < " + TempYY + " UP");

						for (int i = yy; i < TempYY; i++)
						{
							if (i <= TempYY)
							{
								// Debug.Log("Nikhil - UP");
								moveValuey = -gridheight*yy+gridheight*3.07f+cameraOffstY;
							}
							// tempPos = new Vector3((xx-6)*gridwidth+gridwidth*3.98f,-gridheight*yy+gridheight*3.07f+cameraOffstY,0);
						}
						moveValuex = (xx-6)*gridwidth+gridwidth*3.98f;
						LastObj.transform.DOMoveY(moveValuey, 0.2f);
						LastObj.transform.DOMoveX(moveValuex, 0.2f);
					}
					else if (yy > TempYY)
					{
						float moveValuex = -1;
						float moveValuey = -1;
						// Debug.Log("<color=red>Index yy : </color>" + " Current Index "+ yy + " > " + TempYY + " Down");

						for (int i = yy; i > TempYY; i--)
						{
							if (i >= TempYY)
							{
								Debug.Log("Nikhil - DOWN");
								moveValuey = -gridheight*yy+gridheight*3.04f+cameraOffstY;
							}
							// tempPos = new Vector3((xx-6)*gridwidth+gridwidth*3.98f,-gridheight*yy+gridheight*3.07f+cameraOffstY,0);
							
						}
						moveValuex = (xx-6)*gridwidth+gridwidth*3.98f;
						LastObj.transform.DOMoveY(moveValuey, 0.2f);
						LastObj.transform.DOMoveX(moveValuex, 0.2f);
					}
					else
					{
						// Debug.LogError("Its a Not Complete" + k + " : " + j);
					}
					#endregion
					}
				}
			}

					
					

					#endregion 
					LastObj.name = "jew_"+xx+"_"+yy;
					Debug.Log("<color=green>LastObj name : </color>" + LastObj.name);

#region String List in move Code Complete
					// if (xxxxx.Count != 0)
					// {
					// 	for (int i = 0; i < xxxxx.Count; i++)
					// 	{
					// 		int x = -1;
					// 		int y = -1;
					// 		float moveValuex = -1;
					// 		float moveValuey = -1;
					// 		for (int j = 0; j < xxxxx[i].Length; j++)
					// 		{
					// 			x = xxxxx[i][0];
					// 			y = xxxxx[i][1];

					// 			Debug.Log("prient x And y "+ x+ " " + y);
					// 		}

					// 		int xvalue = x - 48;
					// 		int yvalue = y - 48;

					// 		moveValuex = (xvalue-6)*gridwidth+gridwidth*3.98f;
					// 		moveValuey = -gridheight*yvalue+gridheight*3.04f+cameraOffstY;
					// 		LastObj.transform.DOMoveX(moveValuex, 0.2f);
					// 		LastObj.transform.DOMoveY(moveValuey, 0.2f);
					// 	}
					// }
#endregion
					
					// LastObj.transform.DOMove(jpos, .2f);
					LastObj.transform.DOScale(new Vector3(.5f,.5f,.5f), .2f); //original scale
					LastObj.transform.Translate(toffsetX ,toffsetY,0);  
					LastObj.tag = "jew";
					mapData[xx][yy] = LastPreviewNo;
#endregion
#region Logic
					mapData[TempXX][TempYY] = 0;
					removeCGrid(TempXX, TempYY);
					// Destroy(LastObj);
					GameObject tjew = LastObj;
					LastObj = null;
					Destroy(t_Jewrey);
					LastPreviewNo = 0;

					if(cJewNo < 1000 || cJewNo == 101)
					{
						checkCombine(xx,yy);
						// Debug.Log("Dev - New Log to complete CheckCombine");
					}
					// GameObject tjew = LastObj;
					if (combineObjects.Count >= 4)
					{
						for (int i = 0; i < combineObjects.Count; i++)
						{
							// Debug.Log("<color> It Is A List Of the Item RemoveFrid And Destory Item </color>" + combineObjects[i].name);

							string _name = combineObjects[i].name;

							for (int j = 0; j < _name.Length; j++)
							{
								int intxx = _name[4];
								int intyy = _name[6];

								intxx = intxx - 48;
								intyy = intyy - 48;

								// Debug.Log("Dev - New log intxx : " + intxx + " || intyy : " + intyy);
								mapData[intxx][intyy] = 0;
								removeCGrid(intxx , intyy);
								Destroy(combineObjects[i]);
							}
						}
						combineScore = 0;
						combineScore += 50*combineObjects.Count;
						Vector3 tpos = new Vector3(tjew.transform.position.x,tjew.transform.position.y,0);
						// Debug.LogError("Score is the " + combineScore + " combineObjects Count : " + combineObjects.Count);
						Debug.Log("Score is the " + combineScore + " combineObjects Count : " + combineObjects.Count);
						showScore(combineScore.ToString(),tpos+new Vector3(0,.1f,0));
						return;
					}

#endregion

					int intx = -1;
					int inty = -1;
					if (SpawnItemName!= null && _ringcolorList.Count != 0)
					{
						int num = _ringcolorList[0];
						

						for (int i = 0; i < SpawnItemName.Length; i++)
						{
							intx = SpawnItemName[4];
							inty = SpawnItemName[6];

							intx = intx - 48;
							inty = inty - 48;

							// Debug.Log("Dev - New log Ring Spawn intxx : " + intx + " || intyy : " + inty);
							// if (intx != -1 && inty != -1)
							// {
								
							// }
						}
						if (mapData[intx][inty] == 0)
						{
							Vector3 jpos1 = new Vector3((intx-6)*gridwidth+gridwidth*3.48f,-gridheight*inty+gridheight*2.57f+cameraOffstY,0);

							GameObject tJewrey = Resources.Load ("j" + num + "_" + GameData.mapType) as GameObject;
							cJew = Instantiate(tJewrey,jpos1,Quaternion.identity) as GameObject;
							cJew.transform.parent = container.transform;
							
							cJew.name = "jew_"+intx+"_"+inty;
							cJew.transform.position = jpos1;
							cJew.transform.Translate(toffsetX ,toffsetY,0);  
							cJew.tag = "jew";
							mapData[intx][inty] = num;

							// checkCombine(intx,inty);
							// Debug.Log("<color=red>checkCombine It's Complete </color>");
						}
						else
						{
							// Debug.LogError("Its a Not Space");
						}

						SpawnItemName = null;
						_ringcolorList.Clear();
						tjew = RingSpawnItem[0];
						Destroy(RingSpawnItem[0]);
					}


					GenerateNewRingSpawn();
					
					checkCombine(intx,inty);

					if (combineObjects.Count >= 4)
					{
						for (int i = 0; i < combineObjects.Count; i++)
						{
							// Debug.Log("<color><b> It Is A List Of the Item RemoveFrid And Destory Item </b></color>" + combineObjects[i].name);

							string _name = combineObjects[i].name;

							for (int j = 0; j < _name.Length; j++)
							{
								int intxx = _name[4];
								int intyy = _name[6];

								intxx = intxx - 48;
								intyy = intyy - 48;

								// Debug.Log("Dev - New log intxx : " + intxx + " || intyy : " + intyy);
								mapData[intxx][intyy] = 0;
								removeCGrid(intxx , intyy);
								Destroy(combineObjects[i]);
							}
						}
						combineScore = 0;
						combineScore += 50*combineObjects.Count;
						Vector3 tpos = new Vector3(tjew.transform.position.x,tjew.transform.position.y,0);
						// Debug.LogError("Score is the " + combineScore + " combineObjects Count : " + combineObjects.Count);
						Debug.Log("Score is the " + combineScore + " combineObjects Count : " + combineObjects.Count);
						showScore(combineScore.ToString(),tpos+new Vector3(0,.1f,0));
					}

					N_S_Item = GameObject.FindGameObjectsWithTag("jew");
					if (N_S_Item.Length == 24)
					{
						gameOver();
					}

				}

					// stopZooms ();
					// cBlock = xx + "_"+yy;

				// if(cJewNo < 1000 || cJewNo == 101)
				// {
				// 		//only check normal jew and all fit
				// 		checkCombine(xx,yy);
				// }
			}
		}
	}

	public void GenerateNewRingSpawn()
	{

		float toffsetX = (gridwidth - .64f)/2+.64f/2;
		float toffsetY = (gridheight - .64f)/2+.64f/2;

		N_S_Item = GameObject.FindGameObjectsWithTag("jew");

		N_S_ItemName.Clear();
		N_S_ItemName = new List<string>();
		_ringcolorList = new List<int>();

		for (int i = 0; i < N_S_Item.Length; i++)
		{
			// N_S_ItemName[i] = N_S_Item[i].name;
			N_S_ItemName.Add(N_S_Item[i].name);
		}

		bool active = false;

		for (int j = 0; j < 5; j++)
		{
			for (int k = 0; k < 5; k++)
			{
				int  createxx = UnityEngine.Random.Range(0,5);
				int  createyy = UnityEngine.Random.Range(0,5);

				string tempname = "jew_" + createxx + "_" + createyy;
				// while (N_S_ItemName.Contains(tempname))
				if (!N_S_ItemName.Contains(tempname))
				{
					N_S_ItemName.Add("jew_" + createxx + "_" + createyy);
					SpawnItemName = tempname;
					Debug.Log(tempname);
					active = true;
					// break;
				}
				else
				{
					// Debug.Log("Else  " + tempname);	
				}
				if (active)
					break;
			}
			if (active)
				break;
		}

		if (SpawnItemName != null)
		{
			RingSpawnItem.Clear();
			RingSpawnItem = new List<GameObject>();
			//  Vector3 Value : (0.6, -0.3, 0.0)
			int intxx = 0;
			int intyy = 0;

			for (int i = 0; i < SpawnItemName.Length; i++)
			{
				intxx = SpawnItemName[4];
				intyy = SpawnItemName[6];

				intxx = intxx - 48;
				intyy = intyy - 48;

				// Debug.Log("Dev - New log intxx : " + intxx + " || intyy : " + intyy);
			}

			int _ringcolor =  getRnd(); //UnityEngine.Random.Range(1, P_Color.Count);
			_ringcolorList.Add(_ringcolor);

			// Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3.98f,-gridheight*yy+gridheight*3.07f+cameraOffstY,0);
			Vector3 jpos = new Vector3((intxx-6)*gridwidth+gridwidth*3.48f,-gridheight*intyy+gridheight*2.57f+cameraOffstY,0);

			GameObject Ring = Resources.Load ("j" + 25 + "_" + GameData.mapType) as GameObject;
			cJew = Instantiate(Ring,jpos,Quaternion.identity) as GameObject;

			cJew.GetComponent<SpriteRenderer>().color = P_Color[_ringcolor];
			cJew.transform.parent = container.transform;
			
			cJew.name = "Ring_"+intxx+"_"+intyy;
			cJew.transform.position = jpos;
			cJew.transform.Translate(toffsetX ,toffsetY,0); 

			RingSpawnItem.Add(cJew);
		}
	}

	ObscuredInt glinktime = 0;
	Vector3 lastTestPos = Vector3.zero;
	
	void FixedUpdate(){
//		if (isCrazy > 0) {
//			isCrazy --;
//			if(isCrazy == 0){
//				GameObject.Find("crazytime").GetComponent<SpriteRenderer>().enabled = false;
//			}
//		}
//		if (glinktime < 100) {
//			glinktime++;
//			if(glinktime == 100){
//				glinktime = 0;
//				createGlink();
//			}
//		}
		
		
	}
	
	// At the touch beginning 
	string cBlock = "";
	GameObject cJew = null;
	
	
// 	public void On_TouchStart(Gesture gesture)
// 	{
// 		if (TimeUpImage.activeInHierarchy == true)
// 		{
// 			return;
// 		}

// 		// Verification that the action on the object
// 		if (GameData.getInstance ().isWin || GameData.getInstance ().isFail)
// 			return;
// 		if (gesture.pickObject!= null && gesture.pickObject.name == "gridbg") {
			
// 			if(GameData.getInstance().isLock != 0)return;
// 			float toffsetX = (gridwidth - .64f)/2+.64f/2;
// 			float toffsetY = (gridheight - .64f)/2+.64f/2;
// 			// Debug.Log("Nik G Position " + gesture.position);
// 			Vector3 a = Camera.main.ScreenToWorldPoint(gesture.position);
// 			cameraOffstY = bg.transform.position.y;
			
// //			print (a.x);

// 			// Debug.Log("Nik - Is Enter On_TouchStart");
// //			Debug.Log("<color=black><b>Nik - Is A grid Vale 3 Change</b></color>");
// 			// Nik - Code Change || 07-04
// 			// ObscuredInt xx = Mathf.FloorToInt((a.x-gridwidth*3)/gridwidth)+6;
// 			// ObscuredInt yy = -1*Mathf.FloorToInt((a.y-gridheight*3f-cameraOffstY)/gridheight);
// 			ObscuredInt xx = Mathf.FloorToInt((a.x-gridwidth*3.48f)/gridwidth)+6;
// 			ObscuredInt yy = -1*Mathf.FloorToInt((a.y-gridheight*2.57f-cameraOffstY)/gridheight);


// 			// Nik - Code Change || 06-04
// 			// if(xx < 0 || xx > 4 || yy < 0 || yy > 4)return;
// 	#region  Tutorial Use
// 			if(xx < 0 || xx > 5 || yy < 0 || yy > 5)return;
// 			//tutorial restrict area
// 			if(GameData.isTutorial){
// 				bool isWrongplace = false;
// 				Debug.Log("Nik - Is Enter isTutorial");
// 				switch(tutorStep){
// 				case 1:
// 					// Nik - Code Change || 06-04
// 					// if(xx!= 1 || yy != 5){
// 					if(xx!= 1 || yy != 4){
// 						return;
// 					}else{
// 						tutorStep++;
// 					}
// 					break;
// 				case 2:
// 					// Nik - Code Change || 06-04
// 					// if(xx!= 1 || yy != 5){
// 					if(xx!= 1 || yy != 4){
// 						return;
// 					}else{
// 						tutorStep++;
// 						// finger.transform.position = setPos(finger,2,5,true);
// 						finger.transform.position = setPos(finger,2,4,true);
// 						Debug.Log("Nik - Is Enter isTutorial case 2");
// 					}
// 					break;
// 				case 3:
// 					// Nik - Code Change || 06-04
// 					// if(xx!= 2 || yy != 5){
// 					if(xx!= 2 || yy != 4){
// 						return;
// 					}else{
// 						tutorStep++;

// 					}
// 					break;
// 				case 4:
// 					// Nik - Code Change || 06-04
// 					// if(xx!= 2 || yy != 5){
// 					if(xx!= 2 || yy != 4){
// 						return;
// 					}else{
// 						tutorStep++;
// 						// finger.transform.position = setPos(finger,5,4,true);
// 						finger.transform.position = setPos(finger,4,3,true);
// 						Debug.Log("Nik - Is Enter isTutorial case 4");
// 					}
// 					break;
// 				case 5:
// 					// Nik - Code Change || 06-04
// 					// if(xx!= 5 || yy != 4){
// 					if(xx!= 4 || yy != 3){
// 						return;
// 					}else{
// 						tutorStep++;
// 					}
// 					break;
// 				case 6:
// 					// Nik - Code Change || 06-04
// 					// if(xx!= 5 || yy != 4){
// 					if(xx!= 4 || yy != 3){
// 						return;
// 					}else{
// 						tutorStep++;
// 						// finger.transform.position = setPos(finger,5,5,true);
// 						finger.transform.position = setPos(finger,4,4,true);
// 						Debug.Log("Nik - Is Enter isTutorial case 6");
// 					}
// 					break;
// 				case 7:
// 					// Nik - Code Change || 06-04
// 					// if(xx!= 5 || yy != 5){
// 					if(xx!= 4 || yy != 4){
// 						return;
// 					}else{
// 						tutorStep++;
// 						// finger.transform.position = setPos(finger,5,5,true);
// 						finger.transform.position = setPos(finger,2,4,true);
// 						Debug.Log("Nik - Is Enter isTutorial case 7");
// 					}
// 					break;
// 				case 8:
// 					// Nik - Code Change || 06-04
// 					// if(xx!= 5 || yy != 5){
// 					if(xx!= 4 || yy != 4){
// 						return;
// 					}else{
// 						tutorStep++;
// 						finger.transform.position = setPos(finger,115,0,true);
// 						// finger.transform.position = setPos(finger,0,0,true);
// 						Debug.Log("Nik - Is Enter isTutorial case 8");
// 					}
// 					break;
// 				case 9:
// 					tutorStep++;
// 					GameData.isTutorial = false;
// 					SceneManager.LoadScene("level1");
					
// 					// Application.LoadLevel(Application.loadedLevelName);
// 					Debug.Log("Nik - Is Enter isTutorial case 9");
// 					return;
// 					break;
				
// 				default:
// 					return;
// 					break;
// 				}
// 				setTutorText();
// 			}
// 	#endregion

// 			//			print (xx+"  "+yy);
// 			//			print (mapData[xx][yy]+"dada");
// 			if(mapData[xx][yy] != 0){
// 				// Debug.Log("Nik - Is Enter !=0");
// 				//if is tresure
// 				if(mapData[xx][yy] == 202){
// 					Debug.Log("Nik - Is Enter  == 202");
// 					GameObject tTresure = GameObject.Find("jew_"+xx+"_"+yy) as GameObject;
// 					tTresure.GetComponent<Animator>().SetTrigger("open");
// 					Destroy(tTresure,.2f);
// 					mapData[xx][yy] = 0;
// 					removeCGrid(xx,yy);
// 					refreshEage();
// 					showScore("",moneyC.transform.position,2);
// 					GameData.getInstance().cCoin+=200;
// 					ObscuredPrefs.SetString ("coin", GameData.getInstance().cCoin.ToString());
// 					lb_coin.text = GameData.getInstance ().cCoin.ToString ();

// 					GameManager.getInstance().playSfx("getcoin");
// 				}

// 				// if is tresure
// 				if(mapData[xx][yy] == 10)
// 				{
// 					Debug.Log("Nik - Is Enter ==10");
// 					GameObject tTresure = GameObject.Find("jew_"+xx+"_"+yy) as GameObject;
// 					tTresure.GetComponent<Animator>().SetTrigger("open");
// 					Destroy(tTresure,10f);
// 					mapData[xx][yy] = 0;
// 					removeCGrid(xx,yy);
// 					refreshEage();
// 					showScore("",moneyC.transform.position,2);
// 					GameData.getInstance().cCoin+=400;
// 					ObscuredPrefs.SetString ("coin", GameData.getInstance().cCoin.ToString());
// 					lb_coin.text = GameData.getInstance ().cCoin.ToString ();

// 					GameManager.getInstance().playSfx("getcoin");
// 				}
					
// 				// Debug.Log("<color><b>Nik - Click to Number</b></color>");
// 				// already exist
// 				if(cJewNo == 102 || cJewNo == 103)
// 				{
// 					Debug.Log("Nik - Is Enter ==102 || 103");
// 					//if is bomb ,can put new
// 					// Nik - Code Change || 06-04
// 					Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3.48f,-gridheight*yy+gridheight*2.57f+cameraOffstY,0);
// 					// Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3,-gridheight*yy+gridheight*3+cameraOffstY,0);
					
// 					//put a new jewley on it(if not put remove it)
// 					if(cJew == null){
// 						Debug.Log("Nik - Cjew == null");
// 						GameObject tJewrey = Resources.Load ("j" + cJewNo + "_" + GameData.mapType) as GameObject;
// 						cJew = Instantiate(tJewrey,jpos,Quaternion.identity) as GameObject;
// 						cJew.transform.parent = container.transform;
// 						cJew.GetComponent<Jew>().setZoom(true);
						
// 						selected.transform.position = jpos;
// 						selected.transform.Translate(toffsetX ,toffsetY,0);  
// 					}else{
// 						Debug.Log("Nik - Cjew == null in else");
// 						//if bomb,destory current block item
// 						GameObject tJew2Bomb = GameObject.Find("jew_"+xx+"_"+yy);
// 						if(tJew2Bomb.transform.position == selected.transform.position){
// 							//bomb touch twice
// 							//GameObject tpickitemAnim = Instantiate (pickitemAnim, tJew2Bomb.transform.position, Quaternion.identity) as GameObject;
// 							//tpickitemAnim.name = "tactive";
// 							//Destroy(tpickitemAnim,1);

// 							Destroy(cJew);//remove shove or bomb
// 							GameData.getInstance().addStep();
// 							GameData.getInstance().addItemUse();
// 							removeCGrid(xx,yy);
// 							bool destroysth = false;
// 							if(cJewNo == 102){
// 								Debug.Log("Nik - Cjew == null in else == 102");
// 								GameObject tExplodeAnim = Instantiate (explodeAnim, tJew2Bomb.transform.position, Quaternion.identity) as GameObject;
// 								tExplodeAnim.name = "texplode";
// 								Destroy(tExplodeAnim,1);
// 								//if bomb,just destroy origin
// 								Destroy(readyJew0);
// 								ClearCreateNew();
// 								DestroyImmediate(tJew2Bomb);
// 								GameManager.getInstance().playSfx("explosion");
// 								destroysth = true;
								
// 							}else if(cJewNo == 103){
// 								Debug.Log("Nik - Cjew == null in else == 103");
// 								GameObject tpickitemAnim = Instantiate (pickitemAnim, tJew2Bomb.transform.position, Quaternion.identity) as GameObject;
// 								tpickitemAnim.name = "tactive";
// 								Destroy(tpickitemAnim,1);
// 								//if shove ,change ready0 to  digged
// 								Destroy(readyJew0);
// 								//								HOTween.To (tJew2Bomb.transform, .2f, new TweenParms ().Prop ("position", ready0.transform.position, false));
// 								tJew2Bomb.transform.DOMove(ready0.transform.position, .2f).SetLoops(1);
// 								readyJew0 = tJew2Bomb;
// 								readyJew0.tag = "Untagged";
// 								cJewNo = mapData[xx][yy];

// 								GameManager.getInstance().playSfx("bounce");
// 							}
// 							//check if there is no jew on board.create random;other wise 3 bomb makes a bug
// 							GameObject[] cnjews = GameObject.FindGameObjectsWithTag("jew");
// 							GameObject[] cnmonsters = GameObject.FindGameObjectsWithTag("monster");
// 							if(cnjews.Length + cnmonsters.Length == 0)
// 							{
// 								Debug.Log("Nik - Cjew == null in else == 0");
// 								createJews();
// 							}
							
// 							if(destroysth && mapData[xx][yy] >= 1000){//destroy a monster,turn to a tomb
// 								Debug.Log("Nik - Spawn jew_ in 1");
// 								GameData.getInstance().addKill(mapData[xx][yy],1);//bomb kill one
// 								GameObject ttomb = Resources.Load("j"+200+"_"+GameData.mapType)as GameObject;
// 								ttomb = Instantiate(ttomb,Vector3.zero,Quaternion.identity) as GameObject;
// 								ttomb.transform.position = setPos(ttomb,xx,yy,true);
// 								createAnGrid(xx,yy);
// 								mapData[xx][yy] = 200;
// 								ttomb.tag = "jew";
// 								ttomb.name = "jew_"+xx+"_"+yy;
// 								checkCombine(xx,yy,200);

// 								//add a tomb
// 								GameData.getInstance().addJewNo(200);

								
// 								if(combineObjects.Count >= 3){

// 									combineJews(ttomb);
// 									//create result
// 									Debug.Log("Nik - Spawn jew_ in 2");
// 									GameObject tresultjew = Resources.Load ("j" + cCombineResult + "_" + GameData.mapType) as GameObject;
// 									tresultjew = Instantiate (tresultjew, ttomb.transform.position, Quaternion.identity) as GameObject;
// 									tresultjew.transform.parent = container.transform;
// 									tresultjew.tag = "jew";
// 									tresultjew.name = "jew_"+xx+"_"+yy;
// 									mapData[xx][yy] = cCombineResult; 
// 									createAnGrid(xx,yy);

// 									//add tomb result
// 									GameData.getInstance().addJewNo(cCombineResult);


// 									//score axis correction
// 									Vector3 tpos = new Vector3(tresultjew.transform.position.x/Camera.main.orthographicSize - .1f,tresultjew.transform.position.y/Camera.main.orthographicSize,0);
									// showScore(combineScore.ToString(),tpos);
// 									combineObjects.Clear();

// 									refreshEage();

// 								}

// 								checkMonster();

// 							}else{
// 								mapData[xx][yy] = 0;
// 							}
// 							cBlock = "";
// 							cJew = null;

							
							
// 							selected.transform.position = new Vector3(-1000,0,0);
// 						}else{
// 							selected.transform.position = jpos;
// 							selected.transform.Translate(toffsetX ,toffsetY,0);  
// 						}
// 					}
// 					if(cJew!=null){
// 						Debug.Log("Nik - Cjew == null in else != null");
// 						cJew.transform.position = jpos;
// 						cJew.transform.Translate(toffsetX ,toffsetY,0);  
// 					}
					
// 					//					stopZooms ();
// 				}else{

// 					Debug.Log("Nik - Is Enter Else 1");
// 					//objects which can not put on a exist block
// 					if(cJew != null){
// 						Jew tjewc = cJew.GetComponent<Jew>();
// 						if(tjewc)
// 							tjewc.setZoom(false);
// 						DestroyObject(cJew);
// 						selected.transform.position = new Vector3(-1000,0,0);
// 						cJew = null;
// 						cBlock = "";
// 					}
// 					stopZooms();
// 				}
				
// 			}else{

// 				Debug.Log("Nik - Is Enter Else 2");
// 				//can put new
// 				// Nik - Code Change || 06-04
// 				Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3.48f,-gridheight*yy+gridheight*2.57f+cameraOffstY,0);
// 				// Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3,-gridheight*yy+gridheight*3+cameraOffstY,0);
				
// 				//put a new jewley on it(if not put remove it)
				
				
// 				if(cJew == null){
// 					Debug.Log("Nik - Cjew == null in else Next else  == null");
// //					Debug.Log("Nik - Spawn jew_ in 3");
// 					GameObject tJewrey = Resources.Load ("j" + cJewNo + "_" + GameData.mapType) as GameObject;
// 					cJew = Instantiate(tJewrey,jpos,Quaternion.identity) as GameObject;
// 					cJew.transform.parent = container.transform;
// 				}
// 				cJew.name = "jew_"+xx+"_"+yy;
// 				cJew.transform.position = jpos;
// 				cJew.transform.Translate(toffsetX ,toffsetY,0);  
// 				cJew.tag = "jew";
// 				if(cJewNo >= 1000){
// 					cJew.tag = "monster";
// 					Debug.Log("<color=red><b>Nik - Is A Monster Tag 1 </b></color>");
// 				}

// 				stopZooms ();
// 				if(cBlock == xx+"_"+yy){
// 					Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy");
// 					//twice touched,really put it.

					
					
// 					if(cJewNo == 102 || cJewNo == 103){
// 						Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy == 102 || 103");
// 						if(cJew != null){
// 							// Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy == 102 || 103 != null");
// 							cJew.GetComponent<Jew>().setZoom(false);
// 							DestroyObject(cJew);
// 							selected.transform.position = new Vector3(-1000,0,0);
// 							cJew = null;
// 							cBlock = "";
// 						}
// 						stopZooms();
// 						Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy == 102 || 103 is A Return");
// 						return;// bomb can not put on blank;
// 					}

// 					//send broadcast
// 					container.BroadcastMessage("touched",SendMessageOptions.DontRequireReceiver);

// 					selected.transform.position = new Vector3(-1000,0,0);
// 					if(cJewNo < 1000){//if not monster
// 						Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy < 1000");
// 						cJew.GetComponent<Jew>().setZoom(false);
// 					}else{
// 						Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy < 1000 to else");
// 						MonsterAction ma = cJew.GetComponentInChildren<MonsterAction>();
// 						ma.touchGame = this;
// 						ma.xx = xx;ma.yy = yy;
						
// 					}
					
// 					GameManager.getInstance().playSfx("select");
					
// 					//combine
					
// 					ObscuredInt combined = combineJews();
// 					if(cJewNo < 1000){
// 						Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy  < 1000 XX : " + xx + " Create New grid  YY" + yy);
// 						createAnGrid(xx,yy);
// 					}

// 					if(combined > 0){
// 						if (Nik_combineObjects.Count > 0)
// 						{
// 							int BlockCount = Nik_combineObjects.Count;

// 							if (BlockCount == 4)
// 							{
// 								StartAnim(4);
// 							}
// 							if (BlockCount == 5)
// 							{
// 								StartAnim(5);
// 							}
// 							if (BlockCount >= 6)
// 							{
// 								BlockCount = 6;
// 								StartAnim(6);
// 							}
// 							Debug.Log("Nik - Block IS : " + BlockCount + " Score : " + combineScore);
// 							ShowAllSCore(combineScore , BlockCount);

// 							int colorNum = -1;
// 							for (int i = 0; i < Nik_combineObjects.Count -1; i++)
// 							{
// 								string trndNum = Nik_combineObjects[i].GetComponent<SpriteRenderer>().sprite.name;
								
// 								char c = trndNum[1];
// 								colorNum = c;
			
// 								colorNum -= 48;
// 								// Debug.Log("Nik - TrandNum : " + trndNum + " colorNum " + colorNum + " I " + i);

// 								// Debug.Log("Nik - Partical effect 3 more Play");

// 								// Nik_combineObjects[i].transform.GetChild(0).gameObject.SetActive(true);
// 								Nik_combineObjects[i].transform.GetChild(0).gameObject.SetActive(true);
// 								if (colorNum <= 11)
// 								{
// 									// Debug.Log("Nik - ColorNumber is Small");
// 									Nik_combineObjects[i].transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().startColor = P_Color[colorNum];
// 								}
// 								else
// 								{
// 									colorNum = colorNum - 48;
// 									// Debug.Log("Nik - ColorNumber is Small else");
// 									Nik_combineObjects[i].transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().startColor = P_Color[colorNum];
// 								}
// 								// Debug.Log("Nik - ColorNumber is Small : " + colorNum);
// 								// Nik_combineObjects[i].transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().startColor = P_Color[colorNum];
// 								// var col =  Nik_combineObjects[i].gameObject.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().colorBySpeed;
// 								// col.color = P_Color[colorNum];
								
// 								// Nik_BlockLists[i].transform.GetChild(0).gameObject.SetActive(true) as GameObject;
// 							}
// 						// 	Debug.Log("<color=white><b><i>Nik - List More 0 </i></b></color>");
// 						}
// 						// Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy > 0");
// 						//create a combine result
// 						Debug.Log("Nik - Spawn jew_ in 4");
// 						GameObject tjew = Resources.Load ("j" + cCombineResult + "_" + GameData.mapType) as GameObject;
// 						tjew = Instantiate (tjew, cJew.transform.position, Quaternion.identity) as GameObject;
// 						Instantiate (Partical, cJew.transform.position, Quaternion.identity);
// 						tjew.transform.parent = container.transform;
// 						tjew.GetComponent<Jew>().delayShow();
// 						Vector3 tpos = new Vector3(tjew.transform.position.x,tjew.transform.position.y,0);
// 						//show combine detail;
// //						showDetail();
// 						showScore(combineScore.ToString(),tpos+new Vector3(0,.1f,0));
// 						//combine result
// 						// Debug.Log("Nik - cCombineResult : " + cCombineResult);
// 						mapData [xx] [yy] = cCombineResult;
// 						GameData.getInstance().addJewNo(cCombineResult);
// 						tjew.name = "jew_"+xx+"_"+yy;
// 						cCombineResult = 0;
// 						tjew.tag = "jew";

// 						// Debug.Log("Nik - Partical effect Play More List");
						
// 						int colorNumber = -1;
// 						string trndNum1 = cJew.GetComponent<SpriteRenderer>().sprite.name;
// 						char c1 = trndNum1[1];
// 						colorNumber = c1;
	
// 						colorNumber -= 48;

// 						tjew.transform.GetChild(0).gameObject.SetActive(true);
// 						if (colorNumber <= 11)
// 						{
// 							// Debug.Log("Nik - ColorNumber is Small");
// 							tjew.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().startColor = P_Color[colorNumber];
// 						}
// 						else
// 						{
// 							colorNumber = colorNumber - 48;
// 							// Debug.Log("Nik - ColorNumber is Small else");
// 							tjew.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().startColor = P_Color[colorNumber];
// 						}
// 						// Debug.Log("Nik - ColorNumber is Small : " + colorNumber);
// 						// var colCol =  tjew.gameObject.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().colorBySpeed;
// 						// colCol.color = P_Color[colorNumber];
// 						// cJew.gameObject.transform.GetChild(0).gameObject.SetActive(true);
						
// 						// tjew.transform.GetChild(0).gameObject.SetActive(true);


// 					}else{
// 						//just set one on it , no combine. or all fit single
// 						Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy > 0 to Else");

// 						Nik_BlockLists = new List<Nik_BlockList>();
// 						Nik_combineObjects.Add(cJew);

// 						string Name = cJew.name;
// 						Sprite Pic = cJew.GetComponent<SpriteRenderer>().sprite;

// 						Nik_BlockList BlockData = new Nik_BlockList();
// 						BlockData.B_Name = Name;
// 						BlockData.B_Sprite = Pic;

// 						Nik_BlockLists.Add(BlockData);
// 						// Debug.Log("Nik - Partical effect Play");

// 						int colorNum = -1;
// 						string trndNum = cJew.GetComponent<SpriteRenderer>().sprite.name;
// 						char c = trndNum[1];
// 						colorNum = c;
	
// 						colorNum -= 48;

// 						if (cJew.transform.GetChild(0).gameObject != null)
// 						{
// 							cJew.transform.GetChild(0).gameObject.SetActive(true);	
						
// 							if (colorNum <= 11)
// 							{
// 								// Debug.Log("Nik - ColorNumber is Small");
// 								cJew.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().startColor = P_Color[colorNum];
// 							}
// 							else
// 							{
// 								colorNum = colorNum - 48;
// 								// Debug.Log("Nik - ColorNumber is Small else");
// 								cJew.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().startColor = P_Color[colorNum];
// 							}
// 						}
						
// 						if(cJewNo == 101){//all fit,but no combine,put j1
// 						// Debug.Log("Nik - Spawn jew_ in 5");
// 						// Debug.Log("Nik - Cjew == null in else Next else  == xx _ yy > 0 to Else == 101");
// 							GameObject tj1 = Resources.Load("j"+1+"_"+ GameData.mapType) as GameObject;
// 							GameObject tcJew = Instantiate (tj1, cJew.transform.position, Quaternion.identity) as GameObject;
// 							cJewNo = 1;
// 							tcJew.name = "jew_"+xx+"_"+yy;
// 							tcJew.tag = "jew";
// 							Destroy(cJew);
// 						}
						
// 						//plus the jew used
// 						GameData.getInstance().addJewNo(cJewNo);
// 						GameData.getInstance().addStep();

// 						cJew = null;
// 						mapData[xx][yy] = cJewNo;

// 					}
// 					Destroy(readyJew0);
// 					readyJew0 = null;
// 					ClearCreateNew();

// 					checkMonster();
					
// 					GameObject[] cnjews = GameObject.FindGameObjectsWithTag("jew");
// 					GameObject[] cnmonsters = GameObject.FindGameObjectsWithTag("monster");
// 					// Nik - Code Change || 06-04
// 					// if(cnjews.Length + cnmonsters.Length == 36){
// 					if(cnjews.Length + cnmonsters.Length == 25)
// 					{
// 						Debug.Log("<color=green><b>Nik Board Full GameOver</b></color>");
// 						//board is full
// 						if(combined == 0){
// 							gameOver();
// 							ShowAllSCore(0,0);
// 							StopCoroutine("CountDownTime");
// 							CountDownTimeOver();
// 						}
// 					}
					
// 				}else{
// 					//touch once,select grid(or touch another place)

// 					selected.transform.position = jpos;
// 					selected.transform.Translate(toffsetX ,toffsetY,0);   
// 					cBlock = xx + "_"+yy;
					
// 					if(cJewNo < 1000){
// 						cJew.GetComponent<Jew>().setZoom(true);
// 					}
// 					//stop origin combine object zoom
					
// 					if(cJewNo < 1000 || cJewNo == 101){
// 						//only check normal jew and all fit
// 						checkCombine(xx,yy);
// 					}
// 				}
				
// 			}
// 			refreshEage();
// 			if (PlayerPrefs.GetInt("pausegame") == 0)
// 			{
// 				saveAllJew ();
// 			}
// 		}
		
// 	}
	
	

	
	//touch ready section
	public void touchReady(int type){
		if (GameData.isTutorial)
						return;
		if (type == 0) {
			//touch now jew
			if (readyJew1 == null || readyJew2 == null) {//has storage
				
				//put into storage temp place
				if (readyJew1 == null) {
					
					readyJew1 = Instantiate (readyJew0, ready0.transform.position, Quaternion.identity) as GameObject;
					//					HOTween.To (readyJew1.transform, .2f, new TweenParms ().Prop ("position", ready1.transform.position, false));
					//					HOTween.To (readyJew1.transform, .2f, new TweenParms ().Prop ("localScale", new Vector3(.8f,.8f,1), false));
					
					readyJew1.transform.DOMove(ready1.transform.position, .2f);
					readyJew1.transform.DOScale(new Vector3(.35f,.35f,1), .2f);
					ready1No = cJewNo;
				} else if (readyJew2 == null) {
					readyJew2 = Instantiate (readyJew0, ready0.transform.position, Quaternion.identity) as GameObject;
					//					HOTween.To (readyJew2.transform, .2f, new TweenParms ().Prop ("position", ready2.transform.position, false));
					//					HOTween.To (readyJew2.transform, .2f, new TweenParms ().Prop ("localScale", new Vector3(.8f,.8f,1), false));
					readyJew2.transform.DOMove(ready2.transform.position, .2f);
					readyJew2.transform.DOScale(new Vector3(.35f,.35f,1), .2f);
					
					ready2No = cJewNo;
				}
				syncCJew ();
				ClearCreateNew ();
				
			}
		} else if (type == 1) {
			//Touch slot 1
			if (readyJew1 == null) {
				readyJew1 = Instantiate (readyJew0, ready0.transform.position, Quaternion.identity) as GameObject;
				//				HOTween.To (readyJew1.transform, .2f, new TweenParms ().Prop ("position", ready1.transform.position, false));
				//				HOTween.To (readyJew1.transform, .2f, new TweenParms ().Prop ("localScale", new Vector3(.8f,.8f,1), false));
				readyJew1.transform.DOMove(ready1.transform.position, .2f);
				readyJew1.transform.DOScale(new Vector3(.35f,.35f,1), .2f);
				ready1No = cJewNo;
				syncCJew ();
				ClearCreateNew ();
				
			}else{
				
				//change  to ready0 place
				//				HOTween.To (readyJew1.transform, .2f, new TweenParms ().Prop ("position", ready0.transform.position, false));
				//				HOTween.To (readyJew0.transform, .2f, new TweenParms ().Prop ("position", ready1.transform.position, false));
				
				//				HOTween.To (readyJew1.transform, .2f, new TweenParms ().Prop ("localScale", new Vector3(1f,1f,1), false));
				//				HOTween.To (readyJew0.transform, .2f, new TweenParms ().Prop ("localScale", new Vector3(.8f,.8f,1), false));
				
				// readyJew1.transform.DOMove(ready0.transform.position, .2f);
				// readyJew1.transform.DOScale(new Vector3(.35f,.35f,1), .2f);
				
				// readyJew0.transform.DOMove(ready1.transform.position, .2f);
				// readyJew0.transform.DOScale(new Vector3(.35f,.35f,1), .2f);
				
				
				
				GameObject tempJew = readyJew1;
				readyJew1 = readyJew0;
				readyJew0 = tempJew;
				
				ObscuredInt tempNo = ready1No;
				ready1No = cJewNo;
				cJewNo = tempNo;
				syncCJew ();
			}
		} else if (type == 2) {
			if (readyJew2 == null) {
				readyJew2 = Instantiate (readyJew0, ready0.transform.position, Quaternion.identity) as GameObject;
				//				HOTween.To (readyJew2.transform, .2f, new TweenParms ().Prop ("position", ready2.transform.position, false));
				//				HOTween.To (readyJew2.transform, .2f, new TweenParms ().Prop ("localScale", new Vector3(.8f,.8f,1), false));
				readyJew2.transform.DOMove(ready2.transform.position, .2f);
				readyJew2.transform.DOScale(new Vector3(.35f,.35f,1), .2f);
				
				ready2No = cJewNo;
				syncCJew ();
				ClearCreateNew ();
			}else{
				
				//change  to ready0 place
				//				HOTween.To (readyJew2.transform, .2f, new TweenParms ().Prop ("position", ready0.transform.position, false));
				//				HOTween.To (readyJew0.transform, .2f, new TweenParms ().Prop ("position", ready2.transform.position, false));
				//
				//				HOTween.To (readyJew2.transform, .2f, new TweenParms ().Prop ("localScale", new Vector3(1f,1f,1), false));
				//				HOTween.To (readyJew0.transform, .2f, new TweenParms ().Prop ("localScale", new Vector3(.8f,.8f,1), false));
				
				readyJew2.transform.DOMove(ready0.transform.position, .2f);
				readyJew2.transform.DOScale(new Vector3(0.35f,0.35f,1), .2f);
				
				readyJew0.transform.DOMove(ready2.transform.position, .2f);
				readyJew0.transform.DOScale(new Vector3(.35f,.35f,1), .2f);
				
				GameObject tempJew = readyJew2;
				readyJew2 = readyJew0;
				readyJew0 = tempJew;
				
				ObscuredInt tempNo = ready2No;
				ready2No = cJewNo;
				cJewNo = tempNo;
				syncCJew ();
			}
		}
		if (PlayerPrefs.GetInt("pausegame") == 0)
		{
			saveReadyJew ();
		}
	}
	
	//touchItems
	public void touchItems(string itemName){
		if(GameData.isTutorial)return;

		if (GameData.getInstance ().isWin || GameData.getInstance ().isFail)
			return;
		if (GameData.getInstance ().isLock >= 3)
			return;
		GameManager.getInstance().playSfx("select");
		switch (itemName) {
		case "allfit":
		if (AllFitpowerCount == 0)
		{
			return;
		}
		else
		{
			if(GameData.getInstance().nAllFit == 0){
				return;
				// buyItemPanel.SetActive (true);// = true;
				// Black.gameObject.SetActive (true);
				// GameData.getInstance().isLock = 1;
				// GameData.getInstance().cBuyItem = "allfit";
				// refreshItemIcon("allfit");
				
			}else{
				generateSpecial(101);
				GameData.getInstance().nAllFit = 0;
				ObscuredPrefs.SetInt("nAllFit",0);
				refreshItemIcon("allfit");
				redplus1.SetActive(false);
				redplus1.transform.parent.transform.GetChild(1).gameObject.SetActive(false);
				redplus1.transform.parent.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 100);
			}
		
			AllFitpowerCount--;
			Debug.Log("Power Count Is The -- : " + AllFitpowerCount);
		}
			break;
		case "bomb":
		if (BombpowerCount == 0)
		{
			return;
		}
		else
		{
			if(GameData.getInstance().nBomb == 0){
				return;
				// buyItemPanel.SetActive (true);// = true;
				// Black.gameObject.SetActive (true);
				// GameData.getInstance().isLock = 1;
				// GameData.getInstance().cBuyItem = "bomb";
				// refreshItemIcon("bomb");
			}else{
				generateSpecial(102);
				GameData.getInstance().nBomb = 0;
				ObscuredPrefs.SetInt("nBomb",0);
				refreshItemIcon("bomb");
				redplus2.SetActive(false);
				redplus2.transform.parent.transform.GetChild(1).gameObject.SetActive(false);
				redplus2.transform.parent.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 100);
			}
		
			BombpowerCount--;
		}
			break;
		case "shove":
		if (ShovepowerCount == 0)
		{
			return;
		}
		else
		{
			if(GameData.getInstance().nShove == 0){
				return;
				// buyItemPanel.SetActive (true);// = true;
				// Black.gameObject.SetActive (true);
				// GameData.getInstance().isLock = 1;
				// GameData.getInstance().cBuyItem = "shove";
				// refreshItemIcon("shove");
			}else{
				generateSpecial(103);
				GameData.getInstance().nShove = 0;
				ObscuredPrefs.SetInt("nShove",0);
				refreshItemIcon("shove");
				redplus3.SetActive(false);
				redplus3.transform.parent.transform.GetChild(1).gameObject.SetActive(false);
				redplus3.transform.parent.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 100);
			}
		
			ShovepowerCount--;
		}
			break;
		}
		panelBuyItemS.refreshInfo (itemName);
		if (PlayerPrefs.GetInt("pausegame") == 0)
		{
			saveReadyJew ();
		}
		
	}
	
	public void itemBought(string itemName){
		switch (itemName) {
		case "allfit":
			redplus1.SetActive(false);
			break;
		case "bomb":
			redplus2.SetActive(false);
			break;
		case "shove":
			redplus3.SetActive(false);
			break;
		}
		
		ObscuredPrefs.SetString ("coin", GameData.getInstance().cCoin.ToString());
		lb_coin.text = GameData.getInstance ().cCoin.ToString ();
	}
	
	void refreshItemIcon(string itemName){
		foreach (Transform  g in buyItemContainer.transform) {
//			GameObject tsp = g.GetComponent<dfSprite>();
			
			if(g.name != itemName){
				g.GetComponent<Image> ().enabled = false;
			}else{
				g.GetComponent<Image> ().enabled = true;
			}
		}
	}
	
	public void refreshCoin(){
		lb_coin.text = GameData.getInstance ().cCoin.ToString ();
	}
	
	void ClearCreateNew(){
		// if (readyJew0 != null) {
		// 	Destroy (readyJew0);
		// 	readyJew0 = null;
		// }
		// if (previewJew != null) {
		// 	//		//get a new from preview and create a new preview
		// 	//			previewJew.transform.localScale = new Vector3(1,1f,1);
		// 	//			readyJew0 = Instantiate (previewJew, preview.transform.position, Quaternion.identity) as GameObject;
		// 	//
		// 	//			HOTween.To (readyJew0.transform, .2f, new TweenParms ().Prop ("position", ready0.transform.position, false));
		// 	Destroy (previewJew);
		// 	previewJew = null;
		// 	cJew = null;
		// 	//			cJewNo = cPreviewNo;
		// }
		
		// generateNew ();
	}
	
	//destroy selected item while change
	void syncCJew(){
		if(cJew != null){
			Jew jewc = cJew.GetComponent<Jew>();
			if(jewc!=null)jewc.setZoom(false);
			DestroyObject(cJew);
			selected.transform.position = new Vector3(-1000,0,0);
			cJew = null;
			cBlock = "";
		}
		stopZooms();
	}
	
	float gridwidth,gridheight;
	
	ObscuredInt isCrazy = 0;
	string lang = "";
	
	public static int maxN = 10;
	ObscuredInt tutorStep = 1;

	void initData(){
		GameManager.getInstance ().initLocalize ();
		lang = "en";// LanguageManager.Instance.GetTextValue("GAME_TIPCONTEXT");
		
		string tMap = ObscuredPrefs.GetString ("jewMap", "");
		// Debug.Log("Nik 1 - " + tMap + ObscuredPrefs.GetString ("jewMap", ""));
		// Nik - Code Change || 06-04
		// for (int i=0; i<6; i++) {
		for (int i=0; i<5; i++) {
			List<int> rowData = new List<int>();
			// for (int j=0; j<6; j++) {	
			for (int j=0; j<5; j++) {	
				rowData.Add(0);
			}
			
			mapData.Add(rowData);
		}
		// Nik - Code Change || 07-04
		if (ObscuredPrefs.GetInt ("firstPlay", 0) == 0)
		{
			ObscuredPrefs.SetInt ("firstPlay", 1);
			GameData.isTutorial = true;
		}

		if (GameData.isTutorial) {
			tMap = "0_0_0_1_1_0_0_0_2_0_0_0_0_0_0_0_0_0_3_3_0_0_2_0_0";
			// tMap = "0_0_0_1_1_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0_3_3_0_0_0_2_0_0_0";
			// tMap = "0_0_0_0_1_1_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0_3_3_0_0_0_2_0_0";
		}


		// Nik - Code Change || 06-04
		if (tMap != "") 
		{
			string[] map = tMap.Split("_"[0]);
			Debug.Log(" Nik - tmap " + map);
			for (int i = 0; i<25; i++) {
				int trow = Mathf.FloorToInt(i/5);
				int tcol = i % 5;
				mapData[trow][tcol] = int.Parse(map[i]);	
			}
		}
		else
		{
			Debug.Log("Nik - tmap is Null");
		}
		
		
		GameData.getInstance ().cScore = long.Parse (ObscuredPrefs.GetString ("score", "0"));
		GameData.getInstance ().cCoin = long.Parse (ObscuredPrefs.GetString ("coin", "300"));
		
		GameData.getInstance ().nAllFit = ObscuredPrefs.GetInt ("nAllFit", 0);
		GameData.getInstance ().nBomb = ObscuredPrefs.GetInt ("nBomb", 0);
		GameData.getInstance ().nShove = ObscuredPrefs.GetInt ("nShove", 0);
		isCrazy = 0;



	}
	
	GameObject container;
	GameObject itemSection,allmatch;
	GameObject bg,groundbg;
	GameObject grideage;
	public GameObject selected;
	GameObject combineAnim,combineAnim2,explodeAnim,activeitemAnim,pickitemAnim;
	public GameObject readyJew0 = null,readyJew1 = null,readyJew2 = null,previewJew = null;
	GameObject ready0,ready1,ready2,preview;
	GameObject readySection;
	GameObject lbcScoreg0,lbcScoreg1,lbcScoreg2;
	Text lbcScore0,lbcScore1,lbcScore2;
//	dfProgressBar engerybar;
	GameObject redplus1,redplus2,redplus3;
	GameObject buyItemContainer;
	GameObject moneyC;
	GameObject finger;
	public Text tutorContext;
	//
	float cameraOffstY;
	//panels
	public GameObject buyItemPanel;
	GameObject energybarC;
	GameObject detailC;
	GameObject scoreTxtTitle,scoreTxt;
	Text lb_scoreTxt,lb_coin;
	public GameObject panelScoreBoard;
	public GameObject spHelp;
	public GameObject Black;
	//fx
	GameObject splashfx;
	//script
	PanelBuyItem panelBuyItemS;

	int time;
	int ShovepowerCount = 1;
	int BombpowerCount = 1;
	int AllFitpowerCount = 1;
	
	public GameObject IntrigratedImage;

	GameObject GoodjobImage,GreatImage,FantasticImage,HurryUpImahe,TimeUpImage,SecondImage,UnlockDiamondImage,CongratulationsImage;

	Text MERGE3,MERGE4,MERGE5,MERGE6,TimeBonusText;
	int M3Score,M4Score,M5Score,M6Score,TimeBonus;
	public Text CurrentScorePausePanel;



	public void Dev_UndoBtn()
	{
		// if (Nik_BlockLists.Count == 0)
		// {
		// 	return;
		// }

		// int xx = -1;
		// int yy = -1;
		// int trnd = -1;

		// if (Nik_BlockLists.Count == 1)
		// {
		// 	string Xy = Nik_BlockLists[0].B_Name;
		// 	string trndNum = Nik_BlockLists[0].B_Sprite.name;
		// 	char c = trndNum[1];
		// 	Debug.Log("Nik - BlockName :" + Xy);

		// 	char numXX;
		// 	char numYY;
				
		// 	numXX = Xy[4];
		// 	numYY = Xy[6];

		// 	trnd = c;
		// 	xx = numXX;
		// 	yy = numYY;

		// 	xx -= 48;
		// 	yy -= 48;
		// 	trnd -= 48;

		// 	if (Nik_BlockLists.Count == 1)
		// 	{
		// 		GameObject tJew2Bomb = GameObject.Find("jew_"+xx+"_"+yy);
		// 		if (tJew2Bomb != null)
		// 		{
		// 			Debug.Log("Nik - tJew2Bomb is a GO");
		// 			// Destroy(cJew);//remove shove or bomb
		// 			GameData.getInstance().addStep();
		// 			GameData.getInstance().addItemUse();
		// 			removeCGrid(xx,yy);
		// 			bool destroysth = false;

		// 			GameObject tpickitemAnim = Instantiate (pickitemAnim, tJew2Bomb.transform.position, Quaternion.identity) as GameObject;
		// 			tpickitemAnim.name = "tactive";
		// 			Destroy(tpickitemAnim,1);
		// 			//if shove ,change ready0 to  digged
		// 			Destroy(readyJew0);
		// 			//								HOTween.To (tJew2Bomb.transform, .2f, new TweenParms ().Prop ("position", ready0.transform.position, false));
		// 			tJew2Bomb.transform.DOMove(ready0.transform.position, .2f).SetLoops(1);
		// 			readyJew0 = tJew2Bomb;
		// 			readyJew0.tag = "Untagged";
		// 			cJewNo = mapData[xx][yy];

		// 			GameManager.getInstance().playSfx("bounce");

		// 			readyJew0 = Resources.Load("j" + cJewNo + "_" + GameData.mapType) as GameObject;
		// 			readyJew0 = Instantiate(readyJew0, ready0.transform.position,Quaternion.identity) as GameObject;
		// 		}
		// 		else
		// 		{
		// 			Debug.Log("Nik - tJew2Bomb is a Null");
		// 		}

		// 		Debug.Log("Nik - OneSpawn : " + Nik_BlockLists.Count);
		// 		mapData[xx][yy] = 0;
		// 		Destroy(Nik_combineObjects[0]);
		// 		Nik_combineObjects.Remove(Nik_combineObjects[0]);
		// 		Nik_BlockLists = new List<Nik_BlockList>();
		// 	}
		// }
		// else
		// {
		// 	for (int i = 0; i < Nik_BlockLists.Count; i++)
		// 	{
		// 		if (i == Nik_BlockLists.Count)
		// 		{
		// 			Debug.Log("<color=black><b>Nik - if (Nik_BlockLists.Count < Nik_BlockLists.Count - 1)</b></color>");
		// 			Destroy(Nik_combineObjects[i]);
		// 			Nik_BlockLists = new List<Nik_BlockList>();
		// 			break;
		// 		}
				
		// 		string Xy = Nik_BlockLists[i].B_Name;
		// 		string trndNum = Nik_BlockLists[i].B_Sprite.name;
		// 		char c = trndNum[1];
		// 		Debug.Log("Nik - BlockName :" + Xy);

		// 		char numXX;
		// 		char numYY;
					
		// 		numXX = Xy[4];
		// 		numYY = Xy[6];

		// 		trnd = c;
		// 		xx = numXX;
		// 		yy = numYY;

		// 		xx -= 48;
		// 		yy -= 48;
		// 		trnd -= 48;

		// 		Debug.Log("Nik - C : " + c + " XX : " + xx + " YY : " + yy);

				
					
		// 		float toffsetX = (gridwidth - .64f)/2+ .64f/2;
		// 		float toffsetY = (gridheight - .64f)/2+ .64f/2;
		// 		// Nik - Code Change || 06-04
		// 		Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3.48f,-gridheight*yy+gridheight*2.57f+cameraOffstY,0);
				
		// 		Debug.Log("Nik - Spawn jew_ in 7 ");// + toffsetX + " " + toffsetY + " " + jpos);
		// 		Debug.Log("Nik Spawn Time TJEW Value : " + trnd + " GD.MT : " + GameData.mapType);
		// 		GameObject tjew = Resources.Load("j" + trnd + "_" + GameData.mapType) as GameObject;//GameObject.Find("j"+trnd);
		// 		if (tjew != null)
		// 		{
		// 			Debug.Log("Nik - Is TJEW GO");
		// 			tjew = Instantiate(tjew,jpos+new Vector3(toffsetX,toffsetY,0),Quaternion.identity) as GameObject;
		// 			tjew.transform.parent = container.transform;
		// 			tjew.name = "jew_"+xx+"_"+yy;
		// 			tjew.transform.position = jpos;
		// 			tjew.transform.Translate(toffsetX ,toffsetY,0);
		// 			tjew.tag = "jew";
		// 		}
		// 		else
		// 		{
		// 			Debug.Log("Nik - Is TJEW Null");
		// 		}
		// 		if(trnd >= 1000)
		// 		{
		// 			tjew.tag = "monster";
		// 			Debug.Log("<color=red><b>Nik - Is A Monster Tag 1 </b></color>");
		// 		}
		// 		mapData[xx][yy] = trnd;
		// 		GameData.getInstance().addJewNo(cCombineResult);
		// 	}
		// 	Nik_BlockLists = new List<Nik_BlockList>();
		// }
	}


	// Nik - Code Pause Panel || 14-04
	public void BtnPause()
    {
        CurrentScorePausePanel.text =  ""+GameData.getInstance ().cScore;
		StopCoroutine("CountDownTime");

		if (time <= 10)
		{
			CountDownTimeOver();
			GameManager.getInstance().stopAllSFX();
		}
    }

    public void BtnResume()
    {
		StartCoroutine("CountDownTime");

		if (time <= 10)
		{
			StartAnim(3);
			CountDownTimeStart();
			GameManager.getInstance().playSfx("Clock-Ticking");
		}
    }

    public void BtnSubmitScore()
    {
		GameData.getInstance().clearGame();
// #if UNITY_EDITOR
        SceneManager.LoadScene(0);
// #elif UNITY_ANDROID || UNITY_IOS
// 		SkillzCrossPlatform.ReportFinalScore((int)GameData.getInstance ().cScore);
// #endif
    }

	 public void BtnTutorial()
    {
		PlayerPrefs.SetInt("pausegame",1);
		ObscuredPrefs.SetInt("firstPlay", 0);

		PlayerPrefs.SetInt("LevelNum", GameData.getInstance().cLevel);

		PlayerPrefs.SetInt("Time",time);
		PlayerPrefs.SetInt("Merge3Score",M3Score);
		PlayerPrefs.SetInt("Merge4Score",M4Score);
		PlayerPrefs.SetInt("Merge5Score",M5Score);
		PlayerPrefs.SetInt("Merge6Score",M6Score);

		PlayerPrefs.SetInt("BombPower",GameData.getInstance().nBomb);
		PlayerPrefs.SetInt("allfitPower",GameData.getInstance().nAllFit);
		PlayerPrefs.SetInt("shovePower",GameData.getInstance().nShove);

		PlayerPrefs.Save();
		SceneManager.LoadScene("Tutorial");
    }

	IEnumerator WaitGameover()
	{
		CongratulationsImage.SetActive(true);
		GameManager.getInstance().playSfx("Congratulations");
		StopCoroutine("CountDownTime");
		CountDownTimeOver();
		yield return new WaitForSeconds(2);
		CongratulationsImage.SetActive(false);
		gameOver();
		ShowAllSCore(0,0);
	}
	// Nik - Code IntrigratedImage || 14-04
	void StartAnim(int Num)
    {
        switch (Num)
		{
			case 1:
				SecondImage.SetActive(true);
				StartCoroutine(EndAnim(Num));
				break;
			case 2:
				TimeUpImage.SetActive(true);
				StartCoroutine(EndAnim(Num));
				break;
			case 3:
				HurryUpImahe.SetActive(true);
				StartCoroutine(EndAnim(Num));
				break;
			case 4:
				GoodjobImage.SetActive(true);
				StartCoroutine(EndAnim(Num));
				GameManager.getInstance().playSfx("Goodjob");
				break;
			case 5:
				GreatImage.SetActive(true);
				StartCoroutine(EndAnim(Num));
				GameManager.getInstance().playSfx("Great");
				break;
			case 6:
				FantasticImage.SetActive(true);
				StartCoroutine(EndAnim(Num));
				GameManager.getInstance().playSfx("Fantastic");
				break;
			
		}
    }
	IEnumerator EndAnim(int Num)
    {
		switch (Num)
		{
			case 1:
				yield return new WaitForSeconds(1);
				SecondImage.SetActive(false);
				break;
			case 2:
				GameManager.getInstance().stopAllSFX();
				HurryUpImahe.SetActive(false);
				yield return new WaitForSeconds(1);
				TimeUpImage.SetActive(false);
				IntrigratedImage.SetActive(false);
				CountDownTimeOver();
				gameOver();
				ShowAllSCore(0,0);
				break;
			case 3:
				yield return new WaitForSeconds(10f);
				HurryUpImahe.SetActive(false);
				break;
			case 4:
				yield return new WaitForSeconds(2);
				GoodjobImage.SetActive(false);
				break;
			case 5:
				yield return new WaitForSeconds(2);
				GreatImage.SetActive(false);
				break;
			case 6:
				yield return new WaitForSeconds(2);
				FantasticImage.SetActive(false);
				break;
			
		}
    }
		// Nik - Code Score || 14-04
	public void ShowAllSCore(int Score, int Num)
	{
		switch (Num)
		{
			case 3:
				M3Score += Score;
				break;
			case 4:
				M4Score += Score;
				break;
			case 5:
				M5Score += Score;
				break;
			case 6:
				M6Score += Score;
				break;
			case 0:
				MERGE3.transform.GetChild(0).GetComponent<Text>().text = M3Score.ToString();
				MERGE4.transform.GetChild(0).GetComponent<Text>().text = M4Score.ToString();
				MERGE5.transform.GetChild(0).GetComponent<Text>().text = M5Score.ToString();
				MERGE6.transform.GetChild(0).GetComponent<Text>().text = M6Score.ToString();
				break;
		}
	}

	// Nik - Code Change || 03-04
	void convertSecondToMinutes()
    {
        string min = Mathf.FloorToInt(time / 60).ToString();
        string sec = Mathf.FloorToInt(time % 60).ToString();
        lb_coin.text = (min.Length < 2 ? "0" + min : min) + ":" + (sec.Length < 2 ? "0" + sec : sec);
        
    }

    IEnumerator CountDownTime()
    {
        while (time > 0)
        {
            yield return new WaitForSeconds(1);
            time--;

			if (time == 10)
			{
				Debug.Log("Nik - Time Is 10 Second In");
				CountDownTimeStart();
				StartAnim(3);
				GameManager.getInstance().playSfx("Clock-Ticking");
			}

			if (time == 60)
			{
				StartAnim(1);
			}

            if (time == 0)
            {
				Debug.Log("<color=blue>Nik - Time'Up by GameOver</color>");
				StartAnim(2);
				// gameOver();
                // time = 0;
				StopCoroutine("CountDownTime");
            }
            convertSecondToMinutes();
        }
    }
	void CountDownTimeStart()
    {
       WarringPanel.SetActive(true);
    }
	void CountDownTimeOver()
    {
       WarringPanel.SetActive(false);
    }
	void initView(){
		//		4.2f/(640/1136) = X/(Screen.currentResolution.width/Screen.currentResolution.height)
		//		float newas = 4.2f / (640f / 1136f) * ((float)Screen.width/(float)Screen.height);;
		
		//		Camera.main.orthographicSize = newas;
		
		float tradio = (float)Screen.width/(float)Screen.height;
		if(tradio < 768f/1050f){
			//			Camera.main.aspect = .58f;
			//			Camera.main.orthographicSize = 4.2f;
		}else{
			//			Camera.main.aspect = .65f;
			//			Camera.main.orthographicSize = 4.2f;
		}

		energybarC = GameObject.Find ("engerybarC");
//		tutorContext = GameObject.Find ("lbHelpContext").GetComponentInChildren<Text> ();
		GameObject mainBG = GameObject.Find("mainbg");
		moneyC = GameObject.Find("moneyC");
		//		Transform topUI = GameObject.Find("topUI").transform;
		//		Transform SceneBG = GameObject.Find("scenebg").transform;
		//		Transform topUIC =  GameObject.Find("HoriUIC").transform;
		//		energybarC.transform.position = new Vector3(mainBG.transform.position.x/Camera.main.orthographicSize-12,energybarC.transform.position.y,0);
		
		bg = GameObject.Find("gridbg");

		groundbg = Resources.Load ("g10bg_" + GameData.mapType) as GameObject;

		groundbg = Instantiate (groundbg, new Vector3 (0, -.5f, 0), Quaternion.identity)as GameObject;
		groundbg.transform.localScale = new Vector3 (12, 13, 1);
		grideage = GameObject.Find("grideage");
		itemSection = GameObject.Find("itemSection");
		allmatch = itemSection.transform.Find("allfit").gameObject;
		//		itemSection.transform.position = new Vector3 (itemSection.transform.position.x, bg.transform.position.y - .8f * 2.9f, 0);
		selected = GameObject.Find("selected");
		//640pixel 8 grid 80 per grid bg / 100pixel per unit
		
		scoreTxtTitle = GameObject.Find("scoreTxtTitle");
		scoreTxt = GameObject.Find("scoreTxt");
		scoreTxtTitle.GetComponentInChildren<Text>().text = "Game Score: ";//LanguageManager.Instance.GetTextValue("GAME_SCORETITLE");
		//		scoreTxtTitle.transform.position = new Vector3 (topUI.position.x  / Camera.main.orthographicSize -.05f, scoreTxtTitle.transform.position.y, 0);
		//		scoreTxt.transform.position = scoreTxtTitle.transform.position + new Vector3 (.35f, 0, 0);
		
		lb_scoreTxt = scoreTxt.GetComponentInChildren<Text> ();
		lb_scoreTxt.text = GameData.getInstance ().cScore.ToString ();
		lb_coin = GameObject.Find ("money").GetComponentInChildren<Text> ();
		
		// Nik - Code Change || 18-04
		if (PlayerPrefs.GetInt("pausegame") == 1)
		{
			time = PlayerPrefs.GetInt("Time");
			if (time <= 10)
			{
				Debug.Log("Nik - Time Is 10 Second In");
				CountDownTimeStart();
				StartAnim(3);
				GameManager.getInstance().playSfx("Clock-Ticking");
			}
			StartCoroutine("CountDownTime");

			M3Score = PlayerPrefs.GetInt("Merge3Score");
			M4Score = PlayerPrefs.GetInt("Merge4Score");
			M5Score = PlayerPrefs.GetInt("Merge5Score");
			M6Score = PlayerPrefs.GetInt("Merge6Score");
		}
		else
		{
			time = 180;
			// StartCoroutine("CountDownTime");
		}
		
		// lb_coin.text = GameData.getInstance ().cCoin.ToString ();
		if (Camera.main.orthographicSize == 4f) {
			gridwidth = 64f / 100f;
			
		} else {
			gridwidth = 64f / 100f;	
		}
		gridheight = 64 / 100f;
		container = GameObject.Find("jcontainer");
		combineAnim = GameObject.Find ("combineanim");
		combineAnim2 = GameObject.Find ("combineanim2");
		explodeAnim = GameObject.Find ("explodeanim");
		activeitemAnim = GameObject.Find ("activeitemanim");
		pickitemAnim = GameObject.Find ("pickitemanim");

		//		readySection = GameObject.Find("readySection");
		//		float tStartY = readySection.transform.position.y;
		//		readySection.transform.position = Camera.main.ScreenToWorldPoint (Vector3.zero+new Vector3(Screen.width,0,0));
		//		readySection.transform.position = new Vector3 (readySection.transform.position.x, tStartY, 0);	
		
		
		ready0 = GameObject.Find("ready0");
		ready1 = GameObject.Find("ready1");
		ready2 = GameObject.Find("ready2");
		preview = GameObject.Find("preview");
		
		
		
		
		lbcScoreg0 = GameObject.Find("lbcScore0");
		lbcScore0 = lbcScoreg0.GetComponent<Text> ();
		lbcScoreg1 = GameObject.Find("lbcScore1");//energy score
		lbcScore1 = lbcScoreg1.GetComponent<Text> ();
		lbcScoreg2 = GameObject.Find("lbcScore2");
		lbcScore2 = lbcScoreg2.GetComponent<Text> ();
		moneyC = GameObject.Find("moneyC");
//		engerybar = GameObject.Find ("engerybar").GetComponent<dfProgressBar> ();
		//		engerybar.Value = ObscuredPrefs.GetFloat ("energy", 0f);

		Debug.Log("Dev - Power Code remove");
		redplus1 = GameObject.Find("redplus1");
		redplus2 = GameObject.Find("redplus2");
		redplus3 = GameObject.Find("redplus3");
		// Nik - Code Change || 18-04
		if (PlayerPrefs.GetInt("pausegame") == 0)
		{
			// Nik - Code Change || 07-04
			GameData.getInstance().nShove++;
			GameData.getInstance().nAllFit++;
			GameData.getInstance().nBomb++;
		}
		else
		{
			GameData.getInstance().nShove = PlayerPrefs.GetInt("shovePower");
			GameData.getInstance().nAllFit = PlayerPrefs.GetInt("allfitPower");
			GameData.getInstance().nBomb = PlayerPrefs.GetInt("BombPower");

			ShovepowerCount = PlayerPrefs.GetInt("shovePower");
			BombpowerCount = PlayerPrefs.GetInt("BombPower");
			AllFitpowerCount = PlayerPrefs.GetInt("allfitPower");
			
		}

		
		
		if (GameData.getInstance ().nAllFit != 0) {
			
			redplus1.SetActive(false);		
		}else
		{
			redplus1.SetActive(false);
			redplus1.transform.parent.transform.GetChild(1).gameObject.SetActive(false);
			redplus1.transform.parent.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 100);
		}
		if (GameData.getInstance ().nBomb != 0) {
			redplus2.SetActive(false);		
		}else
		{
			redplus2.SetActive(false);
			redplus2.transform.parent.transform.GetChild(1).gameObject.SetActive(false);
			redplus2.transform.parent.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 100);
		}
		if (GameData.getInstance ().nShove != 0) {
			redplus3.SetActive(false);		
		}else
		{
			redplus3.SetActive(false);
			redplus3.transform.parent.transform.GetChild(1).gameObject.SetActive(false);
			redplus3.transform.parent.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 100);
		}
		
//		buyItemPanel = GameObject.Find ("PanelBuyItem").GetComponent<dfSlicedSprite> ();
		buyItemPanel.SetActive(false);// = false;
		buyItemContainer = buyItemPanel.transform.Find ("buyItemContainer").gameObject;
		panelBuyItemS = buyItemPanel.GetComponent<PanelBuyItem> ();
		//		scoreTxtTitle.transform.Translate (-1, 0, 0); //buyItemPanel.transform.position + new Vector3 (-(float)Camera.main.pixelWidth/4f, scoreTxtTitle.transform.position.y, 0);
		
//		panelScoreBoard = GameObject.Find ("PanelScoreBoardC").GetComponent<dfPanel> ();
		//		panelScoreBoard.IsVisible = false;
		
		detailC = GameObject.Find ("detailContainer");
		
		splashfx = GameObject.Find("fx_light");
		
		//load
		cJewNo = ObscuredPrefs.GetInt ("cjew", 0);
		cPreviewNo = ObscuredPrefs.GetInt ("cpreview", 0);
		
		ObscuredInt tready1 = ObscuredPrefs.GetInt ("ready1", 0);
		ObscuredInt tready2 = ObscuredPrefs.GetInt ("ready2", 0);

		if (GameData.isTutorial) {
			cPreviewNo = 2;		
			cJewNo = 1;
			tready1 = 0;
			tready2 = 0;
		} 

		if (tready1 != 0) 
		{
			readyJew1 = Resources.Load("j" + tready1 + "_" + GameData.mapType) as GameObject;
			readyJew1 = Instantiate (readyJew1, ready1.transform.position,Quaternion.identity) as GameObject;		
			ready1No = tready1;
			readyJew1.transform.localScale = new Vector3(.35f,.35f,.35f);
			Debug.Log("<color=black><b>Nik - Spawn Is The Block</b></color>");
		}
		if (tready2 != 0) 
		{
			readyJew2 = Resources.Load("j" + tready2 + "_" + GameData.mapType) as GameObject;
			readyJew2 = Instantiate (readyJew2, ready2.transform.position,Quaternion.identity) as GameObject;		
			ready2No = tready2;
			readyJew2.transform.localScale = new Vector3(.35f,.35f,.35f);
			Debug.Log("<color=black><b>Nik - Spawn Is The Block</b></color>");
		}
		
		if (cJewNo == 0) {
			ClearCreateNew ();
		} else {
			
			// readyJew0 = Resources.Load("j" + cJewNo + "_" + GameData.mapType) as GameObject;
			// readyJew0 = Instantiate (readyJew0, ready0.transform.position,Quaternion.identity) as GameObject;

			// previewJew = Resources.Load("j" + cPreviewNo + "_" + GameData.mapType) as GameObject;
			// previewJew = Instantiate (previewJew, preview.transform.position,Quaternion.identity) as GameObject;	
			// previewJew.transform.localScale = new Vector3(.2f,.2f,.2f);
			Debug.Log("<color=black><b>Nik - Spawn Is The Block Start j not Spawn First 2 line</b></color>");
		}
		
		//init board
		for (int i = 0; i<25; i++) 
		{
			int trow = Mathf.FloorToInt(i/5);
			int tcol = i % 5;
			int tno = mapData[trow][tcol];
			// Debug.Log("Nik - Spawn jew_ in 6");
			if(tno != 0)
			{
				cameraOffstY = bg.transform.position.y;
				float toffsetX = (gridwidth - .64f)/2+.64f/2;
				float toffsetY = (gridheight - .64f)/2+.64f/2;
				// Nik - Code Change || 06-04
				Vector3 jpos = new Vector3((trow-6)*gridwidth+gridwidth*3.48f,-gridheight*tcol+gridheight*2.57f+cameraOffstY,0);
				// Debug.Log("Nik - Spawn jew_ in 6 - a");
				GameObject tjew = Resources.Load("j" + tno + "_" + GameData.mapType) as GameObject;
				tjew = Instantiate(tjew,jpos+new Vector3(toffsetX,toffsetY,0),Quaternion.identity) as GameObject;
				tjew.name = "jew_"+trow+"_"+tcol;
				tjew.tag = "jew";
				tjew.transform.parent = container.transform;
				if(tno >= 1000)
				{
					tjew.tag = "monster";
					Debug.Log("<color=red><b>Nik - Is A Monster Tag 1 </b></color>");
					MonsterAction ma = tjew.GetComponentInChildren<MonsterAction>();
					ma.touchGame = this;
					ma.xx = trow;ma.yy = tcol;
				}
				mapData[trow][tcol] = tno;
			}
		}
		
		
		//if nothing,create some random
		GameObject[] cnjews = GameObject.FindGameObjectsWithTag("jew");
		GameObject[] cnmonsters = GameObject.FindGameObjectsWithTag("monster");
		if(cnjews.Length+cnmonsters.Length == 0)
			createJews();
		
		initGrid ();
		refreshEage ();

		//other
		finger = GameObject.Find("hand");
//		spHelp = GameObject.Find ("helpFrame");
		if (GameData.isTutorial) {
			// Nik - Code Change || 07-04
			// finger.transform.position = setPos(finger,1,5,true);		
			finger.transform.position = setPos(finger,1,4,true);
			spHelp.SetActive (true);
		}

		setTutorText ();
		
	}
	void initGrid(){
		// Nik - Code Change || 06-04
		for (int i = 0; i<25; i++) {
			int trow = Mathf.FloorToInt (i / 5);
			int tcol = i % 5;
			//			int tno = mapData[trow][tcol];
			
			if(mapData[trow][tcol] > 0 && mapData[trow][tcol] < 1000){
				createAnGrid(trow,tcol);
			}
		}
	}
	
	void createAnGrid(int trow,int tcol){
		
		//		GameObject tcgrid = GameObject.Find ("grid_" + trow + "_" + tcol);
		//		if(tcgrid == null){
		float toffsetX = (gridwidth - .64f)/2+.64f/2;
		float toffsetY = (gridheight - .64f)/2+.64f/2;
		// Nik - Code Change || 06-04
		Vector3 jpos = new Vector3((trow-6)*gridwidth+gridwidth*2f,-gridheight*tcol+gridheight*3.5f+cameraOffstY,0);
		// Vector3 jpos = new Vector3((trow-6)*gridwidth+gridwidth*3,-gridheight*tcol+gridheight*3+cameraOffstY,0);

		GameObject tgrid = Resources.Load("g"+10+"_"+GameData.mapType) as GameObject;
		tgrid = Instantiate(tgrid,jpos+new Vector3(toffsetX,toffsetY,0),Quaternion.identity) as GameObject;
		tgrid.name = "grid_"+trow+"_"+tcol;
		tgrid.tag = "grid";
		//		}
	}
	
	void removeCGrid(int cx,int cy)
	{
		GameObject tGrid = GameObject.Find ("grid_" + cx + "_" + cy);
		Destroy (tGrid);
	}
	
	void createAnEage(int type,int cx,int cy,int rotation){
		// Nik - Code Change || 06-04
		// Vector3 jpos = new Vector3((cx-6)*gridwidth+gridwidth*3.48f,-gridheight*cy+gridheight*2.57f+cameraOffstY,0);
		// Nik - Code Change || 11-04
		Vector3 jpos = new Vector3((cx-6)*gridwidth+gridwidth*3.48f,-gridheight*cy+gridheight*2.57f+cameraOffstY,0);
		// Vector3 jpos = new Vector3((cx-6)*gridwidth+gridwidth*3,-gridheight*cy+gridheight*3+cameraOffstY,0);
		GameObject teage = Resources.Load("g"+type+"_"+GameData.mapType) as GameObject;

		float toffsetX = (gridwidth - .64f)/2+.64f/2;
		float toffsetY = (gridheight - .64f)/2+.64f/2;

		teage = Instantiate(teage, jpos+new Vector3(toffsetX,toffsetY,0), Quaternion.identity) as GameObject;
		teage.transform.localEulerAngles = new Vector3(0, 0, rotation);
		teage.transform.parent = grideage.transform;
	}
	
	void refreshEage(){
		foreach (Transform t in grideage.transform) {
			Destroy(t.gameObject);		
		}
		// Nik - Code Change || 06-04
		for (int i = 0; i<25; i++) {
			int trow = Mathf.FloorToInt (i / 5);
			int tcol = i % 5;
			int tno = mapData [trow] [tcol];
			if(tno == 0 || tno > 1000){//check each empth block and enemy block
				int[] teages = {0,0,0,0};
				//if empty or is enemy(mean empty)
				if(tcol > 0 && mapData[trow][tcol - 1] != 0 &&  mapData[trow][tcol - 1] < 1000){//up
					teages[0] = 1;
				}else if(tcol == 0){
					teages[0] = 1;
				}
				// Nik - Code Change || 08-04
				// if(tcol < 5 && mapData[trow][tcol + 1] != 0 && mapData[trow][tcol + 1] < 1000){//down
				if(tcol < 4 && mapData[trow][tcol + 1] != 0 && mapData[trow][tcol + 1] < 1000){//down
					teages[1] = 1;
				}else if(tcol == 5){
					teages[1] = 1;
				}
				if(trow > 0 && mapData[trow - 1][tcol] != 0 && mapData[trow - 1][tcol] < 1000){//left
					teages[2] = 1;
				}else if(trow == 0){
					teages[2] = 1;
				}
				// Nik - Code Change || 08-04
				// if(trow < 5 && mapData[trow+1][tcol] != 0 && mapData[trow+1][tcol] < 1000){//right
				if(trow < 4 && mapData[trow+1][tcol] != 0 && mapData[trow+1][tcol] < 1000){//right
					teages[3] = 1;
				}else if(trow == 5){
					teages[3] = 1;
				}
				
				
				
				//fix eages
				
				string teagetype = teages[0].ToString() + teages[1].ToString()+teages[2].ToString()+teages[3].ToString();
				switch(teagetype){
				case "0000":
					
					break;
				case "1000":
					createAnEage(13,trow,tcol,-90);
					break;
				case "0100":
					createAnEage(16,trow,tcol,-90);
					break;
				case "0010":
					createAnEage(13,trow,tcol,0);
					break;
				case "0001":
					createAnEage(13,trow,tcol,180);
					break;
				case "1100":
					createAnEage(14,trow,tcol,0);
					break;
				case "1010":
					createAnEage(7,trow,tcol,0);
					break;
				case "1001":
					createAnEage(2,trow,tcol,0);
					break;
				case "0110":
					createAnEage(5,trow,tcol,0);
					break;
				case "0101":
					createAnEage(2,trow,tcol,-90);
					break;
				case "0011":
					createAnEage(11,trow,tcol,0);
					break;
				case "0111":
					createAnEage(6,trow,tcol,90);
					break;
				case "1101":
					createAnEage(1,trow,tcol,0);
					break;
				case "1110":
					createAnEage(6,trow,tcol,0);
					break;
				case "1011":
					createAnEage(6,trow,tcol,-90);
					break;
				case "1111":
					createAnEage(3,trow,tcol,0);
					break;
				}
				
				
				//fix corners
				//left up coner
				if((teages[0] == 0 && teages[0] < 1000) && (teages[2] == 0 && teages[2] < 1000)){
					if(trow > 0 && tcol > 0 && mapData[trow-1][tcol-1] != 0 && mapData[trow-1][tcol-1] < 1000){
						createAnEage(111,trow,tcol,0);
					}
				}
				
				//right up coner
				if((teages[0] == 0 && teages[0] < 1000) && (teages[3] == 0 && teages[3] < 1000)){
					// Nik - Code Change || 08-04
					// if(trow < 5 && tcol > 0 && mapData[trow+1][tcol-1] != 0 && mapData[trow+1][tcol-1] < 1000){
					if(trow < 4 && tcol > 0 && mapData[trow+1][tcol-1] != 0 && mapData[trow+1][tcol-1] < 1000){
						createAnEage(112,trow,tcol,0);
					}
				}
				
				//left down coner
				if((teages[1] == 0 && teages[1] < 1000) && (teages[2] == 0 && teages[2] < 1000)){
					// Nik - Code Change || 08-04
					// if(trow > 0 && tcol < 5 && mapData[trow-1][tcol+1] != 0 && mapData[trow-1][tcol+1] < 1000){
					if(trow > 0 && tcol < 4 && mapData[trow-1][tcol+1] != 0 && mapData[trow-1][tcol+1] < 1000){
						createAnEage(111,trow,tcol,90);
					}
				}
				
				
				//right down  coner
				if((teages[1] == 0 && teages[1] < 1000) && (teages[3] == 0 && teages[3] < 1000)){
					// Nik - Code Change || 08-04
					if(trow < 4 && tcol < 4 && mapData[trow+1][tcol+1] != 0 && mapData[trow+1][tcol+1] < 1000){
					// if(trow < 5 && tcol < 5 && mapData[trow+1][tcol+1] != 0 && mapData[trow+1][tcol+1] < 1000){
						createAnEage(112,trow,tcol,-90);
					}
				}
				
			}
		}
	}
	
	void saveReadyJew(){
		if (GameData.isTutorial)
						return;
		ObscuredPrefs.SetInt ("cpreview", cPreviewNo);
		ObscuredPrefs.SetInt ("cjew", cJewNo);
		ObscuredPrefs.SetInt ("ready1", ready1No);
		ObscuredPrefs.SetInt ("ready2", ready2No);
		// Debug.Log("<color> Nik - Dame Data Save ReadyJew</color>");
	}
	
	public void saveAllJew(){
		if (GameData.isTutorial)
			return;
		saveReadyJew ();
		saveOthers ();
		string jewMap = "";
		// Nik - Code Change || 06-04
		for (int i = 0; i<25; i++) {
			int trow = Mathf.FloorToInt(i/5);
			int tcol = i % 5;
			// Debug.Log("Nik mapData Is : " + jewMap + " Row " + trow + " Col " + tcol);
			jewMap += mapData[trow][tcol]+"_";		
		}
		ObscuredPrefs.SetString ("jewMap", jewMap);
		// Debug.Log("<color> Nik - Dame Data Save AllJew</color>");
		
	}
	
	//energy,score,coin
	void saveOthers(){
		if (GameData.isTutorial)
			return;
		//		ObscuredPrefs.SetFloat ("energy", engerybar.Value);
		ObscuredPrefs.SetString ("score", GameData.getInstance().cScore.ToString());
		ObscuredPrefs.SetString ("coin", GameData.getInstance().cCoin.ToString());
		// Debug.Log("<color> Nik - Dame Data Save Others</color>");
		
	}

	
	void createJews(){
		List<int> allEmptys = new List<int> ();
		allEmptys = new List<int> ();
		for (int i = 0; i< 25; i++) {
			allEmptys.Add(i);		
		}
		allEmptys = Util.ListRandomInt (allEmptys);
		for (int  i = 0; i<5; i++) {
			int tjewNo = (allEmptys[i]);	
			int xx = tjewNo % 5;
			int yy = Mathf.FloorToInt(tjewNo/5);

			Debug.Log("Nik - Block Spawn Time XX : " + xx + " YY : " + yy);
			cameraOffstY = bg.transform.position.y;
			
			float toffsetX = (gridwidth - .64f)/2+ .64f/2;
			float toffsetY = (gridheight - .64f)/2+ .64f/2;
			// Nik - Code Change || 06-04
			Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3.48f,-gridheight*yy+gridheight*2.57f+cameraOffstY,0);
			
// #if UNITY_EDITOR
        	int trnd = (int)Random.Range(1,4);
// #elif UNITY_ANDROID || UNITY_IOS
// 			int trnd = (int)SkillzCrossPlatform.Random.Range(1,4);
// #endif
			
			
			
			Debug.Log("Nik - Spawn jew_ in 7 ");// + toffsetX + " " + toffsetY + " " + jpos);
			Debug.Log("Nik Spawn Time TJEW Value : " + trnd + " GD.MT : " + GameData.mapType);
			GameObject tjew = Resources.Load("j" + trnd + "_" + GameData.mapType) as GameObject;//GameObject.Find("j"+trnd);
			tjew = Instantiate(tjew,jpos+new Vector3(toffsetX,toffsetY,0),Quaternion.identity) as GameObject;
			tjew.name = "jew_"+xx+"_"+yy;
			tjew.tag = "jew";
			if(trnd >= 1000){
				tjew.tag = "monster";
				Debug.Log("<color=red><b>Nik - Is A Monster Tag 1 </b></color>");
			}
			mapData[xx][yy] = trnd;
		}
		if (PlayerPrefs.GetInt("pausegame") == 0)
		{
			saveAllJew ();
		}
		
	}
	
	public List<List<int>> mapData = new List<List<int>>();
	int cJewNo = 0,cPreviewNo = 0,ready1No = 0,ready2No = 0;
	//	Tweener readyTween1,readyTween2;
	public void generateNew(){
		
// 		if (readyJew0 == null) {
			
// 			while(cPreviewNo <= 0){
// 				cPreviewNo = getRnd();
// 			}
			
			
// 			GameObject tJewrey;//
// 			// Debug.Log("Nik - Blcok 2 - cPreviewNo : " + cPreviewNo);

// 			// tJewrey = Resources.Load("j" + cPreviewNo + "_" + GameData.mapType) as GameObject;
// //			print ("j" + cPreviewNo + "_" + GameData.mapType+"xxx");
// 			// tJewrey = Instantiate (tJewrey, preview.transform.position, Quaternion.identity) as GameObject;
// 			// tJewrey = Instantiate (tJewrey, ready0.transform.position, Quaternion.identity) as GameObject;
// 			// readyJew0 = tJewrey;
// 			// cJewNo = cPreviewNo;
// 			// readyJew0.transform.localScale = new Vector3(.35f,.35f,1);
			
// 			//				try{
// 			//				if(readyJew0 == null)return;
// 			//				if(ready0 == null)return;
			
// 			//				HOTween.To (readyJew0.transform, .2f, new TweenParms ().Prop ("position",ready0.transform.position,false).Prop ("localScale", new Vector3(1f,1f,1)));//.OnComplete(textOver,lbcScoreg));
// 			// readyJew0.transform.DOMove(ready0.transform.position,.2f);
// 			// readyJew0.transform.DOScale(new Vector3(.35f,.35f,1), .2f); //original scale
// 			//				readyTween2 = HOTween.To (readyJew0.transform, .2f, new TweenParms ().Prop ("localScale", new Vector3(1f,1f,1), false));
// 			//				}catch(UnityException e){
// 			//					
// 			//					readyJew0.transform.position = ready0.transform.position;
// 			//					readyJew0.transform.localScale = new Vector3(1,1,1);
// 			//				}
			
// 			// now create a preveiew
			
// 			ObscuredInt trnd  = getRnd();
// 			//				readyJew0.GetComponent<SpriteRenderer> ().enabled = true;
// 			//				
// 			Debug.Log("Nik - Blcok 2 - cPreviewNo : " + trnd);
// 			tJewrey = Resources.Load("j" + cPreviewNo + "_" + GameData.mapType) as GameObject;
			
			
			
// 			Debug.Log("Nik Code Change Add");
// 			// previewJew = Instantiate (tJewrey, preview.transform.position, Quaternion.identity) as GameObject;
// 			// tJewrey = Instantiate (tJewrey, ready0.transform.position, Quaternion.identity) as GameObject;
// 			// previewJew.transform.localScale = new Vector3(.2f,.2f,1);
// 			// cPreviewNo = trnd;
// 			// readyJew0.SetActive(false);
// 			// StartCoroutine("showPreview",readyJew0);

// 			// tJewrey = Resources.Load("j" + cPreviewNo + "_" + GameData.mapType) as GameObject;
// 			print ("j" + cPreviewNo + "_" + GameData.mapType+"xxx");
// 			// tJewrey = Instantiate (tJewrey, preview.transform.position, Quaternion.identity) as GameObject;
// 			tJewrey = Instantiate (tJewrey, ready0.transform.position, Quaternion.identity) as GameObject;
// 			readyJew0 = tJewrey;
// 			cPreviewNo = trnd;
// 			cJewNo = cPreviewNo;
// 			readyJew0.transform.localScale = new Vector3(.2f,.2f,1);
// 			// Debug.Log("<color=black><b>Nik - Spawn Is The Block 3 </b></color>");
// 			// cJew = null;	
			
			
// 		}
		//		}
		
		
	}
	
	ObscuredInt getRnd(){
		Debug.Log("Nik - Block Random Number Generate");

// #if UNITY_EDITOR
        int trnd = (int)Random.Range (1, 100);
// #elif UNITY_ANDROID || UNITY_IOS
// 		int trnd = (int)SkillzCrossPlatform.Random.Range (1, 100);
// #endif
		
		//		while (previewJew == null) {
		// Nik - Code Change || 13-04
		// if(trnd < 30){
		if(trnd < 0){
			trnd = 1;
		}else if(trnd < 50){
			trnd = 2;
		}else if(trnd < 60){
			GameObject[] nmonsters = GameObject.FindGameObjectsWithTag("monster");
			trnd = 2; // random monsters
			for(int i = 0;i< nmonsters.Length;i++){
// #if UNITY_EDITOR
        		int trnd2 = (int)Random.Range(0,3);
// #elif UNITY_ANDROID || UNITY_IOS
// 				int trnd2 = (int)SkillzCrossPlatform.Random.Range(0,3);
// #endif
				
				if(trnd2 == 0){
					trnd = 1;
					break;
				}
			}

		}else if(trnd < 63){
// #if UNITY_EDITOR
        	trnd = (int)Random.Range(3,6);
// #elif UNITY_ANDROID || UNITY_IOS
// 			trnd = (int)SkillzCrossPlatform.Random.Range(3,6);
// #endif
			
		}else if(trnd < 64){
// #if UNITY_EDITOR
      		trnd = (int)Random.Range(1,4);//random special items
// #elif UNITY_ANDROID || UNITY_IOS
// 			trnd = (int)SkillzCrossPlatform.Random.Range(1,4);//random special items
// #endif
			
		}else {
			trnd = 1;
		}
	

//		trnd = (int)Random.Range(3,6);

//	trnd = 7;//test house

//		int trnd2 = (int)Random.Range (1, 100);	
//		if (trnd2 < 30) {
//			trnd = 1001;		
//		}

		if (GameData.isTutorial) {
			if(tutorStep < 10){
				trnd = 2;
			}
		}

//		if(isCrazy > 0){
//			trnd =  (int)Random.Range(2,5);
//		}
		//			trnd = 12;//temp
		// trnd = 5;

		return trnd;
	}
	
	void setTutorText(){
		switch (tutorStep) {
		case 1:
			if(lang == "TIP_CN"){
				tutorContext.text = "";
			}else if(lang == "en"){
				tutorContext.text = "TO UNLOCK THE DIAMOND YOU NEED A SCORE OF 40000 !";
				// tutorContext.text = "WELCOME TO THE\n9 DIAMONDS CAVE !";
			}
			break;
		// case 2:
		// 	if(lang == "TIP_CN"){
		// 		tutorContext.text = "";
		// 	}else if(lang == "en"){
		// 		tutorContext.text = "IN THIS CAVE YOU MUST PUT SIMILAR NUMBERS TOGETHER !";
		// 	}
		// 	break;
		// case 3:
		// 	if(lang == "TIP_CN"){
		// 		tutorContext.text = "";
		// 	}else if(lang == "en"){
		// 		tutorContext.text = "AND GET TO THE NEW NUMBERS !";
		// 	}
		// 	break;
		// case 4:
		// 	if(lang == "TIP_CN"){
		// 		tutorContext.text = "";
		// 	}else if(lang == "en"){
		// 		tutorContext.text = "BY EARNIN MORE SCORES\nYOU CAN FIND AND UNLOCK\nTHE 9 HIDDEN DIAMONDS !";
		// 	}
		// 	break;
		// case 5:
		// 	if(lang == "TIP_CN"){
		// 		tutorContext.text = "";
		// 	}else if(lang == "en"){
		// 		tutorContext.text = "NOW TRY ON THE NEXT NUMBER ...";
		// 	}

		// 	break;
		// case 6:
		// 	if(lang == "TIP_CN"){
		// 		tutorContext.text = "";
		// 	}else if(lang == "en"){
		// 		tutorContext.text = "NOW TRY ON THE NEXT NUMBER ...";
		// 	}
		// 	break;
		// case 7:
		// 	if(lang == "TIP_CN"){
		// 		tutorContext.text = "";
		// 	}else if(lang == "en"){
		// 		tutorContext.text = "NOW TRY ON THE NEXT NUMBER ...";
		// 	}
		// 	break;
		// case 8:
		// 	if(lang == "TIP_CN"){
		// 		tutorContext.text = "";
		// 	}else if(lang == "en"){
		// 		tutorContext.text = "NOW TRY ON THE NEXT NUMBER ...";
		// 	}
		// 	break;
		// case 9:
		// 	if(lang == "TIP_CN"){
		// 		tutorContext.text = "";
		// 	}else if(lang == "en"){
		// 		tutorContext.text = "THAT'S ALL ! GOOD LUCK !";
		// 	}
		// 	break;
		}
	}


	void showScore(string tscore,Vector3 pos,int type = 0)
	{
		
		if(type < 2){
			int tscore1 = int.Parse (tscore);
			if (tscore1 > 3) {
				tscore1 = cCombineResult * tscore1;
			}
		}
		GameObject lbcScoreg = null;
		if (type == 0) {
			//is score,not message
			lbcScore0.text = "+" + tscore;
			lbcScoreg = lbcScoreg0;
			GameData.getInstance().cScore += int.Parse(tscore);
		} else if (type == 1) {
			//engery
			lbcScore1.text = "+" + tscore;	
			lbcScoreg = lbcScoreg1;
		} else if (type == 2) {
			//message
			lbcScore2.text = tscore;	
			lbcScoreg = lbcScoreg2;
		}
		Transform tparent = lbcScoreg.transform.parent;
		lbcScoreg = Instantiate (lbcScoreg, pos, Quaternion.identity) as GameObject;
		lbcScoreg.transform.parent = tparent;
		lbcScoreg.transform.localScale = new Vector3 (lbcScoreg.transform.localScale.x / 70, lbcScoreg.transform.localScale.y / 70, lbcScoreg.transform.localScale.z);
		//		HOTween.To (lbcScoreg.transform, 1.2f, new TweenParms ().Prop ("position", new Vector3 (0, .1f, 0), true));//.OnComplete(textOver,lbcScoreg));
		lbcScoreg.transform.DOMove (new Vector3 (lbcScoreg.transform.position.x, lbcScoreg.transform.position.y+.1f, 0), 1.2f);
		lb_scoreTxt.text = GameData.getInstance ().cScore.ToString ();
		Destroy (lbcScoreg, 1.3f);

		if((int)GameData.getInstance ().cScore > 40000)
		{
			Debug.Log("<color=green><b>Nik Score GameOver</b></color>");
			//board is full
			StartCoroutine(WaitGameover());
		}
	}
	
	IEnumerator showPreview(GameObject tjew){
		yield return new WaitForSeconds (.2f);
		if (tjew != null) {
			tjew.SetActive(true);	
		}
	}
	
	void generateSpecial(int itemNo){
		GameObject tjew = Resources.Load ("j" + itemNo + "_" + GameData.mapType) as GameObject;
		tjew = Instantiate (tjew, ready0.transform.position, Quaternion.identity) as GameObject;
		Destroy (readyJew0);
		
		
		readyJew0 = tjew;
		cJewNo = itemNo;
		
		GameObject tExplodeAnim = Instantiate (activeitemAnim, ready0.transform.position, Quaternion.identity) as GameObject;
		tExplodeAnim.name = "texplode";
		Destroy (tExplodeAnim, 1);
		syncCJew ();
	}
	
	void checkMonster(){
		GameObject[] allMonster = GameObject.FindGameObjectsWithTag("monster");
		foreach (GameObject tmonster in allMonster) {
			if(tmonster != null && tmonster.tag == "monster"){//after each check ,the tag maybe removed
				string[] tname = tmonster.name.Split ("_"[0]);
				int tx =int.Parse(tname [1]);
				int ty = int.Parse(tname [2]);
				checkCombine(tx,ty,mapData[tx][ty],true); 
				//combineJews(tmonster);
				GameObject newTomb = null;
				ObscuredInt cMonsterNo = 0;
				//turn all to tomb,not combine it,after all turned,check the first tomb to combine
					foreach(GameObject tm in combineObjects){//maybe < 3
						if(tm != null && tm.tag != "Untagged"){
							tname = tm.name.Split ("_"[0]);
							tx =int.Parse(tname [1]);
							ty = int.Parse(tname [2]);

							//ingore something may have been changed into not a monster(unkonwn,not well code)
							if(mapData[tx][ty] < 1000)continue;
							cMonsterNo = mapData[tx][ty] ;
							
							GameObject ttomb = Resources.Load ("j" + 200 + "_" + GameData.mapType) as GameObject;
							ttomb = Instantiate (ttomb, tm.transform.position, Quaternion.identity) as GameObject;
							ttomb.transform.position = setPos(ttomb,tx,ty,true);//correction postion while in high speed;
							ttomb.transform.parent = container.transform;
							ttomb.name = tm.name;
							ttomb.tag = "jew";
							
							if(newTomb == null){
								newTomb = ttomb;
							}

						
							mapData[tx][ty] = 200;
							
							createAnGrid(tx,ty);
							
							
							DestroyImmediate(tm);
							
							//add tomb
							GameData.getInstance().addJewNo(200);
							
						}
					}
					//add kill
					if(cMonsterNo >= 1000){
						GameData.getInstance().addKill(cMonsterNo,combineObjects.Count);
					}
					combineObjects.Clear ();
					cCombineResult = 0;



				//now check new tomb combine
				
				
				if(newTomb!= null){
					tname = newTomb.name.Split ("_"[0]);
					tx =int.Parse(tname [1]);
					ty = int.Parse(tname [2]);
					checkCombine(tx,ty,mapData[tx][ty],true);
					
					if(combineObjects.Count >= 3){
						
						//only create result,the old is remove automatically
						GameObject tresultjew = Resources.Load ("j" + cCombineResult + "_" + GameData.mapType) as GameObject;
						tresultjew = Instantiate (tresultjew, newTomb.transform.position, Quaternion.identity) as GameObject;
						tresultjew.transform.position = setPos(tresultjew,tx,ty,true);//correction postion while in high speed;
						tresultjew.transform.parent = container.transform;
						
						tresultjew.tag = "jew";
						
						mapData[tx][ty] = cCombineResult; 
						
						//add tomb result
						GameData.getInstance().addJewNo(cCombineResult);

						
						combineJews(newTomb);
						tresultjew.name = newTomb.name;
						Jew jewc = tresultjew.GetComponent<Jew>();
						if(jewc)jewc.delayShow();
						createAnGrid(tx,ty);
						
						
						combineObjects.Clear ();
						cCombineResult = 0;
						//axis correction
						Vector3 tpos = new Vector3(newTomb.transform.position.x/Camera.main.orthographicSize - .1f,newTomb.transform.position.y/Camera.main.orthographicSize,0);
						showScore(combineScore.ToString(),tpos);
					}
//					
//					
				}
				
				//now check new tomb end
				
			}//for each !null

		}//for each
		cJew = null;
		cBlock = "";
		refreshEage();
		stopZooms();
	}

	
	Dictionary<string,int> checkedDick;
	
	public List<GameObject>combineObjects;
	public List<GameObject>Nik_combineObjects;
	public List<Nik_BlockList>Nik_BlockLists;
	ObscuredInt cCombineResult = 0;
	ObscuredInt combineScore = 0;
	int[] cCombinedJews;
	public List<GameObject> tCombineObjects;//just record the same,if <3 clear
	public GameObject orijew;
	
	void checkCombine(int xx,int yy,int isSpecial = -1,bool isAutoCheck = false){
		ObscuredInt cSpecify = isSpecial;
		combineScore = 0;
		ObscuredInt ori = 0;
		if (cSpecify == -1) {
			ori	= mapData [xx][yy]; //cJewNo;//mapData [xx] [yy];
		} else {
			ori = cSpecify;

		}
		// Debug.Log("<color=red>checkCombine It's Enter x And Y Value :  </color>" + xx + " : " + yy + " 3 " + isSpecial + " bool " + isAutoCheck + " ori " + ori);
		checkedDick = new Dictionary<string,int > ();
		cCombinedJews = new int[2000];
		
		wait4check = new List<int[]> ();
		// stopZooms ();
		//need use for stop zoom.so clear it after
		combineObjects.Clear ();
		combineObjects = new List<GameObject>();
		Nik_combineObjects = new List<GameObject> ();
		Nik_BlockLists = new List<Nik_BlockList>();
		// List<GameObject> tCombineObjects;//just record the same,if <3 clear
		
		orijew = GameObject.Find ("jew_" + xx + "_" + yy);

		bool isAllFit = false;
		if (ori == 101) {
			ori = 1;
			isAllFit = true;
		}
		ObscuredInt max;//

		if (ori <= maxN) {
			max = maxN;	

		}else{
			max = ori+5;
			if(max < 1000){
				max = 202;
			}
		}
		bool alreadyhas1Combine = false;
		while (ori < max){//12 can't combine
			tCombineObjects = new List<GameObject> ();
			
			check4 (xx, yy, ori);
			bool canCombine = false;
			while (wait4check!=null && wait4check.Count > 0) {
				canCombine = true;
				int txx = wait4check [0] [0];
				int tyy = wait4check [0] [1];

				int tPosType = mapData [txx] [tyy];
				check4 (txx, tyy, tPosType);

				wait4check.RemoveAt (0);
				
				GameObject tjew = GameObject.Find ("jew_" + txx + "_" + tyy);
				tCombineObjects.Add (tjew);
			}
			
			if (canCombine || cSpecify != -1) {
				tCombineObjects.Add (orijew);
			}

			int minCombineReport = 3;
			if(cSpecify != -1)minCombineReport = 1;
			//if is check monster (1 or 2 numbers) dead combination also should be reported
			if (tCombineObjects.Count >= minCombineReport) 
			{
				foreach(GameObject tjew in tCombineObjects){
					if(tjew!=null)
					{
						string[] tname = tjew.name.Split ("_"[0]);
						int tjewPosX =int.Parse(tname [1]);
						int tjewPosY = int.Parse(tname [2]);
						bool isdead = checkDead(tjewPosX,tjewPosY,mapData[tjewPosX][tjewPosY]);
						// Debug.Log("<color> Nik - New Value is : </color> X : " + tjewPosX + " || Y : " + tjewPosY + " isdead : " + isdead);
						if(isdead){
							if(alreadyhas1Combine && tCombineObjects.Count <= 3){		}
							else
							{
								combineObjects.Add(tjew);
							}
							cCombinedJews[ori]++; 
						}else{
							combineObjects = new List<GameObject>();
							//if another check if after this ,the clear is useless.So must break for
							return;
						}
					}
				}
				
				Nik_combineObjects = combineObjects;
#region Commite Code
// Nik - Code TO List Undo Add Class 
				// for (int i = 0; i < Nik_combineObjects.Count; i++)
				// {
				// 	string Name = Nik_combineObjects[i].name;
				// 	Sprite Pic = Nik_combineObjects[i].GetComponent<SpriteRenderer>().sprite;

				// 	Nik_BlockList BlockData = new Nik_BlockList();
				// 	BlockData.B_Name = Name;
				// 	BlockData.B_Sprite = Pic;

				// 	if (!Nik_BlockLists.Contains(BlockData))
				// 	{
				// 		Nik_BlockLists.Add(BlockData);	
				// 	}
				// }
#endregion

				// if(tCombineObjects.Count <= 3)return;
				if(tCombineObjects.Count <= 4)return;
				//combine score 100 grade 1 200 grade 2,then x n

				ObscuredInt scorebase = ori;
				if(scorebase >= 200){
					scorebase = scorebase - 200 +1;
				}

				combineScore+=scorebase*100*tCombineObjects.Count;
				
				if(ori == 1001){//
					ori = 200;
					max = 202;
				}
				
				ori++;
				//reopen this node to check; continue check
				checkedDick[xx + "_" + yy] = 0;
				
				cCombineResult = ori;

				alreadyhas1Combine = true;
			}else{
				//not found answer(if all fit can check next round.if normal jew,end)
				if (isAllFit) {//all fit jewley,continue test;
					if(combineObjects.Count < 3){
						//have not found any combine yet
						ori++;
						//reopen this node to check;
						checkedDick[xx + "_" + yy] = 0;

						if(ori == maxN && combineObjects.Count < 3){
							max = 202;
							ori = 200;
						}
						
					}else{
						//already found one answer
						break;
					}
					
					
				}else{
					break;
				}
			}
		}//while
		
		if ((combineObjects != null) && combineObjects.Count >= 3) 
		{
			foreach (GameObject tjew in combineObjects) {
				if (tjew){
					Jew tjewc = tjew.GetComponent<Jew> ();

					// Nik - New Code Change || 02-07
					// if(tjewc !=null)
					// 	if(!isAutoCheck){
					// 	//auto check is quick only show ends ,show need not show zoom
					// 		tjewc.setZoom (true);
					// }
				}
			}
		} 
		else 
		{
			//self always zooming;
			Jew tjewc = orijew.GetComponent<Jew> ();

			// Nik - New Code Change || 02-07
			// if(tjewc != null)
			// 	if(!isAutoCheck){
			// 		tjewc.setZoom (true);
			// }
		}
	}
	
	
	
	void stopZooms(){
		if (combineObjects != null) {
			
			foreach (GameObject tjew in combineObjects) {
				if(tjew!=null){
					Jew tjewc = tjew.GetComponent<Jew> ();
					if(tjewc != null)
						tjewc.setZoom (false);
				}
			}
			
			
		}
	}
	
	void gameOver()
	{
		// Nik - Code Diamond Collection || 14-04
		int DiamondNum = GameData.getInstance().cLevel;
		int DiamondScore = (int)GameData.getInstance ().cScore;
		Debug.Log("<color=red><b>Nik - Diamond Is +  </b></color>" + DiamondNum + " Total Score : " + GameData.getInstance ().cScore);

		DiamondCollection(DiamondScore, DiamondNum);


		GameObject gameoverobj = GameObject.Find("gameover");
		gameoverobj.transform.position = Vector3.zero;
		gameoverobj.transform.localScale = new Vector3 (.35f, .35f, .35f);
		//		HOTween.To (gameoverobj.transform, 2f, new TweenParms ().Prop ("localScale", Vector3.one, false).Ease(EaseType.EaseOutElastic).OnComplete(gameovered));
		gameoverobj.transform.DOScale (new Vector3(.75f,.75f,.75f), 2f).SetEase(Ease.OutElastic).OnComplete (gameovered);
		GameData.getInstance ().isFail = true;
		
		StartCoroutine("waitlastjewadded");
		//		GameManager.getInstance ().showInterestitial ();
		GameManager.getInstance().playSfx("gameover");
		
	}

	// Nik - Code Diamond Collection || 14-04
	public void DiamondCollection(int Score, int DiamondNum)
	{
		if (Score >= 40000)
		{
			TimeBonus = time * 100;

			TimeBonusText.transform.GetChild(0).GetComponent<Text>().text = TimeBonus.ToString();

			GameData.getInstance().cScore += TimeBonus;
			UnlockDiamondImage.transform.parent.gameObject.SetActive(true);
			UnlockDiamondImage.GetComponent<Image>().sprite = DiamondSprite[DiamondNum];

			// panelScoreBoard.GetComponent<PanelScoreBoard> ().basicScore.text = ""+GameData.getInstance ().cScore;

			Debug.Log("<color=red><b>Nik Score Is 40000 Up </b></color>");
			int num = PlayerPrefs.GetInt("Diamond"+DiamondNum);
			num++;
			PlayerPrefs.SetInt("Diamond"+DiamondNum, num);
			PlayerPrefs.Save();
		}
		else
		{
			MERGE3.transform.GetChild(0).GetComponent<Text>().text = "0";
			MERGE4.transform.GetChild(0).GetComponent<Text>().text = "0";
			MERGE5.transform.GetChild(0).GetComponent<Text>().text = "0";
			MERGE6.transform.GetChild(0).GetComponent<Text>().text = "0";
			TimeBonusText.transform.GetChild(0).GetComponent<Text>().text = TimeBonus.ToString();
			UnlockDiamondImage.transform.parent.gameObject.SetActive(false);
			Debug.Log("<color=black><b>Nik Score Is 40000 Down </b></color>");
		}
	}
	
	IEnumerator waitlastjewadded(){
		yield return new WaitForSeconds (.1f);
		panelScoreBoard.SetActive (true);
		panelScoreBoard.GetComponent<PanelScoreBoard> ().refreshView ();
		//clear data
		
		GameData.getInstance ().clearGame ();
		if (testmode) {
			// Application.LoadLevel(Application.loadedLevelName);		
		}
	}
	
	void gameovered(){
		panelScoreBoard.SetActive (true);
		
		
	}
	
	ObscuredInt combineJews(GameObject desJew_ = null){
		ObscuredInt combined = 0;
		if ((combineObjects != null) && combineObjects.Count >= 3)
		{
			// if(cCombineResult <= 2){
			// 	GameManager.getInstance().playSfx("level1");
			// }else if(cCombineResult <= 5){
			// 	GameManager.getInstance().playSfx("level5");
			// }else if(cCombineResult <= 6){
			// 	GameManager.getInstance().playSfx("level6");
			// }else if(cCombineResult <= 9){
			// 	GameManager.getInstance().playSfx("level9");
			// }else if(cCombineResult == 10){
			// 	GameManager.getInstance().playSfx("level6");
			// }else{//tomb
			// 	GameManager.getInstance().playSfx("level11");
			// }
			GameObject desjew = cJew;
			if(desJew_ != null)desjew = desJew_;
			foreach (GameObject tjew in combineObjects) {
				if (tjew){
					string[] tjewParams = tjew.name.Split ("_"[0]);
					int tjewPosX =int.Parse(tjewParams [1]);
					int tjewPosY = int.Parse(tjewParams [2]);
	
					tjew.BroadcastMessage("stopMove",SendMessageOptions.DontRequireReceiver);
					if(desjew.transform.position == tjew.transform.position){
						//combine to self,actually not move,show a float up action
						tjew.transform.DOMove(desjew.transform.position+new Vector3(0,.5f,0), 1f);
						SpriteRenderer tsp = tjew.GetComponent<SpriteRenderer>();
						if(tsp){
							tsp.color = new Color(1,1,1,.5f);
							tsp.sortingOrder = 1000;
						}
						Destroy(tjew,.5f);
						mapData [tjewPosX][tjewPosY] = cCombineResult;
					}else{
						tjew.transform.DOMove(desjew.transform.position, .2f).SetEase(Ease.InBack).SetLoops(1);
						Destroy(tjew,.2f);
					}
					
					//clear data immidiently;

					if (tjew.name != desjew.name) {
						//not clear combine position's data;
						mapData [tjewPosX] [tjewPosY] = 0;
					}
					tjew.tag = "Untagged";
					removeCGrid (tjewPosX,tjewPosY);
				}
			}
			//			GameObject cCombineAnim;
			//			if(isCrazy > 0){
			//				cCombineAnim = GameObject.Instantiate(combineAnim2,desjew.transform.position,Quaternion.identity) as GameObject;
			//			}else{
			//				cCombineAnim = GameObject.Instantiate(combineAnim,desjew.transform.position,Quaternion.identity) as GameObject;
			//			}
			//			StartCoroutine("combineAnimOver",cCombineAnim);
			combined = combineObjects.Count;
			//			Destroy(cCombineAnim,1);
		}
		combineObjects = new List<GameObject> ();
		return combined;
	}
	
	
	

	// Nik - Code Change || 11 - 04
	public List<int[]> wait4check;
	// List<int[]> wait4check;
	void check4(int xx,int yy,int type){
		//check 4 directions
		int tdicValue = 0;
		checkedDick.TryGetValue(xx + "_" + yy,out tdicValue);
		if (tdicValue != 0)
			return;
		
		
		//check up;
		if (yy - 1 >= 0) {
			string tdicname = xx + "_" +(yy - 1).ToString();
			tdicValue = 0;
			checkedDick.TryGetValue(tdicname,out tdicValue);
			if (tdicValue == 0) {
				
				int up = mapData [xx] [yy - 1];
				if (up == type) {
				
				
						int[] trightPos = {xx,yy-1};
						wait4check.Add (trightPos);

					
					//					print ("up detect");
				}
			}
		}
		//check down
		// Nik - Code Change || 08-04
		// if (yy + 1 <= 5) {
		if (yy + 1 <= 4) {
			string tdicname = xx + "_" +(yy + 1).ToString();
			tdicValue = 0;
			checkedDick.TryGetValue(tdicname,out tdicValue);
			if (tdicValue == 0) {
				
				int down = mapData [xx] [yy + 1];
				if (down == type) {


						int[] trightPos = {xx,yy+1};
						wait4check.Add (trightPos);

					//					print ("down detect");
				}
			}
		}
		
		//check left
		if (xx - 1 >= 0) {
			string tdicname = (xx-1).ToString() + "_" +yy;
			tdicValue = 0;
			checkedDick.TryGetValue(tdicname,out tdicValue);
			if (tdicValue == 0) {
				
				int left = mapData [xx-1] [yy];
				if (left == type) {
				
						int[] trightPos = {xx-1,yy};
						wait4check.Add (trightPos);

					//					print ("left detect");
				}
			}
		}
		
		//check right
		// Nik - Code Change || 08-04
		// if (xx + 1 <= 5) {
		if (xx + 1 <= 4) {
			string tdicname = (xx+1).ToString() + "_" +yy;
			tdicValue = 0;
			checkedDick.TryGetValue(tdicname,out tdicValue);
			if (tdicValue == 0) {
				
				int right = mapData [xx+1] [yy];
				if (right == type) {
					bool tIsDead = checkDead(xx+1,yy,type);

						int[] trightPos = {xx+1,yy};
						wait4check.Add (trightPos);

					//					print ("right detect");
				}
			}
		}
		
		checkedDick [xx + "_" + yy] = 1;
	}
	
	
	bool checkDead(int xx,int yy,int type){
		bool isdead = true;
		if (type >= 1000) {//only check monsters
			if ((xx > 0 && mapData [xx - 1] [yy] == 0) || (xx < 5 && mapData [xx + 1] [yy] == 0) ||
			    (yy > 0 && mapData [xx] [yy - 1] == 0) || (yy < 5 && mapData [xx] [yy + 1] == 0)) {
				
				isdead = false;
			}
		}
		// Debug.Log("Nik - New Log ISDEAD : " + isdead);
		return isdead;
	}
	
	public Vector3 setPos(GameObject g,int xx,int yy,bool normal = false){
		float toffsetX = (gridwidth - .64f)/2+.64f/2;
		float toffsetY = (gridheight - .64f)/2+.64f/2;
		
		cameraOffstY = bg.transform.position.y;
		// Nik - Code Change || 06-04
		Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3.48f,-gridheight*yy+gridheight*2.57f+cameraOffstY,0) + new Vector3(toffsetX,toffsetY,0);;
		// Vector3 jpos = new Vector3((xx-6)*gridwidth+gridwidth*3,-gridheight*yy+gridheight*3+cameraOffstY,0) + new Vector3(toffsetX,toffsetY,0);
		if (!normal) {
			//		g.transform.position = jpos;
			g.name = "jew_" + xx + "_" + yy;
			cBlock = "";
		}
		return jpos;
	}
	
}
