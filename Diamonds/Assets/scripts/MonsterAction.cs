﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;
public class MonsterAction : MonoBehaviour {
	
	// Use this for initialization
	public ObscuredInt xx,yy;
	public TouchGame touchGame;
	Animator anim;
	void Start () {
		Debug.Log("Dev - Start MosterAction");
		//		StartCoroutine ("t");
		anim = GetComponent<Animator> ();
	}
	IEnumerator t(){
		while (true) {
			yield return new WaitForSeconds (.2f);
			detectMove ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	ObscuredInt initx,inity;
	void detectMove(){
		//fix a bug which animals moving down from the waiting place.
		if (transform.parent.localScale.y < 1f)
			return;


		if (stopmove)
						return;
		if (touchGame == null)
			return;
		List<int> canmovePos = new List<int> ();
		bool canmove = false;
		if((xx -1 >= 0) && touchGame.mapData[xx-1][yy] == 0){
			canmovePos.Add(1);//this way can move
			canmove = true;
		}else{
			canmovePos.Add(0);
		}
		
		
		if((xx +1 <= 5) && touchGame.mapData[xx+1][yy] == 0){
			canmovePos.Add(2);//this way can move
			canmove = true;
		}else{
			canmovePos.Add(0);
		}
		
		
		
		if((yy -1 >= 0) && touchGame.mapData[xx][yy-1] == 0){
			canmovePos.Add(3);//this way can move
			canmove = true;
		}else{
			canmovePos.Add(0);
		}
		
		
		
		if((yy + 1 <= 5) && touchGame.mapData[xx][yy+1] == 0){
			canmovePos.Add(4);//this way can move
			canmove = true;
		}else{
			canmovePos.Add(0);
		}
		
		
		if (canmove) {
			ObscuredInt tdir = (int)Random.Range(0,4);
			while(canmovePos[tdir] == 0){
				//loop unity found a can move dir
				tdir = (int)Random.Range(0,4);
				
				
			}
			switch(canmovePos[tdir]){
			case  1://left
				touchGame.mapData[xx-1][yy] = 1001;
				touchGame.mapData[xx][yy] = 0;
			
				anim.SetTrigger("walk");

				xx -= 1;
				break;
			case  2://right
				
				touchGame.mapData[xx+1][yy] = 1001;
				touchGame.mapData[xx][yy] = 0;

				anim.SetTrigger("walk");
				xx+=1;
				break;
			case  3://up
				touchGame.mapData[xx][yy-1] = 1001;
				touchGame.mapData[xx][yy] = 0;
			
				anim.SetTrigger("back");

				yy-=1;
				break;
			case  4://down
				touchGame.mapData[xx][yy+1] = 1001;
				touchGame.mapData[xx][yy] = 0;
			
				anim.SetTrigger("walk");
				yy += 1;
				break;
				
			}

			Vector3 tdesPos = touchGame.setPos(transform.parent.gameObject,xx,yy);
			transform.parent.DOMove(tdesPos,.5f);
	
			if (PlayerPrefs.GetInt("pausegame") == 0)
			{
				touchGame.saveAllJew();
			}
		}
		
		
	}
	
	public void jump(){
//		Sequence sq = DOTween.Sequence();
//		sq.Append (transform.DOLocalMoveY (transform.localPosition.y+.1f,.1f));
//		//		sq.Append (transform.DOMoveY (-.01f,.1f).SetEase(Ease.Linear));
//		sq.Play ();
//		sq.SetLoops(4,LoopType.Restart);

	}

	IEnumerator waitdetectMove(){
		yield return new WaitForSeconds (.2f);
		detectMove ();
	}

	Coroutine wm;
	public void touched(){
		wm = StartCoroutine("waitdetectMove");
	}
	bool stopmove = false;
	public void stopMove(){
		stopmove = true;
		StopCoroutine ("waitdetectMove");
	}
}
