﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dev_PlayerPrefsManager : MonoBehaviour
{
    private void Awake()
    {
        if (PlayerPrefs.GetInt("Dev_FirstTime") == 0)
        {
            PlayerPrefs.SetInt("Dev_FirstTime", 1);

            PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", 0);
            PlayerPrefs.SetInt("H_ForcePage_LoadAtStart", 0);
            PlayerPrefs.SetInt("F1_Purchased", 1);
            PlayerPrefs.SetInt("F2_Purchased", 1);
            PlayerPrefs.SetInt("F3_Purchased", 1);
        }

        if (PlayerPrefs.GetInt("Dev_SecondTime") == 0)
        {
            PlayerPrefs.SetInt("Dev_SecondTime", 1);

            PlayerPrefs.SetInt("HelpMenuShow", 0);
        }
    }
}
