﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class Dev_IAPManager : MonoBehaviour, IStoreListener
{
    public bool isIphone = false;
    public bool isIpad = false;
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    // Product identifiers for all products capable of being purchased: 
    // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
    // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
    // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    // General product identifiers for the consumable, non-consumable, and subscription products.
    // Use these handles in the code to reference which product to purchase. Also use these values 
    // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
    // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    // specific mapping to Unity Purchasing's AddProduct, below.

    #region DEV_IPAD_IDS

    //public GameObject clickedButton;
    public List<GameObject> listOfPButtons = new List<GameObject>();

    public static string purchase_1_iPad = "com.digdog.imotsflechesdeluxe.grids_0_0_100_01";
    public static string purchase_2_iPad = "com.digdog.imotsflechesdeluxe.grids_0_1_100_01";
    public static string purchase_3_iPad = "com.digdog.imotsflechesdeluxe.grids_0_2_100_01";
    public static string purchase_4_iPad = "com.digdog.imotsflechesdeluxe.grids_0_3_300_01";
    public static string purchase_5_iPad = "com.digdog.imotsflechesdeluxe.grids_1_0_100_01";
    public static string purchase_6_iPad = "com.digdog.imotsflechesdeluxe.grids_1_1_100_01";
    public static string purchase_7_iPad = "com.digdog.imotsflechesdeluxe.grids_1_2_100_01";
    public static string purchase_8_iPad = "com.digdog.imotsflechesdeluxe.grids_1_3_300_01";
    public static string purchase_9_iPad = "com.digdog.imotsflechesdeluxe.grids_2_0_100_01";
    public static string purchase_10_iPad = "com.digdog.imotsflechesdeluxe.grids_2_1_100_01";
    public static string purchase_11_iPad = "com.digdog.imotsflechesdeluxe.grids_2_2_100_01";
    public static string purchase_12_iPad = "com.digdog.imotsflechesdeluxe.grids_2_3_300_01";
    public static string purchase_13_iPad = "com.digdog.imotsflechesdeluxe.solutions_10_01";
    public static string purchase_14_iPad = "com.digdog.imotsflechesdeluxe.solutions_50_01";
    #endregion
    #region DEV_IPHONE_IDS
    public List<GameObject> listOfiPhonePurchaseButtons = new List<GameObject>();
    public static string purchase_1_iPhone = "com.digdog.MF.grids_0_0_100_01";
    public static string purchase_2_iPhone = "com.digdog.MF.grids_1_0_100_01";
    public static string purchase_3_iPhone = "com.digdog.MF.grids_0_1_100_01";
    public static string purchase_4_iPhone = "com.digdog.MF.grids_1_1_100_01";
    public static string purchase_5_iPhone = "com.digdog.MF.grids_0_2_100_01";
    public static string purchase_6_iPhone = "com.digdog.MF.grids_1_2_100_01";
    public static string purchase_7_iPhone = "com.digdog.MF.grids_0_3_300_01";
    public static string purchase_8_iPhone = "com.digdog.MF.grids_1_3_300_01";
    public static string purchase_9_iPhone = "com.digdog.MF.solutions_10_01"; //CONSUMABLE
    public static string purchase_10_iPhone = "com.digdog.MF.solutions_50_01"; //CONSUMABLE

    #endregion

    // Apple App Store-specific product identifier for the subscription product.
    private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

    // Google Play Store-specific product identifier subscription product.
    private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

    void Start()
    {
        if (!isIpad && !isIphone)
        {
            Debug.LogError("Please Select Any Platform First !");
            return;
        }

        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        if (isIpad)
        {
            builder.AddProduct(purchase_1_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_2_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_3_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_4_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_5_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_6_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_7_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_8_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_9_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_10_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_11_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_12_iPad, ProductType.NonConsumable);
            builder.AddProduct(purchase_13_iPad, ProductType.Consumable);
            builder.AddProduct(purchase_14_iPad, ProductType.Consumable);
        }

        if (isIphone)
        {
            builder.AddProduct(purchase_1_iPhone, ProductType.NonConsumable);
            builder.AddProduct(purchase_2_iPhone, ProductType.NonConsumable);
            builder.AddProduct(purchase_3_iPhone, ProductType.NonConsumable);
            builder.AddProduct(purchase_4_iPhone, ProductType.NonConsumable);
            builder.AddProduct(purchase_5_iPhone, ProductType.NonConsumable);
            builder.AddProduct(purchase_6_iPhone, ProductType.NonConsumable);
            builder.AddProduct(purchase_7_iPhone, ProductType.NonConsumable);
            builder.AddProduct(purchase_8_iPhone, ProductType.NonConsumable);
            builder.AddProduct(purchase_9_iPhone, ProductType.Consumable);
            builder.AddProduct(purchase_10_iPhone, ProductType.Consumable);
        }

        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    #region DEVELOPER_CODE_IPAD

    public void Purchase_1()
    {
        BuyProductID(purchase_1_iPad);
    }
    public void Purchase_2()
    {
        BuyProductID(purchase_2_iPad);
    }
    public void Purchase_3()
    {
        BuyProductID(purchase_3_iPad);
    }
    public void Purchase_4()
    {
        BuyProductID(purchase_4_iPad);
    }
    public void Purchase_5()
    {
        BuyProductID(purchase_5_iPad);
    }
    public void Purchase_6()
    {
        BuyProductID(purchase_6_iPad);
    }
    public void Purchase_7()
    {
        BuyProductID(purchase_7_iPad);
    }
    public void Purchase_8()
    {
        BuyProductID(purchase_8_iPad);
    }
    public void Purchase_9()
    {
        BuyProductID(purchase_9_iPad);
    }
    public void Purchase_10()
    {
        BuyProductID(purchase_10_iPad);
    }
    public void Purchase_11()
    {
        BuyProductID(purchase_11_iPad);
    }
    public void Purchase_12()
    {
        BuyProductID(purchase_12_iPad);
    }
    public void Purchase_13()
    {
        BuyProductID(purchase_13_iPad);
    }
    public void Purchase_14()
    {
        BuyProductID(purchase_14_iPad);
    }

    #endregion

    #region PURCHASE_CODE_IPHONE

    public void Purchase_1_iPhone()
    {
        BuyProductID(purchase_1_iPhone);
    }

    public void Purchase_2_iPhone()
    {
        BuyProductID(purchase_2_iPhone);
    }

    public void Purchase_3_iPhone()
    {
        BuyProductID(purchase_3_iPhone);
    }

    public void Purchase_4_iPhone()
    {
        BuyProductID(purchase_4_iPhone);
    }

    public void Purchase_5_iPhone()
    {
        BuyProductID(purchase_5_iPhone);
    }

    public void Purchase_6_iPhone()
    {
        BuyProductID(purchase_6_iPhone);
    }

    public void Purchase_7_iPhone()
    {
        BuyProductID(purchase_7_iPhone);
    }

    public void Purchase_8_iPhone()
    {
        BuyProductID(purchase_8_iPhone);
    }

    public void Purchase_9_iPhone()
    {
        BuyProductID(purchase_9_iPhone);
    }

    public void Purchase_10_iPhone()
    {
        BuyProductID(purchase_10_iPhone);
    }

    #endregion

    void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }


    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                Dev_RestorePurchase();
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }

    public void Dev_RestorePurchase()
    {

        #region IPAD_CODE

        if (isIpad)
        {
            Product product_1 = m_StoreController.products.WithID(purchase_1_iPad);
            Product product_2 = m_StoreController.products.WithID(purchase_2_iPad);
            Product product_3 = m_StoreController.products.WithID(purchase_3_iPad);
            Product product_4 = m_StoreController.products.WithID(purchase_4_iPad);
            Product product_5 = m_StoreController.products.WithID(purchase_5_iPad);
            Product product_6 = m_StoreController.products.WithID(purchase_6_iPad);
            Product product_7 = m_StoreController.products.WithID(purchase_7_iPad);
            Product product_8 = m_StoreController.products.WithID(purchase_8_iPad);
            Product product_9 = m_StoreController.products.WithID(purchase_9_iPad);
            Product product_10 = m_StoreController.products.WithID(purchase_10_iPad);
            Product product_11 = m_StoreController.products.WithID(purchase_11_iPad);
            Product product_12 = m_StoreController.products.WithID(purchase_12_iPad);
            Product product_13 = m_StoreController.products.WithID(purchase_13_iPad);
            Product product_14 = m_StoreController.products.WithID(purchase_14_iPad);

            if (product_1.hasReceipt)
            {
                listOfPButtons[0].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
            }
            if (product_2.hasReceipt)
            {
                listOfPButtons[1].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
            }
            if (product_3.hasReceipt)
            {
                listOfPButtons[2].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
            }
            if (product_4.hasReceipt)
            {
                listOfPButtons[3].GetComponent<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
                FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
            }
            if (product_5.hasReceipt)
            {
                listOfPButtons[4].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
            }
            if (product_6.hasReceipt)
            {
                listOfPButtons[5].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
            }
            if (product_7.hasReceipt)
            {
                listOfPButtons[6].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
            }
            if (product_8.hasReceipt)
            {
                listOfPButtons[7].GetComponent<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
                FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
            }
            if (product_9.hasReceipt)
            {
                listOfPButtons[8].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
            }
            if (product_10.hasReceipt)
            {
                listOfPButtons[9].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
            }
            if (product_11.hasReceipt)
            {
                listOfPButtons[10].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
            }
            if (product_12.hasReceipt)
            {
                listOfPButtons[11].GetComponent<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
                FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
            }
            if (product_13.hasReceipt)
            {
                listOfPButtons[12].GetComponent<ButtonStoreItemConsumable>().UnlockMyGrids_Dev_Consumable();
            }
            if (product_14.hasReceipt)
            {
                listOfPButtons[13].GetComponent<ButtonStoreItemConsumable>().UnlockMyGrids_Dev_Consumable();
            }
        }

        #endregion

        #region IPHONE_CODE

        if (isIphone)
        {
            Product product_1_iPhone = m_StoreController.products.WithID(purchase_1_iPhone);
            Product product_2_iPhone = m_StoreController.products.WithID(purchase_2_iPhone);
            Product product_3_iPhone = m_StoreController.products.WithID(purchase_3_iPhone);
            Product product_4_iPhone = m_StoreController.products.WithID(purchase_4_iPhone);
            Product product_5_iPhone = m_StoreController.products.WithID(purchase_5_iPhone);
            Product product_6_iPhone = m_StoreController.products.WithID(purchase_6_iPhone);
            Product product_7_iPhone = m_StoreController.products.WithID(purchase_7_iPhone);
            Product product_8_iPhone = m_StoreController.products.WithID(purchase_8_iPhone);
            Product product_9_iPhone = m_StoreController.products.WithID(purchase_9_iPhone);
            Product product_10_iPhone = m_StoreController.products.WithID(purchase_10_iPhone);

            if (product_1_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[0].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
            }
            if (product_2_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[1].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
            }
            if (product_3_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[2].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
            }
            if (product_4_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[3].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
            }
            if (product_5_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[4].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
            }
            if (product_6_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[5].GetComponent<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
            }
            if (product_7_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[6].GetComponent<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
                FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
            }
            if (product_8_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[7].GetComponent<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
                FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
            }
            if (product_9_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[8].GetComponent<ButtonStoreItemConsumable>().UnlockMyGrids_Dev_Consumable();
            }
            if (product_10_iPhone.hasReceipt)
            {
                listOfiPhonePurchaseButtons[9].GetComponent<ButtonStoreItemConsumable>().UnlockMyGrids_Dev_Consumable();
            }
        }

        #endregion
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        #region OLD_CODE

        // if (String.Equals(args.purchasedProduct.definition.id, purchase_1_iPad, StringComparison.Ordinal))
        // {
        //     //0.0
        //     if (FindObjectOfType<ButtonStoreItem>())
        //     {
        //         FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
        //         Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_2_iPad, StringComparison.Ordinal))
        // {
        //     //1.0
        //     if (FindObjectOfType<ButtonStoreItem>())
        //     {
        //         FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_3_iPad, StringComparison.Ordinal))
        // {
        //     //2.0
        //     if (FindObjectOfType<ButtonStoreItem>())
        //     {
        //         FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_4_iPad, StringComparison.Ordinal))
        // {
        //     //3.0
        //     if (FindObjectOfType<ButtonStoreItemMultiple>())
        //     {
        //         FindObjectOfType<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_5_iPad, StringComparison.Ordinal))
        // {
        //     //0.1
        //     if (FindObjectOfType<ButtonStoreItem>())
        //     {
        //         FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_6_iPad, StringComparison.Ordinal))
        // {
        //     //1.1
        //     if (FindObjectOfType<ButtonStoreItem>())
        //     {
        //         FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_7_iPad, StringComparison.Ordinal))
        // {
        //     //2.1
        //     if (FindObjectOfType<ButtonStoreItem>())
        //     {
        //         FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_8_iPad, StringComparison.Ordinal))
        // {
        //     //3.1
        //     if (FindObjectOfType<ButtonStoreItemMultiple>())
        //     {
        //         FindObjectOfType<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_9_iPad, StringComparison.Ordinal))
        // {
        //     //0.2
        //     if (FindObjectOfType<ButtonStoreItem>())
        //     {
        //         FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
        //     }
        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_10_iPad, StringComparison.Ordinal))
        // {
        //     //1.2
        //     if (FindObjectOfType<ButtonStoreItem>())
        //     {
        //         FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_11_iPad, StringComparison.Ordinal))
        // {
        //     //2.2
        //     if (FindObjectOfType<ButtonStoreItem>())
        //     {
        //         FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
        //     }

        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_12_iPad, StringComparison.Ordinal))
        // {
        //     //3.2
        //     if (FindObjectOfType<ButtonStoreItemMultiple>())
        //     {
        //         FindObjectOfType<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
        //         FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
        //     }
        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_13_iPad, StringComparison.Ordinal))
        // {
        //     //0.3 - 1
        //     if (FindObjectOfType<ButtonStoreItemConsumable>())
        //     {
        //         FindObjectOfType<ButtonStoreItemConsumable>().UnlockMyGrids_Dev_Consumable();
        //     }
        // }
        // else if (String.Equals(args.purchasedProduct.definition.id, purchase_14_iPad, StringComparison.Ordinal))
        // {
        //     //0.3 - 2
        //     if (FindObjectOfType<ButtonStoreItemConsumable>())
        //     {
        //         FindObjectOfType<ButtonStoreItemConsumable>().UnlockMyGrids_Dev_Consumable();
        //     }
        // }
        // else
        // {
        //     Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        // }                                                                                                                                                                                                                                                                           

        #endregion

        if (String.Equals(args.purchasedProduct.definition.id, purchase_1_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItem>())
            {
                FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, purchase_2_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItem>())
            {
                FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF1_Final(3);
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, purchase_3_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItem>())
            {
                FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, purchase_4_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItem>())
            {
                FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF2_Final(3);
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, purchase_5_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItem>())
            {
                FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, purchase_6_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItem>())
            {
                FindObjectOfType<ButtonStoreItem>().UnlockMyGrid_Dev_StoreItem();
                FindObjectOfType<Dev_ItemUnlockerEditor>().ItemUnlockerF3_Final(3);
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, purchase_7_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItemMultiple>())
            {
                FindObjectOfType<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
                FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, purchase_8_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItem>())
            {
                FindObjectOfType<ButtonStoreItemMultiple>().UnlockMyGrids_Dev_Multiple();
                FindObjectOfType<Dev_ItemUnlockerEditor>().UnlockMethodForYellowPages_Final();
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, purchase_9_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItemConsumable>())
            {
                FindObjectOfType<ButtonStoreItemConsumable>().UnlockMyGrids_Dev_Consumable();
            }
        }
        else if (String.Equals(args.purchasedProduct.definition.id, purchase_10_iPhone, StringComparison.Ordinal))
        {
            if (FindObjectOfType<ButtonStoreItemConsumable>())
            {
                FindObjectOfType<ButtonStoreItemConsumable>().UnlockMyGrids_Dev_Consumable();
            }
        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}