﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Dev_UIManager : MonoBehaviour
{
    public static Dev_UIManager instance;

    #region VARIABLES

    public bool isUiElement = false;
    private bool resetPagePosition = false;

    [Space(10)]
    public int currentVerticalIndex;
    public int currentHorizontalIndex;
    public int dummyIndexForButtons;
    public GameObject loadingPanel;
    public GameObject currentSelectedPage;

    [Space(10)]
    public Text forceText;
    public Text gridText;
    public Button currentForceButton;
    public Button currentGridButton;
    public List<Button> listOfButtonsForChangingGridNames = new List<Button>();

    [Space(10)]
    public Sprite newGridSprite_F;
    public Sprite newGridSprite_M;
    public Sprite newGridSprite_D;
    public Sprite startedGridSprite_F;
    public Sprite startedGridSprite_M;
    public Sprite startedGridSprite_D;
    public Sprite completedGridSprite_F;
    public Sprite completedGridSprite_M;
    public Sprite completedGridSprite_D;

    [Space(10)]
    public GameObject pressedButton;

    [Space(10)]
    public List<GameObject> listOfCurrentPageItem = new List<GameObject>();
    public List<Button> listOfCurrentHorizontalActiveButtons = new List<Button>();
    public List<GameObject> listOfSpawnedPages = new List<GameObject>();
    [Space(10)]
    public List<GameObject> listOfSpawnedPages_9x13 = new List<GameObject>();
    public List<Button> listOfHorizontalButtons_9x13 = new List<Button>();
    public List<GameObject> listOfSpawnedPages_13x9 = new List<GameObject>();
    public List<Button> listOfHorizontalButtons_13x9 = new List<Button>();
    public List<GameObject> listOfSpawnedPages_18x18 = new List<GameObject>();
    public List<Button> listOfHorizontalButtons_18x18 = new List<Button>();
    public List<GameObject> listOfSpawnedPages_8X5 = new List<GameObject>();
    public List<Button> listOfHorizontalButtons_8X5 = new List<Button>();
    public List<GameObject> listOfSpawnedPages_10X10 = new List<GameObject>();
    public List<Button> listOfHorizontalButtons_10X10 = new List<Button>();
    [Space(10)]
    public List<Button> listOfVerticalPageButtons_Menu = new List<Button>();
    public List<Button> listOfHorizontalButtons_Menu = new List<Button>();

    

    #endregion

    private void OnEnable()
    {
        StartCoroutine(StartObj());
    }

    private void Awake()
    {
        instance = this;
        //StartCoroutine(StartObj());

        currentVerticalIndex = PlayerPrefs.GetInt("V_ForcePage_LoadAtStart");
        currentHorizontalIndex = PlayerPrefs.GetInt("H_ForcePage_LoadAtStart");
    }

    IEnumerator StartObj()
    {
        yield return new WaitForSeconds(0);

        if (!this.gameObject.activeInHierarchy) gameObject.SetActive(true);

        DisableAllSpawnedPagesAtStart();

        UnlockHorizontalButtons();

        GetCurrentPage();

        LoadMainMenuItems();
    }

    public void OpenLoadingPanel()
    {
        loadingPanel.SetActive(true);
    }

    public void UnlockHorizontalButtons()
    {
        for (int i = 0; i < PlayerPrefs.GetInt("F1_Purchased"); i++)
        {
            listOfHorizontalButtons_9x13[i].gameObject.SetActive(true);
        }

        for (int j = 0; j < PlayerPrefs.GetInt("F2_Purchased"); j++)
        {
            listOfHorizontalButtons_13x9[j].gameObject.SetActive(true);
        }

        for (int k = 0; k < PlayerPrefs.GetInt("F3_Purchased"); k++)
        {
            listOfHorizontalButtons_18x18[k].gameObject.SetActive(true);
        }
    }

    public void DisableAllSpawnedPagesAtStart()
    {
        Debug.Log("<color=red>Spawned Page Count = </color>" + listOfSpawnedPages.Count);

        for (int i = 0; i < listOfSpawnedPages.Count; i++)
        {
            if (listOfSpawnedPages[i].activeInHierarchy) listOfSpawnedPages[i].SetActive(false);
        }
    }

    public void GetCurrentPage()
    {
        if (currentVerticalIndex == 0)
        {
            Debug.Log("Getting from page 1");
            for (int i = 0; i < listOfSpawnedPages_9x13.Count; i++)
            {
                if (i == currentHorizontalIndex)
                {
                    currentSelectedPage = listOfSpawnedPages_9x13[i];
                    listOfSpawnedPages_9x13[i].SetActive(true);
                }
            }
        }
        else if (currentVerticalIndex == 1)
        {
            Debug.Log("Getting from page 2");
            for (int i = 0; i < listOfSpawnedPages_13x9.Count; i++)
            {
                if (i == currentHorizontalIndex)
                {
                    currentSelectedPage = listOfSpawnedPages_13x9[i];
                    listOfSpawnedPages_13x9[i].SetActive(true);
                }
            }
        }
        else if (currentVerticalIndex == 2)
        {
            Debug.Log("Getting from page 3");
            for (int i = 0; i < listOfSpawnedPages_18x18.Count; i++)
            {
                if (i == currentHorizontalIndex)
                {
                    currentSelectedPage = listOfSpawnedPages_18x18[i];
                    listOfSpawnedPages_18x18[i].SetActive(true);
                }
            }
        }
        // Nik - Add Code
        else if (currentVerticalIndex == 3)
        {
            Debug.Log("Getting from page 4");
            for (int i = 0; i < listOfSpawnedPages_8X5.Count; i++)
            {
                if (i == currentHorizontalIndex)
                {
                    currentSelectedPage = listOfSpawnedPages_8X5[i];
                    listOfSpawnedPages_8X5[i].SetActive(true);
                }
            }
        }
        else if (currentVerticalIndex == 4)
        {
            Debug.Log("Getting from page 5");
            for (int i = 0; i < listOfSpawnedPages_10X10.Count; i++)
            {
                if (i == currentHorizontalIndex)
                {
                    currentSelectedPage = listOfSpawnedPages_10X10[i];
                    listOfSpawnedPages_10X10[i].SetActive(true);
                }
            }
        }

    }

    public void ChangeForcePage()
    {
        pressedButton = EventSystem.current.currentSelectedGameObject;
        //pressedButton.GetComponent<ButtonOverlayIcon>().ButtonAction();

    }

    public void ChangeForce()
    {
        pressedButton = EventSystem.current.currentSelectedGameObject;

    }

    public void LoadMainMenuItems()
    {
        Debug.Log("Current Selected Page Index - " + currentHorizontalIndex + " // " + currentVerticalIndex);

        for (int i = 0; i < listOfVerticalPageButtons_Menu.Count; i++)
        {
            if (i == currentVerticalIndex)
            {
                //TODO || GRID 2
                if (i == 0)
                {
                    gridText.text = "9X13";
                }
                else if (i == 1)
                {
                    gridText.text = "13X9";
                }
                else if (i == 2)
                {
                    gridText.text = "18X18";
                }
                // Dev || 06-06-2020 || Code Change
                else if (i == 3)
                {
                    gridText.text = "8X5";
                }
                else if (i == 4)
                {
                    gridText.text = "10X10";
                }
            }
        }

        for (int i = 0; i < listOfHorizontalButtons_Menu.Count; i++)
        {
            if (listOfHorizontalButtons_Menu[i].gameObject.activeInHierarchy)
            {
                listOfCurrentHorizontalActiveButtons.Add(listOfHorizontalButtons_Menu[i]);
            }

            if (i == currentHorizontalIndex)
            {
                if (i >= 0 && i < 7)
                {
                    forceText.text = "Force 1 - ";
                }
                else if (i >= 7 && i < 14)
                {
                    forceText.text = "Force 2 - ";
                }
                else if (i >= 14)
                {
                    forceText.text = "Force 3 - ";
                }
            }
        }

        listOfCurrentHorizontalActiveButtons[currentHorizontalIndex].GetComponent<Button>().image.sprite = listOfCurrentHorizontalActiveButtons[currentHorizontalIndex].GetComponent<Button>().spriteState.pressedSprite;
        listOfVerticalPageButtons_Menu[currentVerticalIndex].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[currentVerticalIndex].GetComponent<Button>().spriteState.pressedSprite;

        for (int i = 0; i < listOfCurrentHorizontalActiveButtons.Count; i++)
        {
            listOfCurrentHorizontalActiveButtons[i].GetComponent<Dev_HVButtonsLogic>().myIndex = dummyIndexForButtons;
            dummyIndexForButtons++;
        }
    }


    public void EnablePanelsBasedOnSwipe(string swipeDirection)
    {
        if (swipeDirection == "Right")
        {
            for (int i = 0; i < listOfCurrentPageItem.Count; i++)
            {
                if (currentHorizontalIndex > 0)
                {
                    listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveRight");
                    listOfCurrentPageItem[currentHorizontalIndex - 1].gameObject.SetActive(true);

                    currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex - 1].gameObject;

                    listOfCurrentPageItem[currentHorizontalIndex - 1].GetComponent<Animator>().Play("MoveRightNew");

                    StartCoroutine(DisableLastActivePanel(0.2f, currentHorizontalIndex));

                    currentHorizontalIndex--;

                    PlayerPrefs.SetInt("H_ForcePage_LoadAtStart", currentHorizontalIndex);

                    for (int j = 0; j < listOfCurrentHorizontalActiveButtons.Count; j++)
                    {
                        if (j != currentHorizontalIndex)
                        {
                            listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().image.sprite = listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                        }
                        else
                        {
                            listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().image.sprite = listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().spriteState.pressedSprite;

                            if (listOfHorizontalButtons_9x13.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 1 - ";
                            }
                            else if (listOfHorizontalButtons_13x9.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 2 - ";
                            }
                            else if (listOfHorizontalButtons_18x18.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 3 - ";
                            }
                        }
                    }

                    break;
                }
            }
        }
        else if (swipeDirection == "Left")
        {
            for (int i = 0; i < listOfCurrentPageItem.Count; i++)
            {
                if (currentHorizontalIndex < listOfCurrentPageItem.Count - 1)
                {
                    listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveLeft");
                    listOfCurrentPageItem[currentHorizontalIndex + 1].gameObject.SetActive(true);
                    currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex + 1].gameObject;
                    listOfCurrentPageItem[currentHorizontalIndex + 1].GetComponent<Animator>().Play("MoveLeftNew");
                    StartCoroutine(DisableLastActivePanel(0.2f, currentHorizontalIndex));

                    currentHorizontalIndex++;

                    PlayerPrefs.SetInt("H_ForcePage_LoadAtStart", currentHorizontalIndex);

                    for (int j = 0; j < listOfCurrentHorizontalActiveButtons.Count; j++)
                    {
                        if (j != currentHorizontalIndex)
                        {
                            listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().image.sprite = listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                        }
                        else
                        {
                            listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().image.sprite = listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().spriteState.pressedSprite;

                            if (listOfHorizontalButtons_9x13.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 1 - ";
                            }
                            else if (listOfHorizontalButtons_13x9.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 2 - ";
                            }
                            else if (listOfHorizontalButtons_18x18.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 3 - ";
                            }
                        }
                    }

                    break;
                }
            }
        }
        else if (swipeDirection == "Up")
        {
            if (currentVerticalIndex == 0)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_13x9);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_9x13, currentHorizontalIndex));

                currentVerticalIndex++;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);

                //TODO || GRID 3
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;


                    }
                }
            }
            else if (currentVerticalIndex == 1)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_18x18);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_13x9, currentHorizontalIndex));

                currentVerticalIndex++;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);

                //TODO || GRID 4
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            // Nik - Add Code
            else if (currentVerticalIndex == 2)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_8X5);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_18x18, currentHorizontalIndex));

                currentVerticalIndex++;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);

                //TODO || GRID 4
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (currentVerticalIndex == 3)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_10X10);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_8X5, currentHorizontalIndex));

                currentVerticalIndex++;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);

                //TODO || GRID 4
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
        }
        else if (swipeDirection == "Down")
        {
            // Nik - Add Code
            if (currentVerticalIndex == 4)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_8X5);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_10X10, currentHorizontalIndex));

                currentVerticalIndex--;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 5
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (currentVerticalIndex == 3)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_18x18);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_8X5, currentHorizontalIndex));

                currentVerticalIndex--;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 5
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (currentVerticalIndex == 2)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_13x9);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_18x18, currentHorizontalIndex));

                currentVerticalIndex--;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 5
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (currentVerticalIndex == 1)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_9x13);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_13x9, currentHorizontalIndex));

                currentVerticalIndex--;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 6
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
        }
    }

    IEnumerator DisableLastActivePanel(float disableTime, int itemIndex)
    {
        yield return new WaitForSeconds(disableTime);

        listOfCurrentPageItem[itemIndex].SetActive(false);
        listOfCurrentPageItem[itemIndex].GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
    }

    IEnumerator DisableLastActivePalenFromList(float disableTime, List<GameObject> anyListName, int listItemIndex)
    {
        yield return new WaitForSeconds(disableTime);

        anyListName[listItemIndex].SetActive(false);
        anyListName[listItemIndex].GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
    }

    public void VerticalClickButtons()
    {
        GameObject btn = EventSystem.current.currentSelectedGameObject;

        //move up
        if (btn.GetComponent<Dev_HVButtonsLogic>().myIndex > currentVerticalIndex)
        {
            // Debug.Log("Nik - MyindexNumber : " + btn.GetComponent<Dev_HVButtonsLogic>().myIndex + " || > currentVerticalIndex : " + currentVerticalIndex);
            //move up
            if (currentVerticalIndex == 0)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_13x9);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_9x13, currentHorizontalIndex));

                currentVerticalIndex++;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 7
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (currentVerticalIndex == 1)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_18x18);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_13x9, currentHorizontalIndex));

                currentVerticalIndex++;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 8
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            // Nik - Add Code
            else if (currentVerticalIndex == 2)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_8X5);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_18x18, currentHorizontalIndex));

                currentVerticalIndex++;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 8
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (currentVerticalIndex == 3)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_10X10);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_8X5, currentHorizontalIndex));

                currentVerticalIndex++;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 8
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
        }
        else
        {
            // Debug.Log("Nik - MyindexNumber : " + btn.GetComponent<Dev_HVButtonsLogic>().myIndex + " || Else currentVerticalIndex : " + currentVerticalIndex);
            //move down
            // Nik - Add Code
            if (currentVerticalIndex == 4)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_8X5);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_10X10, currentHorizontalIndex));

                currentVerticalIndex--;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 9
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (currentVerticalIndex == 3)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_18x18);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_8X5, currentHorizontalIndex));

                currentVerticalIndex--;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 9
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (currentVerticalIndex == 2)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_13x9);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_18x18, currentHorizontalIndex));

                currentVerticalIndex--;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 9
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (currentVerticalIndex == 1)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPageItem.Clear();
                listOfCurrentPageItem.AddRange(listOfSpawnedPages_9x13);
                listOfCurrentPageItem[currentHorizontalIndex].SetActive(true);
                listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfSpawnedPages_13x9, currentHorizontalIndex));

                currentVerticalIndex--;
                PlayerPrefs.SetInt("V_ForcePage_LoadAtStart", currentVerticalIndex);
                if (currentVerticalIndex == 0)
                {
                    gridText.text = "9X13";
                }
                else if (currentVerticalIndex == 1)
                {
                    gridText.text = "13X9";
                }
                else if (currentVerticalIndex == 2)
                {
                    gridText.text = "18X18";
                }
                //TODO || GRID 10
                // Dev || 06-06-2020 || Code Change
                else if (currentVerticalIndex == 3)
                {
                    gridText.text = "8X5";
                }
                else if (currentVerticalIndex == 4)
                {
                    gridText.text = "10X10";
                }

                for (int j = 0; j < listOfVerticalPageButtons_Menu.Count; j++)
                {
                    if (j != currentVerticalIndex)
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerticalPageButtons_Menu[j].GetComponent<Button>().image.sprite = listOfVerticalPageButtons_Menu[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
        }
    }

    public void HorizontalClickButtons()
    {
        GameObject btn = EventSystem.current.currentSelectedGameObject;

        if (btn.GetComponent<Dev_HVButtonsLogic>().myIndex > currentHorizontalIndex)
        {
            //move left
            for (int i = 0; i < listOfCurrentPageItem.Count; i++)
            {
                if (currentHorizontalIndex < listOfCurrentPageItem.Count - 1)
                {
                    listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveLeft");
                    listOfCurrentPageItem[currentHorizontalIndex + 1].gameObject.SetActive(true);
                    currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex + 1].gameObject;
                    listOfCurrentPageItem[currentHorizontalIndex + 1].GetComponent<Animator>().Play("MoveLeftNew");
                    StartCoroutine(DisableLastActivePanel(0.2f, currentHorizontalIndex));

                    currentHorizontalIndex++;

                    PlayerPrefs.SetInt("H_ForcePage_LoadAtStart", currentHorizontalIndex);

                    for (int j = 0; j < listOfCurrentHorizontalActiveButtons.Count; j++)
                    {
                        if (j != currentHorizontalIndex)
                        {
                            listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().image.sprite = listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                        }
                        else
                        {
                            listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().image.sprite = listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                            if (listOfHorizontalButtons_9x13.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 1 - ";
                            }
                            else if (listOfHorizontalButtons_13x9.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 2 - ";
                            }
                            else if (listOfHorizontalButtons_18x18.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 3 - ";
                            }
                            // else if (listOfHorizontalButtons_18x18.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            // {
                            //     forceText.text = "Force 3 - ";
                            // }
                        }
                    }
                    break;
                }
            }
        }
        else
        {
            //move right
            for (int i = 0; i < listOfCurrentPageItem.Count; i++)
            {
                if (currentHorizontalIndex > 0)
                {
                    listOfCurrentPageItem[currentHorizontalIndex].GetComponent<Animator>().Play("MoveRight");
                    listOfCurrentPageItem[currentHorizontalIndex - 1].gameObject.SetActive(true);

                    currentSelectedPage = listOfCurrentPageItem[currentHorizontalIndex - 1].gameObject;

                    listOfCurrentPageItem[currentHorizontalIndex - 1].GetComponent<Animator>().Play("MoveRightNew");

                    StartCoroutine(DisableLastActivePanel(0.2f, currentHorizontalIndex));

                    currentHorizontalIndex--;

                    PlayerPrefs.SetInt("H_ForcePage_LoadAtStart", currentHorizontalIndex);

                    for (int j = 0; j < listOfCurrentHorizontalActiveButtons.Count; j++)
                    {
                        if (j != currentHorizontalIndex)
                        {
                            listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().image.sprite = listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                        }
                        else
                        {
                            listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().image.sprite = listOfCurrentHorizontalActiveButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                            if (listOfHorizontalButtons_9x13.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 1 - ";
                            }
                            else if (listOfHorizontalButtons_13x9.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 2 - ";
                            }
                            else if (listOfHorizontalButtons_18x18.Contains(listOfCurrentHorizontalActiveButtons[j]))
                            {
                                forceText.text = "Force 3 - ";
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    public void ChangeGridAndForceNames()
    {

    }
}
