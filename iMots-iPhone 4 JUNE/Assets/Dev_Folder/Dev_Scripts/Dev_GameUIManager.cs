﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Dev_GameUIManager : MonoBehaviour
{
    public static Dev_GameUIManager instance;

    public GameObject clockObject;
    public Text clockObjectText;
    [Space(10)]
    public bool panelIsActive = false;
    public GameObject topPanelParent;
    [Space(10)]
    public GameObject helpPanel;
    public GameObject loadingPanel;

    [Space(10)]
    public GameObject gridEntitiesObj;
    public Material correctMaterial;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        if (PlayerPrefs.GetInt("HelpMenuShow") == 0)
        {
            OpenHelpPanel();
        }
        else
        {
            helpPanel.SetActive(false);
        }
    }

    void Update()
    {
        clockObjectText.text = clockObject.GetComponent<TextMesh>().text;
    }

    public void ShowHideTopPanel()
    {
        if (!panelIsActive)
        {
            topPanelParent.GetComponent<Animator>().Play("Show");
            panelIsActive = true;
        }
        else
        {
            topPanelParent.GetComponent<Animator>().Play("Hide");
            panelIsActive = false;
        }
    }

    public void OpenHelpPanel()
    {
        if (PlayerPrefs.GetInt("HelpMenuShow") == 0)
        {
            PlayerPrefs.SetInt("HelpMenuShow", 1);
            helpPanel.SetActive(true);
            helpPanel.GetComponent<Animator>().Play("Show");
        }

    }

    public void CloseHelpPanel()
    {
        helpPanel.GetComponent<Animator>().Play("Hide");
        StartCoroutine(DisableHelpPanel());
    }

    IEnumerator DisableHelpPanel()
    {
        yield return new WaitForSeconds(0.5f);
        helpPanel.SetActive(false);
    }

    public void OpenLoadingPanel()
    {
        loadingPanel.SetActive(true);
    }

    public void ResetGridPositions(GameObject token)
    {
        StartCoroutine(DoThing(token));
    }

    IEnumerator DoThing(GameObject token)
    {
        yield return new WaitForSeconds(0f);

        //token.transform.localPosition = new Vector3(0, 0, 0);
        token.transform.position = new Vector3(0, 0, 0);

        //for (int i = 0; i < gridEntitiesObj.transform.childCount; i++)
        //{
        //    if (gridEntitiesObj.transform.GetChild(i).GetComponent<TokenEntity>() && gridEntitiesObj.transform.GetChild(i).GetComponent<MeshRenderer>().materials[0] == correctMaterial)
        //    {
        //        gridEntitiesObj.transform.GetChild(i).localPosition = new Vector3(0, 0, 0);
        //        Debug.Log("DATA FOUND " + gridEntitiesObj.transform.GetChild(i).name);
        //    }
        //    else
        //    {
        //        Debug.Log("NO DATA FOUND");
        //    }
        //}
    }
}
