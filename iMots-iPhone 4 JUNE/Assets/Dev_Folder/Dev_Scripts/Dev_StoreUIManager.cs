﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Dev_StoreUIManager : MonoBehaviour
{
    public static Dev_StoreUIManager instance;
    public Color normalColor;
    public Color purchasedColor;
    public bool isUiElement = true;

    public int verticalIndex;
    public int horizontalIndex;
    public GameObject currentSelectedPage;
    public GameObject loadingPanel;
    public List<GameObject> listOfCurrentPages = new List<GameObject>();
    [Space(10)]
    public List<GameObject> listOfAllPages = new List<GameObject>();
    public List<GameObject> listOf9x13_Pages = new List<GameObject>();
    public List<GameObject> listOf13x9_Pages = new List<GameObject>();
    public List<GameObject> listOf18x18_Pages = new List<GameObject>();
    public List<GameObject> listOfExtra_Pages = new List<GameObject>();
    [Space(10)]
    public List<GameObject> listOfHorButtons = new List<GameObject>();
    public List<GameObject> listOfVerButtons = new List<GameObject>();

    private void OnEnable()
    {
        instance = this;
        if (!isUiElement) isUiElement = true;
    }

    void Start()
    {
        GetPagesAtStart();
    }

    public void OpenLoadingPanel()
    {
        loadingPanel.SetActive(true);
    }

    void Update()
    {

    }

    void GetPagesAtStart()
    {
        listOfCurrentPages.AddRange(listOf9x13_Pages);
        listOfCurrentPages[0].SetActive(true);
        currentSelectedPage = listOfCurrentPages[0];
        listOfVerButtons[0].GetComponent<Button>().image.sprite = listOfVerButtons[0].GetComponent<Button>().spriteState.pressedSprite;
        listOfHorButtons[0].GetComponent<Button>().image.sprite = listOfHorButtons[0].GetComponent<Button>().spriteState.pressedSprite;
    }

    public void EnablePanelsBasedOnSwipe(string swipeDirection)
    {
        if (swipeDirection == "Right")
        {
            for (int i = 0; i < listOfCurrentPages.Count; i++)
            {
                if (horizontalIndex > 0)
                {
                    listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveRight");
                    listOfCurrentPages[horizontalIndex - 1].gameObject.SetActive(true);

                    currentSelectedPage = listOfCurrentPages[horizontalIndex - 1].gameObject;

                    listOfCurrentPages[horizontalIndex - 1].GetComponent<Animator>().Play("MoveRightNew");

                    StartCoroutine(DisableLastActivePanel(0.2f, horizontalIndex));

                    horizontalIndex--;

                    for (int j = 0; j < listOfHorButtons.Count; j++)
                    {
                        if (j != horizontalIndex)
                        {
                            listOfHorButtons[j].GetComponent<Button>().image.sprite = listOfHorButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                        }
                        else
                        {
                            listOfHorButtons[j].GetComponent<Button>().image.sprite = listOfHorButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                        }
                    }

                    break;
                }
            }
        }
        else if (swipeDirection == "Left")
        {
            for (int i = 0; i < listOfCurrentPages.Count; i++)
            {
                if (listOfCurrentPages[i].activeInHierarchy)
                {
                    if (currentSelectedPage != listOfCurrentPages[listOfCurrentPages.Count - 1].gameObject)
                    {
                        listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveLeft");
                        listOfCurrentPages[horizontalIndex + 1].gameObject.SetActive(true);
                        currentSelectedPage = listOfCurrentPages[horizontalIndex + 1].gameObject;
                        listOfCurrentPages[horizontalIndex + 1].GetComponent<Animator>().Play("MoveLeftNew");
                        StartCoroutine(DisableLastActivePanel(0.2f, horizontalIndex));

                        horizontalIndex++;

                        for (int j = 0; j < listOfHorButtons.Count; j++)
                        {
                            if (j != horizontalIndex)
                            {
                                listOfHorButtons[j].GetComponent<Button>().image.sprite = listOfHorButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                            }
                            else
                            {
                                listOfHorButtons[j].GetComponent<Button>().image.sprite = listOfHorButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                            }
                        }

                        break;
                    }
                }
            }

        }
        else if (swipeDirection == "Up")
        {
            if (verticalIndex == 0)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf13x9_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf9x13_Pages, horizontalIndex));

                verticalIndex++;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (verticalIndex == 1)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf18x18_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf13x9_Pages, horizontalIndex));

                verticalIndex++;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (verticalIndex == 2 && horizontalIndex == 0)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOfExtra_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf18x18_Pages, horizontalIndex));

                verticalIndex++;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
        }
        else if (swipeDirection == "Down")
        {
            if (verticalIndex == 1)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf9x13_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf13x9_Pages, horizontalIndex));
                verticalIndex--;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (verticalIndex == 2)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf13x9_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf18x18_Pages, horizontalIndex));
                verticalIndex--;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (verticalIndex == 3)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf18x18_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfExtra_Pages, horizontalIndex));
                verticalIndex--;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
        }

        if (horizontalIndex == 0)
        {
            listOfVerButtons[listOfVerButtons.Count - 1].SetActive(true);
        }
        else
        {
            listOfVerButtons[listOfVerButtons.Count - 1].SetActive(false);
        }
    }

    IEnumerator DisableLastActivePanel(float disableTime, int itemIndex)
    {
        yield return new WaitForSeconds(disableTime);

        listOfCurrentPages[itemIndex].SetActive(false);
        listOfCurrentPages[itemIndex].GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
    }

    IEnumerator DisableLastActivePalenFromList(float disableTime, List<GameObject> anyListName, int listItemIndex)
    {
        yield return new WaitForSeconds(disableTime);

        anyListName[listItemIndex].SetActive(false);
        anyListName[listItemIndex].GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
    }

    public void HorizontalClickButtons()
    {
        //this method will only move right/left

        GameObject btn = EventSystem.current.currentSelectedGameObject;

        if (btn.GetComponent<Dev_HVButtonsLogic>().myIndex > horizontalIndex)
        {
            //move left
            for (int i = 0; i < listOfCurrentPages.Count; i++)
            {
                if (listOfCurrentPages[i].activeInHierarchy)
                {
                    if (currentSelectedPage != listOfCurrentPages[listOfCurrentPages.Count - 1].gameObject)
                    {
                        listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveLeft");
                        listOfCurrentPages[horizontalIndex + 1].gameObject.SetActive(true);
                        currentSelectedPage = listOfCurrentPages[horizontalIndex + 1].gameObject;
                        listOfCurrentPages[horizontalIndex + 1].GetComponent<Animator>().Play("MoveLeftNew");
                        StartCoroutine(DisableLastActivePanel(0.2f, horizontalIndex));

                        horizontalIndex++;

                        for (int j = 0; j < listOfHorButtons.Count; j++)
                        {
                            if (j != horizontalIndex)
                            {
                                listOfHorButtons[j].GetComponent<Button>().image.sprite = listOfHorButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                            }
                            else
                            {
                                listOfHorButtons[j].GetComponent<Button>().image.sprite = listOfHorButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                            }
                        }

                        break;
                    }
                }
            }
        }
        else
        {
            //move right
            for (int i = 0; i < listOfCurrentPages.Count; i++)
            {
                if (horizontalIndex > 0)
                {
                    listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveRight");
                    listOfCurrentPages[horizontalIndex - 1].gameObject.SetActive(true);

                    currentSelectedPage = listOfCurrentPages[horizontalIndex - 1].gameObject;

                    listOfCurrentPages[horizontalIndex - 1].GetComponent<Animator>().Play("MoveRightNew");

                    StartCoroutine(DisableLastActivePanel(0.2f, horizontalIndex));

                    horizontalIndex--;

                    for (int j = 0; j < listOfHorButtons.Count; j++)
                    {
                        if (j != horizontalIndex)
                        {
                            listOfHorButtons[j].GetComponent<Button>().image.sprite = listOfHorButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                        }
                        else
                        {
                            listOfHorButtons[j].GetComponent<Button>().image.sprite = listOfHorButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                        }
                    }

                    break;
                }
            }
        }

        if (horizontalIndex == 0)
        {
            listOfVerButtons[listOfVerButtons.Count - 1].SetActive(true);
        }
        else
        {
            listOfVerButtons[listOfVerButtons.Count - 1].SetActive(false);
        }
    }

    public void VerticalClickButtons()
    {
        GameObject btn = EventSystem.current.currentSelectedGameObject;

        if (btn.GetComponent<Dev_HVButtonsLogic>().myIndex > verticalIndex)
        {
            //move up
            if (verticalIndex == 0)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf13x9_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf9x13_Pages, horizontalIndex));

                verticalIndex++;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (verticalIndex == 1)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf18x18_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf13x9_Pages, horizontalIndex));

                verticalIndex++;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (verticalIndex == 2 && horizontalIndex == 0)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveUp");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOfExtra_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveUpNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf18x18_Pages, horizontalIndex));

                verticalIndex++;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
        }
        else
        {
            //move down
            if (verticalIndex == 1)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf9x13_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf13x9_Pages, horizontalIndex));
                verticalIndex--;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (verticalIndex == 2)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf13x9_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOf18x18_Pages, horizontalIndex));
                verticalIndex--;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
            else if (verticalIndex == 3)
            {
                currentSelectedPage.GetComponent<Animator>().Play("MoveDown");
                listOfCurrentPages.Clear();
                listOfCurrentPages.AddRange(listOf18x18_Pages);
                listOfCurrentPages[horizontalIndex].SetActive(true);
                listOfCurrentPages[horizontalIndex].GetComponent<Animator>().Play("MoveDownNew");
                currentSelectedPage = listOfCurrentPages[horizontalIndex].gameObject;

                StartCoroutine(DisableLastActivePalenFromList(0.2f, listOfExtra_Pages, horizontalIndex));
                verticalIndex--;

                for (int j = 0; j < listOfVerButtons.Count; j++)
                {
                    if (j != verticalIndex)
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.disabledSprite;
                    }
                    else
                    {
                        listOfVerButtons[j].GetComponent<Button>().image.sprite = listOfVerButtons[j].GetComponent<Button>().spriteState.pressedSprite;
                    }
                }
            }
        }
    }
}
