﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dev_RotateStateIcon : MonoBehaviour
{
    public bool rotateState = false;
    [SerializeField]
    RectTransform rect;

    void Start()
    {
        rect = GetComponent<RectTransform>();
    }

    void Update()
    {
        if (rotateState) rect.transform.eulerAngles += new Vector3(0, 0, Time.deltaTime * 200f);
    }
}
