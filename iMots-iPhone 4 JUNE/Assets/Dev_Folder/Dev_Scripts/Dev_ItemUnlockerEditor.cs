﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dev_ItemUnlockerEditor : MonoBehaviour
{

    void Start()
    {

    }

    public void ItemUnlockerF1(int totalIndex)
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            PlayerPrefs.SetInt("F1_Purchased", totalIndex);
        }
    }

    public void ItemUnlockerF2(int totalIndex)
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            PlayerPrefs.SetInt("F2_Purchased", totalIndex);
        }
    }

    public void ItemUnlockerF3(int totalIndex)
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            PlayerPrefs.SetInt("F3_Purchased", totalIndex);
        }
    }

    public void UnlockMethodForYellowPages()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            if (PlayerPrefs.GetInt("F1_Purchased") > 1 || PlayerPrefs.GetInt("F2_Purchased") > 1 || PlayerPrefs.GetInt("F3_Purchased") > 1)
            {
                PlayerPrefs.SetInt("F1_Purchased", 6);
                PlayerPrefs.SetInt("F2_Purchased", 6);
                PlayerPrefs.SetInt("F3_Purchased", 6);
            }
            else
            {
                PlayerPrefs.SetInt("F1_Purchased", 3);
                PlayerPrefs.SetInt("F2_Purchased", 3);
                PlayerPrefs.SetInt("F3_Purchased", 3);
            }
        }
    }

    public void ItemUnlockerF1_Final(int totalIndex)
    {
        PlayerPrefs.SetInt("F1_Purchased", totalIndex);
        Debug.Log("Doing this" + PlayerPrefs.GetInt("F1_Purchased"));
    }

    public void ItemUnlockerF2_Final(int totalIndex)
    {
        PlayerPrefs.SetInt("F2_Purchased", totalIndex);
        Debug.Log("Doing this" + PlayerPrefs.GetInt("F2_Purchased"));
    }

    public void ItemUnlockerF3_Final(int totalIndex)
    {
        PlayerPrefs.SetInt("F3_Purchased", totalIndex);
        Debug.Log("Doing this" + PlayerPrefs.GetInt("F3_Purchased"));
    }

    public void UnlockMethodForYellowPages_Final()
    {
        if (PlayerPrefs.GetInt("F1_Purchased") > 1 || PlayerPrefs.GetInt("F2_Purchased") > 1 || PlayerPrefs.GetInt("F3_Purchased") > 1)
        {
            PlayerPrefs.SetInt("F1_Purchased", 6);
            PlayerPrefs.SetInt("F2_Purchased", 6);
            PlayerPrefs.SetInt("F3_Purchased", 6);
        }
        else
        {
            PlayerPrefs.SetInt("F1_Purchased", 3);
            PlayerPrefs.SetInt("F2_Purchased", 3);
            PlayerPrefs.SetInt("F3_Purchased", 3);
        }
    }
}
