﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Dev_LogManager : MonoBehaviour
{
    public static Dev_LogManager instance;

    private void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// anyText = Your Text, anyColor = R,G,B - Input 1,2,3
    /// </summary>
    /// <param name="anytext"></param>
    /// <param name="anyColor"></param>
    public void PrintLog(string anytext, int anyColor)
    {
        if (anyColor == 1)
        {
            Debug.Log("<color=red>" + anytext + "</color>");
        }
        else if (anyColor == 2)
        {
            Debug.Log("<color=green>" + anytext + "</color>");
        }
        else if (anyColor == 3)
        {
            Debug.Log("<color=blue>" + anytext + "</color>");
        }
    }
}
