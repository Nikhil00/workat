using UnityEngine;
using System.Collections;


public class TranslateYAnimation : RangedAnimation
{


    public override void StartAnimation()
    {
        startValue = transform.position.y;

        base.StartAnimation();
    }


    // Abstract method: implement it with all the modifications you want to apply to the animated object
    protected override void ApplyChanges()
    {
        transform.position = new Vector3(transform.position.x, currentValue, transform.position.z);
    }
}
