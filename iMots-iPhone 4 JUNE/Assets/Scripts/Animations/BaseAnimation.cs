using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BaseEntity))]

public abstract class BaseAnimation : MonoBehaviour
{

    protected BaseEntity entity;

    public bool animating { get; protected set; }

    protected float animationStartTime;

    public float animationSpeed = 1;

    public string animName = "";


    // Use this for initialization
    protected virtual void Start()
    {
        entity = transform.GetComponent("BaseEntity") as BaseEntity;

        entity.startAnimation += startAnimation;

        animating = false;

        if (animName == "")
            animName = GetType().ToString();
    }


    protected virtual void OnDisable()
    {
        entity.startAnimation -= startAnimation;
    }


    // Update is called once per frame
    protected virtual void Update()
    {
        if (animating)
            ApplyChanges();
    }


    public virtual void StartAnimation()
    {
        animating = true;

        animationStartTime = Time.time;
    }


    public virtual void StopAnimation()
    {
        // ApplyChanges one more time, to make sure the final values are the boundaries
        ApplyChanges();

        animating = false;

        entity.AnimationStopped(animName);
    }


    // Abstract method: implement it with all the modifications you want to apply to the animated object
    protected abstract void ApplyChanges();


    //	public void RegisterStartAnimationToMe(BaseEntity asker)
    //	{
    //		asker.startAnimation += startAnimation;
    //	}


    #region Events

    void startAnimation(string animationToPlay)
    {
        if (animationToPlay == "" || animationToPlay == animName)
        {
            Debug.Log(name + " Event -> startAnimation " + animName);
            StartAnimation();
        }
    }

    #endregion
}
