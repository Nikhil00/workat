using UnityEngine;
using System.Collections;


public class TranslateParent3DAnimation : Ranged3DAnimation
{

    public Vector3 slideOffset;

    public override void StartAnimation()
    {
        startValue = transform.parent.position;

        //		aValue = new Vector3(transform.parent.position.x, transform.parent.position.y, transform.parent.position.z);
        //
        //		if (IsGoingAToB)
        //		{
        //			bValue = new Vector3(aValue.x - slideOffset.x, aValue.y - slideOffset.y, aValue.z - slideOffset.z);
        //		}
        //		else
        //		{
        //			bValue = new Vector3(aValue.x + slideOffset.x, aValue.y + slideOffset.y, aValue.z + slideOffset.z);
        //		}
        //		
        //		IsGoingAToB = false;

        if (IsGoingAToB)
        {
            bValue = startValue;
            aValue = new Vector3(bValue.x - slideOffset.x, bValue.y - slideOffset.y, bValue.z - slideOffset.z);
        }
        else
        {
            aValue = startValue;
            bValue = new Vector3(aValue.x + slideOffset.x, aValue.y + slideOffset.y, aValue.z + slideOffset.z);
        }

        base.StartAnimation();
    }


    // Abstract method: implement it with all the modifications you want to apply to the animated object
    protected override void ApplyChanges()
    {
        transform.parent.position = new Vector3(currentValue.x, currentValue.y, currentValue.z);
    }


    //	public override void StopAnimation()
    //	{
    //		transform.position = new Vector3(currentValue.x, currentValue.y, currentValue.z);
    //		
    //		base.StopAnimation();
    //	}
}
