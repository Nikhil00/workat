using UnityEngine;
using System.Collections;

public abstract class Ranged3DAnimation : BaseAnimation
{

    public bool IsGoingAToB;

    public bool startByGoingFromAToB = true;

    public Vector3 aValue, bValue;

    public Vector3 currentValue { get; protected set; }
    public Vector3 startValue { get; protected set; }

    protected float animationSpeedMultiplier; // Used to speed up the animation if the startValue is not one of the boundaries so that the animation actually appears to be at the same speed instead of slower


    protected override void Start()
    {
        base.Start();
        IsGoingAToB = !startByGoingFromAToB;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (animating)
        {
            float animTime = (Time.time - animationStartTime) * animationSpeed * animationSpeedMultiplier;
            if (IsGoingAToB)
            {
                currentValue = Vector3.Lerp(startValue, bValue, animTime);

                if (animTime >= 1f)
                {
                    currentValue = bValue;
                    StopAnimation();
                }
            }
            else
            {
                currentValue = Vector3.Lerp(startValue, aValue, animTime);

                if (animTime >= 1f)
                {
                    currentValue = aValue;
                    StopAnimation();
                    return;
                }
            }

            ApplyChanges();
        }
    }


    public override void StartAnimation()
    {
        base.StartAnimation();

        currentValue = startValue;

        if (!IsGoingAToB)
        {
            IsGoingAToB = true;
            if (bValue == currentValue)
                animationSpeedMultiplier = 1;
            else
                animationSpeedMultiplier = Vector3.Distance(bValue, aValue) / Vector3.Distance(bValue, currentValue);
        }
        else
        {
            IsGoingAToB = false;
            if (aValue == currentValue)
                animationSpeedMultiplier = 1;
            else
                animationSpeedMultiplier = Vector3.Distance(aValue, bValue) / Vector3.Distance(aValue, currentValue);
        }
    }
}
