using UnityEngine;
using System.Collections;


public class FadeAnimation : RangedAnimation
{

    public bool alsoFadeMyText = false;

    public override void StartAnimation()
    {
        startValue = GetComponent<Renderer>().material.color.a;

        base.StartAnimation();
    }


    // Abstract method: implement it with all the modifications you want to apply to the animated object
    protected override void ApplyChanges()
    {
        GetComponent<Renderer>().material.color = new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, currentValue);

        if (alsoFadeMyText)
        {
            Renderer textRenderer = transform.Find("Text").GetComponent<Renderer>();
            textRenderer.material.color = new Color(textRenderer.material.color.r, textRenderer.material.color.g, textRenderer.material.color.b, currentValue);
        }
    }
}
