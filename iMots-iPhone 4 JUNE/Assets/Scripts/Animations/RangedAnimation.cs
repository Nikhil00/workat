using UnityEngine;
using System.Collections;

public abstract class RangedAnimation : BaseAnimation
{

    protected bool IsGoingAToB;

    public bool startByGoingFromAToB = true;

    public float aValue, bValue;

    protected float currentValue, startValue;

    protected float animationSpeedMultiplier; // Used to speed up the animation if the startValue is not one of the boundaries so that the animation actually appears to be at the same speed instead of slower


    protected override void Start()
    {
        base.Start();
        //		IsGoingAToB = !startByGoingFromAToB;
        SetGoAToB(startByGoingFromAToB);
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (animating)
        {
            float animTime = (Time.time - animationStartTime) * animationSpeed * animationSpeedMultiplier;
            if (IsGoingAToB)
            {
                currentValue = Mathf.Lerp(startValue, bValue, animTime);

                if (animTime >= 1f)
                {
                    currentValue = bValue;
                    StopAnimation();
                }
            }
            else
            {
                currentValue = Mathf.Lerp(startValue, aValue, animTime);

                if (animTime >= 1f)
                {
                    currentValue = aValue;
                    StopAnimation();
                    return;
                }
            }

            ApplyChanges();
        }
    }


    public override void StartAnimation()
    {
        base.StartAnimation();

        currentValue = startValue;

        if (!IsGoingAToB)
        {
            IsGoingAToB = true;
            if (bValue == currentValue)
                animationSpeedMultiplier = 1;
            else
                animationSpeedMultiplier = (bValue - aValue) / (bValue - currentValue);
        }
        else
        {
            IsGoingAToB = false;
            if (aValue == currentValue)
                animationSpeedMultiplier = 1;
            else
                animationSpeedMultiplier = (aValue - bValue) / (aValue - currentValue);
        }
    }


    public void SetGoAToB(bool aToB)
    {
        IsGoingAToB = !aToB;
    }
}
