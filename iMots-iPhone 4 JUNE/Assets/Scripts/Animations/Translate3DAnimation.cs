using UnityEngine;
using System.Collections;


public class Translate3DAnimation : Ranged3DAnimation
{


    public override void StartAnimation()
    {
        startValue = transform.position;

        base.StartAnimation();
    }


    // Abstract method: implement it with all the modifications you want to apply to the animated object
    protected override void ApplyChanges()
    {
        transform.position = new Vector3(currentValue.x, currentValue.y, currentValue.z);
    }


    //	public override void StopAnimation()
    //	{
    //		transform.position = new Vector3(currentValue.x, currentValue.y, currentValue.z);
    //		
    //		base.StopAnimation();
    //	}
}
