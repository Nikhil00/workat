using UnityEngine;
using System.Collections;


public class GrowAnimation : RangedAnimation
{


    public override void StartAnimation()
    {
        startValue = transform.localScale.x;

        base.StartAnimation();
    }


    // Abstract method: implement it with all the modifications you want to apply to the animated object
    protected override void ApplyChanges()
    {
        transform.localScale = new Vector3(currentValue, transform.localScale.y, currentValue);
    }
}
