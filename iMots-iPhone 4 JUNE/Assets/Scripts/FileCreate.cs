﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Text;

public class FileCreate : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
				SaveItemInfo ();
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void SaveItemInfo ()
		{
				string path = null;
				#if UNITY_EDITOR
				path = "Assets/Resources/XMLs/XMLDatabase-iPad.txt";
				#endif
				#if UNITY_STANDALONE
				// You cannot add a subfolder, at least it does not work for me
				path = "MyGame_Data/Resources/ItemInfo.json";
				#endif

				StringBuilder sb = new StringBuilder ();
				for (int i = 1; i <= 250; i++) {
						if (i <= 9) {
								sb.Append ("Ipad-0-0-00" + i);
						} else if (i > 9 && i <= 99) {
								sb.Append ("Ipad-0-0-0" + i);
						} else {
								sb.Append ("Ipad-0-0-" + i);
						}

						sb.Append (Environment.NewLine);

				}
				//01
				for (int i = 1; i <= 250; i++) {
						if (i <= 9) {
								sb.Append ("Ipad-0-1-00" + i);
						} else if (i > 9 && i <= 99) {
								sb.Append ("Ipad-0-1-0" + i);
						} else {
								sb.Append ("Ipad-0-1-" + i);
						}

						sb.Append (Environment.NewLine);

				}
				// 02
				for (int i = 1; i <= 250; i++) {
						if (i <= 9) {
								sb.Append ("Ipad-0-2-00" + i);
						} else if (i > 9 && i <= 99) {
								sb.Append ("Ipad-0-2-0" + i);
						} else {
								sb.Append ("Ipad-0-2-" + i);
						}

						sb.Append (Environment.NewLine);

				}
				// 10
				for (int i = 1; i <= 250; i++) {
						if (i <= 9) {
								sb.Append ("Ipad-1-0-00" + i);
						} else if (i > 9 && i <= 99) {
								sb.Append ("Ipad-1-0-0" + i);
						} else {
								sb.Append ("Ipad-1-0-" + i);
						}

						sb.Append (Environment.NewLine);

				}

				// 11
				for (int i = 1; i <= 250; i++) {
						if (i <= 9) {
								sb.Append ("Ipad-1-1-00" + i);
						} else if (i > 9 && i <= 99) {
								sb.Append ("Ipad-1-1-0" + i);
						} else {
								sb.Append ("Ipad-1-1-" + i);
						}

						sb.Append (Environment.NewLine);

				}

				//12
				for (int i = 1; i <= 250; i++) {
						if (i <= 9) {
								sb.Append ("Ipad-1-2-00" + i);
						} else if (i > 9 && i <= 99) {
								sb.Append ("Ipad-1-2-0" + i);
						} else {
								sb.Append ("Ipad-1-2-" + i);
						}

						sb.Append (Environment.NewLine);

				}
				// 20
				for (int i = 1; i <= 250; i++) {
						if (i <= 9) {
								sb.Append ("Ipad-2-0-00" + i);
						} else if (i > 9 && i <= 99) {
								sb.Append ("Ipad-2-0-0" + i);
						} else {
								sb.Append ("Ipad-2-0-" + i);
						}

						sb.Append (Environment.NewLine);

				}

				// 21
				for (int i = 1; i <= 250; i++) {
						if (i <= 9) {
								sb.Append ("Ipad-2-1-00" + i);
						} else if (i > 9 && i <= 99) {
								sb.Append ("Ipad-2-1-0" + i);
						} else {
								sb.Append ("Ipad-2-1-" + i);
						}

						sb.Append (Environment.NewLine);

				}

				//22
				for (int i = 1; i <= 250; i++) {
						if (i <= 9) {
								sb.Append ("Ipad-2-2-00" + i);
						} else if (i > 9 && i <= 99) {
								sb.Append ("Ipad-2-2-0" + i);
						} else {
								sb.Append ("Ipad-2-2-" + i);
						}

						sb.Append (Environment.NewLine);

				}
				string str = sb.ToString ();

				using (FileStream fs = new FileStream (path, FileMode.Create)) {
						using (StreamWriter writer = new StreamWriter (fs)) {
								writer.Write (str);
						}
				}
				#if UNITY_EDITOR
				UnityEditor.AssetDatabase.Refresh ();
				#endif
		}
}
