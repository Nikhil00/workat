using UnityEngine;
using System.Collections;

public class LoadingSpinner : MonoBehaviour
{
	
		protected bool spin ;
	
		public float frameDuration = 0.1f;
		protected float frameStartTime;
	
		protected int currentFrame;

		// Use this for initialization
		void Start ()
		{
				spin = false;
//				spin = true;

		}
	
	
		// Update is called once per frame
		void Update ()
		{
				if (spin) {
						
						if (Time.time >= frameStartTime + frameDuration) {
								
								currentFrame = (currentFrame + 1) % 12;
				
								float xOffset = (currentFrame % 4) * 0.25f;
								float yOffset = (3 - (Mathf.FloorToInt (currentFrame / 4f))) * 0.25f;
				
//								Debug.Log ("Spinner texture offset: " + currentFrame + " => " + xOffset + "," + yOffset);
				
								GetComponent<Renderer> ().material.mainTextureOffset = new Vector2 (xOffset, yOffset);
				
								frameStartTime = Time.time;
						}
				}
		}

	
		public void StartSpinning ()
		{
				spin = true;
				currentFrame = 11;
		
				frameStartTime = Time.time - frameDuration;
		}

	
		public void StopSpinning ()
		{
				spin = false;
		}
	
}
