﻿using UnityEngine;
using System.Collections;

public class DeviceDetector : MonoBehaviour
{
		public static DeviceDetector instance;

		void Awake ()
		{
				if (instance == null) {
						// Makes the object target not be destroyed automatically when loading a new scene
						DontDestroyOnLoad (gameObject); 
						instance = this;
				} else if (instance != this) {
						Destroy (gameObject);
				}
				GetDeviceType ();
		}
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void GetDeviceType ()
		{
			#if UNITY_IOS
				if (UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5S) {
						Debug.Log ("Unity iPhone 5S");
				}
			#endif
		}
}
