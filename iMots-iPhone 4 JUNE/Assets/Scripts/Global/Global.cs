using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Global
{

    #region enums

    public enum GridState
    {
        Error = -1,
        Empty,
        Started,
        Completed

    }

    public enum Difficulty
    {
        Error = -1,
        Easy,
        Medium,
        Hard,
        vEasy,
        vMedium,
        All

    }

    public enum Size
    {
        Error = -1,
        Small,
        Medium,
        Big,
        VSmall,
        VMedium,

    }

    public enum DefinitionSize
    {
        Error = -1,
        Quarter,
        Half,
        ThreeQuarters,
        Full

    }

    public enum DefinitionPosition
    {
        Error = -1,
        Upper,
        Lower

    }

    public enum Direction
    {
        Error = -1,
        Horizontal,
        Vertical

    }

    public enum ArrowDirection
    {
        Error = -1,
        Horizontal,
        Vertical,
        Both

    }

    public enum TileType
    {
        Error = -1,
        Definition,
        Letter

    }

    public enum GameMode
    {
        Error = -1,
        DragNDrop,
        Keyboard

    }

    public enum KeyboardState
    {
        Error = -1,
        Off,
        Unfocused,
        Focused

    }
    // Off = Not visible nor receving input / Unfocused = Transparent and not receving input / Focused = Not transparent and receving input

    public enum AlertViews
    {
        Error = -1,
        ResetGrid,
        ResetAllGrids,
        RateThisApp,
        SolutionToDefinitionAlone,
        SolutionToDefinitionDouble,
        NoWordSolutionStock,
        SolutionAlreadyCorrect,
        InAppProductData,
        LaunchGridCompletedReset,
        OpenRealVersionPage

    }

    public enum EndOfGridAnim
    {
        Error = -1,
        Waterfall,
        Helium,
        Grenade,
        RotateZ,
        Grow,
        BigCollision

    }

    public enum LogicType
    {
        Error = -1,
        GameLogic,
        MenuLogic,
        StoreLogic

    }

    #endregion


    #region structs

    public struct GridSize
    {
        public Size size;
        public int xSize, ySize;

        public GridSize(int s)
        {
            size = (Size)s;
            Debug.Log("Nik - Global GrideSize Is : " + size + " Method Paramater Is : " + s);
            switch (size)
            {
                case Size.Small:
                    // iPad
                    if (Global.iPadVersion)
                    {
                        xSize = 9;
                        ySize = 13;
                    }
                    // iPhone/iPod
                    else
                    {
                        xSize = 8;
                        ySize = 5;
                    }
                    break;

                case Size.Medium:
                    // iPad
                    if (Global.iPadVersion)
                    {
                        xSize = 13;
                        ySize = 9;
                    }
                    // iPhone/iPod
                    else
                    {
                        xSize = 10;
                        ySize = 10;
                    }
                    break;

                case Size.Big:
                    // iPad
                    if (Global.iPadVersion)
                    {
                        xSize = 18;
                        ySize = 18;
                    }
                    // iPhone/iPod
                    else
                    {
                        xSize = 10;
                        ySize = 10;
                    }
                    break;
                //  Nik - Add Code
                case Size.VSmall:
                    // iPad
                    if (Global.iPadVersion)
                    {
                        xSize = 8;
                        ySize = 5;
                    }
                    // iPhone/iPod
                    else
                    {
                        xSize = 9;
                        ySize = 13;
                    }
                    break;
                case Size.VMedium:
                    // iPad
                    if (Global.iPadVersion)
                    {
                        xSize = 10;
                        ySize = 10;
                    }
                    // iPhone/iPod
                    else
                    {
                        xSize = 13;
                        ySize = 9;
                    }
                    break;


                default:
                    xSize = 0;
                    ySize = 0;
                    break;
            }
        }
    }


    public struct Definition
    {
        public string text;
        // The actual definition

        public DefinitionSize defSize;
        // DefinitionSize of this def (see enum DefinitionSize)
        public DefinitionPosition defPos;
        // DefinitionPosition of this def (see enum DefinitionPosition)

        public Answer answer;
        // The answer to this definition (see struct Answer)

        // Used as default constructor to init an empty def (structs don't support parameterless constructors)
        public Definition(string def)
        {
            text = def;
            defPos = DefinitionPosition.Error;
            defSize = DefinitionSize.Error;
            answer = new Answer(def);
        }


        public Definition(string def, string defSizeString, string defPosString, string ans, string ansDirString, int xAns, int yAns, string ansIsHyphen)
        {
            // Definition
            text = def;

            // Take care of the lowercases
            defPosString = defPosString.ToUpper();
            defSizeString = defSizeString.ToUpper();


            if (defPosString == "LOWER")
                defPos = DefinitionPosition.Lower;
            else if (defPosString == "UPPER")
                defPos = DefinitionPosition.Upper;
            else
            {
                defPos = DefinitionPosition.Error;
                if (text != "")
                    Debug.LogError("Couldn't create Definition because of invalid [defPosString]: " + defPosString);
            }


            if (defSizeString == "A_QUARTER")
                defSize = DefinitionSize.Quarter;
            else if (defSizeString == "HALF")
                defSize = DefinitionSize.Half;
            else if (defSizeString == "THREE_QUARTERS")
                defSize = DefinitionSize.ThreeQuarters;
            else if (defSizeString == "SIMPLE")
                defSize = DefinitionSize.Full;
            else
            {
                defSize = DefinitionSize.Error;
                if (text != "")
                    Debug.LogError("Couldn't create Definition because of invalid [defSizeString]: " + defSizeString);
            }

            // Answer
            answer = new Answer(ans, ansDirString, xAns, yAns, ansIsHyphen);

            if (answer.ansDir == Direction.Error && answer.text != "")
                Debug.LogError("Couldn't create Definition because of an error during answer creation.");
        }
    }


    public struct Answer
    {
        public string text;
        // The actual answer

        public Direction ansDir;
        // Direction of this ans (see enum Direction)

        public int xStart, yStart;
        // x and y position of this answer's startTile

        public bool isHyphen;
        // is it an acronym

        // Used as default constructor to init an empty ans (structs don't support parameterless constructors)
        public Answer(string ans)
        {
            text = ans;
            ansDir = Direction.Error;
            xStart = 0;
            yStart = 0;
            isHyphen = false;
        }


        public Answer(string ans, string ansDirString, int x, int y, string ansIsHyphenString)
        {
            text = ans;

            if (ansDirString == "ACROSS")
                ansDir = Direction.Horizontal;
            else if (ansDirString == "DOWN")
                ansDir = Direction.Vertical;
            else
            {
                ansDir = Direction.Error;
                if (text != "")
                    Debug.LogError("Couldn't create Answer because of invalid [ansDir]: " + ansDir);
            }

            xStart = x;
            yStart = y;


            if (ansIsHyphenString == "yes")
                isHyphen = true;
            else
                isHyphen = false;
        }
    }


    public struct TilesSelection
    {
        public List<LetterTileEntity> tiles;
        public int currentIndex;
        public Direction direction;

        // Used as default constructor to init an empty tiles selection (structs don't support parameterless constructors)
        public TilesSelection(string emptyStr)
        {
            tiles = new List<LetterTileEntity>();
            currentIndex = 0;
            direction = Direction.Error;
        }
    }


    public struct WordValidation
    {
        public float lastLetterValidationTime;
        public LetterEntity[] tilesToValidate;
        public int currentLetterBeingValidatedIndex;

        // Used as default constructor to init an empty word validation (structs don't support parameterless constructors)
        public WordValidation(string emptyStr)
        {
            tilesToValidate = new LetterEntity[0];
            currentLetterBeingValidatedIndex = 0;
            lastLetterValidationTime = 0;
        }
    }

    #endregion


    #region Vars and Static methods

    public class Global
    {
        // Versioning
        public static bool iPadVersion = true;
        public static bool adEnabled = true;

        // SFX
        //		public static float SFXVolume = 0.3f;

        // Apps URLs
        public static string rateThisAppURLiPad = "http://itunes.apple.com/fr/app/imotsfleches-deluxe/id410018460?mt=8";
        public static string rateThisAppURLiPhone = "http://itunes.apple.com/fr/app/imotsfleches/id394884647?mt=8";
        public static string rateThisAppURLiPadFree = "http://itunes.apple.com/fr/app/free-fleches-hd/id484554803?mt=8";
        public static string rateThisAppURLiPhoneFree = "http://itunes.apple.com/fr/app/free-fleches/id484552337?mt=8";
        public static string rateThisAppURLAndroid = "market://details?id=com.digdog.imotsfleches";
        public static string rateThisAppURLAndroidFree = "market://details?id=com.digdog.freefleches";


        // Methods
        public static string ConvertToStringID(int id)
        {
            string gridIndexString;
            if (id < 10)
                gridIndexString = "00" + id;
            else if (id < 100)
                gridIndexString = "0" + id;
            else
                gridIndexString = id.ToString();

            return gridIndexString;
        }


        public static string ConvertToStringSize(int size)
        {
            string gridSizeString = "Error";
            Debug.Log("Nik - Global GrideSize Is : " + size + " by ConverToStringSize");
            if (Global.iPadVersion)
            {
                if (size == 0)
                    gridSizeString = "9x13";
                else if (size == 1)
                    gridSizeString = "13x9";
                else if (size == 2)
                    gridSizeString = "18x18";
                // Dev || 06-06-2020 || Code Change
                else if (size == 3)
                    gridSizeString = "8x5";
                else if (size == 4)
                    gridSizeString = "10x10";
                //TODO || GRID 1
            }
            else
            {
                if (size == 0)
                    gridSizeString = "8x5";
                else if (size == 1)
                    gridSizeString = "10x10";
            }

            return gridSizeString;
        }


        public static string ConvertToStringDifficulty(int difficulty)
        {
            string gridDifficultyString = "Error";

            if (difficulty == 0)
                gridDifficultyString = "Force 1";
            else if (difficulty == 1)
                gridDifficultyString = "Force 2";
            else if (difficulty == 2)
                gridDifficultyString = "Force 3";
            else if (difficulty == 3)
                gridDifficultyString = "Force 4";
            else if (difficulty == 4)
                gridDifficultyString = "Force 5";

            return gridDifficultyString;
        }


        public static List<string> TrimLockedGrids(List<string> srcList)
        {
            Debug.Log("Nik - Header Unloak Level : " + srcList.Count);
            List<string> destList = new List<string>();
            //						Debug.Log ("scList size " + srcList.Count);
            int cout = 0;
            foreach (string header in srcList)
            {
                //								Debug.Log ("header -  " + header);
                if (!PlayerPrefs.HasKey(header))
                {
                    //										Debug.Log ("grid " + header + " is locked so it won't be displayed");
                    Debug.Log("Nik - Header in not Enter");
                    continue;
                }
                else
                {
                    cout++;
                    //										Debug.Log (header + " c " + cout);
                    //										if (PlayerPrefs.HasKey (header)) {
                    //												Debug.Log ("H " + header + " c " + cout);
                    //										} else {
                    if (!destList.Contains(header))
                    {
                        destList.Add(header);
                        // Debug.Log("Nik - Header In Enter to line : " + header);
                    }
                    //										}
                    //										Debug.Log ("grid " + header + " is unlocked locked so " + cout);

                }
            }
            //						Debug.Log ("destination list size " + destList.Count);
            return destList;
        }
    }

    #endregion
}