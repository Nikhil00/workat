using UnityEngine;
using System.Collections;

//using System;
//using System.Runtime.Serialization;
//using System.Reflection;

using Global;


public class DefinitionTile : Tile{
	
	
	public Definition[] definitions = new Definition[2]; // A def tile can have up to 2 definitions
	
	
	public DefinitionTile (Grid grid, int x, int y, string def, string defSizeString, string defPosString, string ans, string ansDirString, int xAns, int yAns, string ansIsHyphen) : base(grid, x,y)
	{	
		definitions[0] = new Definition(""); // Init a default empty def
		definitions[1] = new Definition(""); // Init a default empty def
		
		AddDefinition(def, defSizeString, defPosString, ans, ansDirString, xAns, yAns, ansIsHyphen);
	}
	
	
	public void AddDefinition(string def, string defSizeString, string defPosString, string ans, string ansDirString, int xAns, int yAns, string ansIsHyphen)
	{
		if (defPosString == "LOWER")
			definitions[1] = new Definition(def, defSizeString, defPosString, ans, ansDirString, xAns, yAns, ansIsHyphen);
		else if (defPosString == "UPPER")
			definitions[0] = new Definition(def, defSizeString, defPosString, ans, ansDirString, xAns, yAns, ansIsHyphen);
		else
			Debug.LogError(this.GetType() + " - Couldn't add definition because of invalid [defPosString]: " + defPosString);
	}
	
	
	public override TileType GetTileType()
	{
		return TileType.Definition;
	}
	
	
	// Fill the letter tiles concerned by all my definitions with their answers
	public void FillLetterTilesWithAnswers()
	{
		// No need for a security check for the upper def as every def tile has an upper def
		FillLetterTilesWithAnswer(0);

		// Check if there is also a lower def for this tile
		if (definitions[1].defPos != DefinitionPosition.Error)
		{
			FillLetterTilesWithAnswer(1);
		}
	}
	
	
	// Fill the letter tiles concerned by the given definition with their answers
	public void FillLetterTilesWithAnswer(int definitionIndex)
	{
		// Check if the given defintion index is valid
		if (definitionIndex < 0 || definitionIndex > 1 || definitions[definitionIndex].defPos == DefinitionPosition.Error)
		{
			Debug.LogError(this.GetType() + " - Couldn't fill letter tiles with answer because of invalid [definitionIndex] " + definitionIndex);
			return;
		}
		
		
		for (int i = 0; i < definitions[definitionIndex].answer.text.Length; i++)
		{
			Tile tileToFill = null;
			
			if (definitions[definitionIndex].answer.ansDir == Direction.Horizontal)
				tileToFill = grid.tiles[definitions[definitionIndex].answer.yStart, definitions[definitionIndex].answer.xStart + i];
			else if (definitions[definitionIndex].answer.ansDir == Direction.Vertical)
				tileToFill = grid.tiles[definitions[definitionIndex].answer.yStart + i, definitions[definitionIndex].answer.xStart];
			
			
			if (tileToFill == null || tileToFill != null && tileToFill.GetTileType() != TileType.Letter)
			{
				Debug.LogError(this.GetType() + " - Couldn't fill letter tiles with answer because of trying to access an invalid tile");
			}
			else
			{
				(tileToFill as LetterTile).answerLetter = definitions[definitionIndex].answer.text[i].ToString().ToUpper();
				// Handling of the arrows for the first tile of the answer
				if (i == 0)
				{
					(tileToFill as LetterTile).isAnswerStart = true;
					
					if (definitions[definitionIndex].answer.ansDir == Direction.Horizontal)
					{
						if ((tileToFill as LetterTile).arrowDirection == ArrowDirection.Error)
							(tileToFill as LetterTile).arrowDirection = ArrowDirection.Horizontal;
						else if ((tileToFill as LetterTile).arrowDirection == ArrowDirection.Vertical)
							(tileToFill as LetterTile).arrowDirection = ArrowDirection.Both;
								
						if (definitions[definitionIndex].answer.yStart != yPos)
						{
							(tileToFill as LetterTile).needDottedLine = true;
						}
					}
					else
					{
						if ((tileToFill as LetterTile).arrowDirection == ArrowDirection.Error)
							(tileToFill as LetterTile).arrowDirection = ArrowDirection.Vertical;
						else if ((tileToFill as LetterTile).arrowDirection == ArrowDirection.Horizontal)
							(tileToFill as LetterTile).arrowDirection = ArrowDirection.Both;
						
						if (definitions[definitionIndex].answer.xStart != xPos)
						{
							(tileToFill as LetterTile).needDottedLine = true;
						}
					}
				}
				
				// Handling of the hyphens for every tile of the answer
				if (definitions[definitionIndex].answer.isHyphen)
				{
					if (definitions[definitionIndex].answer.ansDir == Direction.Horizontal)
					{
						if ((tileToFill as LetterTile).hyphenDirection == ArrowDirection.Error)
							(tileToFill as LetterTile).hyphenDirection = ArrowDirection.Horizontal;
						else if ((tileToFill as LetterTile).hyphenDirection == ArrowDirection.Vertical)
							(tileToFill as LetterTile).hyphenDirection = ArrowDirection.Both;
					}
					else
					{
						if ((tileToFill as LetterTile).hyphenDirection == ArrowDirection.Error)
							(tileToFill as LetterTile).hyphenDirection = ArrowDirection.Vertical;
						else if ((tileToFill as LetterTile).hyphenDirection == ArrowDirection.Horizontal)
							(tileToFill as LetterTile).hyphenDirection = ArrowDirection.Both;
					}
				}
			}
		}
	}
}
