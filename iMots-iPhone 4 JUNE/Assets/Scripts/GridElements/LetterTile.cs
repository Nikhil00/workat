using UnityEngine;
using System.Collections;

//using System;


public class LetterTile : Tile{
	
	public string answerLetter = ""; // Right letter for this tile (use of string over char because char can't be empty)
	public string guessLetter = ""; // User guess for this tile (use of string over char because char can't be empty)
	public bool alreadyMoved = false;
	
	public bool isAnswerStart = false; // If true, this tile is the first tile of an answer.
	public Global.ArrowDirection arrowDirection = Global.ArrowDirection.Error; // Only used if isAnswerStart
	public bool needDottedLine = false; // If true, will need to add a dotted line to the arrow texture. Only used if isAnswerStart
	
	public bool dontSwap = false; // If true, this tile has been guessed by the user and thus needs not to be swapped when shuffling the tokens
	
	public Global.ArrowDirection hyphenDirection = Global.ArrowDirection.Error; // If not Error, this tile is part of an acronym and thus will need to be added the respective dotted line border
	
	public LetterTile (Grid grid, int x, int y) : base(grid, x,y)
	{
	}
	
}
