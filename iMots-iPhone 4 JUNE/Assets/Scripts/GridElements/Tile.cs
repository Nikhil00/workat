using UnityEngine;
using System.Collections;
using Global;


//[Serializable ()]
public class Tile {
	
	protected Grid grid;
	
	public int xPos, yPos;
	
	
	public Tile (Grid grid, int x, int y)
	{
		xPos = x;
		yPos = y;
		
		this.grid = grid;
	}
	
	
	public virtual TileType GetTileType()
	{
		return TileType.Letter;
	}
}
