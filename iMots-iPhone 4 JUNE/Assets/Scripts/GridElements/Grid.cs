using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//using System;
//using System.Runtime.Serialization;
//using System.Reflection;

using Global;


public class Grid {

	
	public string name;
	public GridSize size;
	public Difficulty difficulty;
	public GridState state;
	
	public Tile[,] tiles;
	
	public GameMode gameMode = GameMode.Error;
	
	
	public Grid () {
	}
	
	
	public Grid (string id) {
		name = id;
		
		string[] splittedId = id.Split('-');
		
		if (splittedId.Length != 4)
		{
			Debug.LogWarning("Be careful, grid ID " + id +" doesn't match the intended format");
		}
		
		Debug.Log("Nik - Grid In splittedId : " + id.Split('-') + " || " + splittedId[0]);
		if (splittedId.Length > 2)
		{
			size = new GridSize(int.Parse(splittedId[1]));
			Debug.Log("Nik - Grid - size " + size + "  : value " + new GridSize(int.Parse(splittedId[1])));
			difficulty = (Difficulty)int.Parse(splittedId[2]);
		}
		
		tiles = new Tile[(int)size.ySize, (int)size.xSize];
	}
	
	
	public void CreateDefinitionTile (int x, int y, string def, string defSizeString, string defPosString, string ans, string ansDirString, int xAns, int yAns, string ansIsHyphen)
	{
		// If this tile is already created and is a definition tile => just add another def
		if(tiles[y, x] != null && tiles[y, x].GetTileType() == TileType.Definition)
			(tiles[y, x] as DefinitionTile).AddDefinition(def, defSizeString, defPosString, ans, ansDirString, xAns, yAns, ansIsHyphen);
		// If this tile is already created but is NOT a definition tile => error
		else if(tiles[y, x] != null)
			Debug.LogError(this.GetType() + " - Couldn't add a definition to the tile [" + x + "," + y + "] because it is not a definition tile");
		// Else create it
		else		
			tiles[y, x] = new DefinitionTile(this, x, y, def, defSizeString, defPosString, ans, ansDirString, xAns, yAns, ansIsHyphen);
	}
	
	
	public void FillRemainingWithLetterTiles()
	{
		// First pass to create the empty letter tiles
		for (int i = 0; i < size.xSize; i++)
		{			
			for (int j = 0; j < size.ySize; j++)
			{	
				// Only create letter tiles where there is not already a definition tile
				if (tiles[j,i] == null)
					tiles[j,i] = new LetterTile(this, i, j);
			}
		}
		
		// Second pass to fill their answerLetter
		for (int i = 0; i < size.xSize; i++)
		{			
			for (int j = 0; j < size.ySize; j++)
			{	
				// Look for the definition tiles
				if (tiles[j,i].GetTileType() == TileType.Definition)
					(tiles[j, i] as DefinitionTile).FillLetterTilesWithAnswers();
			}
		}
	}
}
