using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuLogic : Logic
{

    #region Vars

    // GO references
    public GameObject horizontalPagesOverlayGO, verticalPagesOverlayGO, inputDisablingMaskGO, closeButtonGO;
    protected Transform pagesGOTransform;

    public List<MenuPageEntity>[] pages;

    List<string>[] easyGrids, mediumGrids, hardGrids, veasyGrids, vmediumGrids;
    // protected const int nbGridSizes = 3;
    // Nik - Add Code
    protected const int nbGridSizes = 5;

    ButtonMenuGridInstance buttonMenuGridInstance;

    public Vector2 currentPage;
    [HideInInspector]
    public bool currentPageAtInitFound = false;

    protected Object gridPagePrefabRessource;
    [HideInInspector]
    public Object buttonMenuGridInstancePrefabRessource;

    // Grids buttons
    public Rect buttonsPlacementZoneIPad;// = new Rect(-0.445f, -0.42f, 0.5f, 0.38f);
    public Rect buttonsPlacementZoneIPhone;// = new Rect(-0.445f, -0.42f, 0.5f, 0.38f);
    [HideInInspector]
    public Rect buttonsPlacementZone;// = new Rect(-0.445f, -0.42f, 0.5f, 0.38f);

    public float horizontalSpaceBetweenGridButtons = 0.05f;
    public Vector2 maxGridsButtonPerPage = new Vector2(8, 5);
    protected int nbButtonsPerPage;

    // [HideInInspector]
    public Vector2 spaceAllowedForAGridButton, sizeOfAGridButton;

    // Grids pages overlays
    public float spaceBetweenOverlayIcons = 0.05f;
    [HideInInspector]
    public GameObject[] verticalOverlayIcons, horizontalOverlayIcons;
    protected ButtonOverlayIcon[] verticalOverlayIconsButtons, horizontalOverlayIconsButtons;
    public Material[] horizontalOverlayIconNormalMat, horizontalOverlayIconCurrentMat;
    [HideInInspector]
    public Material[] verticalOverlayIconNormalMat, verticalOverlayIconCurrentMat;
    public Material[] verticalOverlayIconNormalMatIPad, verticalOverlayIconCurrentMatIPad;
    public Material[] verticalOverlayIconNormalMatIPhone, verticalOverlayIconCurrentMatIPhone;
    public Vector3 overlayIconNormalScale, overlayIconCurrentScale;

    // Audio
    [HideInInspector]
    public AudioSource LaunchGridAS;

    // Delayed pending load level
    const float waitBeforePendingLoadLevelDuration = 2;
    float waitBeforePendingLoadLevelStartTime;
    bool isWaitingBeforePendingLoadLevelStartTime = false;
    int levelToBeLoadedAfterWait;

    // Pages activation management
    public bool DebugPagesInitOnMovementEnd = true;
    protected bool instantiateAllPagesAtLoading = true;
    [HideInInspector]
    public bool needToUpdateActivePagesAfterHorizontalMove = false, needToUpdateActivePagesAfterVerticalMove = false;
    bool isWaitingForManageActivePages = false, didWaitBeforeManageActivePages = false;
    bool isWaitingAfterManageActivePages = false, didWaitAfterManageActivePages = false;
    public const int lengthOfPagesToInstantiateAtLoading = 7; // Always an odd number
    [HideInInspector]
    public int startIndexOfPagesToInstantiateAtLoading, endIndexOfPagesToInstantiateAtLoading;

    // Pages activation management technique choice (mostly debug)
    public enum PagesInstantiationMethod { Error = -1, OnMovementEnd, OnMovementStart, OnChangeLine }
    public PagesInstantiationMethod pagesInstantiationMethod = PagesInstantiationMethod.OnMovementEnd;

    // AVs
    public string resetCompletedGridTitle, resetCompletedGridMessage;

    #endregion


    #region Init

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        int nbGridsLimitToInstantiateAllPagesAtLoading;

        // iPods/old iPhones
        if (Screen.width == 320 || Screen.height == 320)
        {
            nbGridsLimitToInstantiateAllPagesAtLoading = 800;
        }
        else
        {
            nbGridsLimitToInstantiateAllPagesAtLoading = 1200;
        }

        // If there is more than a nbGridsLimitToInstantiateAllPagesAtLoading grids unlocked, we switch to a mode where we don't instantiate everything at loading time because it's too long
        if (headers.Count >= nbGridsLimitToInstantiateAllPagesAtLoading)
        {
            instantiateAllPagesAtLoading = false;
            Debug.Log("Too many grids unlocked to instantiate all of them at loading time");
        }
        else
        {
            Debug.Log("All grids instantiated at loading time");
        }

        LaunchGridAS = GameObject.Find("SoundLaunchGrid").GetComponent("AudioSource") as AudioSource;


        if (Global.Global.iPadVersion)
        {
            buttonsPlacementZone = buttonsPlacementZoneIPad;

            verticalOverlayIconNormalMat = verticalOverlayIconNormalMatIPad;
            verticalOverlayIconCurrentMat = verticalOverlayIconCurrentMatIPad;
        }
        else
        {
            buttonsPlacementZone = buttonsPlacementZoneIPhone;

            verticalOverlayIconNormalMat = verticalOverlayIconNormalMatIPhone;
            verticalOverlayIconCurrentMat = verticalOverlayIconCurrentMatIPhone;

            closeButtonGO.transform.localPosition = new Vector3(-0.7f, closeButtonGO.transform.localPosition.y, closeButtonGO.transform.localPosition.z);
        }


        easyGrids = new List<string>[nbGridSizes];
        for (int i = 0; i < easyGrids.Length; i++)
            easyGrids[i] = new List<string>();
        mediumGrids = new List<string>[nbGridSizes];
        for (int i = 0; i < mediumGrids.Length; i++)
            mediumGrids[i] = new List<string>();
        hardGrids = new List<string>[nbGridSizes];
        for (int i = 0; i < hardGrids.Length; i++)
            hardGrids[i] = new List<string>();
        // Nik - Add Code
        veasyGrids = new List<string>[nbGridSizes];
        for (int i = 0; i < easyGrids.Length; i++)
            veasyGrids[i] = new List<string>();
        vmediumGrids = new List<string>[nbGridSizes];
        for (int i = 0; i < mediumGrids.Length; i++)
            vmediumGrids[i] = new List<string>();

        Debug.Log("Nik - easyGrids length : " + easyGrids.Length + " || mediumGrids length : " + mediumGrids.Length + " || hardGrids length : " + hardGrids.Length);// + " || veasyGrids length : " + veasyGrids.Length);
        // Debug.Log("Nik - Gridname is : " + veasyGrids[0].Count);

        Debug.Log("Nik - headers length : " + headers.Count);
        foreach (string header in headers)
        {
            string[] splittedId = header.Split('-');

            Debug.Log("Nik - Header Name Is : " + header.Split('-') + " : " + splittedId[0]);

            if (splittedId.Length != 4)
            {
                Debug.LogWarning("Be careful, grid ID " + header + " doesn't match the intended format");
                continue;
            }

            // int gridDifficulty = int.Parse(splittedId[2]);
            // Nik - Add Code
            int gridDifficulty = int.Parse(splittedId[2]);
            int gridSize = int.Parse(splittedId[1]);

            Debug.Log("Nik - gridSize : " + gridSize + " || value : " + int.Parse(splittedId[1]));
            Debug.Log("Nik - gridDifficulty : " + gridDifficulty + " || value : " + int.Parse(splittedId[2]));

            if (gridSize < 0 || gridSize > nbGridSizes)
            {
                Debug.LogWarning("Be careful, grid ID " + header + " has size ID outside the size IDs allowed range");
                continue;
            }

            if ((Global.Difficulty)gridDifficulty == Global.Difficulty.Easy)
            {
                easyGrids[gridSize].Add(header);
            }
            else if ((Global.Difficulty)gridDifficulty == Global.Difficulty.Medium)
            {
                mediumGrids[gridSize].Add(header);
            }
            else if ((Global.Difficulty)gridDifficulty == Global.Difficulty.Hard)
            {
                hardGrids[gridSize].Add(header);
            }
            // Nik - Add Code
            if ((Global.Difficulty)gridDifficulty == Global.Difficulty.vEasy)
            {
                veasyGrids[gridSize].Add(header);
            }else if ((Global.Difficulty)gridDifficulty == Global.Difficulty.vMedium)
            {
                vmediumGrids[gridSize].Add(header);
            }
        }

        int maxGridSize = -1;
        // Get the higher value for the grid sizes
        for (int i = 0; i < easyGrids.Length; i++)
        {
            Debug.Log("Nik - easyGrids name :" + easyGrids[i]);
            if (easyGrids[i].Count == 0)
            {
                maxGridSize = i;
                
                break;
            }
        }

        if (maxGridSize == -1)
            maxGridSize = easyGrids.Length;
        
        pages = new List<MenuPageEntity>[maxGridSize];
        for (int i = 0; i < pages.Length; i++)
            pages[i] = new List<MenuPageEntity>();

        Debug.Log("Nik - MAXGRIDSIZE " + maxGridSize);

        //		if (PlayerPrefs.HasKey("CurrentMenuPageX") && PlayerPrefs.HasKey("CurrentMenuPageY"))
        //		{
        //			currentPage = new Vector2(PlayerPrefs.GetInt("CurrentMenuPageX"), PlayerPrefs.GetInt("CurrentMenuPageY"));
        //		}
        //		else
        //		{
        //			PlayerPrefs.SetInt("CurrentMenuPageX", 0);
        //			PlayerPrefs.SetInt("CurrentMenuPageY", 0);
        //			currentPage = Vector2.zero;
        //		}

        // Compute the number of buttons per menu page
        nbButtonsPerPage = (int)(maxGridsButtonPerPage.x * maxGridsButtonPerPage.y);
        Debug.Log("Nik - nbButtonsPerPage to set : " + nbButtonsPerPage);

        // Compute the size of the "slots" assigned for each button depending on the number of buttons and the buttonsPlacementZone
        spaceAllowedForAGridButton = new Vector2((buttonsPlacementZone.width - buttonsPlacementZone.x) / maxGridsButtonPerPage.x, (buttonsPlacementZone.height - buttonsPlacementZone.y) / maxGridsButtonPerPage.y);

        // Compute the actual size of the buttons depending on the size of their slots and the horizontal space between them
        sizeOfAGridButton = new Vector2(spaceAllowedForAGridButton.x - horizontalSpaceBetweenGridButtons, (spaceAllowedForAGridButton.x - horizontalSpaceBetweenGridButtons));

        gridPagePrefabRessource = Resources.Load("Prefabs/Menu/Pages/MenuPage");
        buttonMenuGridInstancePrefabRessource = Resources.Load("Prefabs/Buttons/ButtonMenuGridInstance");
        pagesGOTransform = GameObject.Find("Pages").transform;

        Init();

    }


    // Init the menu pages.
    void Init()
    {
        InitCam();

        InitPagesForTheseGrids(easyGrids, Global.Difficulty.Easy);
        InitPagesForTheseGrids(mediumGrids, Global.Difficulty.Medium);
        InitPagesForTheseGrids(hardGrids, Global.Difficulty.Hard);
        // Nik - Add Code
        InitPagesForTheseGrids(veasyGrids, Global.Difficulty.Easy);
        InitPagesForTheseGrids(vmediumGrids, Global.Difficulty.Medium);

        // If we didn't instantiate all the pages during loading time => decide which pages (close tothe current page) to load at loading time
        if (!instantiateAllPagesAtLoading)
        {
            Debug.Log("-- Now instantiating the pages close to the current page");
            int lengthOfPagesToInstantiateAtLoadingForEachSide = (int)((lengthOfPagesToInstantiateAtLoading - 1) * 0.5f);

            // There is less pages on the left side than what we allowed => instead load more pages on the right side
            if (currentPage.x - lengthOfPagesToInstantiateAtLoadingForEachSide < 0)
            {
                Debug.Log("Not enough space on the left");

                startIndexOfPagesToInstantiateAtLoading = 0;
                endIndexOfPagesToInstantiateAtLoading = lengthOfPagesToInstantiateAtLoading - 1;
            }
            else
            {
                // There is less pages on the right side than what we allowed => instead load more pages on the left side
                if (currentPage.x + lengthOfPagesToInstantiateAtLoadingForEachSide > pages[0].Count - 1)
                {
                    Debug.Log("Not enough space on the right");

                    endIndexOfPagesToInstantiateAtLoading = pages[0].Count - 1;
                    startIndexOfPagesToInstantiateAtLoading = endIndexOfPagesToInstantiateAtLoading - lengthOfPagesToInstantiateAtLoading + 1;
                }
                // There is enough pages on both the left and the right sides => load the same amount of pages on the left and right sides
                else
                {
                    Debug.Log("Enough space on both sides");

                    startIndexOfPagesToInstantiateAtLoading = (int)currentPage.x - lengthOfPagesToInstantiateAtLoadingForEachSide;
                    endIndexOfPagesToInstantiateAtLoading = (int)currentPage.x + lengthOfPagesToInstantiateAtLoadingForEachSide;
                }
            }

            for (int i = startIndexOfPagesToInstantiateAtLoading; i < endIndexOfPagesToInstantiateAtLoading + 1; i++)
            {
                for (int j = 0; j < pages.Length; j++)
                {

                    pages[j][i].Init();
                }
            }
        }

        InitPagesOverlays();

        cam.transform.position = new Vector3(pages[(int)currentPage.y][(int)currentPage.x].transform.position.x, cam.transform.position.y, pages[(int)currentPage.y][(int)currentPage.x].transform.position.z);

        GoToPage(currentPage);
    }


    // Init the pages for the given grids. One set of grids here represents all the grids of one difficulty for all existing sizes
    void InitPagesForTheseGrids(List<string>[] grids, Global.Difficulty pageDifficulty)
    {
        Debug.Log("Nik - Spawn Page Length : " + grids.Length + " Defficulty : " + pageDifficulty);
        float nbHorizontalPagesAsFloat = 0;
        for (int i = 0; i < grids.Length; i++)
        {
            float nbHorPagesAsFloat = (float)grids[i].Count / nbButtonsPerPage;
            if (nbHorPagesAsFloat > nbHorizontalPagesAsFloat)
                nbHorizontalPagesAsFloat = nbHorPagesAsFloat;
        }

        int nbHorizontalPages = Mathf.CeilToInt(nbHorizontalPagesAsFloat);
        Debug.Log("Nik - nbHorizontalPagesAsFloat " + nbHorizontalPagesAsFloat + " int to " + nbHorizontalPages);
        GameObject menuPageGO;

        for (int i = 0; i < pages.Length; i++)
        {
            for (int j = 0; j < nbHorizontalPages; j++)
            {
                menuPageGO = Instantiate(gridPagePrefabRessource) as GameObject;
                menuPageGO.transform.parent = pagesGOTransform;
                Dev_UIManager.instance.listOfSpawnedPages.Add(menuPageGO);
                pages[i].Add(menuPageGO.transform.Find("MenuPageEntity").GetComponent("MenuPageEntity") as MenuPageEntity);

                List<string> currentSubset;
                if (grids[i].Count >= nbButtonsPerPage)
                {
                    currentSubset = grids[i].GetRange(0, nbButtonsPerPage);
                    grids[i].RemoveRange(0, nbButtonsPerPage);
                }
                else
                {
                    currentSubset = grids[i].GetRange(0, grids[i].Count);
                    grids[i].RemoveRange(0, grids[i].Count);
                }
                Debug.Log("Nik - Page Spawn Here");
                pages[i][pages[i].Count - 1].Init(new Vector2(pages[i].Count - 1, i), currentSubset, pageDifficulty);

                if (instantiateAllPagesAtLoading)
                    pages[i][pages[i].Count - 1].Init();

                #region DEV_CODE

                if (Dev_UIManager.instance.listOfSpawnedPages_9x13.Contains(menuPageGO.gameObject))
                {
                    if (Dev_UIManager.instance.listOfSpawnedPages_9x13[0] == menuPageGO)
                    {
                        menuPageGO.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
                        Debug.Log("<color=red>Nik - List Contains Me</color> " + menuPageGO.name + " - " + menuPageGO.name);
                    }
                    else if (Dev_UIManager.instance.listOfSpawnedPages_9x13[1] == menuPageGO)
                    {
                        menuPageGO.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(1000, 0, 0);
                        Debug.Log("<color=red>Nik - List Contains Me</color> " + menuPageGO.name + " - " + menuPageGO.name);
                    }
                    else
                    {

                    }
                }
                else if (Dev_UIManager.instance.listOfSpawnedPages_13x9.Contains(menuPageGO.gameObject))
                {
                    //Debug.Log("<color=red>List contains me</color> " + menuPageGO.name + " - " + menuPageGO.transform.GetChild(2).gameObject.name);
                }
                else if (Dev_UIManager.instance.listOfSpawnedPages_18x18.Contains(menuPageGO.gameObject))
                {
                    //Debug.Log("<color=red>List contains me</color> " + menuPageGO.name + " - " + menuPageGO.transform.GetChild(2).gameObject.name);
                }
                // Nik - Add Code
                else if (Dev_UIManager.instance.listOfSpawnedPages_8X5.Contains(menuPageGO.gameObject))
                {
                    //Debug.Log("<color=red>List contains me</color> " + menuPageGO.name + " - " + menuPageGO.transform.GetChild(2).gameObject.name);
                }
                else if (Dev_UIManager.instance.listOfSpawnedPages_10X10.Contains(menuPageGO.gameObject))
                {
                    //Debug.Log("<color=red>List contains me</color> " + menuPageGO.name + " - " + menuPageGO.transform.GetChild(2).gameObject.name);
                }


                //DEV_EDIT || PAGES || RESETTING PAGE SIZE FROM HERE
                menuPageGO.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
                menuPageGO.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

                #endregion
            }
        }
    }


    // Init the cam. Always call this when a grid is loaded, as it will adapt to the grid size
    protected override void InitCam()
    {
        //		base.InitCam();
        cam.transform.position = new Vector3(0, cam.transform.position.y, 0);
        cam.SetOrthoSize();

        // Nik || Code Change || 06-06-2020
        // cam.AllowOrientations(false, true);
        cam.AllowOrientations(true, true);
    }


    void InitPagesOverlays()
    {
        Vector2 fitToCam = OrthoCam.GetFitToCamViewMultipliers(true);

        // Horizontal overlay
        horizontalOverlayIcons = new GameObject[pages[0].Count];
        horizontalOverlayIconsButtons = new ButtonOverlayIcon[pages[0].Count];
        horizontalPagesOverlayGO.transform.localPosition = new Vector3((buttonsPlacementZone.width + buttonsPlacementZone.x) * 0.5f, (-0.5f + buttonsPlacementZone.y + horizontalSpaceBetweenGridButtons) * 0.5f, cam.transform.position.y - 0.5f);

        float spaceBetweenOverlayIconsMultiplier;// = 1f /(Mathf.FloorToInt(pages[0].Count / 10f) + 1);
        if (pages[0].Count >= 10)
        {
            spaceBetweenOverlayIconsMultiplier = 0.5f;
        }
        else
        {
            spaceBetweenOverlayIconsMultiplier = 1;
        }

        for (int i = 0; i < pages[0].Count; i++)
        {
            horizontalOverlayIcons[i] = Instantiate(Resources.Load("Prefabs/Menu/Pages/PagesOverlayIcon")) as GameObject;
            horizontalOverlayIcons[i].name = "Horizontal Overlay Icon " + i;
            horizontalOverlayIcons[i].transform.parent = GameObject.Find("PagesDifficultyOverlay").transform;
            horizontalOverlayIcons[i].transform.localPosition = new Vector3((-pages[0].Count * 0.5f + i + 0.5f) * (horizontalOverlayIcons[i].transform.localScale.x + spaceBetweenOverlayIcons * spaceBetweenOverlayIconsMultiplier), 0, 0);
            if (spaceBetweenOverlayIconsMultiplier != 1)
            {
                BoxCollider colliderAsBox = (horizontalOverlayIcons[i].GetComponent<Collider>() as BoxCollider);
                colliderAsBox.size = new Vector3(colliderAsBox.size.x * spaceBetweenOverlayIconsMultiplier, colliderAsBox.size.y, colliderAsBox.size.z);
            }

            horizontalOverlayIconsButtons[i] = horizontalOverlayIcons[i].GetComponent("ButtonOverlayIcon") as ButtonOverlayIcon;
            horizontalOverlayIconsButtons[i].isVerticalOverlay = false;
            horizontalOverlayIconsButtons[i].index = i;

            SetOverlayIconVisualStyle(i, true, false);
        }


        // Vertical overlay
        verticalOverlayIcons = new GameObject[pages.Length];
        verticalOverlayIconsButtons = new ButtonOverlayIcon[pages.Length];
        verticalPagesOverlayGO.transform.localPosition = new Vector3((-0.5f * fitToCam.x + buttonsPlacementZone.x + horizontalSpaceBetweenGridButtons) * 0.5f, (buttonsPlacementZone.height + buttonsPlacementZone.y) * 0.5f, cam.transform.position.y - 0.5f);

        for (int i = 0; i < pages.Length; i++)
        {
            verticalOverlayIcons[i] = Instantiate(Resources.Load("Prefabs/Menu/Pages/PagesOverlayIcon")) as GameObject;
            verticalOverlayIcons[i].name = "Vertical Overlay Icon " + i;
            verticalOverlayIcons[i].transform.parent = GameObject.Find("PagesSizeOverlay").transform;
            verticalOverlayIcons[i].transform.localPosition = new Vector3(0, 0, (pages.Length * 0.5f - (i + 0.5f)) * (verticalOverlayIcons[i].transform.localScale.z + spaceBetweenOverlayIcons));

            verticalOverlayIconsButtons[i] = verticalOverlayIcons[i].GetComponent("ButtonOverlayIcon") as ButtonOverlayIcon;
            verticalOverlayIconsButtons[i].isVerticalOverlay = true;
            verticalOverlayIconsButtons[i].index = i;

            SetOverlayIconVisualStyle(i, false, false);
        }


        SetCurrentPage(currentPage);

        ManageActivePages();
    }



    #endregion


    #region Update

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        #region ORIGINAL_CODE

        // if (isWaitingBeforePendingLoadLevelStartTime)
        // {
        //     if (Time.time >= waitBeforePendingLoadLevelStartTime + waitBeforePendingLoadLevelDuration)
        //     {
        //         isWaitingBeforePendingLoadLevelStartTime = false;

        //         inputDisablingMaskGO.active = false;

        //         SetPendingLoadLevel(levelToBeLoadedAfterWait);
        //     }
        // }

        // if (isWaitingForManageActivePages)
        // {
        //     if (didWaitBeforeManageActivePages)
        //     {
        //         Debug.Log("didWaitBeforeManageActivePages");
        //         isWaitingForManageActivePages = false;
        //         ManageActivePages();
        //     }
        //     else
        //     {
        //         Debug.Log("WaitingBeforeManageActivePages");
        //         didWaitBeforeManageActivePages = true;
        //     }
        // }
        // else if (isWaitingAfterManageActivePages)
        // {
        //     if (didWaitAfterManageActivePages)
        //     {
        //         Debug.Log("didWaitAfterManageActivePages");
        //         isWaitingAfterManageActivePages = false;
        //         inputDisablingMaskGO.active = false;
        //     }
        //     else
        //     {
        //         Debug.Log("WaitingAfterManageActivePages");
        //         didWaitAfterManageActivePages = true;
        //     }
        // }

        #endregion

        #region DEV_CODE

        if (isWaitingBeforePendingLoadLevelStartTime)
        {
            if (Time.time >= waitBeforePendingLoadLevelStartTime + waitBeforePendingLoadLevelDuration)
            {
                isWaitingBeforePendingLoadLevelStartTime = false;

                inputDisablingMaskGO.SetActive(false);

                SetPendingLoadLevel(levelToBeLoadedAfterWait);
            }
        }

        if (isWaitingForManageActivePages)
        {
            if (didWaitBeforeManageActivePages)
            {
                Debug.Log("didWaitBeforeManageActivePages");
                isWaitingForManageActivePages = false;
                ManageActivePages();
            }
            else
            {
                Debug.Log("WaitingBeforeManageActivePages");
                didWaitBeforeManageActivePages = true;
            }
        }
        else if (isWaitingAfterManageActivePages)
        {
            if (didWaitAfterManageActivePages)
            {
                //DEV_EDIT || ************************
                //Debug.Log("didWaitAfterManageActivePages");
                //DEV_EDIT || MAIN MENU || ALL PAGES LOADED SUCCESSFULLY
                Debug.Log("<color=yellow><b>All Pages Loaded</b></color>");
                isWaitingAfterManageActivePages = false;
                inputDisablingMaskGO.SetActive(false);

                if (PlayerPrefs.GetInt("V_ForcePage_LoadAtStart") == 0)
                {
                    Dev_UIManager.instance.listOfCurrentPageItem.AddRange(Dev_UIManager.instance.listOfSpawnedPages_9x13);
                }
                else if (PlayerPrefs.GetInt("V_ForcePage_LoadAtStart") == 1)
                {
                    Dev_UIManager.instance.listOfCurrentPageItem.AddRange(Dev_UIManager.instance.listOfSpawnedPages_13x9);
                }
                else if (PlayerPrefs.GetInt("V_ForcePage_LoadAtStart") == 2)
                {
                    Dev_UIManager.instance.listOfCurrentPageItem.AddRange(Dev_UIManager.instance.listOfSpawnedPages_18x18);
                }
                // Nik - Add Code
                else if (PlayerPrefs.GetInt("V_ForcePage_LoadAtStart") == 3)
                {
                    Dev_UIManager.instance.listOfCurrentPageItem.AddRange(Dev_UIManager.instance.listOfSpawnedPages_8X5);
                }
                else if (PlayerPrefs.GetInt("V_ForcePage_LoadAtStart") == 4)
                {
                    Dev_UIManager.instance.listOfCurrentPageItem.AddRange(Dev_UIManager.instance.listOfSpawnedPages_10X10);
                }

                for (int i = 0; i < Dev_UIManager.instance.listOfSpawnedPages.Count; i++)
                {
                    //if (i > 0)
                    //{
                    //    if (Dev_UIManager.instance.listOfSpawnedPages[i].activeInHierarchy) Dev_UIManager.instance.listOfSpawnedPages[i].gameObject.SetActive(false);
                    //}
                    Dev_UIManager.instance.listOfSpawnedPages[i].gameObject.SetActive(false);
                    //if (i != PlayerPrefs.GetInt("H_ForcePage_LoadAtStart"))
                    //{
                    //    Dev_UIManager.instance.listOfSpawnedPages[i].gameObject.SetActive(false);
                    //}
                }

                if (Dev_UIManager.instance.currentSelectedPage)
                {
                    Dev_UIManager.instance.currentSelectedPage.SetActive(true);
                }
                else
                {
                    Debug.Log("<color=red><b>NO CURRENT PAGE FOUND</b></color>");
                }
            }
            else
            {
                //DEV_EDIT || ************************
                //Debug.Log("WaitingAfterManageActivePages");
                didWaitAfterManageActivePages = true;
            }
        }

        #endregion
    }


    //	protected void OnGUI()
    //	{
    //		if (GUI.Button(new Rect(0, Screen.height - 30, 150, 30), pagesInstantiationMethod.ToString()))
    //		{
    ////			DebugPagesInitOnMovementEnd = !DebugPagesInitOnMovementEnd;
    //			if (pagesInstantiationMethod == PagesInstantiationMethod.OnMovementEnd)
    //				pagesInstantiationMethod = PagesInstantiationMethod.OnMovementStart;
    //			else if (pagesInstantiationMethod == PagesInstantiationMethod.OnMovementStart)
    //				pagesInstantiationMethod = PagesInstantiationMethod.OnChangeLine;
    //			else
    //				pagesInstantiationMethod = PagesInstantiationMethod.OnMovementEnd;
    //		}
    //	}

    #endregion


    #region Unity App Events

    //	protected override void OnApplicationQuit()
    //	{
    //		base.OnApplicationQuit();
    //		
    //	}
    //	
    //	
    //	protected override void OnApplicationPause(bool pause)
    //	{
    //		base.OnApplicationPause(pause);
    //		
    //		Debug.Log("PAUSE: " + pause);
    //		
    //		// App pausing
    //		if(pause)
    //		{
    //			
    //		}
    //		// App resuming
    //		else
    //		{
    //		}
    //	}

    #endregion


    #region Pages Management

    public MenuPageEntity GetCurrentPage()
    {
        return pages[(int)currentPage.y][(int)currentPage.x];
    }


    // Prevents currentPage from being out of the pages range
    protected void SetCurrentPage(Vector2 index)
    {
        // Set the overlays icons of the previous current page back to the normal state
        SetOverlayIconVisualStyle((int)currentPage.x, true, false);
        SetOverlayIconVisualStyle((int)currentPage.y, false, false);

        if (currentPage.x != index.x)
        {
            needToUpdateActivePagesAfterHorizontalMove = true;
        }
        else if (currentPage.y != index.y)
        {
            needToUpdateActivePagesAfterVerticalMove = true;
        }

        currentPage = index;

        if (currentPage.y > pages.Length - 1)
        {
            currentPage = new Vector2(currentPage.x, pages.Length - 1);
        }
        else if (currentPage.y < 0)
        {
            currentPage = new Vector2(currentPage.x, 0);
        }

        if (currentPage.x > pages[0].Count - 1)
        {
            currentPage = new Vector2(pages[0].Count - 1, currentPage.y);
        }
        else if (currentPage.x < 0)
        {
            currentPage = new Vector2(0, currentPage.y);
        }

        PlayerPrefs.SetInt("CurrentMenuPageX", (int)currentPage.x);
        PlayerPrefs.SetInt("CurrentMenuPageY", (int)currentPage.y);

        // Set the overlays icons of the new current page to the current state
        SetOverlayIconVisualStyle((int)currentPage.x, true, true);
        SetOverlayIconVisualStyle((int)currentPage.y, false, true);


        pages[(int)currentPage.y][(int)currentPage.x].transform.parent.gameObject.SetActiveRecursively(true);

        if (!pages[(int)currentPage.y][(int)currentPage.x].fullyInitialised)
        {
            pages[(int)currentPage.y][(int)currentPage.x].Init();
        }

        if (pagesInstantiationMethod == PagesInstantiationMethod.OnMovementStart)
        {
            ManageActivePages();
        }
    }


    // Set the current page based on the cam position. Also set the cam's panBorders to fit the current page
    //DEV_EDIT || MAIN MENU || CHANGE CAMERA POSITION FROM HERE
    public void SetCurrentPageBasedOnCamPosition()
    {
        // Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        // RaycastHit hitInfo;
        // LayerMask menuPagesMask = 1 << 8;
        // //if the Ray hit something create a new touch
        // if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, menuPagesMask))
        // {
        //     GoToPage((hitInfo.transform.GetComponent("MenuPageEntity") as MenuPageEntity).index);
        // }
    }


    public void GoToNextPage()
    {
        Debug.Log("NEXT");
        GoToPage(new Vector2(currentPage.x + 1, currentPage.y));
    }


    public void GoToPreviousPage()
    {
        Debug.Log("PREVIOUS");
        GoToPage(new Vector2(currentPage.x - 1, currentPage.y));
    }


    public void GoToUpwardPage()
    {
        Debug.Log("UP");
        GoToPage(new Vector2(currentPage.x, currentPage.y - 1));
    }


    public void GoToDownwardPage()
    {
        Debug.Log("DOWN");
        GoToPage(new Vector2(currentPage.x, currentPage.y + 1));
    }


    // Set the page at the given index as the current one
    public void GoToPage(Vector2 index)
    {
        Vector2 tmp = currentPage;

        SetCurrentPage(index);

        if (tmp.x > index.x)
            cam.panVelocity = new Vector3(-0.5f, 0, 0);
        else if (tmp.x < index.x)
            cam.panVelocity = new Vector3(0.5f, 0, 0);
        else if (tmp.y > index.y)
            cam.panVelocity = new Vector3(0, 0, 0.5f);
        else if (tmp.y < index.y)
            cam.panVelocity = new Vector3(0, 0, -0.5f);

        cam.ComputePanBorders();
    }


    public void SetPendingManageActivePages()
    {
        if (pagesInstantiationMethod == PagesInstantiationMethod.OnMovementEnd)
        {
            needToUpdateActivePagesAfterHorizontalMove = false;
            needToUpdateActivePagesAfterVerticalMove = false;


            int i = (int)currentPage.x;
            int j = (int)currentPage.y;

            if ((i < pages[j].Count - 1 && !pages[j][i + 1].fullyInitialised) || (i > pages[0].Count && !pages[j][i - 1].fullyInitialised) || (j < pages.Length - 1 && !pages[j + 1][i].fullyInitialised) || (j > 0 && !pages[j - 1][i].fullyInitialised))
                inputDisablingMaskGO.SetActive(true);


            isWaitingForManageActivePages = true;
            didWaitBeforeManageActivePages = false;

            Debug.Log("End SetPendingManageActivePages");
        }
        else if (pagesInstantiationMethod == PagesInstantiationMethod.OnChangeLine)
        {
            needToUpdateActivePagesAfterHorizontalMove = false;
            needToUpdateActivePagesAfterVerticalMove = false;

            if (!pages[(int)currentPage.y][1].fullyInitialised)
            {
                inputDisablingMaskGO.SetActive(true);
            }

            isWaitingForManageActivePages = true;
            didWaitBeforeManageActivePages = false;
        }
    }


    public void ManageActivePages()
    {
        Debug.Log("Managing active pages");


        if (pagesInstantiationMethod == PagesInstantiationMethod.OnMovementStart)
        {
            needToUpdateActivePagesAfterHorizontalMove = false;
            needToUpdateActivePagesAfterVerticalMove = false;
        }


        // for (int i = 0; i < pages[0].Count; i++)
        // {
        //     for (int j = 0; j < pages.Length; j++)
        //     {
        //         if (pagesInstantiationMethod == PagesInstantiationMethod.OnChangeLine)
        //         {
        //             if (j == currentPage.y && !pages[j][i].fullyInitialised)
        //             {
        //                 pages[j][i].Init();
        //             }
        //         }

        //         if ((i >= currentPage.x - 1 && i <= currentPage.x + 1 && j == currentPage.y) || (i == currentPage.x && j >= currentPage.y - 1 && j <= currentPage.y + 1))
        //         {
        //             if (!pages[j][i].gameObject.active)
        //             {
        //                 pages[j][i].transform.parent.gameObject.SetActiveRecursively(true);
        //             }

        //             if (pagesInstantiationMethod != PagesInstantiationMethod.OnChangeLine)
        //             {
        //                 if (!pages[j][i].fullyInitialised)
        //                 {
        //                     pages[j][i].Init();
        //                 }
        //             }
        //         }
        //         else
        //         {
        //             if (pages[j][i].gameObject.active)
        //                 pages[j][i].transform.parent.gameObject.SetActiveRecursively(false);
        //         }
        //     }
        // }

        isWaitingAfterManageActivePages = true;
        didWaitAfterManageActivePages = false;
    }

    #endregion


    #region Unity App Events

    protected override void OnApplicationQuit()
    {

    }


    protected override void OnApplicationPause(bool pause)
    {
        Debug.Log("PAUSE: " + pause);

        // App pausing
        if (pause)
        {

        }
        // App resuming
        else
        {

        }
    }

    #endregion


    #region Wrappers

    //	public override bool isGameLogic()
    //	{
    //		return false;
    //	}

    public override Global.LogicType GetLogicType()
    {
        return Global.LogicType.MenuLogic;
    }


    protected void SetOverlayIconVisualStyle(int index, bool belongsToHorizontalOverlay, bool isCurrent)
    {
        #region ORIGINAL_CODE



        #endregion

        #region DEV_CODE

        if (belongsToHorizontalOverlay)
        {
            if (isCurrent)
            {
                if (Dev_UIManager.instance.isUiElement)
                {

                }
                else
                {
                    //horizontalOverlayIcons[index].renderer.material = horizontalOverlayIconCurrentMat[(int)pages[0][index].difficulty];
                    horizontalOverlayIconsButtons[index].SetNormalMaterial(horizontalOverlayIconCurrentMat[(int)pages[0][index].difficulty]);
                    horizontalOverlayIconsButtons[index].SetPressedMaterial(horizontalOverlayIconCurrentMat[(int)pages[0][index].difficulty]);
                    horizontalOverlayIcons[index].transform.localScale = overlayIconCurrentScale;
                }
            }
            else
            {
                if (Dev_UIManager.instance.isUiElement)
                {

                }
                else
                {
                    //horizontalOverlayIcons[index].renderer.material = horizontalOverlayIconNormalMat[(int)pages[0][index].difficulty];
                    horizontalOverlayIconsButtons[index].SetNormalMaterial(horizontalOverlayIconNormalMat[(int)pages[0][index].difficulty]);
                    horizontalOverlayIconsButtons[index].SetPressedMaterial(horizontalOverlayIconNormalMat[(int)pages[0][index].difficulty]);
                    horizontalOverlayIcons[index].transform.localScale = overlayIconNormalScale;
                }
            }
        }
        else
        {
            if (isCurrent)
            {
                if (Dev_UIManager.instance.isUiElement)
                {

                }
                else
                {
                    //verticalOverlayIcons[index].renderer.material = verticalOverlayIconCurrentMat[index];
                    verticalOverlayIconsButtons[index].SetNormalMaterial(verticalOverlayIconCurrentMat[index]);
                    verticalOverlayIconsButtons[index].SetPressedMaterial(verticalOverlayIconCurrentMat[index]);
                    verticalOverlayIcons[index].transform.localScale = overlayIconCurrentScale;
                }
            }
            else
            {
                if (Dev_UIManager.instance.isUiElement)
                {

                }
                else
                {
                    //verticalOverlayIcons[index].renderer.material = verticalOverlayIconNormalMat[index];
                    verticalOverlayIconsButtons[index].SetNormalMaterial(verticalOverlayIconNormalMat[index]);
                    verticalOverlayIconsButtons[index].SetPressedMaterial(verticalOverlayIconNormalMat[index]);
                    verticalOverlayIcons[index].transform.localScale = overlayIconNormalScale;
                }
            }
        }

        #endregion
    }


    public void LaunchCompletedGridAssociatedWithButton(ButtonMenuGridInstance button)
    {
        buttonMenuGridInstance = button;
        openedAlertView = Global.AlertViews.LaunchGridCompletedReset;
#if UNITY_IOS
        AVBinding.CreateAlertView(resetCompletedGridTitle, resetCompletedGridMessage);
#elif UNITY_ANDROID
        //TODO || DEV24
        //EtceteraAndroid.showAlert(resetCompletedGridTitle, resetCompletedGridMessage, "Continuer", "Annuler");
#endif
    }


    // Reset the state of the grid with the given id
    public void ResetGrid(string id)
    {
        XMLManager.RemoveFromGameStateFile(id);
        PlayerPrefs.SetInt(id, (int)Global.GridState.Empty);
        PlayerPrefs.SetFloat(id + "_Timer", 0);
        PlayerPrefs.SetInt(id + "_Combo", 0);
    }

    #endregion


    #region Loading

    public void SetDelayedPendingLoadLevel(int level)
    {
        inputDisablingMaskGO.GetComponent<Renderer>().enabled = false;
        inputDisablingMaskGO.SetActive(true);

        isWaitingBeforePendingLoadLevelStartTime = true;
        waitBeforePendingLoadLevelStartTime = Time.time;
        levelToBeLoadedAfterWait = level;

        CreateLoadingOverlay();
    }

    #endregion


    #region Callbacks From Native

    public override void AVCallback(string message)
    {
        base.AVCallback(message);

        int parsedMessage = int.Parse(message);

        if (openedAlertView == Global.AlertViews.LaunchGridCompletedReset)
        {
            if (parsedMessage == 1) // OK
            {
                ResetGrid(buttonMenuGridInstance.associatedGrid);
                buttonMenuGridInstance.LaunchMyGrid();
            }
        }

        openedAlertView = Global.AlertViews.Error;
    }

    #endregion

}
