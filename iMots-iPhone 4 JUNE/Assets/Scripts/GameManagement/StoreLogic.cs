using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoreLogic : Logic
{

    #region Vars

    // GO references
    public GameObject horizontalPagesOverlayGO, verticalPagesOverlayGO, closeButtonGO;
    protected GameObject specialOverlayGO;

    public List<StorePageEntity>[] pages;
    List<string>[] easyGrids, mediumGrids, hardGrids;
    protected const int nbGridSizes = 3;

    public Vector2 currentPage;

    // Grids buttons
    public Rect buttonsPlacementZoneIPad;
    public Rect buttonsPlacementZoneIPhone;
    [HideInInspector]
    public Rect buttonsPlacementZone;

    public float horizontalSpaceBetweenGridButtons = 0.05f;
    public Vector2 maxGridsButtonPerPage = new Vector2(8, 5);

    // Grids pages overlays
    public float spaceBetweenOverlayIcons = 0.05f;
    [HideInInspector]
    public GameObject[] verticalOverlayIcons, horizontalOverlayIcons;
    protected ButtonOverlayIcon[] verticalOverlayIconsButtons, horizontalOverlayIconsButtons;
    public Material[] horizontalOverlayIconNormalMat, horizontalOverlayIconCurrentMat;
    [HideInInspector]
    public Material[] verticalOverlayIconNormalMat, verticalOverlayIconCurrentMat;
    public Material[] verticalOverlayIconNormalMatIPad, verticalOverlayIconCurrentMatIPad;
    public Material[] verticalOverlayIconNormalMatIPhone, verticalOverlayIconCurrentMatIPhone;
    public Vector3 overlayIconNormalScale, overlayIconCurrentScale;

    // Security
    protected float timeOutDuration = 30;
    protected float timeOutStartTime;
    protected bool checkForTimeOut = false;
    protected bool ignoreUpcomingInAppEvents = true;

    [HideInInspector]
    public ButtonStoreItem buttonCurrentlyBeingProcessed;

    public List<ButtonStoreItem> itemButtons = new List<ButtonStoreItem>();

    public GameObject inAppPopup, inAppPopupMask;
    protected LoadingSpinner loadingSpinner;

    // AVs
    public string productListRequestFailedString, purchaseSuccessfulString, purchaseFailedString, timeOutString, cantMakePaymentsString, waitForBillingSupportedEvent;

    // Android IAB
#if UNITY_ANDROID
    string publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkRJbe2eEO0ukRl7OenuZHuigZ6j92Tiapu8adQkN1sm+c/M1l6RlWVka8/e+y/rfr/OhVozKbmNl1EaY6KGI+FVQi/B6WRJa/BCVI71Oy6PJxW/KFDHyFYiO3ELVNCURIOUPmedhhCH+GqiEl6PXr1qR2tn7jam1VicEZitPoZdbWhNWAceAd12+hoYmpnNMr3AGMFAgUlWORGJf19R05CzQxAOpWPsA/OTq6OBfvVaAYtSPdLY6xYYkWErh6FNP7yhtY7wFG91NJWAnrxX9g1HofSi0uwBtlj/Yl1muwZmUieq4zP+N6ulmM5pQ1F2ZTFnaE0UuUQ1i2dH68UHLLwIDAQAB";
    [HideInInspector]
    public bool waitingForBillingSupportedEvent = false, isBillingSupported = false;
#endif

    #endregion


    #region Init

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
#if UNITY_ANDROID
        Debug.Log("Android");
        Debug.Log("**************Running GOOGLE Marketplace Test Purchase******************");

        //TODO || DEV15
        //IABAndroid.init(publicKey);
        waitingForBillingSupportedEvent = true;
#endif

        if (!PlayerPrefs.HasKey("OpenStoreOnSpecialPage"))
        {
            PlayerPrefs.SetInt("OpenStoreOnSpecialPage", 0);
        }

        if (PlayerPrefs.GetInt("OpenStoreOnSpecialPage") == 1)
        {
            Debug.Log("Nik - StoreLogic - Start in OpenStoreOnSpecialPage PlayerPrefs == 1");
            if (Global.Global.iPadVersion)
                currentPage = new Vector2(0, 3);
            else
                currentPage = new Vector2(0, 2);

            PlayerPrefs.SetInt("OpenStoreOnSpecialPage", 0);
        }

        if (Application.platform == RuntimePlatform.OSXEditor)
        {
            inAppPopupMask.transform.localScale = new Vector3(7.68f * Camera.main.aspect, 1, 7.68f);
        }

        loadingSpinner = FindObjectOfType(typeof(LoadingSpinner)) as LoadingSpinner;



        if (Global.Global.iPadVersion)
        {
            buttonsPlacementZone = buttonsPlacementZoneIPad;

            verticalOverlayIconNormalMat = verticalOverlayIconNormalMatIPad;
            verticalOverlayIconCurrentMat = verticalOverlayIconCurrentMatIPad;
        }
        else
        {
            buttonsPlacementZone = buttonsPlacementZoneIPhone;

            verticalOverlayIconNormalMat = verticalOverlayIconNormalMatIPhone;
            verticalOverlayIconCurrentMat = verticalOverlayIconCurrentMatIPhone;

            closeButtonGO.transform.localPosition = new Vector3(-0.7f, closeButtonGO.transform.localPosition.y, closeButtonGO.transform.localPosition.z);
        }

        if (Global.Global.iPadVersion)
        {
            pages = new List<StorePageEntity>[4];
        }
        else
        {
            pages = new List<StorePageEntity>[3];
        }

        for (int i = 0; i < pages.Length; i++)
            pages[i] = new List<StorePageEntity>() { null, null, null, null };

        StorePageEntity[] storePagesEntities = FindObjectsOfType(typeof(StorePageEntity)) as StorePageEntity[];
        foreach (StorePageEntity storePageEntity in storePagesEntities)
        {
            storePageEntity.Init();
        }

        Init();



    }

    void OnEnable()
    {
#if UNITY_IOS

        //DEV_CODE || PURCHASE || COMMENT
        //StoreKitManager.restoreTransactionsFailedEvent += restoreFailed;
#elif UNITY_ANDROID
        //TODO || DEV17
        /*IABAndroidManager.billingSupportedEvent += billingSupportedEvent;
        IABAndroidManager.purchaseSucceededEvent += purchaseSucceededEvent;
        IABAndroidManager.purchaseCancelledEvent += purchaseCancelledEvent;
        IABAndroidManager.purchaseFailedEvent += purchaseFailedEvent;*/
#endif
    }

    void OnDisable()
    {
#if UNITY_IOS

        //DEV_CODE || PURCHASE || COMMENT
        //StoreKitManager.restoreTransactionsFailedEvent -= restoreFailed;
#elif UNITY_ANDROID
        //TODO || DEV18
        /*IABAndroidManager.billingSupportedEvent -= billingSupportedEvent;
        IABAndroidManager.purchaseSucceededEvent -= purchaseSucceededEvent;
        IABAndroidManager.purchaseCancelledEvent -= purchaseCancelledEvent;
        IABAndroidManager.purchaseFailedEvent -= purchaseFailedEvent;*/
#endif
    }

    // Init the menu pages.
    void Init()
    {
        InitCam();

        InitPagesOverlays();

        if (!Dev_StoreUIManager.instance.isUiElement) cam.transform.position = new Vector3(pages[(int)currentPage.y][(int)currentPage.x].transform.position.x, cam.transform.position.y, pages[(int)currentPage.y][(int)currentPage.x].transform.position.z);

        GoToPage(currentPage);
    }

    // Init the cam. Always call this when a grid is loaded, as it will adapt to the grid size
    protected override void InitCam()
    {
        //		base.InitCam();
        cam.transform.position = new Vector3(0, cam.transform.position.y, 0);
        cam.SetOrthoSize();

        // Nik || Code Change || 06-06-2020
        cam.AllowOrientations(true, true);
        // cam.AllowOrientations(false, true);
    }


    void InitPagesOverlays()
    {
        Vector2 fitToCam = OrthoCam.GetFitToCamViewMultipliers(true);

        // Horizontal overlay
        horizontalOverlayIcons = new GameObject[pages[0].Count];
        horizontalOverlayIconsButtons = new ButtonOverlayIcon[pages[0].Count];
        horizontalPagesOverlayGO.transform.localPosition = new Vector3((buttonsPlacementZone.width + buttonsPlacementZone.x) * 0.5f, (-0.5f + buttonsPlacementZone.y + horizontalSpaceBetweenGridButtons) * 0.5f, cam.transform.position.y - 0.5f);

        for (int i = 0; i < pages[0].Count; i++)
        {
            horizontalOverlayIcons[i] = Instantiate(Resources.Load("Prefabs/Store/Pages/PagesOverlayIcon")) as GameObject;
            horizontalOverlayIcons[i].name = "Horizontal Overlay Icon " + i;
            horizontalOverlayIcons[i].transform.parent = GameObject.Find("PagesDifficultyOverlay").transform;
            horizontalOverlayIcons[i].transform.localPosition = new Vector3((-pages[0].Count * 0.5f + i + 0.5f) * (horizontalOverlayIcons[i].transform.localScale.x + spaceBetweenOverlayIcons), 0, 0);

            horizontalOverlayIconsButtons[i] = horizontalOverlayIcons[i].GetComponent("ButtonOverlayIcon") as ButtonOverlayIcon;
            horizontalOverlayIconsButtons[i].isVerticalOverlay = false;
            horizontalOverlayIconsButtons[i].index = i;

            SetOverlayIconVisualStyle(i, true, false);
        }


        // Vertical overlay
        verticalOverlayIcons = new GameObject[pages.Length];
        verticalOverlayIconsButtons = new ButtonOverlayIcon[pages.Length];
        verticalPagesOverlayGO.transform.localPosition = new Vector3((-0.5f * fitToCam.x + buttonsPlacementZone.x + horizontalSpaceBetweenGridButtons) * 0.5f, (buttonsPlacementZone.height + buttonsPlacementZone.y) * 0.5f, cam.transform.position.y - 0.5f);

        for (int i = 0; i < pages.Length; i++)
        {
            verticalOverlayIcons[i] = Instantiate(Resources.Load("Prefabs/Store/Pages/PagesOverlayIcon")) as GameObject;
            verticalOverlayIcons[i].name = "Vertical Overlay Icon " + i;
            verticalOverlayIcons[i].transform.parent = GameObject.Find("PagesSizeOverlay").transform;
            verticalOverlayIcons[i].transform.localPosition = new Vector3(0, 0, (pages.Length * 0.5f - (i + 0.5f)) * (verticalOverlayIcons[i].transform.localScale.z + spaceBetweenOverlayIcons));

            verticalOverlayIconsButtons[i] = verticalOverlayIcons[i].GetComponent("ButtonOverlayIcon") as ButtonOverlayIcon;
            verticalOverlayIconsButtons[i].isVerticalOverlay = true;
            verticalOverlayIconsButtons[i].index = i;

            SetOverlayIconVisualStyle(i, false, false);
        }

        specialOverlayGO = verticalOverlayIcons[pages.Length - 1];


        SetCurrentPage(currentPage);
    }



    #endregion


    #region Update

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        //		if (checkForTimeOut)
        //		{
        //			// TimeOut
        //			if (Time.time >= timeOutStartTime + timeOutDuration)
        //			{
        //				HideInAppPopup();
        //				AVBinding.CreateInfoAlertView("ERREUR", timeOutString);
        //			}
        //		}
    }


    //	protected override void OnGUI()
    //	{
    //		base.OnGUI();
    //
    //		if (Input.GetKeyDown(KeyCode.A))
    //			ShowInAppPopup();
    //		if (Input.GetKeyDown(KeyCode.Z))
    //			HideInAppPopup();
    //	}

    #endregion


    #region Pages Management

    public StorePageEntity GetPage(Vector2 index)
    {
        if (index.x < 0 || index.y < 0 || index.x > pages[0].Count - 1 || index.y > pages.Length - 1)
            return null;

        return pages[(int)index.y][(int)index.x];
    }


    public StorePageEntity GetCurrentPage()
    {
        return pages[(int)currentPage.y][(int)currentPage.x];
    }


    // Prevents currentPage from being out of the pages range
    protected void SetCurrentPage(Vector2 index)
    {
        StorePageEntity destPage = GetPage(index);
        if (destPage != null && destPage.notAccessible)
            return;

        // Set the overlays icons of the previous current page back to the normal state
        SetOverlayIconVisualStyle((int)currentPage.x, true, false);
        SetOverlayIconVisualStyle((int)currentPage.y, false, false);

        currentPage = index;

        if (currentPage.y > pages.Length - 1)
        {
            currentPage = new Vector2(currentPage.x, pages.Length - 1);
        }
        else if (currentPage.y < 0)
        {
            currentPage = new Vector2(currentPage.x, 0);
        }

        if (currentPage.x > pages[0].Count - 1)
        {
            currentPage = new Vector2(pages[0].Count - 1, currentPage.y);
        }
        else if (currentPage.x < 0)
        {
            currentPage = new Vector2(0, currentPage.y);
        }

        // Set the overlays icons of the new current page to the current state
        SetOverlayIconVisualStyle((int)currentPage.x, true, true);
        SetOverlayIconVisualStyle((int)currentPage.y, false, true);


        // Handle the "Special" line
        if (currentPage.y == pages.Length - 1)
        { // If we're going to the Seecial line, hide the horizontal overlay
            horizontalPagesOverlayGO.SetActive(false);
        }
        else
        { // If we're leaving the Special line, show the horizontal overlay
            horizontalPagesOverlayGO.SetActive(true);
        }

        if (currentPage.x == 0)
        { // If we're going to the first row, show the "S" overlay
            specialOverlayGO.SetActive(true);
        }
        else
        { // If we're leaving the first row, hide the "S" overlay
            specialOverlayGO.SetActive(false);
        }

    }


    // Set the current page based on the cam position. Also set the cam's panBorders to fit the current page
    public void SetCurrentPageBasedOnCamPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        RaycastHit hitInfo;
        LayerMask menuPagesMask = 1 << 8;
        //if the Ray hit something create a new touch
        if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, menuPagesMask))
        {
            GoToPage((hitInfo.transform.GetComponent("StorePageEntity") as StorePageEntity).index);
        }
    }


    public void GoToNextPage()
    {
        Debug.Log("NEXT");
        GoToPage(new Vector2(currentPage.x + 1, currentPage.y));
    }


    public void GoToPreviousPage()
    {
        Debug.Log("PREVIOUS");
        GoToPage(new Vector2(currentPage.x - 1, currentPage.y));
    }


    public void GoToUpwardPage()
    {
        Debug.Log("UP");
        GoToPage(new Vector2(currentPage.x, currentPage.y - 1));
    }


    public void GoToDownwardPage()
    {
        Debug.Log("DOWN");
        GoToPage(new Vector2(currentPage.x, currentPage.y + 1));
    }


    // Set the page at the given index as the current one
    public void GoToPage(Vector2 index)
    {
        Vector2 tmp = currentPage;

        SetCurrentPage(index);

        if (tmp.x > index.x)
            cam.panVelocity = new Vector3(-0.5f, 0, 0);
        else if (tmp.x < index.x)
            cam.panVelocity = new Vector3(0.5f, 0, 0);
        else if (tmp.y > index.y)
            cam.panVelocity = new Vector3(0, 0, 0.5f);
        else if (tmp.y < index.y)
            cam.panVelocity = new Vector3(0, 0, -0.5f);

        cam.ComputePanBorders();
    }

    #endregion


    #region Unity App Events

    protected override void OnApplicationQuit()
    {

    }


    protected override void OnApplicationPause(bool pause)
    {
        Debug.Log("PAUSE: " + pause);

        // App pausing
        if (pause)
        {

        }
        // App resuming
        else
        {

        }
    }

    #endregion


    #region InApp Events

#if UNITY_IOS

    //DEV_CODE || PURCHASE || COMMENT
    //void productListReceived(List<StoreKitProduct> productList)
    //{
    //    Debug.Log("total productsReceived SL: " + productList.Count);

    //    if (ignoreUpcomingInAppEvents)
    //        return;

    //    if (productList.Count == 0)
    //    {
    //        productListRequestFailed("List returned is empty.");
    //    }
    //    else
    //    {
    //        // Do something more useful with the products than printing them to the console
    //        foreach (StoreKitProduct product in productList)
    //        {
    //            Debug.Log("product name - " + product.ToString() + "\n");
    //        }

    //        HideInAppPopup();

    //        openedAlertView = Global.AlertViews.InAppProductData;
    //        //						AVBinding.CreateAlertView (productList [0].title, productList [0].description + System.Environment.NewLine + System.Environment.NewLine + "Prix : " + productList [0].price + " " + productList [0].currencySymbol);

    //    }
    //}


    void productListRequestFailed(string error)
    {
        Debug.Log("productListRequestFailed: " + error);

        if (ignoreUpcomingInAppEvents)
            return;

        HideInAppPopup();

        //				AVBinding.CreateInfoAlertView ("INFORMATION", productListRequestFailedString);
    }

    void restoreFinished()
    {
        HideInAppPopup();
        if (ignoreUpcomingInAppEvents)
            return;

        Debug.Log("restoreFinished1");
        //				HideInAppPopup ();
    }



    void restoreFailed(string error)
    {
        HideInAppPopup();
        Debug.Log("restoreFailed " + error);
    }

    void purchaseFailed(string error)
    {
        Debug.Log("purchase failed with error: " + error);

        if (ignoreUpcomingInAppEvents)
            return;

        HideInAppPopup();

        //				AVBinding.CreateInfoAlertView ("INFORMATION", purchaseFailedString);
    }


    void purchaseCancelled(string error)
    {
        Debug.Log("purchase cancelled with error: " + error);

        if (ignoreUpcomingInAppEvents)
            return;

        HideInAppPopup();
    }

    //DEV_CODE || PURCHASE || COMMENT
    //void purchaseSuccessful(StoreKitTransaction transaction)
    //{
    //    //				Debug.Log ("purchased product: IDS " + transaction);

    //    string purchaseID;

    //    int version;
    //    if (Global.Global.iPadVersion)
    //        version = 0;
    //    else
    //        version = 1;

    //    purchaseID = buttonCurrentlyBeingProcessed.itemID[version];

    //    if (transaction.productIdentifier != purchaseID)
    //    {
    //        Debug.LogWarning("ERROR: purchaseID different than expected.");
    //        return;
    //    }

    //    HideInAppPopup();

    //    if (buttonCurrentlyBeingProcessed == null)
    //        return;

    //    //				Debug.Log ("purchased product: IDS " + transaction);
    //    buttonCurrentlyBeingProcessed.UnlockMyGrids();

    //    buttonCurrentlyBeingProcessed = null;

    //    //				AVBinding.CreateInfoAlertView ("INFORMATION", purchaseSuccessfulString);
    //}





#elif UNITY_ANDROID
    void billingSupportedEvent(bool isSupported)
    {
        Debug.Log("billingSupportedEvent: " + isSupported);

        waitingForBillingSupportedEvent = false;

        isBillingSupported = isSupported;

        //		EtceteraAndroid.showAlert("INFORMATION", cantMakePaymentsString, "OK");
    }


    void purchaseSucceededEvent(string productId)
    {
        Debug.Log("purchaseSucceededEvent: " + productId);

        if (buttonCurrentlyBeingProcessed == null)
            return;

        buttonCurrentlyBeingProcessed.UnlockMyGrids();

        buttonCurrentlyBeingProcessed = null;

        HideInAppPopup();

        //TODO || DEV19
        //EtceteraAndroid.showAlert("INFORMATION", purchaseSuccessfulString, "OK");
    }


    void purchaseCancelledEvent(string productId)
    {
        Debug.Log("purchaseCancelledEvent: " + productId);

        HideInAppPopup();
    }


    void purchaseFailedEvent(string productId)
    {
        Debug.Log("purchaseFailedEvent: " + productId);

        HideInAppPopup();

        //TODO || DEV20
        //EtceteraAndroid.showAlert("INFORMATION", purchaseFailedString, "OK");
    }

#endif
    #endregion




    #region Callbacks From Native

    //public override void AVCallback (string message)
    //{//
    //				base.AVCallback (message);
    //
    //
    //				if (openedAlertView == Global.AlertViews.Error) {
    //						return;
    //				}
    //
    //				if (openedAlertView == Global.AlertViews.InAppProductData) {
    //						// OK
    //						if (int.Parse (message) == 1) {
    //								string purchaseID;
    //
    //								int version;
    //								if (Global.Global.iPadVersion)
    //										version = 0;
    //								else
    //										version = 1;
    //
    //								purchaseID = buttonCurrentlyBeingProcessed.itemID [version];
    //#if UNITY_IPHONE
    //								StoreKitBinding.purchaseProduct (purchaseID, 1);
    //#endif
    //								ShowInAppPopup ();
    //						}
    //				} else if (openedAlertView == Global.AlertViews.OpenRealVersionPage) {
    //						// OK
    //						if (int.Parse (message) == 1) {
    //								string url;
    //#if UNITY_IPHONE
    //								if (Global.Global.iPadVersion)
    //										url = Global.Global.rateThisAppURLiPad;
    //								else
    //										url = Global.Global.rateThisAppURLiPhone;
    //#elif UNITY_ANDROID
    //				url = Global.Global.rateThisAppURLAndroid;
    //#endif
    //								Application.OpenURL (url);
    //
    //						}
    //				}
    //
    //}

    #endregion


    #region Wrappers

    //		public override bool isStoreLogic ()
    //		{
    //				return true;
    //		}

    public override Global.LogicType GetLogicType()
    {
        return Global.LogicType.StoreLogic;

    }


    protected void SetOverlayIconVisualStyle(int index, bool belongsToHorizontalOverlay, bool isCurrent)
    {
        if (belongsToHorizontalOverlay && !FindObjectOfType<Dev_StoreUIManager>().isUiElement)
        {
            if (isCurrent)
            {
                //				horizontalOverlayIcons[index].renderer.material = horizontalOverlayIconCurrentMat[(int)pages[0][index].difficulty];
                horizontalOverlayIconsButtons[index].SetNormalMaterial(horizontalOverlayIconCurrentMat[(int)pages[0][index].difficulty]);
                horizontalOverlayIconsButtons[index].SetPressedMaterial(horizontalOverlayIconCurrentMat[(int)pages[0][index].difficulty]);
                horizontalOverlayIcons[index].transform.localScale = overlayIconCurrentScale;
            }
            else
            {
                //				horizontalOverlayIcons[index].renderer.material = horizontalOverlayIconNormalMat[(int)pages[0][index].difficulty];
                horizontalOverlayIconsButtons[index].SetNormalMaterial(horizontalOverlayIconNormalMat[(int)pages[0][index].difficulty]);
                horizontalOverlayIconsButtons[index].SetPressedMaterial(horizontalOverlayIconNormalMat[(int)pages[0][index].difficulty]);
                horizontalOverlayIcons[index].transform.localScale = overlayIconNormalScale;
            }
        }
        else if (!belongsToHorizontalOverlay && !FindObjectOfType<Dev_StoreUIManager>().isUiElement)
        {
            if (isCurrent)
            {
                //				verticalOverlayIcons[index].renderer.material = verticalOverlayIconCurrentMat[index];
                verticalOverlayIconsButtons[index].SetNormalMaterial(verticalOverlayIconCurrentMat[index]);
                verticalOverlayIconsButtons[index].SetPressedMaterial(verticalOverlayIconCurrentMat[index]);
                verticalOverlayIcons[index].transform.localScale = overlayIconCurrentScale;
            }
            else
            {
                //				verticalOverlayIcons[index].renderer.material = verticalOverlayIconNormalMat[index];
                verticalOverlayIconsButtons[index].SetNormalMaterial(verticalOverlayIconNormalMat[index]);
                verticalOverlayIconsButtons[index].SetPressedMaterial(verticalOverlayIconNormalMat[index]);
                verticalOverlayIcons[index].transform.localScale = overlayIconNormalScale;
            }
        }
    }


    public void ShowInAppPopup()
    {
        inAppPopup.transform.localPosition = new Vector3(inAppPopup.transform.localPosition.x, inAppPopup.transform.localPosition.y, 1.2f);

        timeOutStartTime = Time.time;
        checkForTimeOut = true;

        ignoreUpcomingInAppEvents = false;

        loadingSpinner.StartSpinning();
    }


    public void HideInAppPopup()
    {
        inAppPopup.transform.localPosition = new Vector3(inAppPopup.transform.localPosition.x, inAppPopup.transform.localPosition.y, 0);

        checkForTimeOut = false;

        ignoreUpcomingInAppEvents = true;

        loadingSpinner.StopSpinning();
    }

    public void CheckForRequest()
    {
        //				Debug.Log ("item count" + itemButtons.Count);

        if (Global.Global.iPadVersion)
        {
            //						version = 0;
        }
        else
        {
            //						version = 1;
            //						Debug.Log ("item count" + itemButtons.Count);
            if (itemButtons.Count == 10)
            {
                //								ShowInAppPopup ();
                var iosProductIds = new string[itemButtons.Count];
                for (int i = 0; i < 10; i++)
                {
                    iosProductIds[i] = itemButtons[i].product;
                    //										Debug.Log ("iOS iD - " + itemButtons [i].product);
                    //										Debug.Log ("iOS iD2 - " + iosProductIds [i]);
                }

                //IAP.requestProductData(iosProductIds, iosProductIds, productList =>
                //{
                //    //								Debug.Log ("Product list received " + productList);
                //    HideInAppPopup();
                //});

            }
        }
    }

    public void RestorePurchase()
    {
        ShowInAppPopup();
        //				Debug.Log ("Restore Purchase " + itemButtons.Count);
        //				for (int i = 0; i < itemButtons.Count; i++) {
        //						Debug.Log ("items - " + itemButtons [i].product);
        //						itemButtons [i].RestoreTransaction ();
        //				}
        //				StoreKitBinding.restoreCompletedTransactions ();
        //IAP.restoreCompletedTransactions(productId =>
        //{
        //    HideInAppPopup();
        //    Debug.Log("restored purchased product: " + productId);
        //    for (int i = 0; i < itemButtons.Count; i++)
        //    {
        //        Debug.Log("purchase restore list " + itemButtons[i].product);
        //        if (productId == itemButtons[i].product)
        //        {
        //            Debug.Log("purchase restore if " + productId);
        //            itemButtons[i].UnlockMyGrids();
        //        }
        //    }
        //});




    }

    #endregion

    void OnGUI()
    {
        //				if (GUI.Button (new Rect (Screen.width - 100, 0, 100, 40), "Restore")) {
        //						RestorePurchase ();
        //				}

    }

}
