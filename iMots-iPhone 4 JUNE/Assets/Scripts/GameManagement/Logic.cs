using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Logic : MonoBehaviour
{

    #region Vars

    public OrthoCam cam;

    public bool inputBlocked = false;

    public List<string> headers = null;

    public string platform;

    // Levels loading
    public bool pendingLoadLevel = false;
    public int levelToBeLoaded = -1;

    // Alert Views
    public Global.AlertViews openedAlertView = Global.AlertViews.Error;

    protected AudioSource[] audioSources;


    #endregion


    #region Init

    // Use this for initialization
    protected virtual void Start()
    {

        //				PlayerPrefs.DeleteAll ();
        Debug.Log("STARTING " + GetType());
        // iPad
        if (Global.Global.iPadVersion)
        {
            platform = "iPad";
        }
        // iPhone/iPod
        else
        {
            platform = "iPhone";
        }


        // Handles the flagging of the unlocked grids at the first launch ever of the game
        if (!PlayerPrefs.HasKey("FirstTimeLaunchedFlagUnlockedGrids"))
        {
            Debug.Log("Game is launched for the first time so we flag the unlocked grids.");
            PlayerPrefs.SetInt("FirstTimeLaunchedFlagUnlockedGrids", 0);

            string platformID;
            int nbSizes, nbUnlockedGrids;

            if (Global.Global.iPadVersion)
            {
                platformID = "Ipad";
                // Nik - Add Code
                nbSizes = 5;
                // nbSizes = 3;
            }
            else
            {
                platformID = "Iphone";
                nbSizes = 2;
            }

            if (Global.Global.adEnabled)
            {
                nbUnlockedGrids = 10;
            }
            else
            {
                nbUnlockedGrids = 50;
            }
            Debug.Log("<color=yellow>Nik - Level unLock Code</color>");

            for (int size = 0; size < nbSizes; size++)
            {
                for (int difficulty = 0; difficulty < 3; difficulty++)
                {
                    for (int index = 1; index < nbUnlockedGrids + 1; index++)
                    {
                        Debug.Log("key = " + platformID + "-" + size + "-" + difficulty + "-" + Global.Global.ConvertToStringID(index));
                        string key = platformID + "-" + size + "-" + difficulty + "-" + Global.Global.ConvertToStringID(index);
                        if (!PlayerPrefs.HasKey(key))
                            PlayerPrefs.SetInt(key, (int)Global.GridState.Empty);
                    }
                }
            }
        }

        // Light load all the grid headers
        headers = XMLManager.LoadGridFilesHeaders();
        Debug.Log(headers.Count + " grids light-loaded.");
        headers = Global.Global.TrimLockedGrids(headers);
        Debug.Log("Nik -" + headers.Count + " grids light-loaded after removal of the locked ones.");

        // Game is launched for the first time
        if (!PlayerPrefs.HasKey("CurrentGrid"))
        {
            Debug.Log("Game is launched for the first time so we set the CurrentGrid to the first one.");
            if (headers.Count > 0)
            {
                PlayerPrefs.SetString("CurrentGrid", headers[0]);
            }
            else
                Debug.LogError("No grid found at application start.");
        }


        // Used to setup the player prefs concerning the volume of the ASs the first time
        AudioSource[] audioSourcesTmp = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        audioSources = new AudioSource[audioSourcesTmp.Length + 2];
        audioSourcesTmp.CopyTo(audioSources, 0);
        audioSources[audioSources.Length - 2] = (Resources.Load("Prefabs/AudioSources/SoundDefGrow") as GameObject).GetComponent("AudioSource") as AudioSource;
        audioSources[audioSources.Length - 1] = (Resources.Load("Prefabs/AudioSources/SoundButtonDefault") as GameObject).GetComponent("AudioSource") as AudioSource;

        foreach (AudioSource audioSource in audioSources)
        {
            if (!PlayerPrefs.HasKey(audioSource.name + "_Volume"))
            {
                PlayerPrefs.SetFloat(audioSource.name + "_Volume", audioSource.volume);
            }
        }

        // Setting of the sound volume
        if (PlayerPrefs.HasKey("MuteSound"))
        {
            int ms = PlayerPrefs.GetInt("MuteSound");
            if (ms == 1)
            {
                SetSoundMuted(true);
            }
            else
            {
                SetSoundMuted(false);
            }
        }
        else
        {
            SetSoundMuted(false);
        }

#if UNITY_ANDROID
        //TODO || DEV14
        //EtceteraAndroidManager.alertButtonClickedEvent += alertButtonClickedEvent;
        //EtceteraAndroidManager.alertCancelledEvent += alertCancelledEvent;
#endif
    }


    public virtual void SetupCamDependantObjects(bool firstTime)
    {
    }


    // Init the cam. Always call this when a grid is loaded, as it will adapt to the grid size
    protected virtual void InitCam()
    {
        cam.transform.position = new Vector3(0, cam.transform.position.y, 0);
        cam.SetOrthoSize();
        cam.ComputePanBorders();
    }

    #endregion


    #region Update

    // Update is called once per frame
    protected virtual void Update()
    {
        //		if (Input.GetKeyDown(KeyCode.L))
        //		{
        //			cam.SetOrthoSize();
        //		}


        if (pendingLoadLevel && levelToBeLoaded != -1)
        {
            pendingLoadLevel = false;

            Application.LoadLevel(levelToBeLoaded);
        }
    }


    //	protected virtual void OnGUI()
    //	{
    //
    //	}

    #endregion


    #region Unity App Events

    protected virtual void OnApplicationQuit()
    {

    }


    protected virtual void OnApplicationPause(bool pause)
    {

    }

    #endregion


    #region Wrappers

    public abstract Global.LogicType GetLogicType();


    public void SetSoundMuted(bool mute)
    {
        if (mute)
        {
            PlayerPrefs.SetInt("MuteSound", 1);

            foreach (AudioSource audioSource in audioSources)
            {
                audioSource.volume = 0;
            }
        }
        else
        {
            PlayerPrefs.SetInt("MuteSound", 0);

            foreach (AudioSource audioSource in audioSources)
            {
                audioSource.volume = PlayerPrefs.GetFloat(audioSource.name + "_Volume");
            }
        }
    }


    //	public static string GetLocalizedDifficulty(Global.Difficulty difficulty)
    //	{
    //		switch (difficulty)
    //		{
    //		case Global.Difficulty.Easy:
    //			return "Force 1";
    //
    //		case Global.Difficulty.Medium:
    //			return "Force 2";
    //
    //		case Global.Difficulty.Hard:
    //			return "Force 3";
    //
    //		default:
    //			return "ERROR";
    //		}
    //	}
    //
    //
    //	public static string GetLocalizedSize(Global.Size size)
    //	{
    //		switch (size)
    //		{
    //		case Global.Size.Small:
    //			if (Global.Global.iPadVersion)
    //				return "9x13";
    //			else
    //				return "8x5";
    //
    //		case Global.Size.Medium:
    //			if (Global.Global.iPadVersion)
    //				return "13x9";
    //			else
    //				return "10x10";
    //
    //		case Global.Size.Big:
    //			if (Global.Global.iPadVersion)
    //				return "18x18";
    //			else
    //				return "NA";
    //
    //		default:
    //			return "ERROR";
    //		}
    //	}

    #endregion


    #region Loading



    public virtual void SetPendingLoadLevel(int level)
    {
        pendingLoadLevel = true;
        levelToBeLoaded = level;

        CreateLoadingOverlay();
    }




    public virtual void CreateLoadingOverlay()
    {
        GameObject loading = Instantiate(Resources.Load("Prefabs/Loading")) as GameObject;
        loading.name = "Loading";
        loading.transform.parent = cam.transform;
        loading.transform.localPosition = new Vector3(0, 0, 9);

        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight || Application.platform == RuntimePlatform.OSXEditor)
        {
            float size = Camera.main.orthographicSize * 2;
            loading.transform.localScale = new Vector3(size * Camera.main.aspect, 1, size * Camera.main.aspect);
        }
        else
        {
            float size = Camera.main.orthographicSize * 2 / Camera.main.aspect;
            loading.transform.localScale = new Vector3(size * Camera.main.aspect, 1, size * Camera.main.aspect);
        }
    }


    public void DestroyLoadingOverlay()
    {
        Destroy(GameObject.Find("Loading"));
    }

    #endregion


    #region Callbacks From Native

    public virtual void AVCallback(string message)
    {
        Debug.Log("AVCALLBACK: " + message);

        if (openedAlertView == Global.AlertViews.Error)
        {
            return;
        }
    }

    #endregion


    #region Game Center

    public void GCReportAchievementProgress(string achievementId, double progress)
    {
#if UNITY_EDITOR

#elif UNITY_ANDROID || UNITY_IOS

        if (Social.localUser.authenticated)
        {
            Social.ReportProgress(achievementId, progress, GCProgressReportedHandler);
        }

#endif
    }


    private void GCProgressReportedHandler(bool success)
    {
        Debug.Log("*** HandleProgressReported: success = " + success);
    }

    #endregion


    #region Android Etcetera

    protected void alertButtonClickedEvent(string clickedButton)
    {
        Debug.Log("alertButtonClickedEvent");

        string message = "0"; // Default => button "Cancel" was pressed

        if (openedAlertView != null)
        {
            if (openedAlertView == Global.AlertViews.RateThisApp)
            {
                return;
            }
            else if (openedAlertView == Global.AlertViews.SolutionToDefinitionDouble)
            {
                ButtonIngameSolutionForWord buttonIngameSolutionForWord = GameObject.Find("SolutionToWordButton").GetComponent("ButtonIngameSolutionForWord") as ButtonIngameSolutionForWord;

                if (clickedButton == buttonIngameSolutionForWord.solutionToDefinitionDoubleAVUpButtonString)
                {
                    message = "1";
                }
                else if (clickedButton == buttonIngameSolutionForWord.solutionToDefinitionDoubleAVDownButtonString)
                {
                    message = "2";
                }
            }
            else
            {
                if (clickedButton == "Continuer")
                {
                    message = "1";
                }
            }
        }


        AVCallback(message);
    }


    protected void alertCancelledEvent()
    {
        Debug.Log("alertCancelledEvent");

        openedAlertView = Global.AlertViews.Error;
    }

    #endregion
}
