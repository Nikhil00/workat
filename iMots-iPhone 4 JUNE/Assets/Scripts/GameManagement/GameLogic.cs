using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif
using UnityEngine.SocialPlatforms;

public class GameLogic : Logic
{

    #region Vars

    // GO references
    public GameObject gridGO, ingameMenuGO, solutionToWordButton, inputDisablingMaskGO;

    [HideInInspector]
    public GameObject tokenPS;

    public Grid grid;

    bool isGridCompleted = false;

    // Score
    protected ScoreManager scoreManager;
    protected float timer;
    protected string bestTimer;
    protected TextMesh ingameMenuClockText, ingameMenuClockTextShadow, ingameMenuClockBestText, ingameMenuClockBestTextShadow;
    [HideInInspector]
    public int currentWordsCombo = 0;
    private int maxWordCombo = 0;
    public ScoreMenuButton scoreMenuButton;
    protected long localPlayerGCScore = 0;

    // Game Center
    UnityEngine.SocialPlatforms.ILeaderboard leaderboard;
    protected string leaderboardID;

    // Game mode properties
    public Global.GameMode gameMode;
    public bool onlyCreateLetterEntities = false;
    public float LetterEntitiesFadeDuration = 1f;
    public float LetterEntitiesFadeStartTime;

    protected Global.TilesSelection tilesSelection;
    // Tiles currently selected (only used in keyboard gameMode)
    protected List<LetterTileEntity> previousTilesSelection;
    // Tiles previously selected; used to restore the materials of these tiles when deselected (only used in keyboard gameMode)

    // Color
    protected string tintString;
    // To be set procedurally, according to the grid's size and difficulty. This will determine the assets to be used for that grid

    // Physics
    [HideInInspector]
    public bool physicsEnabled = true;


    // Difficulty properties
    public Global.Difficulty gameDifficulty;

    public DefinitionEntity focusedDefinition = null;

    // Ingame menu properties
    public bool isIngameMenuOpened = false;

    // Audio
    [HideInInspector]
    public AudioSource tokenDragAS, tokenDropAS, tileValidationAS, vKeyPressedAS, tokenBackToPosAS, pickCorrectLetterAS, selectTileAS, gridFinishedAS, tileSkipAS;
    AudioClip[] tileValidationSounds = new AudioClip[10];
    [HideInInspector]
    public bool tileSkipSoundPlaying = false;
    //	public bool muteSound = false;

    // Delayed word validation (medium difficulty)
    public bool isWordBeingValidated = false;
    const float delayBetweenValidations = 0.5f;
    List<Global.WordValidation> wordValidations = new List<Global.WordValidation>();


    // Delayed Load/Destroy management
    bool pendingDestroyGridElements = false, waitedForOneUpdate = false;
    string pendingGridToBeLoaded = null;

    // Delayed Setup cam
    bool pendingSetCamOrthoSize = false, waitedForOneUpdateOrthoSize = false;

    // Delayed Change GameMode
    Global.GameMode pendingGameModeToBeChanged = Global.GameMode.Error;

    // Delayed Reset Grid
    int pendingResetGrids = -1;

    // Delayed Hide Input Disabling Mask
    bool pendingHideMask = false;

    // Delayed GridCompleted() call
    bool pendingGridCompleted = false;

    public GUICam guiCam;

    // End of grid
    [HideInInspector]
    public bool waitingForEOGA = false;
    public EndOfGridAnim endOfGridAnim = null;

    // Rate this app
    string rateThisAppURL;

    // AV
    public string solutionWordAlreadyCorrectAVString, solutionGridAlreadyCorrectAVString, rateThisAppString;

    #endregion


    #region Init

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        if (Global.Global.iPadVersion)
        {
            if (Global.Global.adEnabled)
                leaderboardID = "com.digdog.freeflechesipad.";
            else
                leaderboardID = "com.digdog.imotsflechesdeluxe.";
        }
        else
        {
            if (Global.Global.adEnabled)
                leaderboardID = "com.digdog.freefleches.";
            else
                leaderboardID = "com.digdog.MotsFleches.";
        }
        leaderboardID += "totalscore";

        Debug.Log("leaderboardID is " + leaderboardID);


        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate(GCAuthenticationHandler);
        }
        else
        {
            leaderboard = Social.CreateLeaderboard();
            leaderboard.id = leaderboardID;//"com.digdog.imotsflechesdeluxe.totalscore";
        }




        scoreManager = GameObject.FindObjectOfType(typeof(ScoreManager)) as ScoreManager;
        scoreMenuButton = GameObject.FindObjectOfType(typeof(ScoreMenuButton)) as ScoreMenuButton;

        ingameMenuClockText = guiCam.ingameMenuClockTextGO.GetComponent("TextMesh") as TextMesh;
        ingameMenuClockTextShadow = guiCam.ingameMenuClockTextShadowGO.GetComponent("TextMesh") as TextMesh;

        ingameMenuClockBestText = guiCam.ingameMenuClockBestTextGO.GetComponent("TextMesh") as TextMesh;
        ingameMenuClockBestTextShadow = guiCam.ingameMenuClockBestTextShadowGO.GetComponent("TextMesh") as TextMesh;



        if (!PlayerPrefs.HasKey("PhysicsDisabled"))
        {
            PlayerPrefs.SetInt("PhysicsDisabled", 0);
        }
        Debug.Log("Nik - Playerprefs PhysicsDisabled : " +  PlayerPrefs.GetInt("PhysicsDisabled"));
        int phy = PlayerPrefs.GetInt("PhysicsDisabled");
        if (phy == 0)
            physicsEnabled = true;
        else
            physicsEnabled = false;


        // Initial setting of the rate this app
        if (!PlayerPrefs.HasKey("LastNbGridsCompletedAsk2"))
            PlayerPrefs.SetInt("LastNbGridsCompletedAsk2", 0);

        Debug.Log("Nik - Playerprefs LastNbGridsCompletedAsk2 : " +  PlayerPrefs.GetInt("LastNbGridsCompletedAsk2"));
        // Initial setting of the number of available solutions
        //		if (!PlayerPrefs.HasKey("nbGridSolutions"))
        //			PlayerPrefs.SetInt("nbGridSolutions", 2);
        if (!PlayerPrefs.HasKey("nbWordSolutions"))
            PlayerPrefs.SetInt("nbWordSolutions", 5);

        Debug.Log("Nik - Playerprefs nbWordSolutions : " +  PlayerPrefs.GetInt("nbWordSolutions"));
        // Setting of the gameMode
        if (PlayerPrefs.HasKey("gameMode"))
        {
            int gm = PlayerPrefs.GetInt("gameMode");
            if ((Global.GameMode)gm == Global.GameMode.DragNDrop)
                gameMode = Global.GameMode.DragNDrop;
            else if ((Global.GameMode)gm == Global.GameMode.Keyboard)
                gameMode = Global.GameMode.Keyboard;
            else
                gameMode = Global.GameMode.Error;

            Debug.Log("Nik - Playerprefs gameMode crated : " +  PlayerPrefs.GetInt("gameMode"));
        }
        else
        {
            gameMode = Global.GameMode.DragNDrop;
            PlayerPrefs.SetInt("gameMode", (int)gameMode);
            Debug.Log("Nik - Playerprefs gameMode not crated : " +  PlayerPrefs.GetInt("gameMode"));
        }


        // Setting of the game difficulty
        if (PlayerPrefs.HasKey("GameDifficulty"))
        {
            int gd = PlayerPrefs.GetInt("GameDifficulty");
            gameDifficulty = (Global.Difficulty)gd;
            Debug.Log("Nik - Playerprefs GameDifficulty crated : " +  PlayerPrefs.GetInt("GameDifficulty"));
        }
        else
        {
            gameDifficulty = Global.Difficulty.Easy;
            PlayerPrefs.SetInt("GameDifficulty", (int)gameDifficulty);
            Debug.Log("Nik - Playerprefs GameDifficulty not crated : " +  PlayerPrefs.GetInt("GameDifficulty"));
        }
        ChangeGameDifficulty(gameDifficulty);


        previousTilesSelection = new List<LetterTileEntity>();
        tilesSelection = new Global.TilesSelection("");


        LoadGrid(PlayerPrefs.GetString("CurrentGrid"));
        Debug.Log("Nik - Playerprefs CurrentGrid : " +  PlayerPrefs.GetString("CurrentGrid"));

        InitCam();

        // Set the definitions' translation animation values so that they will always fit into the panBorders. This has to be done after the init of the cam
        DefinitionEntity[] defEntities = FindObjectsOfType(typeof(DefinitionEntity)) as DefinitionEntity[];

        foreach (DefinitionEntity defEntity in defEntities)
        {
            defEntity.SetGrowTranslationToFitPanBorders();
        }

        pendingSetCamOrthoSize = true;


        // Audio sources
        tokenDragAS = GameObject.Find("SoundTokenDrag").GetComponent("AudioSource") as AudioSource;
        tokenDropAS = GameObject.Find("SoundTokenDrop").GetComponent("AudioSource") as AudioSource;
        tileValidationAS = GameObject.Find("SoundTileValidation").GetComponent("AudioSource") as AudioSource;
        vKeyPressedAS = GameObject.Find("SoundVKeyPressed").GetComponent("AudioSource") as AudioSource;
        tokenBackToPosAS = GameObject.Find("SoundTokenBackToPos").GetComponent("AudioSource") as AudioSource;
        pickCorrectLetterAS = GameObject.Find("SoundPickCorrectLetter").GetComponent("AudioSource") as AudioSource;
        selectTileAS = GameObject.Find("SoundSelectTile").GetComponent("AudioSource") as AudioSource;
        gridFinishedAS = GameObject.Find("SoundGridFinished").GetComponent("AudioSource") as AudioSource;
        tileSkipAS = GameObject.Find("SoundTileSkip").GetComponent("AudioSource") as AudioSource;


        // Tiles validation sounds loading
        for (int i = 0; i < tileValidationSounds.Length; i++)
        {
            tileValidationSounds[i] = Resources.Load("Sounds/Success" + i) as AudioClip;
        }

#if UNITY_IOS
        if (Global.Global.iPadVersion)
        {
            if (!Global.Global.adEnabled)
                rateThisAppURL = Global.Global.rateThisAppURLiPad;
            else
                rateThisAppURL = Global.Global.rateThisAppURLiPadFree;
        }
        else
        {
            if (!Global.Global.adEnabled)
                rateThisAppURL = Global.Global.rateThisAppURLiPhone;
            else
                rateThisAppURL = Global.Global.rateThisAppURLiPhoneFree;
        }
#elif UNITY_ANDROID
        if (!Global.Global.adEnabled)
            rateThisAppURL = Global.Global.rateThisAppURLAndroid;
        else
            rateThisAppURL = Global.Global.rateThisAppURLAndroidFree;
#endif


        // Handles the display of the help menu at the first launch ever of the game
        if (!PlayerPrefs.HasKey("FirstTimeLaunchedFlagHelpMenuOpen"))
        {
            guiCam.StartHelpMenuAnimation();
            PlayerPrefs.SetInt("FirstTimeLaunchedFlagHelpMenuOpen", 0);
        }


        // Generates the PS that will be used by the tokens
        tokenPS = new GameObject("tokenPSReference");
        tokenPS.transform.parent = transform;
        tokenPS.transform.localPosition = new Vector3(0, 0.05f, 0);

        Object subPSRessource;
        if (grid.difficulty == Global.Difficulty.Easy)
            subPSRessource = Resources.Load("Prefabs/Ingame/Particle Systems/TokenSubPSEasy");
        else if (grid.difficulty == Global.Difficulty.Medium)
            subPSRessource = Resources.Load("Prefabs/Ingame/Particle Systems/TokenSubPSMedium");
        else // if (grid.difficulty == Global.Difficulty.Hard)
            subPSRessource = Resources.Load("Prefabs/Ingame/Particle Systems/TokenSubPSHard");

        GameObject tmpSubPS;
        for (int i = 0; i < 360; i += 20)
        {
            tmpSubPS = Instantiate(subPSRessource) as GameObject;
            tmpSubPS.active = false;
            tmpSubPS.transform.parent = tokenPS.transform;
            tmpSubPS.transform.localPosition = new Vector3(0, 0, 0);
            tmpSubPS.transform.localEulerAngles = new Vector3(0, i, 0);
        }


#if UNITY_ANDROID
        //TODO || DEV16
        /*EtceteraAndroidManager.askForReviewDontAskAgainEvent += askForReviewDontAskAgainEvent;
        EtceteraAndroidManager.askForReviewRemindMeLaterEvent += askForReviewRemindMeLaterEvent;
        EtceteraAndroidManager.askForReviewWillOpenMarketEvent += askForReviewWillOpenMarketEvent;*/

        // Fix to workaround the screen rotation problems at launch time
        //		if ((Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight) && )
        //		cam.changingOrientation = true;
        //		cam.previousScreenHeight = 0;
#endif
    }


    // Init the grid. If firstTimeLoaded, it will assign random letters to the tokens (DnD game mode only)
    void InitGrid(bool firstTimeCreation)
    {
        if (grid == null)
        {
            Debug.LogError("No grid loaded, thus can't initialize it.");
            return;
        }

        List<string> answerLetters = new List<string>();

        previousTilesSelection.Clear();
        tilesSelection.tiles.Clear();

        if (!onlyCreateLetterEntities)
        {
            gridGO = Instantiate(Resources.Load("Prefabs/Grids/" + grid.size.xSize + "x" + grid.size.ySize)) as GameObject;
            gridGO.transform.parent = GameObject.Find("GameElements").transform;
            gridGO.transform.position = new Vector3(gridGO.transform.position.x, -0.6f, gridGO.transform.position.z);
            gridGO.name = "Grid";

            // Set the tintString according to the difficulty and the size of the grids
            // Set the difficulty part of the tintString
            if (grid.difficulty == Global.Difficulty.Easy)
            {
                tintString = "Easy";
            }
            else if (grid.difficulty == Global.Difficulty.Medium)
            {
                tintString = "Medium";
            }
            else if (grid.difficulty == Global.Difficulty.Hard)
            {
                tintString = "Hard";
            }
            else if (grid.difficulty == Global.Difficulty.vEasy)
            {
                tintString = "Easy";
            }
        }

        // If this is the first time this grid is being played, create an array with all the letters contained in the grid (answers); it will be used later to assign letters to tokens
        if (firstTimeCreation)
        {
            Debug.Log("Grid " + grid.name + " is loaded for the first time, thus we gotta assign initial values to the tokens");
            for (int i = 0; i < grid.tiles.GetLength(1); i++)
                for (int j = 0; j < grid.tiles.GetLength(0); j++)
                    if (grid.tiles[j, i].GetTileType() == Global.TileType.Letter)
                        answerLetters.Add((grid.tiles[j, i] as LetterTile).answerLetter);
        }


        int gridSizeX = grid.tiles.GetLength(1);
        int gridSizeY = grid.tiles.GetLength(0);


        // Grid elements creation
        GameObject gridElemGO;
        GridElementEntity gridElem;

        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                Tile currentTile = grid.tiles[j, i];

                gridElem = null;
                gridElemGO = null;

                if (currentTile.GetTileType() == Global.TileType.Definition && !onlyCreateLetterEntities)
                {
                    gridElemGO = Instantiate(Resources.Load("Prefabs/Ingame/Definition")) as GameObject;
                    gridElem = gridElemGO.GetComponent("DefinitionEntity") as DefinitionEntity;
                    gridElemGO.GetComponent<Renderer>().material = Resources.Load("Materials/Ingame/Definitions/Definition" + tintString) as Material;
                }
                else if (currentTile.GetTileType() == Global.TileType.Letter)
                {
                    if (gameMode == Global.GameMode.DragNDrop)
                    {
                        gridElemGO = Instantiate(Resources.Load("Prefabs/Ingame/Token")) as GameObject;
                        gridElem = gridElemGO.GetComponent("TokenEntity") as TokenEntity;
                        (gridElem as TokenEntity).blinkingMat = Resources.Load("Materials/Ingame/LetterEntities/Tokens/TokenBlinking" + tintString) as Material;
                    }
                    else
                    { // if (gameMode == Global.GameMode.Keyboard)
                        gridElemGO = Instantiate(Resources.Load("Prefabs/Ingame/LetterTile")) as GameObject;
                        gridElem = gridElemGO.GetComponent("LetterTileEntity") as LetterTileEntity;
                        (gridElem as LetterTileEntity).selectedMat = Resources.Load("Materials/Ingame/LetterEntities/LetterTiles/LetterTileSelected" + tintString) as Material;
                        (gridElem as LetterTileEntity).selectedWordMat = Resources.Load("Materials/Ingame/LetterEntities/LetterTiles/LetterTileWordSelected" + tintString) as Material;
                    }

                    // If this tile is the first tile of an answer
                    if ((grid.tiles[j, i] as LetterTile).isAnswerStart && !onlyCreateLetterEntities)
                    {
                        GameObject arrowGO = (Instantiate(Resources.Load("Prefabs/Ingame/ArrowTile")) as GameObject);
                        arrowGO.transform.parent = gridGO.transform;
                        Vector3 pos = GetWorldPositionFromGridPosition(new Vector2(i, j));
                        arrowGO.transform.localPosition = new Vector3(pos.x, arrowGO.transform.position.y, pos.z);

                        // Arrow is only horizontal
                        if ((grid.tiles[j, i] as LetterTile).arrowDirection == Global.ArrowDirection.Horizontal)
                        {
                            arrowGO.transform.Find("Vertical").gameObject.SetActive(false);
                        }
                        // Arrow is only vertical
                        else if ((grid.tiles[j, i] as LetterTile).arrowDirection == Global.ArrowDirection.Vertical)
                        {
                            arrowGO.transform.Find("Horizontal").gameObject.SetActive(false);
                        }
                    }
                }

                if (gridElem != null)
                {
                    gridElem.gridPosition = new Vector2(i, j);
                    gridElem.InitGridElement();
                }
            }
        }


        if (gameMode == Global.GameMode.DragNDrop)
        {
            // If this is the first time this grid is being played, we have to check that the newly assigned letters don't match the answer letters for each tile
            if (firstTimeCreation)
            {
                TokenEntity[] tokens = FindObjectsOfType(typeof(TokenEntity)) as TokenEntity[];
                foreach (TokenEntity token in tokens)
                {
                    AssignRandomLetterToLetterEntity(token, ref answerLetters);
                }

                PreventGoodAnswerAtFirstLoad();
            }
        }


        SetupCorrectGuesses();


        // Handles the fading in of the objects created when changing game mode
        if (onlyCreateLetterEntities)
        {
            LetterEntity[] letterEntities = FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];
            foreach (LetterEntity letterEntity in letterEntities)
            {
                if (!letterEntity.isBeingDestroyed)
                {
                    letterEntity.FadeIn();
                }
            }
        }

        // Set the state of the Vkeyboard; also, if we're changing game mode, make it fade, otherwise make it instantaneous
        if (gameMode == Global.GameMode.DragNDrop)
            guiCam.SetKeyboardState(Global.KeyboardState.Off, !onlyCreateLetterEntities);
        else
            guiCam.SetKeyboardState(Global.KeyboardState.Unfocused, !onlyCreateLetterEntities);

        onlyCreateLetterEntities = false;
    }


    // Init the cam. Always call this when a grid is loaded, as it will adapt to the grid size
    protected override void InitCam()
    {
        // Set the autorotations depending on the device and the grid loaded
        if (Global.Global.iPadVersion)
        {
            // if (grid.size.size == Global.Size.Small)
            // {
            //     cam.AllowOrientations(true, false);
            // }
            // else if (grid.size.size == Global.Size.Medium)
            // {
            //     cam.AllowOrientations(false, true);
            // }
            // else if (grid.size.size == Global.Size.Big)
            // {
            //     cam.AllowOrientations(true, true);
            // }
            if (grid.size.size == Global.Size.Small)
            {
                cam.AllowOrientations(true, true);
            }
            else if (grid.size.size == Global.Size.Medium)
            {
                cam.AllowOrientations(true, true);
            }
            else if (grid.size.size == Global.Size.Big)
            {
                cam.AllowOrientations(true, true);
            }
            else if (grid.size.size == Global.Size.VSmall)
            {
                cam.AllowOrientations(true, true);
            }
            else if (grid.size.size == Global.Size.VMedium)
            {
                cam.AllowOrientations(true, true);
            }
        }
        else
        {
            if (grid.size.size == Global.Size.Small)
            {
                cam.AllowOrientations(false, true);
            }
            else if (grid.size.size == Global.Size.Medium)
            {
                cam.AllowOrientations(true, true);
            }
        }

        base.InitCam();

        //		guiCam.SetOrthoSize();
    }

    #endregion


    #region Grid Elements Management

    // Return the GridElementEntity at the given grid Pos; if no GridElementEntity is found, return null
    public GridElementEntity GetGridElementAtGridPosition(Vector2 gridPos)
    {
        if (gridPos.x < 0 || gridPos.x > grid.tiles.GetLength(1) || gridPos.y < 0 || gridPos.y > grid.tiles.GetLength(0))
        {
            Debug.Log("GetGridElementAtGridPosition(Vector2 gridPos) => gridPos out of the grid size.");
            return null;
        }

        GridElementEntity[] gridElements = FindObjectsOfType(typeof(GridElementEntity)) as GridElementEntity[]; // TODO change that to a ref containing the GridElementEntities

        foreach (GridElementEntity gridElement in gridElements)
        {
            if (gridElement.IsAtGridPosition(gridPos))
                return gridElement;
        }

        return null;
    }


    // Return the grid position (x = 0->grid.xMax-1, y = 0->yMax-1) of the given WorldPosition; returns (-1, -1) if the worldPosition is out of the grid
    public Vector2 GetGridPositionFromWorldPosition(Vector3 worldPos)
    {
        Vector2 gridPos;

        if (worldPos.x < -grid.tiles.GetLength(1) * 0.5f || worldPos.x > grid.tiles.GetLength(1) * 0.5f || worldPos.z < -grid.tiles.GetLength(0) * 0.5f || worldPos.z > grid.tiles.GetLength(0) * 0.5f)
        {
            gridPos = new Vector2(-1, -1);
            Debug.Log("OUTSIDE");
        }
        else
        {
            gridPos = new Vector2(Mathf.FloorToInt(worldPos.x + grid.tiles.GetLength(1) * 0.5f), Mathf.FloorToInt(grid.tiles.GetLength(0) - (worldPos.z + grid.tiles.GetLength(0) * 0.5f)));
        }

        return gridPos;
    }


    // Return the the worldPosition of the given gridPosition (x = 0->grid.xMax-1, y = 0->yMax-1); returns (-1, -1, -1) if the gridPosition is out of the grid
    public Vector3 GetWorldPositionFromGridPosition(Vector2 gridPos)
    {
        Vector3 worldPos;

        if (gridPos.x < 0 || gridPos.x > grid.tiles.GetLength(1) - 1 || gridPos.y < 0 || gridPos.y > grid.tiles.GetLength(0) - 1)
        {
            worldPos = new Vector3(-1, -1, -1);
            Debug.Log("OUTSIDE");
        }
        else
        {
            worldPos = new Vector3((-grid.tiles.GetLength(1) * 0.5f + gridPos.x + 0.5f), 0, grid.tiles.GetLength(0) * 0.5f - gridPos.y - 0.5f);
        }

        return worldPos;
    }


    // Assign a random letter from the given answerLetters to the given tile (the datastruct, not the actual Entity!)
    private void AssignRandomLetterToLetterTile(LetterTile tile, ref List<string> answerLetters)
    {
        if (answerLetters.Count == 0)
            return;

        System.Random rd = new System.Random();

        int rdIndex = rd.Next(answerLetters.Count);
        string rdLetter = answerLetters[rdIndex];

        tile.guessLetter = rdLetter; // Set the guessLetter of the given tile to be rdLetter

        answerLetters.RemoveAt(rdIndex); // Remove this letter from the list as it has been used already

    }


    // Assign a random letter from the given answerLetters to the given grid element
    public void AssignRandomLetterToLetterEntity(LetterEntity letterEntity, ref List<string> answerLetters)
    {
        if (answerLetters.Count == 0)
            return;

        System.Random rd = new System.Random();

        int rdIndex = rd.Next(answerLetters.Count);
        string rdLetter = answerLetters[rdIndex];

        letterEntity.SetGuess(rdLetter, false, false);

        answerLetters.RemoveAt(rdIndex); // Remove this letter from the list as it has been used already

    }


    // Assign the given grid position to the given grid element
    public void AssignPositionToLetterEntity(LetterEntity letterEntity, Vector2 pos, bool checkForCombo)
    {
        letterEntity.gridPosition = pos;
        letterEntity.SetGuess(letterEntity.textMesh.text, true, checkForCombo);
    }


    public void setGuessForCurrentlySelectedTile(string guess)
    {
        if (tilesSelection.tiles.Count <= 0)
            return;


        tilesSelection.tiles[tilesSelection.currentIndex].SetGuess(guess, true, true);

        IncrementTilesSelectionIndex();
    }

    public void DeleteGuessForSelectedTile()
    {
        if (tilesSelection.tiles.Count <= 0)
            return;


        tilesSelection.tiles[tilesSelection.currentIndex].SetGuess("", true, true);

        DecrementTilesSelectionIndex();
    }


    // Look for a token in the grid holding the given letter. The token found will NEVER be already correct. Return the token if found, null otherwise
    public TokenEntity FindTokenEntityWithGuessLetter(string letter)
    {
        // Only available in DnD mode
        if (gameMode != Global.GameMode.DragNDrop)
            return null;

        TokenEntity[] tokenEntities = FindObjectsOfType(typeof(TokenEntity)) as TokenEntity[];

        LetterTile letterTile;
        foreach (TokenEntity tokenEntity in tokenEntities)
        {
            letterTile = (grid.tiles[(int)tokenEntity.gridPosition.y, (int)tokenEntity.gridPosition.x] as LetterTile);
            if (/*tokenEntity.isSwappingPosition ||*/ letterTile.answerLetter == letterTile.guessLetter)
                continue;

            if (tokenEntity.textMesh.text == letter)
                return tokenEntity;
        }

        return null;
    }

    #endregion


    #region End Of Grid

    // Return true if the current grid is completed
    private bool IsGridCompleted()
    {
        LetterEntity[] letterEntities = FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];
        bool isCompleted = true;
        LetterTile correspondingTile;
        foreach (LetterEntity letterEntity in letterEntities)
        {
            if ((int)letterEntity.gridPosition.y == -1 || (int)letterEntity.gridPosition.x == -1)
            {
                isCompleted = false;
                break;
            }
            correspondingTile = grid.tiles[(int)letterEntity.gridPosition.y, (int)letterEntity.gridPosition.x] as LetterTile;

            if (correspondingTile.GetTileType() == Global.TileType.Letter && correspondingTile.guessLetter != correspondingTile.answerLetter)
            {
                isCompleted = false;
                break;
            }
        }

        return isCompleted;
    }


    // Delays the call to GridCompleted from one frame, to allow the last token to be set properly before the anim starts
    protected void SetPendingGridCompleted()
    {
        pendingGridCompleted = true;
        waitedForOneUpdate = false;
    }

    // Everything that needs to be done when a grid is completed
    public void GridCompleted()
    {
        Debug.Log("-- Grid completed --");

        isGridCompleted = true;


        LetterEntity[] letterEntities = FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];

        foreach (LetterEntity letterEntity in letterEntities)
        {
            if (grid.tiles[(int)letterEntity.gridPosition.y, (int)letterEntity.gridPosition.x].GetTileType() == Global.TileType.Letter && !letterEntity.isCorrect)
                letterEntity.SetCorrect(true, false);
        }

        // Only do this stuff if the grid is not completed yet (= not when a completed grid is launched again later on without being reset)
        if (PlayerPrefs.GetInt(grid.name) != (int)Global.GridState.Completed)
        {
            PlayerPrefs.SetInt(grid.name, (int)Global.GridState.Completed);

            gridFinishedAS.Play();

            Debug.Log(">>> " + timer + "s <<<");
            PlayerPrefs.SetFloat(grid.name + "_Timer", timer);
            if (!PlayerPrefs.HasKey(grid.name + "_TimerBest") || PlayerPrefs.HasKey(grid.name + "_TimerBest") && PlayerPrefs.GetFloat(grid.name + "_TimerBest") >= timer)
            {
                PlayerPrefs.SetFloat(grid.name + "_TimerBest", timer);
            }


            Debug.Log(">>> " + maxWordCombo + " combo max <<<");
            Debug.Log("Nik - Grid Name : " + grid.name);
            PlayerPrefs.SetInt(grid.name + "_Combo", maxWordCombo);


            int gridScore = scoreManager.ComputeGridScore(grid.name, false);

            if (!PlayerPrefs.HasKey(grid.name + "_ScoreBest") || PlayerPrefs.HasKey(grid.name + "_ScoreBest") && PlayerPrefs.GetInt(grid.name + "_ScoreBest") < gridScore)
            {
                PlayerPrefs.SetInt(grid.name + "_ScoreBest", gridScore);
                scoreMenuButton.newScoreRecord = true;
            }
            else
            {
                scoreMenuButton.newScoreRecord = false;
            }

            scoreManager.ComputeTotalScore();

            if (Social.localUser.authenticated)
            {
                GCReportScore(scoreManager.totalScore);

                GCReportProgress();
            }

            //			// If onlyCreateLetterEntities is true then the grid has been completed by "giving the solution" of the grid, and thus we don't play the end anim so that the player can look at the solution
            //			if (!onlyCreateLetterEntities)
            //			{
            //				// Play the end effects
            //				StartEndOfGridAnim();
            //			}

            waitingForEOGA = true;

            //DEV_CODE || ADMOB || HIDE BANNER VIEW
            //cam.HideBannerView();
        }
    }


    public void StartEndOfGridAnim()
    {
        if (waitingForEOGA)
        {
            waitingForEOGA = false;
            Color tapToContinueColor = guiCam.tapToContinueGO.GetComponent<Renderer>().material.color;
            guiCam.tapToContinueGO.GetComponent<Renderer>().material.color = new Color(tapToContinueColor.r, tapToContinueColor.g, tapToContinueColor.b, 0);
        }

        Global.EndOfGridAnim lastExistingAnim = Global.EndOfGridAnim.BigCollision;
        int rd = Random.Range(0, (int)lastExistingAnim + 1);
        // Security clamp
        if (rd > (int)lastExistingAnim)
            rd = (int)lastExistingAnim;

        endOfGridAnim = (Instantiate(Resources.Load("Prefabs/Ingame/EndOfGridAnims/EndAnim" + (Global.EndOfGridAnim)rd)) as GameObject).GetComponent("EndOfGridAnim") as EndOfGridAnim;

        // DEBUG => used to see a specific EOG animation
        //		endOfGridAnim = (Instantiate(Resources.Load("Prefabs/Ingame/EndOfGridAnims/EndAnimWaterfall")) as GameObject).GetComponent("EndOfGridAnim") as EndOfGridAnim; // TODO random the end anim
        //		endOfGridAnim = (Instantiate(Resources.Load("Prefabs/Ingame/EndOfGridAnims/EndAnimHelium")) as GameObject).GetComponent("EndOfGridAnim") as EndOfGridAnim;
        //		endOfGridAnim = (Instantiate(Resources.Load("Prefabs/Ingame/EndOfGridAnims/EndAnimGrenade")) as GameObject).GetComponent("EndOfGridAnim") as EndOfGridAnim;
        //		endOfGridAnim = (Instantiate(Resources.Load("Prefabs/Ingame/EndOfGridAnims/EndAnimRotateZ")) as GameObject).GetComponent("EndOfGridAnim") as EndOfGridAnim;
        //		endOfGridAnim = (Instantiate(Resources.Load("Prefabs/Ingame/EndOfGridAnims/EndAnimGrow")) as GameObject).GetComponent("EndOfGridAnim") as EndOfGridAnim;
        //		endOfGridAnim = (Instantiate(Resources.Load("Prefabs/Ingame/EndOfGridAnims/EndAnimBigCollision")) as GameObject).GetComponent("EndOfGridAnim") as EndOfGridAnim;

        endOfGridAnim.Init();

        endOfGridAnim.StartAnim();
    }


    // Called by the EndOfGridAnim when it's finished
    public void EndOfGridAnimFinished()
    {
        cam.AllowOrientations(false, true);

        // Fill the score popup with the correct info
        string[] splittedId = grid.name.Split('-');

        if (splittedId.Length != 4)
        {
            Debug.LogWarning("Be careful, grid ID " + grid.name + " doesn't match the intended format");
            return;
        }

        int size = int.Parse(splittedId[1]);
        int difficulty = int.Parse(splittedId[2]);

        // Values
        scoreMenuButton.valuesString[0] = Global.Global.ConvertToStringDifficulty(difficulty) + " - " + Global.Global.ConvertToStringSize(size);
        scoreMenuButton.valuesString[1] = (guiCam.ingameMenuClockTextGO.GetComponent("TextMesh") as TextMesh).text;
        if (!PlayerPrefs.HasKey(grid.name + "_TimerBest") || PlayerPrefs.HasKey(grid.name + "_TimerBest") && PlayerPrefs.GetFloat(grid.name + "_TimerBest") == timer)
        {
            scoreMenuButton.valuesString[1] = "Record ! " + scoreMenuButton.valuesString[1];
        }
        scoreMenuButton.valuesString[2] = maxWordCombo.ToString() + " Maximum";

        // Score values
        int gridBaseScore = scoreManager.ComputeScoreForGridType(grid.name);
        int gridTimeScore = scoreManager.ComputeScoreForTime(timer, size);
        int gridComboScore = scoreManager.ComputeScoreForCombo(maxWordCombo);
        scoreMenuButton.scores[0] = gridBaseScore;
        scoreMenuButton.scores[1] = gridTimeScore;
        scoreMenuButton.scores[2] = gridComboScore;
        scoreMenuButton.scores[3] = (gridBaseScore + gridTimeScore + gridComboScore);

        // If the score stored locally is lower than the actual GC score for the localPlayer, use the real GC score
        if (localPlayerGCScore > scoreManager.totalScore)
            scoreMenuButton.scores[4] = localPlayerGCScore;
        else
            scoreMenuButton.scores[4] = scoreManager.totalScore;

        guiCam.StartScoreMenuAnimation();
        guiCam.StartTabsAnimation();
    }


    public void LoadNextGrid()
    {
        string previousId = grid.name;

        int index = headers.FindIndex(delegate (string id)
        {
            return id == previousId;
        }) + 1;
        if (index > headers.Count - 1)
            index = 0;

        PlayerPrefs.SetString("CurrentGrid", headers[index]);

        SetPendingLoadLevel(0);
    }

    #endregion


    #region Correct Guesses Handling

    // Loops for each tile and set their state according to their correctness AND the gameDifficulty
    public void SetupCorrectGuesses()
    {
        LetterEntity[] letterEntities = FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];

        foreach (LetterEntity letterEntity in letterEntities)
        {
            letterEntity.SetCorrect(false, false);
        }

        foreach (LetterEntity letterEntity in letterEntities)
        {
            if (!letterEntity.isCorrect)
            {
                CheckForCorrectGuess(letterEntity, true, false);
            }
        }
    }


    // Check if the given tile as been guessed correctly
    public void CheckForCorrectGuess(LetterEntity letterEntity, bool checkForCompletedGrid, bool playSounds)
    {
        if ((grid.tiles[(int)letterEntity.gridPosition.y, (int)letterEntity.gridPosition.x] as LetterTile).guessLetter == (grid.tiles[(int)letterEntity.gridPosition.y, (int)letterEntity.gridPosition.x] as LetterTile).answerLetter)
        {
            // Easy difficulty => every correct letter is validated
            if (gameDifficulty == Global.Difficulty.Easy)
            {
                letterEntity.SetCorrect(true, false);
                if (playSounds)
                {
                    tileValidationAS.clip = tileValidationSounds[0];
                    tileValidationAS.Play();
                }
            }
            else if (gameDifficulty == Global.Difficulty.Medium)
            { // Medium difficulty => every correct word is validated
              // Check every letter of the word(s) concerned by the moved/added letter(s)

                int x = (int)letterEntity.gridPosition.x;
                int y = (int)letterEntity.gridPosition.y;


                // HORIZONTAL WORD
                int xFirst, xLast;

                // Search for the first letter of the horizontal word
                while (x >= 0)
                {
                    if (grid.tiles[y, x].GetTileType() == Global.TileType.Definition)
                        break;
                    x--;
                }
                xFirst = x + 1;

                // Search for the last letter of the horizontal word
                x = (int)letterEntity.gridPosition.x; // Reset x to default
                while (x <= grid.tiles.GetLength(1) - 1)
                {
                    if (grid.tiles[y, x].GetTileType() == Global.TileType.Definition)
                        break;
                    x++;
                }
                xLast = x - 1;

                // Check if the letters in the found range is an actual answer and not a letter alone
                if (xLast > xFirst)
                {
                    // Check every letter inbetween the first and the last to see if they are correct
                    bool isCompletelyCorrect = true;
                    for (int i = xFirst; i <= xLast; i++)
                    {
                        if ((grid.tiles[y, i] as LetterTile).guessLetter != (grid.tiles[y, i] as LetterTile).answerLetter)
                        {
                            isCompletelyCorrect = false;
                            break;
                        }
                    }

                    if (isCompletelyCorrect)
                    {
                        LetterEntity[] letterEntitiesToValidate = new LetterEntity[xLast - xFirst + 1];
                        for (int i = xFirst; i <= xLast; i++)
                        {
                            letterEntitiesToValidate[i - xFirst] = GetGridElementAtGridPosition(new Vector2(i, y)) as LetterEntity;
                        }

                        if (playSounds)
                        {
                            isWordBeingValidated = true;
                            Global.WordValidation newWordValidation = new Global.WordValidation();
                            newWordValidation.tilesToValidate = letterEntitiesToValidate;
                            wordValidations.Add(newWordValidation);
                        }

                        for (int i = 0; i < letterEntitiesToValidate.Length; i++)
                        {
                            if (!letterEntitiesToValidate[i].isCorrect)
                            {
                                letterEntitiesToValidate[i].SetCorrect(true, playSounds);
                            }
                        }
                    }
                }
                // END HORIZONTAL WORD

                // VERTICAL WORD
                x = (int)letterEntity.gridPosition.x; // Reset x to default
                int yFirst, yLast;

                // Search for the first letter of the vertical word
                while (y >= 0)
                {
                    if (grid.tiles[y, x].GetTileType() == Global.TileType.Definition)
                        break;
                    y--;
                }
                yFirst = y + 1;

                // Search for the last letter of the vertical word
                y = (int)letterEntity.gridPosition.y; // Reset x to default
                while (y <= grid.tiles.GetLength(0) - 1)
                {
                    if (grid.tiles[y, x].GetTileType() == Global.TileType.Definition)
                        break;
                    y++;
                }
                yLast = y - 1;

                if (yLast > yFirst)
                {
                    // Check every letter inbetween the first and the last to see if they are correct
                    bool isCompletelyCorrect = true;
                    for (int i = yFirst; i <= yLast; i++)
                    {
                        if ((grid.tiles[i, x] as LetterTile).guessLetter != (grid.tiles[i, x] as LetterTile).answerLetter)
                        {
                            isCompletelyCorrect = false;
                            break;
                        }
                    }

                    if (isCompletelyCorrect)
                    {
                        LetterEntity[] letterEntitiesToValidate = new LetterEntity[yLast - yFirst + 1];
                        for (int i = yFirst; i <= yLast; i++)
                        {
                            letterEntitiesToValidate[i - yFirst] = GetGridElementAtGridPosition(new Vector2(x, i)) as LetterEntity;
                        }

                        if (playSounds)
                        {
                            isWordBeingValidated = true;
                            Global.WordValidation newWordValidation = new Global.WordValidation();
                            newWordValidation.tilesToValidate = letterEntitiesToValidate;
                            wordValidations.Add(newWordValidation);
                        }

                        for (int i = 0; i < letterEntitiesToValidate.Length; i++)
                        {
                            if (!letterEntitiesToValidate[i].isCorrect)
                            {
                                letterEntitiesToValidate[i].SetCorrect(true, playSounds);
                            }
                        }
                    }
                }
                // END VERTICAL WORD

            }

            //			CheckForWordsCombo(tilesSelection.tiles[tilesSelection.currentIndex]);

            // Grid is completed
            if (checkForCompletedGrid && IsGridCompleted())
            {
                //				GridCompleted();
                SetPendingGridCompleted();
            }
        }
        else
        {
            letterEntity.SetCorrect(false, false);
        }
    }


    // Clear the given grid in a way that only the current correct guesses are kept. If no id is provided (id = ""), do this for the currentGrid
    public void OnlyKeepCorrectGuesses(string id)
    {
        // If an id is given, load the corresponding grid.
        if (id != "")
        {
            grid = XMLManager.LoadGridFile("XMLs/" + id);

            if (PlayerPrefs.HasKey(grid.name) && PlayerPrefs.GetInt(grid.name) == (int)Global.GridState.Started)
            {
                XMLManager.LoadGameStateFile(ref grid);
            }
        }


        if (grid == null)
        {
            Debug.LogError(name + " - Couldn't OnlyKeepCorrectGuesses() because the grid is null.");
            return;
        }

        LetterTile tile; // Will be used by all the different steps to get the current tile of the loops, in order to avoid having to access the array every single time

        List<string> answerLetters = new List<string>();

        for (int i = 0; i < grid.tiles.GetLength(1); i++)
        {
            for (int j = 0; j < grid.tiles.GetLength(0); j++)
            {
                // We're only interested in the letter tiles
                if (grid.tiles[j, i].GetTileType() == Global.TileType.Letter)
                    tile = grid.tiles[j, i] as LetterTile;
                else
                    continue;

                if (tile.guessLetter != tile.answerLetter || gameDifficulty == Global.Difficulty.Hard)
                {
                    tile.guessLetter = "";
                    tile.alreadyMoved = false;

                    if (gameMode == Global.GameMode.DragNDrop)
                        answerLetters.Add(tile.answerLetter);
                }
                else if (tile.guessLetter == tile.answerLetter)
                {
                    tile.dontSwap = true;
                }
            }
        }

        // If the new mode is Drag n Drop, we need to spread the remaining letters into the remaining tiles
        if (gameMode == Global.GameMode.DragNDrop)
        {
            // Check if within the remaining letters there are not more than half being the same, otherwise no need to prevent them to be correct, it's not possible!
            bool doPreventCorrectGuessFromRandom = false;
            if (answerLetters.Count > 1)
            {
                doPreventCorrectGuessFromRandom = true;
                answerLetters.Sort();
                string currentLetter = answerLetters[0];
                int count = 0;

                for (int i = 0; i < answerLetters.Count; i++)
                {
                    // Everytime we find a new letter
                    if (answerLetters[i] != currentLetter)
                    {
                        // If the amount of the previous letter is bigger than half the letters to be assigned randomly => no need to try preventing good answers, it's impossible
                        if (count > answerLetters.Count / 2)
                        {
                            doPreventCorrectGuessFromRandom = false;
                            break;
                        }
                        // Else if we already reached more than half of the letters to be assigned randomly => it's ok, as we already scanned more than half of them, it can't happen from now on
                        else if (i > answerLetters.Count / 2)
                        {
                            Debug.Log("OnlyKeepCorrectGuesses - Cutout of the doPreventCorrectGuessFromRandom check because we reached half of the List");
                            break;
                        }

                        currentLetter = answerLetters[i];
                        count = 0;
                    }
                    count++;
                }
            }

            // Assign the remaining tiles with the remaining letters
            for (int i = 0; i < grid.tiles.GetLength(1); i++)
            {
                for (int j = 0; j < grid.tiles.GetLength(0); j++)
                {
                    // We're only interested in the letter tiles
                    if (grid.tiles[j, i].GetTileType() == Global.TileType.Letter)
                        tile = grid.tiles[j, i] as LetterTile;
                    else
                        continue;

                    if (tile.guessLetter == "")
                    {
                        AssignRandomLetterToLetterTile(tile, ref answerLetters);
                    }
                }
            }

            // Prevent any newly assigned letter to be correct
            if (doPreventCorrectGuessFromRandom)
                PreventGoodAnswerAtChangeMode();
            else
                Debug.Log("Can't PreventGoodAnswerAtChangeMode() because more than half of the remaining letters are the same. Thus, some tiles will now be correct even if they were not before.");
        }

        // If an id is given, save the grid. Else there is no need as it is the grid being played (thus it will be saved onAppExit)
        if (id != "")
        {
            SaveGrid(grid.name);
        }
    }


    // Check if the assigned letters of the tiles are not matching their answer letters; If so, swap them so that they don't
    private void PreventGoodAnswerAtChangeMode()
    {
        System.Random rd = new System.Random();

        int rdX, rdY;

        LetterTile tile, randomedTile;

        for (int i = 0; i < grid.tiles.GetLength(1); i++)
        {
            for (int j = 0; j < grid.tiles.GetLength(0); j++)
            {
                // We're only interested in the letter tiles
                if (grid.tiles[j, i].GetTileType() == Global.TileType.Letter)
                    tile = grid.tiles[j, i] as LetterTile;
                else
                    continue;

                // If the tile is a letterTile AND the guess is the same as the answer AND is not set as dontSwap
                if (tile.guessLetter == tile.answerLetter && !tile.dontSwap)
                {
                    bool done = false;

                    while (!done)
                    {
                        // Random a tile
                        rdX = rd.Next(grid.tiles.GetLength(1));
                        rdY = rd.Next(grid.tiles.GetLength(0));

                        // We're only interested in the letter tiles
                        if (grid.tiles[rdY, rdX].GetTileType() == Global.TileType.Letter)
                            randomedTile = grid.tiles[rdY, rdX] as LetterTile;
                        else
                            continue;

                        // If the randomed tile is a letterTile AND the randomed tile's answer doesn't match the current tile's guess AND the randomed tile's guess doesn't match the current tile's answer => swap them so that everybody is happy! 
                        if (!randomedTile.dontSwap && randomedTile.answerLetter != tile.guessLetter && randomedTile.guessLetter != tile.answerLetter)
                        {
                            string tmpLetter = tile.guessLetter;

                            // Assign the letter of the other tile to the current tile
                            tile.guessLetter = randomedTile.guessLetter;

                            // Assign the letter of the current tile to the other tile
                            randomedTile.guessLetter = tmpLetter;

                            done = true;
                        }
                    }

                }
            }
        }
    }


    // Check if the letters assigned to the tiles are not matching their answer letters; If so, swap them so that they don't. Only in DnD game mode
    private void PreventGoodAnswerAtFirstLoad()
    {
        if (gameMode != Global.GameMode.DragNDrop)
            return;

        System.Random rd = new System.Random();

        int rdTile;

        TokenEntity[] tokenEntities = FindObjectsOfType(typeof(TokenEntity)) as TokenEntity[];
        LetterTile currentTile;

        foreach (TokenEntity token in tokenEntities)
        {
            currentTile = grid.tiles[(int)token.gridPosition.y, (int)token.gridPosition.x] as LetterTile;

            // If the guess is the same as the answer
            if (currentTile.guessLetter == currentTile.answerLetter)
            {
                bool done = false;

                while (!done)
                {
                    // Random a token
                    rdTile = rd.Next(tokenEntities.Length);
                    TokenEntity randomedToken = tokenEntities[rdTile];
                    LetterTile randomedTile = grid.tiles[(int)randomedToken.gridPosition.y, (int)randomedToken.gridPosition.x] as LetterTile;

                    // If the randomed token's answer doesn't match the current token's guess AND the randomed token's guess doesn't match the current token's answer => swap them so that everybody is happy! 
                    if (randomedTile.answerLetter != currentTile.guessLetter && randomedTile.guessLetter != currentTile.answerLetter)
                    {
                        string tmpLetter = currentTile.guessLetter;

                        // Assign the letter of the other tile to the current tile
                        token.SetGuess(randomedTile.guessLetter, true, false);
                        token.textMesh.text = randomedTile.guessLetter;

                        // Assign the letter of the current tile to the other tile
                        randomedToken.SetGuess(tmpLetter, false, false);
                        randomedToken.textMesh.text = randomedTile.guessLetter;

                        done = true;
                    }
                }

            }
        }
    }


    // Return true if the words currently associated with the given defTile are correct
    public bool AreAssociatedWordsAlreadyCorrect(DefinitionTile defTile)
    {
        // Check if the words corresponding to this def are already correct
        for (int defIndex = 0; defIndex < 2; defIndex++)
        {
            // If there is no def for this index (only happens for i = 1 when the defTile has only one def)
            if (defTile.definitions[defIndex].defSize == Global.DefinitionSize.Error)
            {
                break;
            }

            Global.Answer ans = defTile.definitions[defIndex].answer;

            Debug.Log("SOLUTION is: " + ans.text);

            LetterTile[] letterTilesComposingTheWord = new LetterTile[ans.text.Length];

            int iMin, jMin, iMax, jMax;
            iMin = ans.xStart;
            jMin = ans.yStart;

            if (ans.ansDir == Global.Direction.Horizontal)
            {
                iMax = iMin + letterTilesComposingTheWord.Length;
                jMax = jMin;
            }
            else
            {
                iMax = iMin;
                jMax = jMin + letterTilesComposingTheWord.Length;
            }

            // Register the tiles corresponding to the answer of the given def 
            for (int i = iMin; i < (iMax == iMin ? iMax + 1 : iMax); i++)
            {
                for (int j = jMin; j < (jMax == jMin ? jMax + 1 : jMax); j++)
                {
                    letterTilesComposingTheWord[i == iMax ? j - jMin : i - iMin] = grid.tiles[j, i] as LetterTile;
                }
            }

            // Check if the word is already correct (if the whole word is correct, no point in giving the solution...)
            bool isCompletelyCorrect = true;
            foreach (LetterTile letterTileComposingTheWord in letterTilesComposingTheWord)
            {
                if (letterTileComposingTheWord.answerLetter != letterTileComposingTheWord.guessLetter)
                {
                    isCompletelyCorrect = false;
                    break;
                }
            }

            if (!isCompletelyCorrect)
            {
                return false;
            }
        }

        return true;
    }


    // Manage the consecutive words combo
    public void CheckForWordsCombo(LetterEntity letterEntity)
    {
        // If the given tile is correct => we keep the current combo and we check if we need to increment it
        if ((grid.tiles[(int)letterEntity.gridPosition.y, (int)letterEntity.gridPosition.x] as LetterTile).guessLetter == (grid.tiles[(int)letterEntity.gridPosition.y, (int)letterEntity.gridPosition.x] as LetterTile).answerLetter)
        {
            // Check every letter of the word(s) concerned by the moved/added letter(s)

            int x = (int)letterEntity.gridPosition.x;
            int y = (int)letterEntity.gridPosition.y;


            // HORIZONTAL WORD
            int xFirst, xLast;

            // Search for the first letter of the horizontal word
            while (x >= 0)
            {
                if (grid.tiles[y, x].GetTileType() == Global.TileType.Definition)
                    break;
                x--;
            }
            xFirst = x + 1;

            // Search for the last letter of the horizontal word
            x = (int)letterEntity.gridPosition.x; // Reset x to default
            while (x <= grid.tiles.GetLength(1) - 1)
            {
                if (grid.tiles[y, x].GetTileType() == Global.TileType.Definition)
                    break;
                x++;
            }
            xLast = x - 1;

            // Check if the letters in the found range is an actual answer and not a letter alone
            if (xLast > xFirst)
            {
                // Check every letter inbetween the first and the last to see if they are correct
                bool isCompletelyCorrect = true;
                for (int i = xFirst; i <= xLast; i++)
                {
                    if ((grid.tiles[y, i] as LetterTile).guessLetter != (grid.tiles[y, i] as LetterTile).answerLetter)
                    {
                        isCompletelyCorrect = false;
                        break;
                    }
                }

                if (isCompletelyCorrect)
                {
                    Debug.Log("COMBO WORD for horizontal word made by tile[" + (int)letterEntity.gridPosition.x + "," + (int)letterEntity.gridPosition.y + "]");
                    IncrementWordCombo(letterEntity);
                    //					currentWordsCombo++;
                    //					ComboPopup comboPopup = (Instantiate(Resources.Load("Prefabs/Ingame/ComboPopup")) as GameObject).GetComponent("ComboPopup") as ComboPopup;
                    //					Vector3 comboPopupPos = GetWorldPositionFromGridPosition(new Vector2(letterEntity.gridPosition.x, letterEntity.gridPosition.y));
                    //					comboPopup.transform.position = new Vector3(comboPopupPos.x, comboPopup.transform.position.y, comboPopupPos.z);
                    //					comboPopup.StartAnimating();
                }
            }
            // END HORIZONTAL WORD

            // VERTICAL WORD
            x = (int)letterEntity.gridPosition.x; // Reset x to default
            int yFirst, yLast;

            // Search for the first letter of the vertical word
            while (y >= 0)
            {
                if (grid.tiles[y, x].GetTileType() == Global.TileType.Definition)
                    break;
                y--;
            }
            yFirst = y + 1;

            // Search for the last letter of the vertical word
            y = (int)letterEntity.gridPosition.y; // Reset x to default
            while (y <= grid.tiles.GetLength(0) - 1)
            {
                if (grid.tiles[y, x].GetTileType() == Global.TileType.Definition)
                    break;
                y++;
            }
            yLast = y - 1;

            if (yLast > yFirst)
            {
                // Check every letter inbetween the first and the last to see if they are correct
                bool isCompletelyCorrect = true;
                for (int i = yFirst; i <= yLast; i++)
                {
                    if ((grid.tiles[i, x] as LetterTile).guessLetter != (grid.tiles[i, x] as LetterTile).answerLetter)
                    {
                        isCompletelyCorrect = false;
                        break;
                    }
                }

                if (isCompletelyCorrect)
                {
                    Debug.Log("COMBO WORD for vertical word made by tile[" + (int)letterEntity.gridPosition.x + "," + (int)letterEntity.gridPosition.y + "]");
                    //					currentWordsCombo++;
                    IncrementWordCombo(letterEntity);
                }
            }
            // END VERTICAL WORD


            //			if (currentWordsCombo > maxWordCombo)
            //				maxWordCombo = currentWordsCombo;
            //			
            //			
            //			// Achievement => first word
            //			if (currentWordsCombo == 1)
            //			{
            //				if (Global.Global.iPadVersion)
            //				{
            //					string appString;
            //					appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";
            //		
            //					GCReportAchievementProgress("com.digdog." + appString + ".Unmotpourdebut", 100);
            //				}
            //				else
            //				{
            //					string appString;
            //					appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";
            //					
            //					GCReportAchievementProgress("com.digdog." + appString + ".Unmotpourdebut", 100);
            //				}
            //			}
            //			// Achievement => Reach a combo of 10
            //			if (currentWordsCombo == 10)
            //			{
            //				if (Global.Global.iPadVersion)
            //				{
            //					string appString;
            //					appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";
            //		
            //					GCReportAchievementProgress("com.digdog." + appString + ".jaimelecombo", 100);
            //				}
            //				else
            //				{
            //					string appString;
            //					appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";
            //					
            //					GCReportAchievementProgress("com.digdog." + appString + ".Jaimelescombos", 100);
            //				}
            //			}
            //			// Achievement => Reach a combo of 20
            //			if (currentWordsCombo == 20)
            //			{
            //				if (Global.Global.iPadVersion)
            //				{
            //					string appString;
            //					appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";
            //		
            //					GCReportAchievementProgress("com.digdog." + appString + ".Art", 100);
            //				}
            //				else
            //				{
            //					string appString;
            //					appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";
            //					
            //					GCReportAchievementProgress("com.digdog." + appString + ".Art", 100);
            //				}
            //			}
        }
        // Given letter is wrong => break the combo
        else if (currentWordsCombo > 0)
        {

            ComboPopup comboPopup = (Instantiate(Resources.Load("Prefabs/Ingame/ComboPopup")) as GameObject).GetComponent("ComboPopup") as ComboPopup;
            Vector3 comboPopupPos = GetWorldPositionFromGridPosition(new Vector2(letterEntity.gridPosition.x, letterEntity.gridPosition.y));
            comboPopup.transform.position = new Vector3(comboPopupPos.x, comboPopup.transform.position.y, comboPopupPos.z);
            comboPopup.Init(currentWordsCombo);
            comboPopup.StartAnimating(true);

            currentWordsCombo = 0;
        }
    }


    void IncrementWordCombo(LetterEntity letterEntity)
    {
        currentWordsCombo++;
        ComboPopup comboPopup = (Instantiate(Resources.Load("Prefabs/Ingame/ComboPopup")) as GameObject).GetComponent("ComboPopup") as ComboPopup;
        Vector3 comboPopupPos = GetWorldPositionFromGridPosition(new Vector2(letterEntity.gridPosition.x, letterEntity.gridPosition.y));
        comboPopup.transform.position = new Vector3(comboPopupPos.x, comboPopup.transform.position.y, comboPopupPos.z);
        comboPopup.Init(currentWordsCombo);
        comboPopup.StartAnimating();

        if (currentWordsCombo > maxWordCombo)
            maxWordCombo = currentWordsCombo;


        // Achievement => first word
        if (currentWordsCombo == 1)
        {
            if (Global.Global.iPadVersion)
            {
                string appString;
                appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";

                GCReportAchievementProgress("com.digdog." + appString + ".Unmotpourdebut", 100);
            }
            else
            {
                string appString;
                appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";

                GCReportAchievementProgress("com.digdog." + appString + ".Unmotpourdebut", 100);
            }
        }
        // Achievement => Reach a combo of 10
        if (currentWordsCombo == 10)
        {
            if (Global.Global.iPadVersion)
            {
                string appString;
                appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";

                GCReportAchievementProgress("com.digdog." + appString + ".jaimelecombo", 100);
            }
            else
            {
                string appString;
                appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";

                GCReportAchievementProgress("com.digdog." + appString + ".Jaimelescombos", 100);
            }
        }
        // Achievement => Reach a combo of 20
        if (currentWordsCombo == 20)
        {
            if (Global.Global.iPadVersion)
            {
                string appString;
                appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";

                GCReportAchievementProgress("com.digdog." + appString + ".Art", 100);
            }
            else
            {
                string appString;
                appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";

                GCReportAchievementProgress("com.digdog." + appString + ".Art", 100);
            }
        }
    }

    #endregion


    #region Solutions

    // Fill the current grid with all the good answers
    public void SolutionToTheGrid()
    {
        int nbGridSolutions = PlayerPrefs.GetInt("nbGridSolutions");
        if (nbGridSolutions <= 0)
        {
            Debug.LogWarning("ERROR: trying to get the solution to a grid when the stock of solutions is empty.");
            return;
        }

        // If grid is already completed no point in giving the solution
        if (PlayerPrefs.GetInt(grid.name) == (int)Global.GridState.Completed)
        {
#if UNITY_IOS
            AVBinding.CreateInfoAlertView("SOLUTION", solutionGridAlreadyCorrectAVString);
#elif UNITY_ANDROID
            //TODO || DEV25
            //EtceteraAndroid.showAlert("SOLUTION", solutionGridAlreadyCorrectAVString, "Continuer", "Annuler");
#endif
            return;
        }

        LetterEntity[] currentLetterEntities = FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];
        for (int i = 0; i < currentLetterEntities.Length; i++)
        {
            currentLetterEntities[i].FadeAndDestroy();
        }


        LetterTile tile; // Will be used by all the different steps to get the current tile of the loops, in order to avoid having to access the array every single time

        for (int i = 0; i < grid.tiles.GetLength(1); i++)
        {
            for (int j = 0; j < grid.tiles.GetLength(0); j++)
            {
                // We're only interested in the letter tiles
                if (grid.tiles[j, i].GetTileType() == Global.TileType.Letter)
                    tile = grid.tiles[j, i] as LetterTile;
                else
                    continue;

                tile.guessLetter = tile.answerLetter;
            }
        }

        onlyCreateLetterEntities = true;

        InitGrid(false);

        LetterEntitiesFadeStartTime = Time.time;

        nbGridSolutions -= 1;

        PlayerPrefs.SetInt("nbGridSolutions", nbGridSolutions);
    }


    // Set the answer corresponding to the given def to the correct one
    public void SolutionToTheWord(DefinitionTile defTile, int defIndex)
    {
        int nbWordSolutions = PlayerPrefs.GetInt("nbWordSolutions");
        if (nbWordSolutions <= 0 && Application.platform != RuntimePlatform.OSXEditor)
        {
            Debug.LogWarning("ERROR: trying to get the solution to a word when the stock of solutions is empty.");
            return;
        }

        // If the given defTile has only one def => defIndex = 0 in any case
        if (defTile.definitions[1].defPos == Global.DefinitionPosition.Error)
        {
            defIndex = 0;
        }

        Global.Answer ans = defTile.definitions[defIndex].answer;

        Debug.Log("SOLUTION is: " + ans.text);

        LetterTile[] letterTilesComposingTheWord = new LetterTile[ans.text.Length];

        int iMin, jMin, iMax, jMax;
        iMin = ans.xStart;
        jMin = ans.yStart;

        if (ans.ansDir == Global.Direction.Horizontal)
        {
            iMax = iMin + letterTilesComposingTheWord.Length;
            jMax = jMin;
        }
        else
        {
            iMax = iMin;
            jMax = jMin + letterTilesComposingTheWord.Length;
        }

        // Register the tiles corresponding to the answer of the given def 
        for (int i = iMin; i < (iMax == iMin ? iMax + 1 : iMax); i++)
        {
            for (int j = jMin; j < (jMax == jMin ? jMax + 1 : jMax); j++)
            {
                letterTilesComposingTheWord[i == iMax ? j - jMin : i - iMin] = grid.tiles[j, i] as LetterTile;
            }
        }

        // Check if the word is already correct (if the whole word is correct, no point in giving the solution...)
        bool isCompletelyCorrect = true;
        foreach (LetterTile letterTileComposingTheWord in letterTilesComposingTheWord)
        {
            if (letterTileComposingTheWord.answerLetter != letterTileComposingTheWord.guessLetter)
            {
                isCompletelyCorrect = false;
                break;
            }
        }

        if (isCompletelyCorrect)
        {
#if UNITY_IOS
            AVBinding.CreateInfoAlertView("SOLUTION", solutionWordAlreadyCorrectAVString);
#elif UNITY_ANDROID
            //TODO || DEV26
            //EtceteraAndroid.showAlert("SOLUTION", solutionWordAlreadyCorrectAVString, "Continuer", "Annuler");
#endif
            return;
        }

        // Keyboard mode => just need to set the letterTiles value to the correct one
        if (gameMode == Global.GameMode.Keyboard)
        {
            LetterTileEntity letterTileEntity;
            foreach (LetterTile letterTileComposingTheWord in letterTilesComposingTheWord)
            {
                if (letterTileComposingTheWord.answerLetter == letterTileComposingTheWord.guessLetter)
                    continue;

                letterTileEntity = GetGridElementAtGridPosition(new Vector2(letterTileComposingTheWord.xPos, letterTileComposingTheWord.yPos)) as LetterTileEntity;

                letterTileEntity.SetGuess(letterTileComposingTheWord.answerLetter, true, false);
            }
        }
        // Dnd mode => need to find for each tile a (not yet correct) token on the grid corresponding to the letter needed for that tile to be correct.
        else
        {
            List<TokenEntity> tokenEntitiesInvolved = new List<TokenEntity>(); // Used to keep track of all the tokenEntities that will be invovled, to be able to reset their isSwappingPosition variable to false afterwards

            TokenEntity thisToken;
            foreach (LetterTile letterTileComposingTheWord in letterTilesComposingTheWord)
            {
                thisToken = GetGridElementAtGridPosition(new Vector2(letterTileComposingTheWord.xPos, letterTileComposingTheWord.yPos)) as TokenEntity;
                if (letterTileComposingTheWord.answerLetter == letterTileComposingTheWord.guessLetter)
                {
                    tokenEntitiesInvolved.Add(thisToken);
                    thisToken.isSwappingPosition = true;
                    Debug.Log("Letter already correct (" + letterTileComposingTheWord.guessLetter + ", " + thisToken.textMesh.text + ") => skipping it.");
                    continue;
                }

                TokenEntity tokenCorrespondingToMyAnswerLetter = FindTokenEntityWithGuessLetter(letterTileComposingTheWord.answerLetter);

                if (tokenCorrespondingToMyAnswerLetter == null)
                {
                    Debug.LogError("SolutionToTheWord() => tokenCorrespondingToMyAnswerLetter " + letterTileComposingTheWord.answerLetter + " not found. Skipping it.");
                    continue;
                }
                Debug.Log("FOUND " + tokenCorrespondingToMyAnswerLetter.textMesh.text + " on tile " + tokenCorrespondingToMyAnswerLetter.gridPosition.x + "," + tokenCorrespondingToMyAnswerLetter.gridPosition.y + " for tile " + letterTileComposingTheWord.xPos + "," + letterTileComposingTheWord.yPos);

                tokenEntitiesInvolved.Add(thisToken);
                tokenEntitiesInvolved.Add(tokenCorrespondingToMyAnswerLetter);

                thisToken.isSwappingPosition = true;
                tokenCorrespondingToMyAnswerLetter.isSwappingPosition = true;

                // Destroy the joints
                thisToken.DestroyImmediateSpringJoint();
                tokenCorrespondingToMyAnswerLetter.DestroyImmediateSpringJoint();

                // Swap the grid positions
                Vector2 myOldPos = thisToken.gridPosition;
                Vector2 myNewPos = tokenCorrespondingToMyAnswerLetter.gridPosition;
                tokenCorrespondingToMyAnswerLetter.gridPosition = new Vector2(-1, -1); // This is set to -1 temporarily to avoid conflicts from having the same pos for a little moment
                AssignPositionToLetterEntity(thisToken, myNewPos, false);
                AssignPositionToLetterEntity(tokenCorrespondingToMyAnswerLetter, myOldPos, false);

                // Change temporarily the position of the tokens to their new ones to be able to create the new joints
                Vector3 sourcePos = GetWorldPositionFromGridPosition(tokenCorrespondingToMyAnswerLetter.gridPosition);
                Vector3 destPos = GetWorldPositionFromGridPosition(thisToken.gridPosition);
                thisToken.transform.position = new Vector3(destPos.x, thisToken.transform.position.y, destPos.z);
                thisToken.CreateSpringJoint();
                thisToken.transform.position = new Vector3(sourcePos.x, thisToken.transform.position.y, sourcePos.z);
                tokenCorrespondingToMyAnswerLetter.transform.position = new Vector3(sourcePos.x, tokenCorrespondingToMyAnswerLetter.transform.position.y, sourcePos.z);
                tokenCorrespondingToMyAnswerLetter.CreateSpringJoint();
                tokenCorrespondingToMyAnswerLetter.transform.position = new Vector3(destPos.x, tokenCorrespondingToMyAnswerLetter.transform.position.y, destPos.z);
            }


            // Reset the isSwappingPosition variable of every involved token to false
            foreach (TokenEntity tokenEntity in tokenEntitiesInvolved)
            {
                tokenEntity.isSwappingPosition = false;

                if (!physicsEnabled)
                {
                    tokenEntity.transform.position = new Vector3(tokenEntity.transform.position.x, 1, tokenEntity.transform.position.z);
                    tokenEntity.waitingForMovementEnd = true;
                    tokenEntity.GetComponent<Collider>().isTrigger = true;
                }
            }
        }

        nbWordSolutions -= 1;

        // Increment words combo (after all, even if cheating, this made a correct word!)
        //		currentWordsCombo++;
        LetterEntity lastLetter = GetGridElementAtGridPosition(new Vector2(letterTilesComposingTheWord[letterTilesComposingTheWord.Length - 1].xPos, letterTilesComposingTheWord[letterTilesComposingTheWord.Length - 1].yPos)) as LetterEntity;
        IncrementWordCombo(lastLetter);

        // Achievement => use a solution
        if (Global.Global.iPadVersion)
        {
            string appString;
            appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";

            GCReportAchievementProgress("com.digdog." + appString + ".unpeudaide", 100);
            GCReportAchievementProgress("com.digdog." + appString + ".Unmotpourdebut", 100);
        }
        else
        {
            string appString;
            appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";

            GCReportAchievementProgress("com.digdog." + appString + ".aide", 100);
            GCReportAchievementProgress("com.digdog." + appString + ".Unmotpourdebut", 100);
        }

        PlayerPrefs.SetInt("nbWordSolutions", nbWordSolutions);

    }


    // Display/Hide the SolutionToWord button for the focused def
    public void DisplaySolutionToWordButton(bool display)
    {
        if (display)
        {
            // Security check
            if (focusedDefinition == null)
            {
                Debug.LogError("Trying to display the solution to word button when focusedDefinition is null");
                return;
            }

            DefinitionTile defTile = grid.tiles[(int)focusedDefinition.gridPosition.y, (int)focusedDefinition.gridPosition.x] as DefinitionTile;

            if (AreAssociatedWordsAlreadyCorrect(defTile))
            {
                return;
            }

            //			// Only one def on this tile
            //			if ((grid.tiles[(int)focusedDefinition.gridPosition.y, (int)focusedDefinition.gridPosition.x] as DefinitionTile).definitions[1].defPos == Global.DefinitionPosition.Error)
            //			{
            //				solutionToWordButton.transform.localPosition = new Vector3(focusedDefinition.transform.localPosition.x + focusedDefinition.transform.localScale.x * 0.5f, solutionToWordButton.transform.localPosition.y, focusedDefinition.transform.localPosition.z);
            //			}
            //			else // Two defs on this tile
            //			{
            //				solutionToWordButton.transform.localPosition = new Vector3(focusedDefinition.transform.localPosition.x + focusedDefinition.transform.localScale.x * 0.5f, solutionToWordButton.transform.localPosition.y, focusedDefinition.textMeshSeparator.transform.position.z - 0.025f);
            //			}

            //			solutionToWordButton.transform.localPosition = new Vector3(focusedDefinition.transform.localPosition.x - focusedDefinition.transform.localScale.x * 0.5f + solutionToWordButton.transform.localScale.x * 0.5f, solutionToWordButton.transform.localPosition.y, focusedDefinition.transform.localPosition.z + focusedDefinition.transform.localScale.z * 0.5f - solutionToWordButton.transform.localScale.z * 0.5f);

            // First row
            if (defTile.xPos == 0)
            {
                solutionToWordButton.transform.localPosition = new Vector3(focusedDefinition.transform.localPosition.x + focusedDefinition.transform.localScale.x * 0.5f + solutionToWordButton.transform.localScale.x * 0.5f, solutionToWordButton.transform.localPosition.y, focusedDefinition.transform.localPosition.z + focusedDefinition.transform.localScale.z * 0.5f - solutionToWordButton.transform.localScale.z * 0.5f);
            }
            else
            {
                solutionToWordButton.transform.localPosition = new Vector3(focusedDefinition.transform.localPosition.x - focusedDefinition.transform.localScale.x * 0.5f - solutionToWordButton.transform.localScale.x * 0.5f, solutionToWordButton.transform.localPosition.y, focusedDefinition.transform.localPosition.z + focusedDefinition.transform.localScale.z * 0.5f - solutionToWordButton.transform.localScale.z * 0.5f);
            }


            solutionToWordButton.active = true;
            (solutionToWordButton.GetComponent("ButtonIngameSolutionForWord") as ButtonIngameSolutionForWord).SetActive(true);
        }
        else
        {
            (solutionToWordButton.GetComponent("ButtonIngameSolutionForWord") as ButtonIngameSolutionForWord).SetActive(false);
        }
    }

    #endregion


    #region Tile Selection

    // Handles the tiles selection. Call with (null) to deselect (Keyboard gameMode only)
    public void SelectTile(LetterTileEntity tile)
    {
        // Tile = null => deselect all
        if (tile == null || tile.isCorrect)
        {
            KeepTrackOfPreviousTilesSelection();

            tilesSelection.tiles.Clear();
            tilesSelection.currentIndex = 0;

            guiCam.SetKeyboardState(Global.KeyboardState.Unfocused, false);

            foreach (LetterTileEntity tileWithinPreviousSelection in previousTilesSelection)
            {
                Debug.Log(tileWithinPreviousSelection);
                tileWithinPreviousSelection.SetProperMaterial();
            }

            return;
        }
        else if (tilesSelection.tiles.Count == 0)
        {
            guiCam.SetKeyboardState(Global.KeyboardState.Focused, false);
        }

        // If this tile is a letter
        if (grid.tiles[(int)tile.gridPosition.y, (int)tile.gridPosition.x].GetTileType() == Global.TileType.Letter)
        {
            // No tile selected yet => select the given tile
            if (tilesSelection.tiles.Count == 0)
            {
                tilesSelection.tiles.Add(tile);
                tilesSelection.currentIndex = 0;
            }
            // Tile(s) already selected
            else
            {
                // If this tile already belongs to the current selection
                if (tilesSelection.tiles.Contains(tile))
                {
                    tilesSelection.currentIndex = tilesSelection.tiles.IndexOf(tile);
                }
                // If this tile is new to this selection
                else
                {
                    // Only one tile already selected
                    if (tilesSelection.tiles.Count == 1)
                    {
                        // New tile is the next horizontally
                        if (tile.gridPosition.x == tilesSelection.tiles[0].gridPosition.x + 1 && tile.gridPosition.y == tilesSelection.tiles[0].gridPosition.y)
                        {
                            tilesSelection.tiles.Add(tile);
                            tilesSelection.direction = Global.Direction.Horizontal;
                        }
                        // New tile is the next vertically
                        else if (tile.gridPosition.x == tilesSelection.tiles[0].gridPosition.x && tile.gridPosition.y == tilesSelection.tiles[0].gridPosition.y + 1)
                        {
                            tilesSelection.tiles.Add(tile);
                            tilesSelection.direction = Global.Direction.Vertical;
                        }
                        // New tile is the next horizontally
                        else if (tile.gridPosition.x == tilesSelection.tiles[0].gridPosition.x - 1 && tile.gridPosition.y == tilesSelection.tiles[0].gridPosition.y)
                        {
                            tilesSelection.tiles.Insert(0, tile);
                            tilesSelection.direction = Global.Direction.Horizontal;
                            tilesSelection.currentIndex = 0;
                        }
                        // New tile is the previous vertically
                        else if (tile.gridPosition.x == tilesSelection.tiles[0].gridPosition.x && tile.gridPosition.y == tilesSelection.tiles[0].gridPosition.y - 1)
                        {
                            tilesSelection.tiles.Insert(0, tile);
                            tilesSelection.direction = Global.Direction.Vertical;
                            tilesSelection.currentIndex = 0;
                        }
                        // New tile has nothing to do with the current selection
                        else
                        {
                            KeepTrackOfPreviousTilesSelection();
                            tilesSelection.tiles.Clear();
                            SelectTile(tile);
                            return;
                        }
                    }
                    // Several tile already selected
                    else
                    {
                        // Selection direction is horizontal AND new tile is the next horizontally
                        if (tilesSelection.direction == Global.Direction.Horizontal && tile.gridPosition.x == tilesSelection.tiles[tilesSelection.tiles.Count - 1].gridPosition.x + 1 && tile.gridPosition.y == tilesSelection.tiles[tilesSelection.tiles.Count - 1].gridPosition.y)
                        {
                            tilesSelection.tiles.Add(tile);
                        }
                        // Selection direction is vertical AND new tile is the next vertically
                        else if (tilesSelection.direction == Global.Direction.Vertical && tile.gridPosition.x == tilesSelection.tiles[tilesSelection.tiles.Count - 1].gridPosition.x && tile.gridPosition.y == tilesSelection.tiles[tilesSelection.tiles.Count - 1].gridPosition.y + 1)
                        {
                            tilesSelection.tiles.Add(tile);
                        }
                        // Selection direction is horizontal AND new tile is the previous horizontally
                        else if (tilesSelection.direction == Global.Direction.Horizontal && tile.gridPosition.x == tilesSelection.tiles[0].gridPosition.x - 1 && tile.gridPosition.y == tilesSelection.tiles[0].gridPosition.y)
                        {
                            tilesSelection.tiles.Insert(0, tile);
                            tilesSelection.currentIndex = 0;
                        }
                        // election direction is vertical AND new tile is the previous vertically
                        else if (tilesSelection.direction == Global.Direction.Vertical && tile.gridPosition.x == tilesSelection.tiles[0].gridPosition.x && tile.gridPosition.y == tilesSelection.tiles[0].gridPosition.y - 1)
                        {
                            tilesSelection.tiles.Insert(0, tile);
                            tilesSelection.currentIndex = 0;
                        }
                        // New tile has nothing to do with the current selection
                        else
                        {
                            KeepTrackOfPreviousTilesSelection();
                            tilesSelection.tiles.Clear();
                            SelectTile(tile);
                            return;
                        }
                    }
                }
            }
        }

        LastPartOfTileSelection();

        // If there are some tiles that were deselected during this call, restore their materials
        if (previousTilesSelection.Count > 0)
        {
            Debug.Log("Restore the materials of the previous tile selection");
            foreach (LetterTileEntity tileWithinPreviousSelection in previousTilesSelection)
            {
                tileWithinPreviousSelection.SetProperMaterial();
            }

            previousTilesSelection.Clear();
        }
    }


    // Handles the tiles selection corresponding to the given def. (Keyboard gameMode only)
    public void SelectDefinition(DefinitionEntity defEntity, bool doKeepTrackOfPreviousTilesSelection)
    {
        LetterTileEntity tileCurrentlySelected = null;

        if (tilesSelection.tiles.Count > 0)
        {
            tileCurrentlySelected = tilesSelection.tiles[0];

            if (doKeepTrackOfPreviousTilesSelection)
                KeepTrackOfPreviousTilesSelection();
        }
        else
        {
            guiCam.SetKeyboardState(Global.KeyboardState.Focused, false);
        }

        tilesSelection.tiles.Clear();
        tilesSelection.currentIndex = 0;

        // If there are some tiles that were deselected during this call, restore their materials
        if (previousTilesSelection.Count > 0)
        {
            Debug.Log("Restore the materials of the previous tile selection");
            foreach (LetterTileEntity tileWithinPreviousSelection in previousTilesSelection)
            {
                tileWithinPreviousSelection.SetProperMaterial();
            }

            previousTilesSelection.Clear();
        }

        DefinitionTile defTile = grid.tiles[(int)defEntity.gridPosition.y, (int)defEntity.gridPosition.x] as DefinitionTile;
        int selectedDefIndex;

        // If this tile has only one definition OR has two defs but the first one is to be selected
        if (defTile.definitions[1].defPos == Global.DefinitionPosition.Error || (tileCurrentlySelected == null || (tileCurrentlySelected.gridPosition.x != defTile.definitions[0].answer.xStart || tileCurrentlySelected.gridPosition.y != defTile.definitions[0].answer.yStart)))
        {
            selectedDefIndex = 0;
        }
        // If this tile has two definitions AND the second one is to be selected
        else
        {
            selectedDefIndex = 1;
        }

        int answerLength = defTile.definitions[selectedDefIndex].answer.text.Length;

        tilesSelection.direction = defTile.definitions[selectedDefIndex].answer.ansDir;

        bool wordIsAlreadyGuessed = true;

        if (defTile.definitions[selectedDefIndex].answer.ansDir == Global.Direction.Horizontal)
        {
            for (int i = 0; i < answerLength; i++)
            {
                LetterTileEntity tile = GetGridElementAtGridPosition(new Vector2(defTile.definitions[selectedDefIndex].answer.xStart + i, defTile.definitions[selectedDefIndex].answer.yStart)) as LetterTileEntity;
                tilesSelection.tiles.Add(tile);
                // If at least one letter of the word is not already guessed, proceed to the selection normally
                if (!tile.isCorrect)
                    wordIsAlreadyGuessed = false;
            }
        }
        else if (defTile.definitions[selectedDefIndex].answer.ansDir == Global.Direction.Vertical)
        {
            for (int i = 0; i < answerLength; i++)
            {
                LetterTileEntity tile = GetGridElementAtGridPosition(new Vector2(defTile.definitions[selectedDefIndex].answer.xStart, defTile.definitions[selectedDefIndex].answer.yStart + i)) as LetterTileEntity;
                tilesSelection.tiles.Add(tile);
                // If at least one letter of the word is not already guessed, proceed to the selection normally
                if (!tile.isCorrect)
                    wordIsAlreadyGuessed = false;
            }
        }

        // If the word corresponding to the definition is already fully guessed, cancel the selection
        if (wordIsAlreadyGuessed)
        {
            // If the selected def was the first one AND there is a second one
            if (selectedDefIndex == 0 && defTile.definitions[1].defPos != Global.DefinitionPosition.Error)
            {
                // Cheat the system by setting the first selected tile as the first one of te  first def and call this function again => will select the second def of this tile
                if (tilesSelection.tiles.Count > 0)
                {
                    tilesSelection.tiles[0] = GetGridElementAtGridPosition(new Vector2(defTile.definitions[selectedDefIndex].answer.xStart, defTile.definitions[selectedDefIndex].answer.yStart)) as LetterTileEntity;
                }
                else
                {
                    tilesSelection.tiles.Add(GetGridElementAtGridPosition(new Vector2(defTile.definitions[selectedDefIndex].answer.xStart, defTile.definitions[selectedDefIndex].answer.yStart)) as LetterTileEntity);
                }
                SelectDefinition(defEntity, false);
            }
            else
            {
                SelectTile(null);
            }
            return;
        }

        LastPartOfTileSelection();
    }



    // Handles the tiles selection corresponding to the given def. (Keyboard gameMode only)
    public void SelectDefinition(DefinitionEntity defEntity, int defIndex, bool doKeepTrackOfPreviousTilesSelection)
    {
        //		LetterTileEntity tileCurrentlySelected = null;

        if (tilesSelection.tiles.Count > 0)
        {
            //			tileCurrentlySelected = tilesSelection.tiles[0];

            if (doKeepTrackOfPreviousTilesSelection)
                KeepTrackOfPreviousTilesSelection();
        }
        else
        {
            guiCam.SetKeyboardState(Global.KeyboardState.Focused, false);
        }

        tilesSelection.tiles.Clear();
        tilesSelection.currentIndex = -1;

        // If there are some tiles that were deselected during this call, restore their materials
        if (previousTilesSelection.Count > 0)
        {
            Debug.Log("Restore the materials of the previous tile selection");
            foreach (LetterTileEntity tileWithinPreviousSelection in previousTilesSelection)
            {
                tileWithinPreviousSelection.SetProperMaterial();
            }

            previousTilesSelection.Clear();
        }

        DefinitionTile defTile = grid.tiles[(int)defEntity.gridPosition.y, (int)defEntity.gridPosition.x] as DefinitionTile;

        int selectedDefIndex = defIndex;

        // Clamp
        if (defTile.definitions[1].defPos == Global.DefinitionPosition.Error || selectedDefIndex < 0)
        {
            selectedDefIndex = 0;
        }
        else if (selectedDefIndex > 1)
        {
            selectedDefIndex = 1;
        }

        //		// If this tile has only one definition OR has two defs but the first one is to be selected
        //		if (defTile.definitions[1].defPos == Global.DefinitionPosition.Error || (tileCurrentlySelected == null || (tileCurrentlySelected.gridPosition.x != defTile.definitions[0].answer.xStart || tileCurrentlySelected.gridPosition.y != defTile.definitions[0].answer.yStart)))
        //		{
        //			selectedDefIndex = 0;
        //		}
        //		// If this tile has two definitions AND the second one is to be selected
        //		else
        //		{
        //			selectedDefIndex = 1;
        //		}

        int answerLength = defTile.definitions[selectedDefIndex].answer.text.Length;

        tilesSelection.direction = defTile.definitions[selectedDefIndex].answer.ansDir;

        bool wordIsAlreadyGuessed = true;

        if (defTile.definitions[selectedDefIndex].answer.ansDir == Global.Direction.Horizontal)
        {
            for (int i = 0; i < answerLength; i++)
            {
                LetterTileEntity tile = GetGridElementAtGridPosition(new Vector2(defTile.definitions[selectedDefIndex].answer.xStart + i, defTile.definitions[selectedDefIndex].answer.yStart)) as LetterTileEntity;
                tilesSelection.tiles.Add(tile);
                // If at least one letter of the word is not already guessed, proceed to the selection normally
                if (!tile.isCorrect)
                    wordIsAlreadyGuessed = false;
            }
        }
        else if (defTile.definitions[selectedDefIndex].answer.ansDir == Global.Direction.Vertical)
        {
            for (int i = 0; i < answerLength; i++)
            {
                LetterTileEntity tile = GetGridElementAtGridPosition(new Vector2(defTile.definitions[selectedDefIndex].answer.xStart, defTile.definitions[selectedDefIndex].answer.yStart + i)) as LetterTileEntity;
                tilesSelection.tiles.Add(tile);
                // If at least one letter of the word is not already guessed, proceed to the selection normally
                if (!tile.isCorrect)
                    wordIsAlreadyGuessed = false;
            }
        }

        // If the word corresponding to the definition is already fully guessed, cancel the selection
        if (wordIsAlreadyGuessed)
        {
            // If the selected def was the first one AND there is a second one
            if (selectedDefIndex == 0 && defTile.definitions[1].defPos != Global.DefinitionPosition.Error)
            {
                // Cheat the system by setting the first selected tile as the first one of te  first def and call this function again => will select the second def of this tile
                if (tilesSelection.tiles.Count > 0)
                {
                    tilesSelection.tiles[0] = GetGridElementAtGridPosition(new Vector2(defTile.definitions[selectedDefIndex].answer.xStart, defTile.definitions[selectedDefIndex].answer.yStart)) as LetterTileEntity;
                }
                else
                {
                    tilesSelection.tiles.Add(GetGridElementAtGridPosition(new Vector2(defTile.definitions[selectedDefIndex].answer.xStart, defTile.definitions[selectedDefIndex].answer.yStart)) as LetterTileEntity);
                }
                SelectDefinition(defEntity, false);
            }
            else
            {
                SelectTile(null);
            }
            return;
        }

        LastPartOfTileSelection();
    }


    // Code stub that is common to TileSelection and DefinitionSelection; is called at the end of these 2 methods to complete the selection
    protected void LastPartOfTileSelection()
    {
        //		// If there are some tiles that were deselected during this call, restore their materials
        //		if (previousTilesSelection.Count > 0)
        //		{
        //			Debug.Log("Restore the materials of the previous tile selection");
        //			foreach(LetterTileEntity tileWithinPreviousSelection in previousTilesSelection)
        //			{
        //				tileWithinPreviousSelection.SetProperMaterial();
        //			}
        //			
        //			previousTilesSelection.Clear();
        //		}

        // Set the materials of the the current selection
        foreach (LetterTileEntity tileWithinSelection in tilesSelection.tiles)
        {
            tileWithinSelection.SetProperMaterial();
        }

        if (tilesSelection.currentIndex < 0)
        {
            IncrementTilesSelectionIndex();
        }
        // If the newly selected tile is correct, skip it
        else if (tilesSelection.tiles[tilesSelection.currentIndex].isCorrect)
        {
            tilesSelection.tiles[tilesSelection.currentIndex].StartBlinking();
            IncrementTilesSelectionIndex();
        }
        else
        {
            cam.FocusViewOnTile(tilesSelection.tiles[tilesSelection.currentIndex]);
        }
    }


    // Increments the tilesSelection index and clamp it
    protected void IncrementTilesSelectionIndex()
    {
        if (tilesSelection.tiles.Count > 0)
        {
            tilesSelection.currentIndex++;

            // Clamp
            if (tilesSelection.currentIndex > tilesSelection.tiles.Count - 1)
                tilesSelection.currentIndex = tilesSelection.tiles.Count - 1;


            tilesSelection.tiles[tilesSelection.currentIndex].GetComponent<Renderer>().material = tilesSelection.tiles[tilesSelection.currentIndex].selectedMat;

            bool selectionCompletelyCorrect = true;
            foreach (LetterTileEntity tile in tilesSelection.tiles)
            {
                if (!tile.isCorrect)
                {
                    selectionCompletelyCorrect = false;
                    break;
                }
            }

            // If The selection is completely correct => deselect it and unfocus the eventual focused definition
            if (selectionCompletelyCorrect)
            {
                SelectTile(null);

                if (focusedDefinition != null)
                {
                    focusedDefinition.Grow();
                    guiCam.StartTabsAnimation();
                }

                tileSkipSoundPlaying = false;

                return;
            }


            // If the newly selected tile is correct, skip it
            if (tilesSelection.tiles[tilesSelection.currentIndex].isCorrect)
            {
                tilesSelection.tiles[tilesSelection.currentIndex].StartBlinking();

                if (!tileSkipSoundPlaying)
                {
                    tileSkipSoundPlaying = true;
                    tileSkipAS.Play();
                }

                // If this is the last one, search for the first non correct letter
                if (tilesSelection.currentIndex == tilesSelection.tiles.Count - 1)
                {
                    int i = 0;
                    foreach (LetterTileEntity tile in tilesSelection.tiles)
                    {
                        if (!tile.isCorrect)
                        {
                            tilesSelection.currentIndex = i - 1;
                            break;
                        }
                        else
                        {
                        }
                        i++;
                    }
                }

                IncrementTilesSelectionIndex();
            }
            else
            {
                tileSkipSoundPlaying = false;
            }

            // Set the materials of the the current selection
            foreach (LetterTileEntity tileWithinSelection in tilesSelection.tiles)
            {
                tileWithinSelection.SetProperMaterial();
            }

            cam.FocusViewOnTile(tilesSelection.tiles[tilesSelection.currentIndex]);
        }
    }


    // Decrements the tilesSelection index and wrap it
    protected void DecrementTilesSelectionIndex()
    {
        if (tilesSelection.tiles.Count > 0)
        {
            tilesSelection.currentIndex--;

            // Clamp
            if (tilesSelection.currentIndex < 0)
                tilesSelection.currentIndex = tilesSelection.tiles.Count - 1;


            tilesSelection.tiles[tilesSelection.currentIndex].GetComponent<Renderer>().material = tilesSelection.tiles[tilesSelection.currentIndex].selectedMat;

            bool selectionCompletelyCorrect = true;
            foreach (LetterTileEntity tile in tilesSelection.tiles)
            {
                if (!tile.isCorrect)
                {
                    selectionCompletelyCorrect = false;
                    break;
                }
            }

            // If The selection is completely correct => deselect it and unfocus the eventual focused definition
            if (selectionCompletelyCorrect)
            {
                SelectTile(null);

                if (focusedDefinition != null)
                {
                    focusedDefinition.Grow();
                    guiCam.StartTabsAnimation();
                }

                return;
            }


            // If the newly selected tile is correct, skip it
            if (tilesSelection.tiles[tilesSelection.currentIndex].isCorrect)
            {
                //				tilesSelection.tiles[tilesSelection.currentIndex].StartBlinking();
                //				
                //				// If this is the last one, search for the first non correct letter
                //				if (tilesSelection.currentIndex == tilesSelection.tiles.Count - 1)
                //				{
                //					int i = 0;
                //					foreach (LetterTileEntity tile in tilesSelection.tiles)
                //					{
                //						if (!tile.isCorrect)
                //						{							
                //							tilesSelection.currentIndex = i - 1;
                //							break;
                //						}
                //						else
                //						{
                //						}
                //						i++;
                //					}
                //				}

                DecrementTilesSelectionIndex();
            }

            // Set the materials of the the current selection
            foreach (LetterTileEntity tileWithinSelection in tilesSelection.tiles)
            {
                tileWithinSelection.SetProperMaterial();
            }


            cam.FocusViewOnTile(tilesSelection.tiles[tilesSelection.currentIndex]);


        }
    }


    // Return true if the given tile belongs to the the current tilesSelection
    public bool IsTileWithinCurrentSelection(LetterTileEntity tile)
    {
        return tilesSelection.tiles.Contains(tile);
    }


    // Return true if the given tile is the one currently selected within the tilesSelection
    public bool IsTileFocused(LetterTileEntity tile)
    {
        if (tilesSelection.tiles.Count > 0 && tilesSelection.currentIndex >= 0)
            return tilesSelection.tiles[tilesSelection.currentIndex] == tile;
        else
            return false;
    }


    protected void KeepTrackOfPreviousTilesSelection()
    {
        Debug.Log("KeepTrackOfPreviousTilesSelection");
        previousTilesSelection.Clear();

        foreach (LetterTileEntity tile in tilesSelection.tiles)
        {
            previousTilesSelection.Add(tile);
        }
    }

    #endregion


    #region Destroy

    // Destroy the letter elements entities
    void DestroyLetterElements()
    {
        LetterEntity[] letterEntities = FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];

        for (int i = 0; i < letterEntities.Length; i++)
        {
            Destroy(letterEntities[i].gameObject);
        }

        waitedForOneUpdate = false;
        pendingDestroyGridElements = true;

        Debug.Log("DESTROY GRID");
    }


    // Destroy the grid entities (defs and letters)
    void DestroyGrid()
    {
        Transform gridEntitiesTransform = GameObject.Find("GridEntities").transform;

        for (int i = 0; i < gridEntitiesTransform.childCount; i++)
        {
            Destroy(gridEntitiesTransform.GetChild(i).gameObject);
        }

        waitedForOneUpdate = false;
        pendingDestroyGridElements = true;

        Debug.Log("DESTROY GRID");
    }


    // Destroy all the game elements (including the grid, arrows, table...)
    public void DestroyGameElements()
    {
        Transform gameElementsTransform = GameObject.Find("GameElements").transform;

        for (int i = 0; i < gameElementsTransform.childCount; i++)
        {
            Destroy(gameElementsTransform.GetChild(i).gameObject);
        }

        Destroy(endOfGridAnim.gameObject);

        Debug.Log("DESTROY GAME ELEMENTS");
    }

    #endregion


    #region Update

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        // DEBUG		
        if (Input.GetKeyDown(KeyCode.E))
            SetCurrentGridAsCompleted();

        // END DEBUG


        if (!isGridCompleted)
        {
            timer += Time.deltaTime;

            System.TimeSpan span = System.TimeSpan.FromSeconds(timer);
            ingameMenuClockText.text = string.Format("{0:0}:{1:00}", span.Minutes, span.Seconds);
            ingameMenuClockTextShadow.text = ingameMenuClockText.text;
        }


        // MANAGES the delay between the actual destroying of the grid and the loading of the new one
        if (pendingDestroyGridElements)
        {
            if (waitedForOneUpdate)
            {
                Debug.Log("DID WAIT");
                pendingDestroyGridElements = false;
            }
            else
            {
                Debug.Log("WAIT");
                waitedForOneUpdate = true;
            }
        }
        else if (pendingGridToBeLoaded != null)
        {
            if (waitedForOneUpdate)
            {
                Debug.Log("LOAD");
                LoadGrid(pendingGridToBeLoaded);
                pendingGridToBeLoaded = null;
            }
        }
        else if (pendingGameModeToBeChanged != Global.GameMode.Error)
        {
            if (waitedForOneUpdate)
            {
                waitedForOneUpdate = false;
                ChangeGameMode(pendingGameModeToBeChanged);
                pendingGameModeToBeChanged = Global.GameMode.Error;
            }
            else
            {
                waitedForOneUpdate = true;
            }
        }
        else if (pendingResetGrids != -1)
        {
            if (waitedForOneUpdate)
            {
                waitedForOneUpdate = false;
                if (pendingResetGrids == 0)
                {
                    ResetGrid(grid.name);
                }
                else if (pendingResetGrids == 1)
                {
                    ResetAllGrids();
                }

                guiCam.StartOptionsMenuAnimation();

                pendingResetGrids = -1;
            }
            else
            {
                waitedForOneUpdate = true;
            }
        }
        else if (pendingHideMask)
        {
            if (waitedForOneUpdate)
            {
                waitedForOneUpdate = false;
                pendingHideMask = false;
                inputDisablingMaskGO.active = false;
            }
            else
            {
                waitedForOneUpdate = true;
            }
        }
        else if (pendingGridCompleted)
        {
            if (waitedForOneUpdate)
            {
                waitedForOneUpdate = false;
                pendingGridCompleted = false;

                GridCompleted();
            }
            else
            {
                waitedForOneUpdate = true;
            }
        }


        // A word is currently being validated (medium diff only)
        if (isWordBeingValidated)
        {
            for (int i = 0; i < wordValidations.Count; i++)
            {
                if (wordValidations[i].lastLetterValidationTime + delayBetweenValidations <= Time.time)
                {
                    // End of the word reached
                    if (wordValidations[i].currentLetterBeingValidatedIndex == wordValidations[i].tilesToValidate.Length)
                    {
                        wordValidations.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        Global.WordValidation wordValidation = wordValidations[i];
                        if (wordValidation.currentLetterBeingValidatedIndex < tileValidationSounds.Length)
                            tileValidationAS.clip = tileValidationSounds[wordValidation.currentLetterBeingValidatedIndex];
                        else
                            tileValidationAS.clip = tileValidationSounds[tileValidationSounds.Length - 1];

                        tileValidationAS.Play();
                        wordValidation.tilesToValidate[wordValidation.currentLetterBeingValidatedIndex].SetCorrect(true, false);

                        wordValidation.lastLetterValidationTime = Time.time;
                        wordValidation.currentLetterBeingValidatedIndex++;

                        wordValidations[i] = wordValidation;
                    }
                }
            }

            if (wordValidations.Count == 0)
                isWordBeingValidated = false;
        }



        if (pendingSetCamOrthoSize)
        {
            if (waitedForOneUpdateOrthoSize)
            {
                Debug.Log("DID WAIT");
                cam.SetOrthoSize();
                guiCam.SetOrthoSize();
                pendingSetCamOrthoSize = false;
                waitedForOneUpdateOrthoSize = false;
            }
            else
            {
                Debug.Log("WAIT");
                waitedForOneUpdateOrthoSize = true;
            }
        }
    }





    //	protected void OnGUI()
    //	{
    ////		base.OnGUI();
    ////		
    //		
    //		if (GUI.Button(new Rect(0, Screen.height - 30, 70, 30), "Complete"))
    //		{
    //			SetCurrentGridAsCompleted();
    //		}	
    ////		
    ////		if (GUI.Button(new Rect(80, Screen.height - 30, 70, 30), "Reset achiev"))
    ////		{
    ////			UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ResetAllAchievements( success => { Debug.Log(success ? "Achievements reset" : "Failed to reset achievements"); });
    ////		}
    //	}

    #endregion

    #region Unity App Events

    protected override void OnApplicationQuit()
    {
        base.OnApplicationQuit();

        if (grid != null)
            SaveGrid(grid.name);
    }


    protected override void OnApplicationPause(bool pause)
    {
        base.OnApplicationPause(pause);

        Debug.Log("PAUSE: " + pause);

        // App pausing
        if (pause)
        {
            if (grid != null)
            {
                Debug.Log("GRID SAVED");
                SaveGrid(grid.name);
            }

            if (Global.Global.adEnabled)
            {
                //DEV_CODE || HIDE BANNER VIEW
                //cam.HideBannerView();
            }
        }
        // App resuming
        else
        {
            if (Global.Global.adEnabled)
            {
                if (cam != null)
                {
                    //DEV_CODE || ADMOB || SET BANNER VIEW
                    //cam.SetBannerView();
                }
            }

            //			if (iAdEnabled)
            //			{	
            //				if (goBuySplashscreenBG != null)
            //				{
            //					isBuySplashScreenDisplayed = true;
            //					goBuySplashscreenBG.guiTexture.enabled = true;
            //				}
            //				
            //#if UNITY_IPHONE
            //				AdBinding.destroyAdBanner();
            //				Destroy(iAdAdapter);
            //#elif UNITY_ANDROID
            //				AdMobAndroid.destroyBanner();
            //				Destroy(iAdAdapter);
            //#endif
            //			}
        }
    }

    #endregion

    #region Grid Data Management

    // Load the grid corresponding to the given id + its state if grid is already started
    public void LoadGrid(string id)
    {
        bool firstTimeLoaded = false;

        // platform = "Iphone";
        // id = "Iphone-0-0-001";


        grid = XMLManager.LoadGridFile("XMLs/" + platform + "/" + id);

        Debug.Log("Nik - Grid :" + grid + " Id : " + id);

        if (PlayerPrefs.HasKey(grid.name) && PlayerPrefs.GetInt(grid.name) != (int)Global.GridState.Empty)
        {
            Debug.Log("Nik - Enter If GridName : " + PlayerPrefs.HasKey(grid.name) + " && " + PlayerPrefs.GetInt(grid.name) + " != " + (int)Global.GridState.Empty);
            XMLManager.LoadGameStateFile(ref grid);

            // GameState is corrupted, delete it
            if (XMLManager.dataCorrupted)
            {
                Debug.Log("GameState is corrupted...");
                XMLManager.ResetAllSavedData(headers);
                LoadGrid(id);
                Debug.Log("Nik - XML data Corrupted");
                return;
            }

            // If the loaded grid was played with a different gameMode than the current one, we need to convert it
            if (grid.gameMode != gameMode)
            {
                grid.gameMode = gameMode;
                OnlyKeepCorrectGuesses(""); // TODO
                Debug.Log("Nik - grid mode not a gamemode");
            }
        }
        else
        {
            //			PlayerPrefs.SetInt(id, (int)Global.GridState.Started);
            firstTimeLoaded = true;
            grid.gameMode = gameMode;
            Debug.Log("Nik - in not PlayerPrefs Creted");
        }

        PlayerPrefs.SetString("CurrentGrid", id);


        if (PlayerPrefs.HasKey(grid.name + "_Timer"))
        {
            timer = PlayerPrefs.GetFloat(grid.name + "_Timer");
            System.TimeSpan span = System.TimeSpan.FromSeconds(timer);
            ingameMenuClockText.text = string.Format("{0:0}:{1:00}", span.Minutes, span.Seconds);
            ingameMenuClockTextShadow.text = ingameMenuClockText.text;
        }
        else
            timer = 0;

        if (PlayerPrefs.HasKey(grid.name + "_TimerBest"))
        {
            System.TimeSpan span = System.TimeSpan.FromSeconds(PlayerPrefs.GetFloat(grid.name + "_TimerBest"));
            ingameMenuClockBestText.text = string.Format("Record\n" + "{0:0}:{1:00}", span.Minutes, span.Seconds);
            ingameMenuClockBestTextShadow.text = ingameMenuClockBestText.text;
        }
        else
        {
            ingameMenuClockBestText.text = "";

            guiCam.ingameMenuClockBestGO.SetActiveRecursively(false);
        }

        if (PlayerPrefs.HasKey(grid.name + "_Combo"))
            maxWordCombo = PlayerPrefs.GetInt(grid.name + "_Combo");
        else
            maxWordCombo = 0;


        InitGrid(firstTimeLoaded);
    }


    // Save the state of the grid with the given id
    public void SaveGrid(string id)
    {
        if (grid != null && PlayerPrefs.HasKey(id) && PlayerPrefs.GetInt(id) != (int)Global.GridState.Empty)
        {
            XMLManager.SaveGameStateFile(grid);

            PlayerPrefs.SetFloat(grid.name + "_Timer", timer);
            PlayerPrefs.SetInt(grid.name + "_Combo", maxWordCombo);
        }
    }


    // Reset the state of the grid with the given id
    public void ResetGrid(string id)
    {
        XMLManager.RemoveFromGameStateFile(id);
        PlayerPrefs.SetInt(id, (int)Global.GridState.Empty);
        PlayerPrefs.SetFloat(id + "_Timer", 0);
        PlayerPrefs.SetInt(id + "_Combo", 0);

        DestroyLetterElements();
        onlyCreateLetterEntities = true;
        LoadGrid(id);
        isGridCompleted = false;
        timer = 0;
        maxWordCombo = 0;
        currentWordsCombo = 0;

        SetPendingHideInputDisablingMask();
    }


    // Reset all the grids to an empty state
    public void ResetAllGrids()
    {
        // Delete the save file
        XMLManager.ResetAllSavedData(headers);

        DestroyLetterElements();
        onlyCreateLetterEntities = true;
        LoadGrid(PlayerPrefs.GetString("CurrentGrid"));

        PlayerPrefs.SetInt("LastNbGridsCompletedAsk2", 0);

        SetPendingHideInputDisablingMask();
    }

    #endregion


    #region Loading

    // Set the given level to be loaded on next update
    public override void SetPendingLoadLevel(int level)
    {
        SaveGrid(grid.name);

        if (Global.Global.adEnabled)
        {
            //DEV_CODE || ADMOB || HIDE BANNER VIEW
            //cam.HideBannerView();
#if UNITY_ANDROID
            Destroy(cam.adMobAdapter);
#endif
        }

        base.SetPendingLoadLevel(level);
    }



    public void SetPendingChangeGameMode(Global.GameMode gm)
    {
        pendingGameModeToBeChanged = gm;
        waitedForOneUpdate = false;

        inputDisablingMaskGO.active = true;
    }


    public void SetPendingResetGrid()
    {
        pendingResetGrids = 0;
        waitedForOneUpdate = false;

        inputDisablingMaskGO.active = true;
    }


    public void SetPendingResetAllGrids()
    {
        pendingResetGrids = 1;
        waitedForOneUpdate = false;

        inputDisablingMaskGO.active = true;
    }


    public void SetPendingHideInputDisablingMask()
    {
        pendingHideMask = true;
        waitedForOneUpdate = false;
    }


    // Display the Loading overlay
    public override void CreateLoadingOverlay()
    {
        base.CreateLoadingOverlay();

        // Disable the GUI cam so that it stops from being rendered on top of the rest (especially on top of the loading screen)
        guiCam.gameObject.active = false;
    }

    #endregion


    #region Rate This App

    // Check if the Rate This App popup should be displayed, and if so display it. Returns true if the popup is to be displayed
    public bool CheckForRateThisAppPopup()
    {
        Debug.Log("Checking for RateThisApp");
        // If the player already clicked on the link OR the "Never ask again" button => return;
        if (PlayerPrefs.HasKey("NeverAskAgain2"))
        {
            Debug.Log("NeverAskAgain2 already set");
            return false;
        }

        int nbCompletedGrids = 0;
        foreach (string str in headers)
        {
            if (PlayerPrefs.HasKey(str) && PlayerPrefs.GetInt(str) == (int)Global.GridState.Completed)
            {
                nbCompletedGrids++;
            }
        }

        // If the number of completed grids is a multiple of 5 and we didn't already ask for that number of completed grids => Display the rate this app popup
        if (PlayerPrefs.HasKey("LastNbGridsCompletedAsk2") && nbCompletedGrids % 5 == 0 && PlayerPrefs.GetInt("LastNbGridsCompletedAsk2") != nbCompletedGrids)
        {
            Debug.Log("Display the RateThisApp");

            PlayerPrefs.SetInt("LastNbGridsCompletedAsk2", nbCompletedGrids);
            DisplayRateThisAppPopup();

            return true;
        }


        Debug.Log("No need to display the RateThisApp");

        return false;
    }


    public void DisplayRateThisAppPopup()
    {
        openedAlertView = Global.AlertViews.RateThisApp;

#if UNITY_IOS
        AVBinding.CreateRateAlertView();
#elif UNITY_ANDROID
        //TODO || DEV1
        //EtceteraAndroid.askForReviewNow("NOTER L'APPLICATION", rateThisAppString, Global.Global.adEnabled ? "com.digdog.freefleches" : "com.digdog.imotsfleches");
#endif
    }

    #endregion


    #region Wrappers

    //		public override bool isGameLogic ()
    //		{
    //				return true;
    //		}

    public override Global.LogicType GetLogicType()
    {
        return Global.LogicType.GameLogic;
    }


    public void EnablePhysics(bool enable)
    {
        physicsEnabled = enable;

        int phy;

        if (physicsEnabled)
        {
            phy = 0;
        }
        else
        {
            phy = 1;
        }

        PlayerPrefs.SetInt("PhysicsDisabled", phy);
    }


    // Search for the given letter in the remaining tokens (DnD mode only)
    public void SearchForLetter(string letter)
    {
        if (gameMode != Global.GameMode.DragNDrop)
            return;

        TokenEntity tokenFound = null;

        TokenEntity[] tokenEntities = FindObjectsOfType(typeof(TokenEntity)) as TokenEntity[];

        foreach (TokenEntity tokenEntity in tokenEntities)
        {
            if (tokenEntity.textMesh.text == letter && !tokenEntity.isCorrect)
            {
                tokenFound = tokenEntity;
                break;
            }
        }

        if (tokenFound != null)
        {
            tokenFound.StartBlinking();

            cam.MoveTo(new Vector3(tokenFound.transform.position.x, cam.transform.position.y, tokenFound.transform.position.z));
        }
        else
        {
#if UNITY_IOS
            AVBinding.CreateInfoAlertView("INFORMATION", "La lettre " + letter + " n'est pas disponible.");
#elif UNITY_ANDROID
            //TODO || DEV27
            //EtceteraAndroid.showAlert("INFORMATION", "La lettre " + letter + " n'est pas disponible.", "OK");
#endif

            Debug.Log("Letter " + letter + " not found.");
        }
    }


    // Set the game mode to the given one
    public void ChangeGameMode(Global.GameMode mode)
    {
        gameMode = mode;
        grid.gameMode = mode;

        onlyCreateLetterEntities = true;

        PlayerPrefs.SetInt("gameMode", (int)mode);

        guiCam.SetGameModeButtonCorrectMaterial();

        // Enable/disable visually the SearchForLetter button depending on the new gameMode
        ButtonIngameMenuSearchLetter searchForLetterButton = (guiCam.ingameMenuButtons[2].GetComponent("ButtonIngameMenuSearchLetter") as ButtonIngameMenuSearchLetter);
        if (gameMode == Global.GameMode.DragNDrop)
        {
            searchForLetterButton.SetNormalMaterial(searchForLetterButton.backupMaterial);
        }
        else
        {
            searchForLetterButton.SetNormalMaterial(searchForLetterButton.pressedMaterial);
        }

        OnlyKeepCorrectGuesses("");


        LetterEntity[] currentLetterEntities = FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];
        for (int i = 0; i < currentLetterEntities.Length; i++)
        {
            currentLetterEntities[i].FadeAndDestroy();
        }

        InitGrid(false);

        if (gameDifficulty == Global.Difficulty.Hard)
        {
            currentWordsCombo = 0;
            maxWordCombo = 0;
        }

        cam.ComputePanBorders();

        LetterEntitiesFadeStartTime = Time.time;

        //		inputDisablingMaskGO.active = false;
        SetPendingHideInputDisablingMask();
    }


    // Set the game mode to the one that is not currently active
    public void SwitchGameMode()
    {
        Global.GameMode newGameMode;

        if (gameMode == Global.GameMode.DragNDrop)
        {
            if (!guiCam.vKeyboardGO.active)
                guiCam.vKeyboardGO.SetActiveRecursively(true);

            newGameMode = Global.GameMode.Keyboard;
        }
        else
            newGameMode = Global.GameMode.DragNDrop;

        SetPendingChangeGameMode(newGameMode);
    }


    // Set the game difficulty to the given one
    public void ChangeGameDifficulty(Global.Difficulty newDifficulty)
    {
        gameDifficulty = newDifficulty;

        (guiCam.difficultyExplanationGO.GetComponent("TextMesh") as TextMesh).text = guiCam.difficultyExplanations[(int)gameDifficulty];

        PlayerPrefs.SetInt("GameDifficulty", (int)gameDifficulty);

        SetupCorrectGuesses();
    }


    // Set the game difficulty to the next one
    public void SwitchGameDifficulty()
    {
        Global.Difficulty newDifficulty;

        if (gameDifficulty == Global.Difficulty.Easy)
            newDifficulty = Global.Difficulty.Medium;
        else if (gameDifficulty == Global.Difficulty.Medium)
            newDifficulty = Global.Difficulty.Hard;
        else
            newDifficulty = Global.Difficulty.Easy;

        ChangeGameDifficulty(newDifficulty);
    }


    //	public void SetSoundMuted(bool mute)
    //	{
    //		float volume;
    //
    //		muteSound = mute;
    //
    //		if (mute)
    //		{
    //			PlayerPrefs.SetInt("MuteSound", 1);
    //			volume = 0;
    //		}
    //		else
    //		{
    //			PlayerPrefs.SetInt("MuteSound", 0);
    //			volume = Global.Global.SFXVolume;
    //		}
    //
    //
    //
    //		tileValidationAS.volume = volume;
    //		tokenDragAS.volume = volume;
    //		tokenDropAS.volume = volume;
    //		vKeyPressedAS.volume = volume;
    //	}


    //	public void SetSoundMuted(bool mute)
    //	{
    //		float volume;
    //
    //		muteSound = mute;
    //
    //		AudioSource[] audioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
    //
    //		if (mute)
    //		{
    //			PlayerPrefs.SetInt("MuteSound", 1);
    //
    //			foreach(AudioSource audioSource in audioSources)
    //			{
    //				audioSource.volume = 0;
    //			}
    //		}
    //		else
    //		{
    //			PlayerPrefs.SetInt("MuteSound", 0);
    //
    //			foreach(AudioSource audioSource in audioSources)
    //			{
    //				audioSource.volume = PlayerPrefs.GetFloat(audioSource.name + "_Volume");
    //			}
    //		}
    //	}





    //	public void GCAuthenticatePlayer()
    //	{
    //		Debug.Log("Player to be authenticated");
    //		waitingForPlayerAuthentication = true;
    //		GameCenterBinding.authenticateLocalPlayer();
    //	}

    #endregion


    #region Callbacks From Native

    public override void AVCallback(string message)
    {
        base.AVCallback(message);

        int parsedMessage = int.Parse(message);

        if (openedAlertView == Global.AlertViews.ResetGrid)
        {
            if (parsedMessage == 1)
            { // OK
                SetPendingResetGrid();
            }
        }
        else if (openedAlertView == Global.AlertViews.ResetAllGrids)
        {
            if (parsedMessage == 1)
            { // OK
                SetPendingResetAllGrids();
            }
        }
        else if (openedAlertView == Global.AlertViews.RateThisApp)
        {
            if (parsedMessage == 2)
            { // Never ask again
                PlayerPrefs.SetInt("NeverAskAgain2", 1);
            }
            else if (parsedMessage == 1)
            { // OK
                Application.OpenURL(rateThisAppURL);

                PlayerPrefs.SetInt("NeverAskAgain2", 1);
            }

            LoadNextGrid(); // Anyways, load next grid
        }
        else if (openedAlertView == Global.AlertViews.SolutionToDefinitionDouble)
        {
            if (parsedMessage == 1)
            { // Def #1
                SolutionToTheWord(grid.tiles[(int)focusedDefinition.gridPosition.y, (int)focusedDefinition.gridPosition.x] as DefinitionTile, 0);
            }
            else if (parsedMessage == 2)
            { // Def #2
                SolutionToTheWord(grid.tiles[(int)focusedDefinition.gridPosition.y, (int)focusedDefinition.gridPosition.x] as DefinitionTile, 1);
            }
        }
        else if (openedAlertView == Global.AlertViews.SolutionToDefinitionAlone)
        {
            if (parsedMessage == 1)
            { // OK
                SolutionToTheWord(grid.tiles[(int)focusedDefinition.gridPosition.y, (int)focusedDefinition.gridPosition.x] as DefinitionTile, 0);
            }
        }
        else if (openedAlertView == Global.AlertViews.NoWordSolutionStock)
        {
            if (parsedMessage == 1)
            { // Get some stock
                if (focusedDefinition != null)
                    focusedDefinition.Grow();

                PlayerPrefs.SetInt("OpenStoreOnSpecialPage", 1);

                (guiCam.ingameMenuButtons[5].GetComponent("ButtonIngameMenuStore") as ButtonIngameMenuStore).ButtonAction();
            }
        }

        openedAlertView = Global.AlertViews.Error;
    }

    #endregion


    #region Game Center

    public void GCAuthenticationHandler(bool success)
    {
        Debug.Log("*** GCAuthenticationHandler: success = " + success);

        if (success)
        {
             // Nik || 06-06-2020 || Code Remove
            Debug.Log("<color=red><b>Nik - Login Code Remove </b></color>");
            #if UNITY_IOS
            UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
            #endif
            leaderboard = Social.CreateLeaderboard();
            leaderboard.id = leaderboardID; //"com.digdog.imotsflechesdeluxe.totalscore";
        }
        //		else
        //		{
        //			AVBinding.CreateInfoAlertView("Echec de la connexion", "La connexion au GameCenter a échoué. Peut-être n'êtes-vous pas connecté à Internet ?");
        //		}
    }


    public void GCReportScore(int score)
    {
        if (Social.localUser.authenticated)
        {
            Debug.Log("<color=red>Nik - Leaderboard remove</color>");
            // Social.ReportScore(score, leaderboardID /*"com.digdog.imotsflechesdeluxe.totalscore"*/, GCScoreReportedHandler);
        }
    }


    public void GCScoreReportedHandler(bool success)
    {
        Debug.Log("*** GCScoreReportedHandler: success = " + success);
        if (success)
        {
            leaderboard.SetUserFilter(new string[] { Social.localUser.id });
            leaderboard.LoadScores(GCLoadScoresHandler);
            Debug.Log("OOOOOOOOO Range => " + leaderboard.range.from + "-" + leaderboard.range.count);
        }
    }


    public void GCLoadScoresHandler(bool success)
    {
        Debug.Log("*** GCLoadScoresHandler: success = " + success);
        if (success)
        {
            bool found = false;

            Debug.Log("Now looking for this user (" + Social.localUser.id + ") in leaderboard");



            foreach (UnityEngine.SocialPlatforms.IScore score in leaderboard.scores)
            {
                Debug.Log(score.userID);
                if (score.userID == Social.localUser.id)
                {
                    Debug.Log("User found!");

                    localPlayerGCScore = score.value;

                    if (PlayerPrefs.HasKey("Rank"))
                    {
                        scoreMenuButton.previousRank = PlayerPrefs.GetInt("Rank");
                        PlayerPrefs.SetInt("PreviousRank", scoreMenuButton.previousRank);
                    }
                    scoreMenuButton.rank = score.rank;
                    PlayerPrefs.SetInt("Rank", score.rank);

                    found = true;

                    break;
                }
            }

            if (!found)
            {
                scoreMenuButton.rank = -1;
            }
        }
    }


    public void GCShowLeaderboard()
    {
        if (Social.localUser.authenticated)
        {
            Social.ShowLeaderboardUI();
        }
    }


    //	public void GCReportAchievementProgress(string achievementId, double progress)
    //    {
    //        if (Social.localUser.authenticated)
    //		{
    //            Social.ReportProgress(achievementId, progress, GCProgressReportedHandler);
    //        }
    //    }
    //
    //
    //    private void GCProgressReportedHandler(bool success)
    //    {
    //        Debug.Log("*** HandleProgressReported: success = " + success);
    //    }


    public void GCReportProgress()
    {
        // TODO Report the progress for all the achievements

        // Number of grids completed for eah difficulty
        int nbCompletedEasyGrids = 0, nbCompletedMediumGrids = 0, nbCompletedHardGrids = 0;

        foreach (string header in headers)
        {
            string[] splittedId = header.Split('-');

            if (splittedId.Length != 4)
            {
                Debug.LogWarning("Be careful, grid ID " + header + " doesn't match the intended format");
                continue;
            }

            if (PlayerPrefs.HasKey(header) && PlayerPrefs.GetInt(header) == (int)Global.GridState.Completed)
            {
                int difficulty = int.Parse(splittedId[2]);

                if (difficulty == (int)Global.Difficulty.Easy)
                {
                    nbCompletedEasyGrids++;
                }
                else if (difficulty == (int)Global.Difficulty.Medium)
                {
                    nbCompletedMediumGrids++;
                }
                else if (difficulty == (int)Global.Difficulty.Hard)
                {
                    nbCompletedHardGrids++;
                }
            }
        }

        /// ACHIEVEMENTS \\\
        if (Global.Global.iPadVersion)
        {
            string appString;
            appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";

            // Solve one grid
            GCReportAchievementProgress("com.digdog." + appString + ".resoudreunegrille", 100);
            // Solve 50 grids in total
            GCReportAchievementProgress("com.digdog." + appString + ".jypassemesnuits", (nbCompletedEasyGrids + nbCompletedMediumGrids + nbCompletedHardGrids) * 2);

            // Solve 10 grids of Easy difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".DanslesTransport", nbCompletedEasyGrids * 10);
            // Solve 10 grids of Medium difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".danslelitjereflechi", nbCompletedMediumGrids * 10);
            // Solve 10 grids of Hard difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".dansletrain", nbCompletedHardGrids * 10);

            // Solve 50 grids of Easy difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".Dansmesbottesjesuisautop", nbCompletedEasyGrids * 2);
            // Solve 50 grids of Medium difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".jereprendraidugateau", nbCompletedMediumGrids * 2);
            // Solve 50 grids of Hard difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".boulimie", nbCompletedHardGrids * 2);

            // Solve 100 grids of Easy difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".debutantmaisjemedebrouille", nbCompletedEasyGrids);
            // Solve 100 grids of Medium difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".amateuretcasevoit", nbCompletedMediumGrids);
            // Solve 100 grids of Hard difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".professionelnonremunere", nbCompletedHardGrids);

            // Solve 200 grids of Easy difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".resoudredesgrillescestdugateau", nbCompletedEasyGrids * 0.5f);
            // Solve 200 grids of Medium difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".Casemangesansfin", nbCompletedMediumGrids * 0.5f);
            // Solve 200 grids of Hard difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".dieuxvivants", nbCompletedHardGrids * 0.5f);

            // Solve one grid for each difficulty
            if (nbCompletedEasyGrids >= 1 && nbCompletedMediumGrids >= 1 && nbCompletedHardGrids >= 1)
                GCReportAchievementProgress("com.digdog." + appString + ".jaimelechallenge", 100);
            // Solve 5 grids for each difficulty
            if (nbCompletedEasyGrids >= 5 && nbCompletedMediumGrids >= 5 && nbCompletedHardGrids >= 5)
                GCReportAchievementProgress("com.digdog." + appString + ".Onpyprendgout", 100);

            // Solve a 13x9 or 9x13 grid in less than 8 min
            if ((grid.size.size == Global.Size.Small || grid.size.size == Global.Size.Medium) && timer <= 480)
                GCReportAchievementProgress("com.digdog." + appString + ".coyote", 100);
            // Solve a 18x18 grid in less than 10 min
            if (grid.size.size == Global.Size.Big && timer <= 600)
                GCReportAchievementProgress("com.digdog." + appString + ".SuperMan", 100);

            // Get a score of 526 on a 13x9 or 9x13 grid
            if ((grid.size.size == Global.Size.Small || grid.size.size == Global.Size.Medium) && scoreManager.ComputeGridScore(grid.name, true) >= 526)
                GCReportAchievementProgress("com.digdog." + appString + ".Pro9x13", 100);
            // Get a score of 556 on a 13x9 or 9x13 grid
            if ((grid.size.size == Global.Size.Small || grid.size.size == Global.Size.Medium) && scoreManager.ComputeGridScore(grid.name, true) >= 556)
                GCReportAchievementProgress("com.digdog." + appString + ".genie9x13", 100);
            // Get a score of 1458 on a 18x18 grid
            if (grid.size.size == Global.Size.Big && scoreManager.ComputeGridScore(grid.name, true) >= 1458)
                GCReportAchievementProgress("com.digdog." + appString + ".Pro18x18", 100);
            // Get a score of 1488 on a 18x18 grid
            if (grid.size.size == Global.Size.Big && scoreManager.ComputeGridScore(grid.name, true) >= 1488)
                GCReportAchievementProgress("com.digdog." + appString + ".genie18x18", 100);
        }
        else
        {
            string appString;
            appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";

            // Solve one grid
            GCReportAchievementProgress("com.digdog." + appString + ".amour", 100);
            // Solve 50 grids in total
            GCReportAchievementProgress("com.digdog." + appString + ".Passemesnuits", (nbCompletedEasyGrids + nbCompletedMediumGrids + nbCompletedHardGrids) * 2);

            // Solve 10 grids of Easy difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".Transports", nbCompletedEasyGrids * 10);
            // Solve 10 grids of Medium difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".litreflechi", nbCompletedMediumGrids * 10);
            // Solve 10 grids of Hard difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".dansletrain", nbCompletedHardGrids * 10);

            // Solve 50 grids of Easy difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".Bottes", nbCompletedEasyGrids * 2);
            // Solve 50 grids of Medium difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".Gateau", nbCompletedMediumGrids * 2);
            // Solve 50 grids of Hard difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".Boulimie", nbCompletedHardGrids * 2);

            // Solve 100 grids of Easy difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".debutant", nbCompletedEasyGrids);
            // Solve 100 grids of Medium difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".Amateur", nbCompletedMediumGrids);
            // Solve 100 grids of Hard difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".pro", nbCompletedHardGrids);

            // Solve 200 grids of Easy difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".resoudregateau", nbCompletedEasyGrids * 0.5f);
            // Solve 200 grids of Medium difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".Sansfin", nbCompletedMediumGrids * 0.5f);
            // Solve 200 grids of Hard difficulty
            GCReportAchievementProgress("com.digdog." + appString + ".dieux", nbCompletedHardGrids * 0.5f);

            // Solve one grid for each difficulty
            if (nbCompletedEasyGrids >= 1 && nbCompletedMediumGrids >= 1 && nbCompletedHardGrids >= 1)
                GCReportAchievementProgress("com.digdog." + appString + ".Jaimelechallenge", 100);
            // Solve 5 grids for each difficulty
            if (nbCompletedEasyGrids >= 5 && nbCompletedMediumGrids >= 5 && nbCompletedHardGrids >= 5)
                GCReportAchievementProgress("com.digdog." + appString + ".gout", 100);

            // Solve a 8x5 grid in less than 4 min
            if (grid.size.size == Global.Size.Small && timer <= 240)
                GCReportAchievementProgress("com.digdog." + appString + ".Viteombre", 100);
            // Solve a 10x10 grid in less than 5 min
            if (grid.size.size == Global.Size.Medium && timer <= 300)
                GCReportAchievementProgress("com.digdog." + appString + ".gordon", 100);

            // Get a score of 180 on a 8x5 grid
            if (grid.size.size == Global.Size.Small && scoreManager.ComputeGridScore(grid.name, true) >= 180)
                GCReportAchievementProgress("com.digdog." + appString + ".Pro8x5", 100);
            // Get a score of 210 on a 8x5 grid
            if (grid.size.size == Global.Size.Small && scoreManager.ComputeGridScore(grid.name, true) >= 210)
                GCReportAchievementProgress("com.digdog." + appString + ".genie8x5", 100);
            // Get a score of 450 on a 10x10 grid
            if (grid.size.size == Global.Size.Medium && scoreManager.ComputeGridScore(grid.name, true) >= 450)
                GCReportAchievementProgress("com.digdog." + appString + ".Pro10x10", 100);
            // Get a score of 480 on a 10x10 grid
            if (grid.size.size == Global.Size.Medium && scoreManager.ComputeGridScore(grid.name, true) >= 480)
                GCReportAchievementProgress("com.digdog." + appString + ".genie10x10", 100);
        }
    }

    #endregion


    #region Android Etcetera

    void askForReviewDontAskAgainEvent()
    {
        Debug.Log("askForReviewDontAskAgainEvent");

        AVCallback("2");
    }


    void askForReviewRemindMeLaterEvent()
    {
        Debug.Log("askForReviewRemindMeLaterEvent");
    }


    void askForReviewWillOpenMarketEvent()
    {
        Debug.Log("askForReviewWillOpenMarketEvent");

        AVCallback("1");
    }

    #endregion


    #region DEBUG

    void SetCurrentGridAsCompleted()
    {
        GridCompleted();
    }

    #endregion
}
