using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreManager : MonoBehaviour
{



    int[] smallValues = null;
    // Score values (1 for each difficulty) for the completion of the small grids
    int[] mediumValues = null;
    // Score values (1 for each difficulty) for the completion of the medium grids
    int[] bigValues = null;
    // Score values (1 for each difficulty) for the completion of the big grids

    // Nik - Add Code
    int[] VsmallValues = null;
    int[] VmediumValues = null;

    int[] timeMaxScoreValue;
    // Max points scored by time; (1 for each size)
    int[] timeMinScoreValue;
    // Min points scored by time; (1 for each size)
    float[] timeMaxValue;
    // Under/equal that time, player scores timeMaxScoreValue; expressed in seconds; (1 for each size)
    float[] timeMinValue;
    // Above/equal that time, player scores timeMinScoreValue; expressed in seconds; (1 for each size)

    int comboBaseScore;

    public int totalScore;


    // Use this for initialization
    void Start()
    {
        if (Global.Global.iPadVersion)
        {
            // Values in points of the grids depending on the difficulties.
            // smallValues = new int[3] { 117, 175, 292 };
            // mediumValues = new int[3] { 117, 175, 292 };
            // bigValues = new int[3] { 324, 486, 810 };

            // // Time boundaries for time bonus score
            // timeMaxValue = new float[3] { 540, 540, 720 };
            // timeMinValue = new float[3] { 1260, 1260, 1800 };

            // // Values in points of the min and max time to complete the grid.
            // timeMaxScoreValue = new int[3] { 234, 234, 648 };
            // timeMinScoreValue = new int[3] { 0, 0, 0 };

            // Nik - Add Code
            smallValues = new int[3] { 117, 175, 292 };
            mediumValues = new int[3] { 117, 175, 292 };
            bigValues = new int[3] { 324, 486, 810 };
            VsmallValues = new int[3] { 117, 175, 292 };
            VmediumValues = new int[3] { 117, 175, 292 };

            // Time boundaries for time bonus score
            timeMaxValue = new float[5] { 540, 540, 720, 300, 420 };
            timeMinValue = new float[5] { 1260, 1260, 1800, 720, 960 };

            // Values in points of the min and max time to complete the grid.
            timeMaxScoreValue = new int[5] { 234, 234, 648, 80, 200 };
            timeMinScoreValue = new int[5] { 0, 0, 0, 0, 0 };
        }
        else
        {
            // Values in points of the grids depending on the difficulties.
            smallValues = new int[3] { 40, 60, 100 };
            mediumValues = new int[3] { 100, 150, 250 };

            // Time boundaries for time bonus score
            timeMaxValue = new float[2] { 300, 420 };
            timeMinValue = new float[2] { 720, 960 };

            // Values in points of the min and max time to complete the grid.
            timeMaxScoreValue = new int[2] { 80, 200 };
            timeMinScoreValue = new int[2] { 0, 0 };
        }
        print("Sccore manager start---------------");
        comboBaseScore = 3;
    }


    // Update is called once per frame
    void Update()
    {

    }


    // Compute the total score
    public void ComputeTotalScore()
    {
        Debug.Log("___COMPUTING TOTAL SCORE___");

        totalScore = 0;

        string platform;

        // iPad
        if (Global.Global.iPadVersion)
        {
            platform = "iPad";
        }
        // iPhone/iPod
        else
        {
            platform = "iPhone";
        }

        //		List<string> headers = XMLManager.LightLoadGridFilesInFolder("XMLs/" + platform);
        List<string> headers = XMLManager.LoadGridFilesHeaders();

        headers = Global.Global.TrimLockedGrids(headers);

        foreach (string header in headers)
        {
            int tmpScore = ComputeGridScore(header, true);
            if (PlayerPrefs.HasKey(header + "_ScoreBest") && PlayerPrefs.GetInt(header + "_ScoreBest") > tmpScore)
            {
                Debug.Log("THE BEST SAVED SCORE FOR THIS GRID IS HIGHER => USE IT INSTEAD");
                totalScore += PlayerPrefs.GetInt(header + "_ScoreBest");
            }
            else
            {
                totalScore += tmpScore;
            }
        }

        Debug.Log("_-_- Total score = " + totalScore + " points. -_-_");
    }


    // Computes the score for the given grid. Returns the score computed
    public int ComputeGridScore(string id, bool useBestTime)
    {
        int gridScore = 0;

        // If the grid is completed (or HAS ALREADY BEEN completed before being reset) => add the grid score to the total score
        if (PlayerPrefs.GetInt(id) == (int)Global.GridState.Completed || PlayerPrefs.HasKey(id + "_ScoreBest"))
        {
            Debug.Log("-- " + id + " :");

            gridScore += ComputeScoreForGridType(id);
            Debug.Log(gridScore + " points");

            // Also add the time bonus score
            if (PlayerPrefs.HasKey(id + "_TimerBest"))
            {
                float time;
                if (useBestTime)
                    time = PlayerPrefs.GetFloat(id + "_TimerBest");
                else
                    time = PlayerPrefs.GetFloat(id + "_Timer");

                string[] splittedId = id.Split('-');
                if (splittedId.Length != 4)
                {
                    Debug.LogWarning("Be careful, grid ID " + id + " doesn't match the intended format");
                    return 0;
                }
                Debug.Log("Nik - Game Complete splittedId[1] : " + splittedId[1]);
                int size = int.Parse(splittedId[1]);

                int timeScore = ComputeScoreForTime(time, size);

                Debug.Log("+ " + timeScore + " points as time bonus");

                gridScore += timeScore;
            }

            if (PlayerPrefs.HasKey(id + "_Combo"))
            {
                int combo = PlayerPrefs.GetInt(id + "_Combo");

                int comboScore = ComputeScoreForCombo(combo);

                Debug.Log("+ " + comboScore + " points as combo bonus");

                gridScore += comboScore;
            }
        }

        return gridScore;
    }


    // Computes the score for a given time of completion. Returns the score computed
    public int ComputeScoreForTime(float time, int gridSize)
    {
        int timeScore;

        if (time <= timeMaxValue[gridSize])
            timeScore = timeMaxScoreValue[gridSize];
        else if (time >= timeMinValue[gridSize])
            timeScore = timeMinScoreValue[gridSize];
        else
        {
            float multiplier = (time - timeMaxValue[gridSize]) / (timeMinValue[gridSize] - timeMaxValue[gridSize]);
            timeScore = (int)((1 - multiplier) * timeMaxScoreValue[gridSize]);
        }


        return timeScore;
    }


    // Computes the score for a given type of grid. Returns the score computed
    public int ComputeScoreForGridType(string id)
    {
        int score;

        string[] splittedId = id.Split('-');

        if (splittedId.Length != 4)
        {
            Debug.LogWarning("Be careful, grid ID " + id + " doesn't match the intended format");
            return 0;
        }

        int size = int.Parse(splittedId[1]);
        int difficulty = int.Parse(splittedId[2]);

        // Small grids scores
        if (size == (int)Global.Size.Small)
        {
            score = smallValues[difficulty];
            Debug.Log("Nik - Score Is SmallValue");
        }
        // Medium grids scores
        else if (size == (int)Global.Size.Medium)
        {
            score = mediumValues[difficulty];
        }
        else if (size == (int)Global.Size.Big)
        {
            score = bigValues[difficulty];
        }
        else if (size == (int)Global.Size.VSmall)
        {
            score = VsmallValues[difficulty];
        }
        else if (size == (int)Global.Size.VMedium)
        {
            score = VmediumValues[difficulty];
        }
        else
        {
            Debug.LogWarning("Be careful, grid " + id + " has an unexpected size.");
            return 0;
        }
        Debug.Log("Nik -");
        return score;
    }


    // Computes the score for a given combo. Returns the score computed
    public int ComputeScoreForCombo(int combo)
    {
        int score = 0;

        for (int i = 0; i < combo; i++)
        {
            score += i * comboBaseScore;
        }

        return score;
    }
}
