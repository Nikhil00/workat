using UnityEngine;
using System.Collections;


public abstract class GridElementEntity : PanningEntity
{

    public Vector2 gridPosition;

    public TextMesh textMesh;

    protected GameLogic gameLogic;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();

        textMesh = GetComponentInChildren(typeof(TextMesh)) as TextMesh;

        gameLogic = logic as GameLogic;
    }


    protected override void Start()
    {
        base.Start();
    }


    public virtual void InitGridElement()
    {
        name = " [" + gridPosition.x + "," + gridPosition.y + "]";

        transform.parent = GameObject.Find("GridEntities").transform;

        Vector3 pos = gameLogic.GetWorldPositionFromGridPosition(gridPosition);
        transform.position = new Vector3(pos.x, transform.position.y, pos.z);
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    // Return true if this entity is at the given grid position
    public bool IsAtGridPosition(Vector2 gridPos)
    {
        return gridPos == gridPosition;
    }


    public abstract bool IsLetterEntity();


    #region Input Management

    public override void TouchBegin(Vector2 touchPosition)
    {
        base.TouchBegin(touchPosition);
    }


    public override void TouchUpdate(Vector2 touchPosition)
    {
        base.TouchUpdate(touchPosition);
    }


    public override void TouchEnd(Vector2 touchPosition)
    {
        base.TouchEnd(touchPosition);
    }

    #endregion
}
