using UnityEngine;
using System.Collections;

public class MenuHelpEntity : BaseEntity
{

    GUICam guiCam;
    public Material iPhoneMat, iPadMat;

    protected override void Awake()
    {
        base.Awake();

        if (Global.Global.iPadVersion)
            GetComponent<Renderer>().material = iPadMat;
        else
            GetComponent<Renderer>().material = iPhoneMat;
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        guiCam = FindObjectOfType(typeof(GUICam)) as GUICam;

    }


    #region Input Management

    public override void TouchEnd(Vector2 touchPosition)
    {
        base.TouchEnd(touchPosition);

        guiCam.StartHelpMenuAnimation();
    }

    #endregion
}
