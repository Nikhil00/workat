using UnityEngine;
using System.Collections;
using System;


public class GUISlidingPanelEntity : GUIEntity
{

    public bool isSliding = false;
    public Vector3 slideOffset;

    protected override void Start()
    {
        base.Start();
    }


    //	// Used by a delegate to create and init a Translate3DAnimation for the children, in order to make them move with
    //	protected void InitChildrenTranslateAnim(GameObject child)
    //	{
    //		child.gameObject.AddComponent(typeof(Translate3DAnimation));
    //		Translate3DAnimation translate3DAnim = child.GetComponent("Translate3DAnimation") as Translate3DAnimation;
    //		translate3DAnim.animationSpeed = myTranslate3DAnim.animationSpeed;
    //		translate3DAnim.IsGoingAToB = myTranslate3DAnim.IsGoingAToB;
    //		translate3DAnim.aValue = new Vector3(child.transform.position.x, child.transform.position.y, child.transform.position.z);
    //		translate3DAnim.bValue = new Vector3(child.transform.position.x + slideOffset.x, child.transform.position.y + slideOffset.y, child.transform.position.z + slideOffset.z);
    //		
    //		translate3DAnim.RegisterStartAnimationToMe(this);
    //	}


    protected override void Update()
    {
        base.Update();
    }


    protected override void ActionsOnTouchUpdateOnlyOnce(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdateOnlyOnce(touchPosition);
    }


    public override void AnimationStopped(string animation)
    {
        base.AnimationStopped(animation);

        logic.cam.disablePan = false;
        isSliding = false;
    }


    public override void StartAnimation(string animationToPlay)
    {
        base.StartAnimation(animationToPlay);

        isSliding = true;
    }
}
