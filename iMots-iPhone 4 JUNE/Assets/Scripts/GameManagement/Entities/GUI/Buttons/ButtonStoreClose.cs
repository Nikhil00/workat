using UnityEngine;
using System.Collections;
using System;


public class ButtonStoreClose : GUIButtonEntity
{

    protected override void Start()
    {
        base.Start();
    }


    protected override void Update()
    {
        base.Update();

    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        #region DEV_CODE
        //DEV_CODE || LOADING PANEL
        if (FindObjectOfType<Dev_UIManager>()) FindObjectOfType<Dev_UIManager>().OpenLoadingPanel();
        if (FindObjectOfType<Dev_StoreUIManager>()) FindObjectOfType<Dev_StoreUIManager>().OpenLoadingPanel();
        StartCoroutine(LoadScene());

        #endregion

        #region ORIGINAL_CODE
        //logic.SetPendingLoadLevel(0);
        #endregion
    }

    //DEV_CODE || METHOD || CREATING NEW METHOD
    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(0.5f);
        logic.SetPendingLoadLevel(0);
    }

    #endregion
}
