using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuGameMode : GUIButtonEntity
{

    protected GameLogic gameLogic;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;

    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        //		// Can't change gamemode when the ad is enabled
        //		if (Global.Global.adEnabled)
        //			return;

        if (gameLogic.endOfGridAnim == null)
            gameLogic.SwitchGameMode();
        else
        {
#if UNITY_IOS
            AVBinding.CreateInfoAlertView("INFORMATION", "Le mode de jeu ne peut pas changer pendant l'animation de fin de grille.");
#elif UNITY_ANDROID
            //TODO || DEV4
			//EtceteraAndroid.showAlert("INFORMATION", "Le mode de jeu ne peut pas changer pendant l'animation de fin de grille.", "OK");
#endif
        }
    }

    public void GameModeButtonAction()
    {
        if (gameLogic.endOfGridAnim == null)
            gameLogic.SwitchGameMode();
        else
        {
#if UNITY_IOS
            AVBinding.CreateInfoAlertView("INFORMATION", "Le mode de jeu ne peut pas changer pendant l'animation de fin de grille.");
#elif UNITY_ANDROID
            //TODO || DEV5
			//EtceteraAndroid.showAlert("INFORMATION", "Le mode de jeu ne peut pas changer pendant l'animation de fin de grille.", "OK");
#endif
        }
    }

    #endregion
}
