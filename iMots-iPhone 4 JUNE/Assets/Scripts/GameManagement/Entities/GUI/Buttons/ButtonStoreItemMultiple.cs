using UnityEngine;
using System.Collections;
using System;


public class ButtonStoreItemMultiple : ButtonStoreItem
{


    // In App
    public string[] firstTargetsGrids;
    public int[] nbTargetsGrids;


    protected override void Awake()
    {
        base.Awake();

        firstTargetGrid = firstTargetsGrids[0];
    }


    public override void UnlockMyGrids()
    {
        string platform;
        if (Global.Global.iPadVersion)
            platform = "Ipad-";
        else
            platform = "Iphone-";

        for (int n = 0; n < firstTargetsGrids.Length; n++)
        {
            string targetsCommonBaseAsString = firstTargetsGrids[n].Substring(0, 4);
            int firstTargetGridIndex = int.Parse(firstTargetsGrids[n].Substring(4, 3));

            string gridIndexString, fullIDString;

            for (int i = 0; i < nbTargetsGrids[n]; i++)
            {
                int currentID = firstTargetGridIndex + i;

                if (currentID < 10)
                    gridIndexString = "00" + currentID;
                else if (currentID < 100)
                    gridIndexString = "0" + currentID;
                else
                    gridIndexString = currentID.ToString();

                fullIDString = platform + targetsCommonBaseAsString + gridIndexString;

                if (!PlayerPrefs.HasKey(fullIDString)) // Shouldn't be, but better check it anyways
                    PlayerPrefs.SetInt(fullIDString, (int)Global.GridState.Empty);

                Debug.Log(fullIDString + " UNLOCKED");
            }
        }

        SetAlreadyBought(true);


        // Achievement => buy a pack of grids
        if (Global.Global.iPadVersion)
        {
            string appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";

            logic.GCReportAchievementProgress("com.digdog." + appString + ".EncoreEncore", 100);
        }
        else
        {
            string appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";

            logic.GCReportAchievementProgress("com.digdog." + appString + ".encore", 100);
        }
    }

    public void UnlockMyGrids_Dev_Multiple()
    {
        string platform;
        if (Global.Global.iPadVersion)
            platform = "Ipad-";
        else
            platform = "Iphone-";

        for (int n = 0; n < firstTargetsGrids.Length; n++)
        {
            string targetsCommonBaseAsString = firstTargetsGrids[n].Substring(0, 4);
            int firstTargetGridIndex = int.Parse(firstTargetsGrids[n].Substring(4, 3));

            string gridIndexString, fullIDString;

            for (int i = 0; i < nbTargetsGrids[n]; i++)
            {
                int currentID = firstTargetGridIndex + i;

                if (currentID < 10)
                    gridIndexString = "00" + currentID;
                else if (currentID < 100)
                    gridIndexString = "0" + currentID;
                else
                    gridIndexString = currentID.ToString();

                fullIDString = platform + targetsCommonBaseAsString + gridIndexString;

                if (!PlayerPrefs.HasKey(fullIDString)) // Shouldn't be, but better check it anyways
                    PlayerPrefs.SetInt(fullIDString, (int)Global.GridState.Empty);

                Debug.Log(fullIDString + " UNLOCKED");
            }
        }

        SetAlreadyBought(true);


        // Achievement => buy a pack of grids
        if (Global.Global.iPadVersion)
        {
            string appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";

            logic.GCReportAchievementProgress("com.digdog." + appString + ".EncoreEncore", 100);
        }
        else
        {
            string appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";

            logic.GCReportAchievementProgress("com.digdog." + appString + ".encore", 100);
        }
    }
}
