using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameSolutionForWord : GUIButtonEntity
{

    protected GameLogic gameLogic;

    public string solutionToDefinitionAVTitleString, solutionToDefinitionAloneAVString, solutionToDefinitionDoubleAVString, noStockForSolutionToDefinitionAVString, solutionToDefinitionDoubleAVUpButtonString, solutionToDefinitionDoubleAVDownButtonString;

    public float fadeDuration;
    protected float fadeStartTime = 0;
    protected int fadeState = 0; // 0 = fading in, 1 = fading out, 2 = invisible
    float lerpOutputValue;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;
    }


    protected override void Update()
    {
        base.Update();

        if (Time.time >= fadeStartTime + fadeDuration)
        {
            fadeState = (fadeState + 1) % 3;
            fadeStartTime = Time.time;
        }

        if (fadeState == 2)
        {
            lerpOutputValue = 0;
        }
        else
        {
            float lerpInputValue = (fadeDuration - (fadeStartTime + fadeDuration - Time.time)) / fadeDuration;
            if (fadeState == 0)
            {
                lerpOutputValue = Mathf.Lerp(0, 1, lerpInputValue * 1.25f);
            }
            else //if (fadeState == 1)
            {
                lerpOutputValue = Mathf.Lerp(1, 0, lerpInputValue);
            }
        }

        GetComponent<Renderer>().material.color = new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, lerpOutputValue);
    }


    public void SetActive(bool active)
    {
        Debug.Log("SET ACTIVE");
        if (active)
        {
            fadeState = 0;
            GetComponent<Renderer>().material.color = new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, 0);
            fadeStartTime = Time.time;
        }

        gameObject.SetActive(active);
    }


    // Take care of unfocusing the currently focused definition if there is one
    protected override void UnfocusDefinition()
    {
    }


    #region Input Management

    //DEV_CODE || IMP || ++++++++++++++++++++++==================
    //DEV_EDIT || 25 OCT || CHANGE IF_UNITY_IPHONE TO UNITY_IOS
    public override void ButtonAction()
    {
        base.ButtonAction();

        //DEBUG
        if (Application.platform == RuntimePlatform.OSXEditor)
        {
            gameLogic.SolutionToTheWord(gameLogic.grid.tiles[(int)gameLogic.focusedDefinition.gridPosition.y, (int)gameLogic.focusedDefinition.gridPosition.x] as DefinitionTile, 0);
        }

        int nbWordSolutions = PlayerPrefs.GetInt("nbWordSolutions");

        // Player is out of stock for word solutions
        if (nbWordSolutions <= 0)
        {
            gameLogic.openedAlertView = Global.AlertViews.NoWordSolutionStock;
#if UNITY_IOS
            AVBinding.CreateAlertView(solutionToDefinitionAVTitleString + "0)", noStockForSolutionToDefinitionAVString);
#elif UNITY_ANDROID
            //TODO || DEV8
			//EtceteraAndroid.showAlert(solutionToDefinitionAVTitleString + "0)", noStockForSolutionToDefinitionAVString, "Continuer", "Annuler");
#endif
        }
        // Player still has some stock for word solutions
        else
        {
            // Solo def
            if ((gameLogic.grid.tiles[(int)gameLogic.focusedDefinition.gridPosition.y, (int)gameLogic.focusedDefinition.gridPosition.x] as DefinitionTile).definitions[1].defPos == Global.DefinitionPosition.Error)
            {
                gameLogic.openedAlertView = Global.AlertViews.SolutionToDefinitionAlone;
#if UNITY_IOS
                AVBinding.CreateAlertView(solutionToDefinitionAVTitleString + nbWordSolutions + ")", solutionToDefinitionAloneAVString);
#elif UNITY_ANDROID
                //TODO || DEV9
                //EtceteraAndroid.showAlert(solutionToDefinitionAVTitleString + nbWordSolutions + ")", solutionToDefinitionAloneAVString, "Continuer", "Annuler");
#endif
            }
            // Two defs
            else
            {
                gameLogic.openedAlertView = Global.AlertViews.SolutionToDefinitionDouble;
#if UNITY_IOS
                AVBinding.CreateSolutionToWordDouble(solutionToDefinitionAVTitleString + nbWordSolutions + ")", solutionToDefinitionDoubleAVString);
#elif UNITY_ANDROID
                //TODO || DEV10
                //EtceteraAndroid.showAlert(solutionToDefinitionAVTitleString + nbWordSolutions + ")", solutionToDefinitionDoubleAVString, solutionToDefinitionDoubleAVUpButtonString, solutionToDefinitionDoubleAVDownButtonString);
#endif
            }
        }
    }

    #endregion
}
