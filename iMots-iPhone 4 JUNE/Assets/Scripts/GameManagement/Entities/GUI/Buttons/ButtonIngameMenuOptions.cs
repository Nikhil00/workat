using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuOptions : GUIButtonEntity
{

    protected GameLogic gameLogic;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        gameLogic.guiCam.StartOptionsMenuAnimation();
    }

    public void OptionsButtonAction()
    {
        gameLogic.guiCam.StartOptionsMenuAnimation();
    }

    #endregion
}
