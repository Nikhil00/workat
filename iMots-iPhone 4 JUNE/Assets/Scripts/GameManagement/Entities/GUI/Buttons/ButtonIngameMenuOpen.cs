using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuOpen : GUIButtonEntity
{

    protected GameLogic gameLogic;

    //	public AudioSource openAS, closeAS;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        if (gameLogic.guiCam.vKeyboardState == Global.KeyboardState.Focused)
        {
            if (gameLogic.gameMode == Global.GameMode.DragNDrop)
                gameLogic.guiCam.SetKeyboardState(Global.KeyboardState.Off, false);
            else
                gameLogic.SelectTile(null);
        }

        gameLogic.guiCam.StartMenuPanelAnimation();


    }

    #endregion
}
