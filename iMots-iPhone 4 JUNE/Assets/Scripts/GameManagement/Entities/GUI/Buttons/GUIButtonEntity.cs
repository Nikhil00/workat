using UnityEngine;
using System.Collections;
using System;


public class GUIButtonEntity : GUIEntity
{

    public Material normalMaterial, pressedMaterial;
    [HideInInspector]
    public Material backupMaterial; // Used whenever there is a need to backup a material (often the normalMaterial)
    public bool needToUpdateMaterial = true;
    protected bool ifReleasedDoButtonAction = false;

    public AudioSource clickAS;

    Camera touchDependsOnThisCam;

    protected override void Start()
    {
        base.Start();

        //DEV_EDIT || GET GAMEPLAY UI BUTTON MATERIAL FROM HERE
        if (normalMaterial == null)
        {
            if (GetComponent<Renderer>()) normalMaterial = GetComponent<Renderer>().material;
        }

        SetNormalMaterial(normalMaterial);

        backupMaterial = normalMaterial;

        // Used to determine which cam will be used by the raycasts later
        if (tag == "GUI")
        {
            touchDependsOnThisCam = ((GameObject.FindObjectOfType(typeof(GUICam)) as GUICam).GetComponent<Camera>()) as Camera;
        }
        else
        {
            touchDependsOnThisCam = Camera.main;
        }
    }


    protected override void Update()
    {
        base.Update();
    }


    public void SetNormalMaterial(Material normalMat)
    {
        normalMaterial = normalMat;

        //		float tmpA = -1;
        //		if (renderer.material.color.a != normalMat.color.a)
        //			tmpA = renderer.material.color.a;

        //DEV_EDIT || XTRA || ADDING CONDITION FOR SPAWN BUTTON 
        if (GetComponent<Renderer>()) GetComponent<Renderer>().material = normalMaterial;
        //		
        //		if (tmpA != -1)
        //			renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, tmpA);

        if (pressedMaterial == null)
            pressedMaterial = normalMaterial;
    }


    public void SetPressedMaterial(Material pressedMat)
    {
        pressedMaterial = pressedMat;
    }


    public void SetCurrentlyUnderTouch(bool isUnderTouch)
    {
        if (isUnderTouch)
        {
            GetComponent<Renderer>().material = pressedMaterial;
        }
        else
        {
            GetComponent<Renderer>().material = normalMaterial;
        }

        ifReleasedDoButtonAction = isUnderTouch;
    }


    #region Input Management

    protected override void ActionsOnTouchUpdate(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdate(touchPosition);

        if (touchPosition != lastTouchPosition && needToUpdateMaterial)
        {
            Ray ray = touchDependsOnThisCam.ScreenPointToRay(touchPosition);
            RaycastHit hitInfo;
            // If the Ray hits this  then the touch is still over the button => change the texture to pressedTexture
            if (Physics.Raycast(ray, out hitInfo) && hitInfo.transform == transform)
            {
                //				if (renderer.material != pressedMaterial)
                //				    renderer.material = pressedMaterial;
                if (!ifReleasedDoButtonAction)
                    SetCurrentlyUnderTouch(true);
            }
            else
            {
                //				if (renderer.material != normalMaterial)
                //				    renderer.material = normalMaterial;
                if (ifReleasedDoButtonAction)
                    SetCurrentlyUnderTouch(false);
            }
        }
    }


    protected override void ActionsOnTouchUpdateOnlyOnce(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdateOnlyOnce(touchPosition);
        //		renderer.material = pressedMaterial;
        SetCurrentlyUnderTouch(true);
    }


    protected override void ActionsOnTouchEnd(Vector2 touchPosition)
    {
        base.ActionsOnTouchEnd(touchPosition);

        GetComponent<Renderer>().material = normalMaterial;

        if (ifReleasedDoButtonAction)
            ButtonAction();
    }


    public virtual void ButtonAction()
    {
        Debug.Log("BUTTON ACTION");

        if (clickAS != null && normalMaterial != pressedMaterial)
            clickAS.Play();
    }

    #endregion
}
