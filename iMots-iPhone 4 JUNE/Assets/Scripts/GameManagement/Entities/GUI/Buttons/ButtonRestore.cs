﻿using UnityEngine;
using System.Collections;

public class ButtonRestore : GUIButtonEntity
{
    protected StoreLogic storeLogic;

    protected override void Start()
    {
        base.Start();
        storeLogic = logic as StoreLogic;
    }


    protected override void Update()
    {
        base.Update();

    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();
        storeLogic.RestorePurchase();
    }

    #endregion

    //		void OnGUI ()
    //		{
    //
    ////				GUI.Box (new Rect (0, 0, 100, 50), "Top-left");
    ////				GUI.Box (new Rect (Screen.width - 100, 0, 100, 50), "Top-right");
    ////				GUI.Button (new Rect (Screen.width - 100, 0, 100, 50), "Top-right");
    //
    ////				GUI.Box (new Rect (0, Screen.height - 50, 100, 50), "Bottom-left");
    ////				GUI.Box (new Rect (Screen.width - 100, Screen.height - 50, 100, 50), "Bottom right");
    //		}

    //		void OnGUI ()
    //		{
    //				if (GUI.Button (new Rect (Screen.width - 100, 0, 100, 50), "Top-right"))
    //						print ("You clicked the button!");
    //
    //		}
}
