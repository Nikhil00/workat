using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuGrids : GUIButtonEntity
{

    protected GameLogic gameLogic;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        gameLogic.SetPendingLoadLevel(1);
    }

    public void GridsButtonAction()
    {
        //gameLogic.SetPendingLoadLevel(1);

        FindObjectOfType<Dev_GameUIManager>().OpenLoadingPanel();
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(0.5f);
        gameLogic.SetPendingLoadLevel(1);
    }

    #endregion
}
