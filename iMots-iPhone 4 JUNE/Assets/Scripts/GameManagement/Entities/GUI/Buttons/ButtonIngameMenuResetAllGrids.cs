using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuResetAllGrids : GUIButtonEntity
{

    protected GameLogic gameLogic;

    public string resetAllGridsConfirmationString;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        gameLogic.openedAlertView = Global.AlertViews.ResetAllGrids;
#if UNITY_IOS
        AVBinding.CreateAlertView("CONFIRMATION", resetAllGridsConfirmationString);
#elif UNITY_ANDROID
        //TODO || DEV3
		//EtceteraAndroid.showAlert("CONFIRMATION", resetAllGridsConfirmationString, "Continuer", "Annuler");
#endif
    }

    #endregion
}
