using UnityEngine;
using System.Collections;
using System;


public class ScoreMenuButton : GUIButtonEntity
{

    protected GameLogic gameLogic;

    [HideInInspector]
    public string[] valuesString;
    [HideInInspector]
    public long[] scores;
    [HideInInspector]
    public int rank = -1, previousRank = -1;

    // Score animation
    int currentLineBeingDisplayed;
    int lineCurrentScore;
    float currentScoreDisplayTime;
    const float delayBetweenLinesScoreIncrementation = 0.005f;
    bool animateScore = false;
    const float delayBetweenLinesDisplay = 0.5f;
    float delayBetweenLinesStartTime;
    bool waitingForDelayBetweenLines = false;

    // Rank animation
    float currentRankDisplayTime = 0;
    const float delayBetweenRankIncrementation = 0.5f;
    int currentRankIndex = 0;
    bool animateRank = false;

    // Tap to continue text animation
    float tapToContinueStartTime = 0, tapToContinueStepDuration = 0.75f;
    public GameObject tapToContinueTextGO;
    bool tapToContinueFadingIn = true;
    bool animateTapToContinue = false;

    // Sounds
    public AudioSource scoreGrowAS, scoreStepAS, rankGrowAS, rankBadAS;

    [HideInInspector]
    public bool newScoreRecord = false;



    protected override void Start()
    {
        base.Start();

        valuesString = new string[3];
        scores = new long[5];

        gameLogic = logic as GameLogic;

        GetComponent<Renderer>().material.color = new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, 0);
    }


    protected override void Update()
    {
        base.Update();

        if (animateScore)
        {
            if (waitingForDelayBetweenLines)
            {
                if (Time.time >= delayBetweenLinesStartTime + delayBetweenLinesDisplay)
                {
                    waitingForDelayBetweenLines = false;
                    InitNewLine();
                }
            }
            else
            {
                if (Time.time >= currentScoreDisplayTime + delayBetweenLinesScoreIncrementation)
                {
                    lineCurrentScore++;
                    currentScoreDisplayTime = Time.time;
                    gameLogic.guiCam.scoreMenuScoreTexts[currentLineBeingDisplayed].text = lineCurrentScore.ToString() + " pts";
                }

                if (lineCurrentScore >= scores[currentLineBeingDisplayed])
                {
                    FinishPreviousLine();
                }
            }
        }


        if (animateTapToContinue)
        {
            // Handles the blinking "tap to continue"
            Color tapToContinueColor = tapToContinueTextGO.GetComponent<Renderer>().material.color;
            if (tapToContinueFadingIn)
                tapToContinueColor = new Color(tapToContinueColor.r, tapToContinueColor.g, tapToContinueColor.b, Mathf.Lerp(0.25f, 1f, (Time.time - tapToContinueStartTime) / tapToContinueStepDuration));
            else
                tapToContinueColor = new Color(tapToContinueColor.r, tapToContinueColor.g, tapToContinueColor.b, Mathf.Lerp(1f, 0.25f, (Time.time - tapToContinueStartTime) / tapToContinueStepDuration));
            tapToContinueTextGO.GetComponent<Renderer>().material.color = tapToContinueColor;

            if (Time.time >= tapToContinueStartTime + tapToContinueStepDuration)
            {
                tapToContinueFadingIn = !tapToContinueFadingIn;
                tapToContinueStartTime = Time.time;
            }
        }


        if (animateRank)
        {
            if (Time.time >= currentRankDisplayTime + delayBetweenRankIncrementation)
            {
                currentRankIndex++;
                currentRankDisplayTime = Time.time;
                gameLogic.guiCam.scoreMenuRankText.text = rank.ToString().Substring(0, currentRankIndex);

                rankGrowAS.Play();
            }

            if (Math.Pow(10, currentRankIndex) > rank)
            {
                StopRankAnim();
            }
        }
    }


    protected void FinishPreviousLine()
    {
        if (currentLineBeingDisplayed >= 0)
        {

            gameLogic.guiCam.scoreMenuScoreTexts[currentLineBeingDisplayed].text = scores[currentLineBeingDisplayed].ToString() + " pts";

            if (scores[currentLineBeingDisplayed] > 0)
            {

                scoreGrowAS.Stop();

                scoreStepAS.Play();

                waitingForDelayBetweenLines = true;
                delayBetweenLinesStartTime = Time.time;
            }
            else
            {
                InitNewLine();
            }
        }
    }


    protected void InitNewLine()
    {
        scoreGrowAS.Play();

        currentLineBeingDisplayed++;

        if (currentLineBeingDisplayed == scores.Length)
        {
            StopScoresAnim();
            return;
        }

        if (currentLineBeingDisplayed < 3)
            gameLogic.guiCam.scoreMenuValuesTexts[currentLineBeingDisplayed].text = valuesString[currentLineBeingDisplayed];
        else if (currentLineBeingDisplayed == 4)
        {
            if (newScoreRecord)
            {
                gameLogic.guiCam.scoreMenuSubScoreRecordText.text = "Record ! ";
            }
        }

        lineCurrentScore = 0;

        gameLogic.guiCam.scoreMenuScoreTexts[currentLineBeingDisplayed].text = "0 pts";
    }


    public void StartScoresAnim()
    {
        currentLineBeingDisplayed = -1;
        currentScoreDisplayTime = 0;
        lineCurrentScore = 0;

        animateScore = true;

        animateTapToContinue = true;

        InitNewLine();
    }


    public void StopScoresAnim()
    {
        animateScore = false;

        scoreGrowAS.Stop();

        StartRankAnim();
    }


    public void StartRankAnim()
    {
        currentRankDisplayTime = 0;
        currentRankIndex = 0;

        if (rank > 0)
        {
            gameLogic.guiCam.scoreMenuRankText.text = rank.ToString();

            if (rank < previousRank || previousRank == -1)
            {
                gameLogic.guiCam.scoreMenuRankEvolutionGO.transform.Rotate(0, -90, 0);
                gameLogic.guiCam.scoreMenuRankEvolutionGO.GetComponent<Renderer>().material.color = Color.green;
            }
            else if (rank > previousRank)
            {
                gameLogic.guiCam.scoreMenuRankEvolutionGO.transform.Rotate(0, 90, 0);
                gameLogic.guiCam.scoreMenuRankEvolutionGO.GetComponent<Renderer>().material.color = Color.red;
            }
            else
            {
                gameLogic.guiCam.scoreMenuRankEvolutionGO.GetComponent<Renderer>().material.color = Color.yellow;
            }

            animateRank = true;
        }
        else
        {
            gameLogic.guiCam.scoreMenuRankText.text = "-";
            gameLogic.guiCam.scoreMenuRankEvolutionGO.GetComponent<Renderer>().enabled = false;
        }
    }


    public void StopRankAnim()
    {
        animateRank = false;
        gameLogic.guiCam.scoreMenuRankText.text = rank.ToString();
        gameLogic.guiCam.scoreMenuRankEvolutionGO.GetComponent<Renderer>().enabled = true;

        if (rank > previousRank)
        {
            rankBadAS.Play();
        }
        else
        {
            scoreStepAS.Play();
        }
    }


    #region Input Management

    public override void ButtonAction()
    {
        base.ButtonAction();


        if (animateScore)
        {
            //			InitNewLine();
            if (!waitingForDelayBetweenLines)
                FinishPreviousLine();

        }
        else if (animateRank)
        {
            StopRankAnim();
        }
        else
        {
            // If we have to display the rate this app popup => don't load the next level now
            if (!gameLogic.CheckForRateThisAppPopup())
            {
                gameLogic.LoadNextGrid();
            }
        }
    }

    #endregion
}
