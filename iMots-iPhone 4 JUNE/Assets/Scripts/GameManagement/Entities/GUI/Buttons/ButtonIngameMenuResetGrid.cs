using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuResetGrid : GUIButtonEntity
{

    protected GameLogic gameLogic;

    public string resetGridConfirmationString;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        //DEBUG
        if (Application.platform == RuntimePlatform.OSXEditor)
        {
            gameLogic.ResetGrid(gameLogic.grid.name);
        }

        gameLogic.openedAlertView = Global.AlertViews.ResetGrid;

#if UNITY_IOS
        AVBinding.CreateAlertView("CONFIRMATION", resetGridConfirmationString);
#elif UNITY_ANDROID
        //TODO || DEV6
		//EtceteraAndroid.showAlert("CONFIRMATION", resetGridConfirmationString, "Continuer", "Annuler");
#endif
    }

    #endregion
}
