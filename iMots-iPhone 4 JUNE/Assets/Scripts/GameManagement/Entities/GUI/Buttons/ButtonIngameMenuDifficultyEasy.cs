using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuDifficultyEasy : GUILinkedButtonEntity
{

    protected GameLogic gameLogic;

    protected override void Start()
    {
        if (PlayerPrefs.HasKey("GameDifficulty"))
        {
            currentlyFocusedLinkedButton = PlayerPrefs.GetInt("GameDifficulty");
        }

        base.Start();

        gameLogic = logic as GameLogic;
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        gameLogic.ChangeGameDifficulty(Global.Difficulty.Easy);
    }

    #endregion
}
