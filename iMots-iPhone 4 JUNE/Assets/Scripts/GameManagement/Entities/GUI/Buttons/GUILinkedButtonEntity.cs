using UnityEngine;
using System.Collections;
using System;


public class GUILinkedButtonEntity : GUIButtonEntity
{

    public GameObject[] linkedButtonsGO;
    protected GUILinkedButtonEntity[] linkedButtons;

    protected int index;
    public int currentlyFocusedLinkedButton = 0;

    protected Material normalMaterialBackup;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();

        linkedButtons = new GUILinkedButtonEntity[linkedButtonsGO.Length];

        for (int i = 0; i < linkedButtonsGO.Length; i++)
        {
            linkedButtons[i] = linkedButtonsGO[i].GetComponent(typeof(GUILinkedButtonEntity)) as GUILinkedButtonEntity;
            linkedButtons[i].index = i;
        }

        normalMaterialBackup = normalMaterial;

        if (index == currentlyFocusedLinkedButton)
            SetNormalMaterial(pressedMaterial);
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        linkedButtons[currentlyFocusedLinkedButton].SetNormalMaterial(linkedButtons[currentlyFocusedLinkedButton].normalMaterialBackup);

        for (int i = 0; i < linkedButtons.Length; i++)
        {
            linkedButtons[i].currentlyFocusedLinkedButton = index;
        }

        SetNormalMaterial(pressedMaterial);
    }

    #endregion
}
