using UnityEngine;
using System.Collections;
using System;


public class ButtonStoreItemRealVersion : ButtonStoreItemConsumable
{

    protected override void Start()
    {
        base.Start();

        if (!Global.Global.adEnabled)
        {
            SetUnavailable(true);
        }


        isAlreadyBought = true;
    }

    public override void SetAlreadyBought(bool bought)
    {

    }


    public override void UnlockMyGrids()
    {

    }


    public override void ButtonAction()
    {
        if (!transferredControlToPage)
        {

            base.ButtonAction();

            // Create AV to warn player that he will leave the app
            storeLogic.openedAlertView = Global.AlertViews.OpenRealVersionPage;
#if UNITY_IOS
            AVBinding.CreateAlertView("Basculer vers navigateur internet", "Ceci va ouvrir votre navigateur internet, voulez-vous continuer ?");
            Application.OpenURL("https://itunes.apple.com/us/app/free-fleches/id484552337?ls=1&mt=8");
#elif UNITY_ANDROID
            //TODO || DEV7
			//EtceteraAndroid.showAlert("Basculer vers navigateur internet", "Ceci va ouvrir votre navigateur internet, voulez-vous continuer ?", "Continuer", "Annuler");
#endif
        }
    }
}
