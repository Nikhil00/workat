using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuSearchLetter : GUIButtonEntity
{

    protected GameLogic gameLogic;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;

        if (gameLogic.gameMode == Global.GameMode.Keyboard)
        {
            SetNormalMaterial(pressedMaterial);
        }
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        // This feature cannot be used while in keyboard mode
        if (gameLogic.gameMode == Global.GameMode.Keyboard)
            return;

        if (gameLogic.guiCam.vKeyboardState == Global.KeyboardState.Off)
        {
            if (!gameLogic.guiCam.vKeyboardGO.active)
                gameLogic.guiCam.vKeyboardGO.SetActiveRecursively(true);

            gameLogic.guiCam.SetKeyboardState(Global.KeyboardState.Focused, false);
        }
        else
            gameLogic.guiCam.SetKeyboardState(Global.KeyboardState.Off, false);

        gameLogic.guiCam.StartMenuPanelAnimation();
    }

    public void SearchLetterButtonAction()
    {
        // This feature cannot be used while in keyboard mode
        if (gameLogic.gameMode == Global.GameMode.Keyboard)
            return;

        if (gameLogic.guiCam.vKeyboardState == Global.KeyboardState.Off)
        {
            if (!gameLogic.guiCam.vKeyboardGO.active)
                gameLogic.guiCam.vKeyboardGO.SetActiveRecursively(true);

            gameLogic.guiCam.SetKeyboardState(Global.KeyboardState.Focused, false);
        }
        else
            gameLogic.guiCam.SetKeyboardState(Global.KeyboardState.Off, false);

        gameLogic.guiCam.StartMenuPanelAnimation();
    }

    #endregion
}
