using UnityEngine;
using System.Collections;
using System;


public class ButtonStoreItemConsumable : ButtonStoreItem
{

    public int amountToAdd;
    public string targetPlayerPref;

    public override void SetAlreadyBought(bool bought)
    {

    }


    public override void UnlockMyGrids()
    {
        int nbStock = PlayerPrefs.GetInt(targetPlayerPref);
        PlayerPrefs.SetInt(targetPlayerPref, nbStock + amountToAdd);
    }

    public void UnlockMyGrids_Dev_Consumable()
    {
        int nbStock = PlayerPrefs.GetInt(targetPlayerPref);
        PlayerPrefs.SetInt(targetPlayerPref, nbStock + amountToAdd);
        Debug.Log("Amount is " + PlayerPrefs.GetInt(targetPlayerPref).ToString());
    }

    public void Unlock()
    {
        int nbStock = PlayerPrefs.GetInt(targetPlayerPref);
        PlayerPrefs.SetInt(targetPlayerPref, nbStock + amountToAdd);
        UnlockMyGrids();
    }
}
