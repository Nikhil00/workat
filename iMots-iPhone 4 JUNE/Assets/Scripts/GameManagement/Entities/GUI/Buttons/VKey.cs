using UnityEngine;
using System.Collections;
using System;


public class VKey : GUIButtonEntity
{

    protected GameLogic gameLogic;

    protected string vKeyValue;

    public TextMesh textMesh;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;

        textMesh = transform.parent.Find("Text").GetComponent(typeof(TextMesh)) as TextMesh;

        vKeyValue = transform.parent.name.Substring(4);

        if (vKeyValue != "Back" && vKeyValue != "Hide")
        {
            textMesh.text = vKeyValue;
        }

        GetComponent<Renderer>().material.color = new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, 0);
    }


    protected override void Update()
    {
        base.Update();
    }


    // Take care of unfocusing the currently focused definition if there is one
    protected override void UnfocusDefinition()
    {
        if (gameLogic.focusedDefinition != null && vKeyValue == "Hide")
        {
            gameLogic.focusedDefinition.Grow();
            (logic as GameLogic).guiCam.StartTabsAnimation();
        }
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        gameLogic.vKeyPressedAS.Play();

        if (vKeyValue == "Back")
        {
            //			gameLogic.setGuessForCurrentlySelectedTile("");
            gameLogic.DeleteGuessForSelectedTile();
        }
        else if (vKeyValue == "Hide")
        {
            // In Keyboard mode, the keyboard goes back to the unfocused mode
            if (gameLogic.gameMode == Global.GameMode.Keyboard)
            {
                gameLogic.SelectTile(null);
            }
            // In DnD mode, the keyboard goes back to the off mode (it was ON only for the search letter feature)
            else
            {
                gameLogic.guiCam.SetKeyboardState(Global.KeyboardState.Off, false);
            }
        }
        else
        {
            if (gameLogic.gameMode == Global.GameMode.Keyboard)
            {
                gameLogic.setGuessForCurrentlySelectedTile(vKeyValue);
            }
            else
            {
                gameLogic.guiCam.SetKeyboardState(Global.KeyboardState.Off, false);
                gameLogic.SearchForLetter(vKeyValue);
            }
        }
    }

    #endregion
}
