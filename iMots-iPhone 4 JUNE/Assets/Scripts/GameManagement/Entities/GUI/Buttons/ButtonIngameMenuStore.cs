using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuStore : GUIButtonEntity
{

    protected GameLogic gameLogic;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;

        //#if UNITY_ANDROID
        //		SetNormalMaterial(pressedMaterial);
        //#endif
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        //#if UNITY_IPHONE

        gameLogic.SetPendingLoadLevel(2);

        //#endif


        //		AVBinding.CreateInfoAlertView("INFORMATION", "Fonctionnalite pas encore implementee !");
    }

    public void StoreButtonAction()
    {
        gameLogic.SetPendingLoadLevel(2);
    }

    #endregion
}
