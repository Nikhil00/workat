using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuSoundYes : GUILinkedButtonEntity
{

    protected GameLogic gameLogic;

    protected override void Start()
    {
        if (PlayerPrefs.HasKey("MuteSound"))
        {
            currentlyFocusedLinkedButton = PlayerPrefs.GetInt("MuteSound");
        }

        base.Start();

        gameLogic = logic as GameLogic;
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        gameLogic.SetSoundMuted(false);
    }

    #endregion
}
