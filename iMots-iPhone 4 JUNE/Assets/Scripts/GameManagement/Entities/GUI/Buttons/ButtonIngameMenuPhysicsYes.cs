using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameMenuPhysicsYes : GUILinkedButtonEntity
{

    protected GameLogic gameLogic;

    protected override void Start()
    {
        if (PlayerPrefs.HasKey("PhysicsDisabled"))
        {
            currentlyFocusedLinkedButton = PlayerPrefs.GetInt("PhysicsDisabled");
        }

        base.Start();

        gameLogic = logic as GameLogic;
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        gameLogic.EnablePhysics(true);
    }

    #endregion
}
