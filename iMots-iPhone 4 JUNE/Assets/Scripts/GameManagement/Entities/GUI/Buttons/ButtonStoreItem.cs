using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;
#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif

public class ButtonStoreItem : GUIButtonEntity
{

    public StorePageEntity myPage;
    public static ButtonStoreItem instance;
    protected StoreLogic storeLogic;


    protected bool transferredControlToPage = false;
    public float transferControlToPageActivationDistance;

    // In App
    public string[] itemID = new string[2];
    public string product;
    public string firstTargetGrid;
    public int nbTargetGrids;
    protected bool isAlreadyBought = false;

    protected Material normalMaterialBackup;
    protected Material normalMaterialNotAvailable;
    protected Material stateIconMaterialNotAvailable;

    public TextMesh idText;
    protected GameObject stateIcon;
    string[] androidSkus = new string[1];


    protected override void Awake()
    {
        base.Awake();
        instance = this;
        androidSkus[0] = "android.test.purchased";

        //DEV_EDIT || IN-APP PURCHASE || CHANGE INAPP BUTTON TEXT
        if (transform.Find("GridId").GetComponent("TextMesh")) idText = transform.Find("GridId").GetComponent("TextMesh") as TextMesh;
        stateIcon = transform.Find("StateIcon").gameObject;
        storeLogic = logic as StoreLogic;
#if UNITY_IOS
        //				if (Global.Global.adEnabled) {
        string bundleID;
        if (Global.Global.iPadVersion)
            bundleID = "com.digdog.imotsflechesdeluxe.";
        else
            bundleID = "com.digdog.MF.";

        for (int i = 0; i < itemID.Length; i++)
        {
            if (String.IsNullOrEmpty(itemID[i]))
                continue;

            string[] splittedId = itemID[i].Split('.');
            if (String.IsNullOrEmpty(itemID[i]))
            {

            }
            else
            {
                itemID[i] = bundleID + splittedId[3];
                product = itemID[i];
                //								Debug.Log ("item id of i " + product);

            }

        }
        if (String.IsNullOrEmpty(product))
        {
            //						Debug.Log ("return");
            return;
        }
        else
        {

        }




        StartCoroutine(IABProductsQueryInventory());

        //				}
#elif UNITY_ANDROID
		// On ANDROID, we only keep the last part of the ID (remove the bundleID part)
		for (int i = 0; i < itemID.Length; i++)
			{
				if (itemID[i] == "")
					continue;
				
				string[] splittedId = itemID[i].Split('.');
				
				itemID[i] = splittedId[3];
			}
#endif
    }


    protected override void Start()
    {
        base.Start();

        needToUpdateMaterial = false;

        normalMaterialBackup = normalMaterial;

        if (!Dev_StoreUIManager.instance.isUiElement) normalMaterialNotAvailable = new Material(normalMaterial);
        if (!Dev_StoreUIManager.instance.isUiElement) normalMaterialNotAvailable.color = new Color(normalMaterial.color.r, normalMaterial.color.g, normalMaterial.color.b, 0.25f);

        if (!Dev_StoreUIManager.instance.isUiElement) stateIconMaterialNotAvailable = new Material(stateIcon.GetComponent<Renderer>().material);

        if (!Dev_StoreUIManager.instance.isUiElement) stateIconMaterialNotAvailable.color = new Color(stateIconMaterialNotAvailable.color.r, stateIconMaterialNotAvailable.color.g, stateIconMaterialNotAvailable.color.b, 0.25f);

        // If there is no text inside the button => the item is not available, so set the button as such
        if (idText.text == "")
        {
            SetUnavailable(true);
        }
        // If the item corresponding to this button has already been bought => set the button to the correct state
        else
        {
            string platform;
            if (Global.Global.iPadVersion)
                platform = "Ipad-";
            else
                platform = "Iphone-";

            if (PlayerPrefs.HasKey(platform + firstTargetGrid))
                SetAlreadyBought(true);
        }




    }

    IEnumerator IABProductsQueryInventory()
    {

        int version;
        if (Global.Global.iPadVersion)
            version = 0;
        else
            version = 1;

        yield return new WaitForSeconds(0f);


        if (String.IsNullOrEmpty(product))
        {
            //						Debug.Log ("product item is empty");
        }
        else
        {
            //						Debug.Log ("requesting in app item - " + product);
            #region DEV_CODE

            //DEV_EDIT || 5 OCT CHANGE HERE
            // storeLogic.itemButtons.Add(this);
            // storeLogic.CheckForRequest();

            #endregion
            //						IAP.requestProductData (new string[] { product }, androidSkus, productList => {
            ////								Debug.Log ("Product list received " + productList);
            //								print ("list item - " + productList);
            //						});
        }
    }


    protected override void Update()
    {
        base.Update();
    }


    public virtual void SetAlreadyBought(bool bought)
    {
        isAlreadyBought = bought;

        #region  ORIGINAL_CODE

        // if (isAlreadyBought)
        // {
        //     SetNormalMaterial(pressedMaterial);
        // }
        // else
        // {
        //     SetNormalMaterial(normalMaterialBackup);
        // }

        #endregion

        #region DEV_CODE

        //DEV_EDIT || 4 OCT STORE BUTTON ICON || CHANGE BUTTON MATERIAL FROM HERE

        if (!Dev_StoreUIManager.instance.isUiElement)
        {
            if (isAlreadyBought)
            {
                SetNormalMaterial(pressedMaterial);
            }
            else
            {
                SetNormalMaterial(normalMaterialBackup);
            }
        }
        else
        {
            if (isAlreadyBought)
            {
                GetComponent<Image>().color = Dev_StoreUIManager.instance.purchasedColor;
            }
            else
            {
                GetComponent<Image>().color = Dev_StoreUIManager.instance.normalColor;
            }
        }

        #endregion
    }


    public void SetUnavailable(bool unavailable)
    {
        isAlreadyBought = unavailable;

        if (isAlreadyBought)
        {
            SetNormalMaterial(normalMaterialNotAvailable);
            SetPressedMaterial(normalMaterialNotAvailable);

            stateIcon.GetComponent<Renderer>().material = stateIconMaterialNotAvailable;

            idText.text = "";
        }
        else
        {
            SetNormalMaterial(normalMaterialBackup);
        }
    }


    public virtual void UnlockMyGrids()
    {
        //DEV_EDIT || UNLOCK PAGES FROM HERE AFTER PURCHASE

        string platform;
        if (Global.Global.iPadVersion)
            platform = "Ipad-";
        else
            platform = "Iphone-";

        string targetsCommonBaseAsString = firstTargetGrid.Substring(0, 4);
        int firstTargetGridIndex = int.Parse(firstTargetGrid.Substring(4, 3));

        string gridIndexString, fullIDString;

        for (int i = 0; i < nbTargetGrids; i++)
        {
            int currentID = firstTargetGridIndex + i;

            if (currentID < 10)
                gridIndexString = "00" + currentID;
            else if (currentID < 100)
                gridIndexString = "0" + currentID;
            else
                gridIndexString = currentID.ToString();

            fullIDString = platform + targetsCommonBaseAsString + gridIndexString;

            if (!PlayerPrefs.HasKey(fullIDString)) // Shouldn't be, but better check it anyways
                PlayerPrefs.SetInt(fullIDString, (int)Global.GridState.Empty);

            Debug.Log(fullIDString + " UNLOCKED");
        }

        SetAlreadyBought(true);

        // Achievement => buy a pack of grids
        if (Global.Global.iPadVersion)
        {
            string appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";

            logic.GCReportAchievementProgress("com.digdog." + appString + ".EncoreEncore", 100);
        }
        else
        {
            string appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";

            logic.GCReportAchievementProgress("com.digdog." + appString + ".encore", 100);
        }

    }

    public void UnlockMyGrid_Dev_StoreItem()
    {
        string platform;
        if (Global.Global.iPadVersion)
            platform = "Ipad-";
        else
            platform = "Iphone-";

        string targetsCommonBaseAsString = firstTargetGrid.Substring(0, 4);
        int firstTargetGridIndex = int.Parse(firstTargetGrid.Substring(4, 3));

        string gridIndexString, fullIDString;

        for (int i = 0; i < nbTargetGrids; i++)
        {
            int currentID = firstTargetGridIndex + i;

            if (currentID < 10)
                gridIndexString = "00" + currentID;
            else if (currentID < 100)
                gridIndexString = "0" + currentID;
            else
                gridIndexString = currentID.ToString();

            fullIDString = platform + targetsCommonBaseAsString + gridIndexString;

            if (!PlayerPrefs.HasKey(fullIDString)) // Shouldn't be, but better check it anyways
                PlayerPrefs.SetInt(fullIDString, (int)Global.GridState.Empty);

            Debug.Log(fullIDString + " UNLOCKED");
        }

        SetAlreadyBought(true);

        // Achievement => buy a pack of grids
        if (Global.Global.iPadVersion)
        {
            string appString = Global.Global.adEnabled ? "freeflechesipad" : "imotsflechesdeluxe";

            logic.GCReportAchievementProgress("com.digdog." + appString + ".EncoreEncore", 100);
        }
        else
        {
            string appString = Global.Global.adEnabled ? "freefleches" : "imotsFleches";

            logic.GCReportAchievementProgress("com.digdog." + appString + ".encore", 100);
        }
    }


    #region Input Management

    protected override void ActionsOnTouchUpdate(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdate(touchPosition);

        if (transferredControlToPage)
        {
            myPage.TouchUpdate(touchPosition);
        }
        else if (Vector3.Distance(initialTouchPosition, touchPosition) > transferControlToPageActivationDistance)
        {
            Debug.Log("Transferred control to page");
            transferredControlToPage = true;
            myPage.TouchBegin(touchPosition);

            GetComponent<Renderer>().material = normalMaterial;

            needToUpdateMaterial = false;
        }

    }


    protected override void ActionsOnTouchUpdateOnlyOnce(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdateOnlyOnce(touchPosition);
    }


    protected override void ActionsOnTouchEnd(Vector2 touchPosition)
    {
        base.ActionsOnTouchEnd(touchPosition);

        if (transferredControlToPage)
        {
            myPage.TouchEnd(touchPosition);

            transferredControlToPage = false;

            needToUpdateMaterial = true;
        }
    }

    public override void ButtonAction()
    {
        if (!transferredControlToPage)
        {
            if (isAlreadyBought)
            {
                return;
            }

            base.ButtonAction();


            // DEBUG
            if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
            {
                UnlockMyGrids();
            }
#if UNITY_IOS
            // Request the corresponding product on the AppStore
            //if (StoreKitBinding.canMakePayments())
            //{ // Check if the user is allowed to buy (no parent control system blocking it)
            //    int version;
            //    if (Global.Global.iPadVersion)
            //        version = 0;
            //    else
            //        version = 1;



            //    //								StoreKitBinding.requestProductData (new string[] { itemID [version] });

            //    //								StoreKitBinding.purchaseProduct(product, 1);

            //    IAP.purchaseNonconsumableProduct(product, (didSucceed, error) =>
            //    {
            //        Debug.Log("purchasing product " + product + " result: " + didSucceed);
            //        if (didSucceed)
            //        {
            //            UnlockMyGrids();
            //        }
            //    });

            //    storeLogic.ShowInAppPopup();
            //    storeLogic.buttonCurrentlyBeingProcessed = this;
            //}
            //else
            //{
            //    AVBinding.CreateInfoAlertView("INFORMATION", storeLogic.cantMakePaymentsString);
            //}
#elif UNITY_ANDROID
			if (storeLogic.isBillingSupported)
			{
				int version;
				if (Global.Global.iPadVersion)
					version = 0;
				else
					version = 1;
				
				storeLogic.ShowInAppPopup();

                //				IABAndroid.testPurchaseProduct(); // Change this to the real purchase request
                //TODO || DEV11
                //IABAndroid.purchaseProduct(itemID[version]);
                storeLogic.buttonCurrentlyBeingProcessed = this;
			}
			else
			{
				if (storeLogic.waitingForBillingSupportedEvent)
				{
                    //TODO || DEV12
                    //EtceteraAndroid.showAlert("INFORMATION", storeLogic.waitForBillingSupportedEvent, "OK");
                }
                else
				{
                    //TODO || DEV13
                    //EtceteraAndroid.showAlert("INFORMATION", storeLogic.cantMakePaymentsString, "OK");
				}
			}
#endif
        }
    }

    #endregion

    public void RestoreTransaction()
    {
        Debug.Log(" product: " + product);
        storeLogic.ShowInAppPopup();

        //DEV_EDIT || 4 OCT || COMMENTED CODE
        //DEV_EDIT || IN-APP PURCHASE || CODE 3
        //IAP.restoreCompletedTransactions(productId =>
        //{

        //    Debug.Log("restored purchased product: " + productId);
        //    Debug.Log(" product: " + product);
        //    if (productId == product)
        //    {
        //        UnlockMyGrids();
        //    }
        //    storeLogic.ShowInAppPopup();
        //});
    }


}
