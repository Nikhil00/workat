using UnityEngine;
using System.Collections;
using System;


public class ButtonOverlayIcon : GUIButtonEntity
{

    public bool isVerticalOverlay = true;
    public int index;

    protected override void Start()
    {
        base.Start();
    }


    protected override void Update()
    {
        base.Update();

    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        if (logic.GetLogicType() == Global.LogicType.MenuLogic)
        {
            MenuLogic menuLogic = (logic as MenuLogic);
            Vector2 targetPageIndex;

            if (isVerticalOverlay)
                targetPageIndex = new Vector2(menuLogic.currentPage.x, index);
            else
                targetPageIndex = new Vector2(index, menuLogic.currentPage.y);

            menuLogic.GoToPage(targetPageIndex);
        }
        else if (logic.GetLogicType() == Global.LogicType.StoreLogic)
        {
            StoreLogic storeLogic = (logic as StoreLogic);
            Vector2 targetPageIndex;

            if (isVerticalOverlay)
                targetPageIndex = new Vector2(storeLogic.currentPage.x, index);
            else
                targetPageIndex = new Vector2(index, storeLogic.currentPage.y);

            storeLogic.GoToPage(targetPageIndex);

        }

        if (clickAS != null)
            clickAS.Play();
    }

    #endregion
}
