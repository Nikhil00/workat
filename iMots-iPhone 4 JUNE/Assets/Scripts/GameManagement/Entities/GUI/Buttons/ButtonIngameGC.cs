using UnityEngine;
using System.Collections;
using System;


public class ButtonIngameGC : GUIButtonEntity
{

    protected GameLogic gameLogic;

    protected override void Start()
    {
        base.Start();

        gameLogic = logic as GameLogic;

        if (GetComponent<Renderer>()) GetComponent<Renderer>().material.color = new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, 0);
    }


    protected override void Update()
    {
        base.Update();
    }


    #region Input Management



    public override void ButtonAction()
    {
        base.ButtonAction();

        if (Social.localUser.authenticated)
        {
            gameLogic.GCShowLeaderboard();
        }
        else
        {
            Social.localUser.Authenticate(gameLogic.GCAuthenticationHandler);
        }

        //		if (GameCenterBinding.isGameCenterAvailable())
        //		{
        //			if (!GameCenterBinding.isPlayerAuthenticated())
        //			{
        //				if (!gameLogic.waitingForPlayerAuthentication) // If the player is not authenticated AND is not currently being authenticated, open the GC authentication popup
        //				{
        //					gameLogic.GCAuthenticatePlayer();
        //				}
        //				else
        //				{
        //					Debug.Log("WAIT, player currently being authenticated...");
        //				}
        //			}
        //			else if (gameLogic.leaderboards != null)
        //			{
        //				GameCenterBinding.showLeaderboardWithTimeScopeAndLeaderboard(GameCenterLeaderboardTimeScope.AllTime, gameLogic.leaderboards[0].leaderboardId);
        //			}
        //		}
        //		else
        //		{
        //			Debug.Log("GC not available.");
        //		}
    }

    public void GCButtonAction()
    {
        if (Social.localUser.authenticated)
        {
            gameLogic.GCShowLeaderboard();
        }
        else
        {
            Social.localUser.Authenticate(gameLogic.GCAuthenticationHandler);
        }
    }

    #endregion
}
