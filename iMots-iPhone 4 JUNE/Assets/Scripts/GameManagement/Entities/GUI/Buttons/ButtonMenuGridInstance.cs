using UnityEngine;
using System.Collections;
using System;


public class ButtonMenuGridInstance : GUIButtonEntity
{

    public MenuPageEntity myPage;

    public string associatedGrid;

    bool transferredControlToPage = false;
    public float transferControlToPageActivationDistance;

    public TextMesh idText;
    public GameObject stateIconGO, ptsTextGO;

    // Rotate animation for started grids icons
    public float iconRotationSpeed = 100;
    public bool rotateIcon = false;

    // Shine animation for copleted grids icons
    const float iconShineFrameDuration = 0.01f;
    const float iconShineFirstFrameDuration = 2.5f;
    float iconShineFrameStartTime;
    bool shineIcon = false;
    int iconShineCurrentFrame = 0;
    bool pendingShineIcon = false;
    float pendingIconShineFrameStartTime, pendingIconShineFrameDuration = 0;


    protected override void Awake()
    {
        base.Awake();

        idText = transform.Find("GridId").GetComponent("TextMesh") as TextMesh;
        ptsTextGO = transform.Find("Pts").gameObject;
        stateIconGO = transform.Find("StateIcon").gameObject;
    }


    protected override void Start()
    {
        base.Start();
        needToUpdateMaterial = false;
    }


    public void Init()
    {
    }


    public void SetPendingStartShining(float timeUntilStartShining, float timeReference)
    {
        pendingShineIcon = true;
        pendingIconShineFrameStartTime = timeReference;
        pendingIconShineFrameDuration = timeUntilStartShining;
    }


    public void StartShining()
    {
        shineIcon = true;
        iconShineFrameStartTime = Time.time;
        iconShineCurrentFrame = 0;
    }

    public void StopShining()
    {
        shineIcon = false;
    }


    protected override void Update()
    {
        base.Update();

        // DEBUG
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (shineIcon)
                StopShining();
            else
                StartShining();
        }

        if (rotateIcon)
        {
            stateIconGO.transform.Rotate(0, -Time.deltaTime * iconRotationSpeed, 0);
        }

        if (shineIcon)
        {
            float duration;
            if (iconShineCurrentFrame == 0)
            {
                duration = iconShineFirstFrameDuration;
            }
            else
            {
                duration = iconShineFrameDuration;
            }

            if (Time.time >= iconShineFrameStartTime + duration)
            {
                iconShineCurrentFrame = (iconShineCurrentFrame + 1) % 32;

                float xOffset = (iconShineCurrentFrame % 8) * 0.125f;
                float yOffset = (3 - (Mathf.FloorToInt(iconShineCurrentFrame / 8f))) * 0.25f;

                //				Debug.Log("Gold texture offset: " + iconShineCurrentFrame + " => " + xOffset + "," + yOffset);

                //DEV_EDIT || STATE ICON || REMOVING STATE ICON OFFSET CODE
                //stateIconGO.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(xOffset, yOffset);

                iconShineFrameStartTime = Time.time;
            }
        }
        else if (pendingShineIcon)
        {
            if (Time.time >= pendingIconShineFrameStartTime + pendingIconShineFrameDuration)
            {
                pendingShineIcon = false;
                StartShining();
            }
        }
    }


    // Start a game with the grid associated to this button
    public void LaunchMyGrid()
    {
        (logic as MenuLogic).LaunchGridAS.Play();

        PlayerPrefs.SetString("CurrentGrid", associatedGrid);

        #region ORIGINAL_CODE
        //(logic as MenuLogic).SetDelayedPendingLoadLevel(0);
        #endregion

        #region DEV_CODE

        FindObjectOfType<Dev_UIManager>().OpenLoadingPanel();
        StartCoroutine(LoadScene());

        #endregion
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(0.5f);

        (logic as MenuLogic).SetDelayedPendingLoadLevel(0);
    }


    #region Input Management

    protected override void ActionsOnTouchUpdate(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdate(touchPosition);

        if (transferredControlToPage)
        {
            myPage.TouchUpdate(touchPosition);
        }
        else if (Vector3.Distance(initialTouchPosition, touchPosition) > transferControlToPageActivationDistance)
        {
            Debug.Log("Transferred control to page");
            transferredControlToPage = true;
            myPage.TouchBegin(touchPosition);

            GetComponent<Renderer>().material = normalMaterial;

            needToUpdateMaterial = false;
        }

    }


    protected override void ActionsOnTouchUpdateOnlyOnce(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdateOnlyOnce(touchPosition);
    }


    protected override void ActionsOnTouchEnd(Vector2 touchPosition)
    {
        base.ActionsOnTouchEnd(touchPosition);

        if (transferredControlToPage)
        {
            myPage.TouchEnd(touchPosition);

            transferredControlToPage = false;

            needToUpdateMaterial = true;
        }
    }


    public override void ButtonAction()
    {
        if (!transferredControlToPage)
        {
            base.ButtonAction();

            if (PlayerPrefs.GetInt(associatedGrid) == (int)Global.GridState.Completed && Application.platform != RuntimePlatform.OSXEditor)
            {
                (logic as MenuLogic).LaunchCompletedGridAssociatedWithButton(this);
            }
            else
            {
                LaunchMyGrid();
            }

            //			(logic as MenuLogic).LaunchGridAS.Play();
            //			
            //			PlayerPrefs.SetString("CurrentGrid", associatedGrid);
            //			
            //			(logic as MenuLogic).SetDelayedPendingLoadLevel(0);
        }
    }

    #endregion
}
