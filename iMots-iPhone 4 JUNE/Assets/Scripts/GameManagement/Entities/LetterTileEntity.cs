using UnityEngine;
using System.Collections;


public class LetterTileEntity : LetterEntity
{

    public Material selectedMat, selectedWordMat;

    protected bool isPendingValidation;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
    }


    protected override void Start()
    {
        base.Start();

        isPendingValidation = false;
    }


    public override void InitGridElement()
    {
        base.InitGridElement();

        name = "LetterTile " + textMesh.text;

        //		blinkingMat = selectedMat;
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    // Take care of unfocusing the currently focused definition if there is one
    protected override void UnfocusDefinition()
    {
        if (gameLogic.focusedDefinition != null && (!gameLogic.IsTileWithinCurrentSelection(this) || isCorrect))
        {
            gameLogic.focusedDefinition.Grow();
            (logic as GameLogic).guiCam.StartTabsAnimation();
        }
    }


    public override void AnimationStopped(string animation)
    {
    }


    // Set the entity state as correct
    public override void SetCorrect(bool correct, bool delayed)
    {
        base.SetCorrect(correct, delayed);

        if (delayed)
        {
            isPendingValidation = true;
        }
        else
        {
            isPendingValidation = false;
        }
    }


    // Set the correct material for this entity according to its state (moved? correct?)
    public new void SetProperMaterial()
    {
        if (isCorrect && !isPendingValidation)
        {
            GetComponent<Renderer>().material = correctMat;
        }
        else
        {
            if (gameLogic.gameMode == Global.GameMode.DragNDrop && gameLogic.gameDifficulty == Global.Difficulty.Hard && alreadyMoved)
                GetComponent<Renderer>().material = movedMat;
            else
            {
                if (gameLogic.gameMode == Global.GameMode.Keyboard && gameLogic.IsTileWithinCurrentSelection(this))
                {
                    if (gameLogic.IsTileFocused(this))
                    {
                        GetComponent<Renderer>().material = selectedMat;
                    }
                    else
                    {
                        GetComponent<Renderer>().material = selectedWordMat;
                    }
                }
                else
                {
                    GetComponent<Renderer>().material = normalMat;
                }
            }
        }
    }


    #region Input Management

    protected override void ActionsOnTouchUpdate(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdate(touchPosition);

        Pan(touchPosition);
    }


    protected override void ActionsOnTouchEnd(Vector2 touchPosition)
    {
        base.ActionsOnTouchEnd(touchPosition);


        if (!isCorrect)
            gameLogic.selectTileAS.Play();
        else
            gameLogic.pickCorrectLetterAS.Play();

        gameLogic.SelectTile(this);
    }

    #endregion
}
