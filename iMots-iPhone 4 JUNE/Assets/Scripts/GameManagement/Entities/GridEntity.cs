using UnityEngine;
using System.Collections;


public class GridEntity : PanningEntity
{

    protected GameLogic gameLogic;

    protected PanningEntity elemToUpdate;

    protected TableEntity tableEntity;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();


        gameLogic = logic as GameLogic;

        tableEntity = FindObjectOfType(typeof(TableEntity)) as TableEntity;
    }


    protected override void Start()
    {
        base.Start();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    #region Input Management

    public override void TouchBegin(Vector2 touchPosition)
    {
        //		base.TouchBegin(touchPosition);

        elemToUpdate = null;

        Ray ray = Camera.main.ScreenPointToRay(touchPosition);
        RaycastHit hitInfo;
        // If the Ray hits this
        if (Physics.Raycast(ray, out hitInfo) && hitInfo.transform == transform)
        {
            elemToUpdate = gameLogic.GetGridElementAtGridPosition(gameLogic.GetGridPositionFromWorldPosition(new Vector3(hitInfo.point.x, 0, hitInfo.point.z)));
        }

        // If elemToUpdate => the click corresponds to outside the grid so update the table instead
        if (elemToUpdate == null)
            elemToUpdate = tableEntity;

        Debug.Log(elemToUpdate);
        elemToUpdate.TouchBegin(touchPosition);
    }


    public override void TouchUpdate(Vector2 touchPosition)
    {
        //		base.TouchUpdate(touchPosition);
        if (elemToUpdate != null)
            elemToUpdate.TouchUpdate(touchPosition);
    }


    public override void TouchEnd(Vector2 touchPosition)
    {
        //		base.TouchEnd(touchPosition);
        if (elemToUpdate != null)
            elemToUpdate.TouchEnd(touchPosition);
    }

    #endregion
}
