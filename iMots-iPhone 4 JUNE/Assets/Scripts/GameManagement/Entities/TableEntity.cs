using UnityEngine;
using System.Collections;


public class TableEntity : PanningEntity
{

    protected GameLogic gameLogic;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();


        gameLogic = logic as GameLogic;
    }


    protected override void Start()
    {
        base.Start();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    #region Input Management

    protected override void ActionsOnTouchUpdateOnlyOnce(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdateOnlyOnce(touchPosition);

        if (gameLogic.gameMode == Global.GameMode.Keyboard)
            gameLogic.SelectTile(null);
    }

    #endregion
}
