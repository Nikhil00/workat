using UnityEngine;
using System.Collections;

public class ComboPopup : MonoBehaviour
{

    public string comboBaseText;

    bool animate = false;
    public float animDuration = 1f;
    float animStartTime;

    bool comboBreak = false;

    Transform bgTransform, textTransform;

    // Movement
    public float animUpMovementPerUpdate = 0.01f;
    Vector3 centerOfSinePosition;
    public float animSineMovementAmplitude = 1f;
    public float animSineMovementFrequency = 10f;
    public float animBreakUpMovementPerUpdate = -0.03f;
    float animActualBreakUpMovementPerUpdate;
    bool animBreakDidRotate = false;




    // Use this for initialization
    void Start()
    {
    }


    public void Init(int currentWordsCombo)
    {
        comboBaseText += currentWordsCombo;

        if (currentWordsCombo % 2 == 0)
            animSineMovementAmplitude = -animSineMovementAmplitude;

        textTransform = transform.Find("Text");
        bgTransform = transform.Find("BG");

        (textTransform.GetComponent(typeof(TextMesh)) as TextMesh).text = comboBaseText;
    }


    // Update is called once per frame
    void Update()
    {
        if (Time.time >= animStartTime + animDuration)
        {
            Destroy(gameObject);
        }
        else
        {
            // If this popup represent a combo break, it will start the same way but half-way will "break" and fall down
            if (comboBreak && Time.time >= animStartTime + animDuration * 0.5f)
            {
                // Handles the "break"
                if (!animBreakDidRotate)
                {
                    #region DEV_CODE

                    //DEV_CODE || 16 JAN || ADDING NEW CODE, FIXING ERRORS. CHECK COMMENTED CODE

                    // TODO play sound?
                    //ColorHSV colorHSV = new ColorHSV(bgTransform.GetComponent<Renderer>().material.color);
                    //colorHSV.v = 0;
                    //bgTransform.GetComponent<Renderer>().material.color = colorHSV.ToColor();

                    //DEV - new code
                    bgTransform.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));

                    #endregion

                    animBreakDidRotate = true;
                    transform.Rotate(new Vector3(0, 45, 0));

                    animActualBreakUpMovementPerUpdate = animBreakUpMovementPerUpdate;
                }

                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + animActualBreakUpMovementPerUpdate);

                animActualBreakUpMovementPerUpdate += animBreakUpMovementPerUpdate;
            }
            // Oscillate right/left while going up
            else
            {
                centerOfSinePosition = new Vector3(centerOfSinePosition.x, centerOfSinePosition.y, centerOfSinePosition.z + animUpMovementPerUpdate);
                float xOffset = Mathf.Sin((Time.time - animStartTime) * animSineMovementFrequency) * animSineMovementAmplitude;
                transform.position = new Vector3(centerOfSinePosition.x + xOffset, centerOfSinePosition.y, centerOfSinePosition.z);
            }

            // Fade out over the full duration of the anim
            bgTransform.GetComponent<Renderer>().material.color = new Color(bgTransform.GetComponent<Renderer>().material.color.r, bgTransform.GetComponent<Renderer>().material.color.g, bgTransform.GetComponent<Renderer>().material.color.b, Mathf.Lerp(1, 0, (Time.time - animStartTime) / animDuration));
            textTransform.GetComponent<Renderer>().material.color = new Color(textTransform.GetComponent<Renderer>().material.color.r, textTransform.GetComponent<Renderer>().material.color.g, textTransform.GetComponent<Renderer>().material.color.b, Mathf.Lerp(1, 0, (Time.time - animStartTime) / animDuration));
        }
    }


    public void StartAnimating(bool comboBreak)
    {
        this.comboBreak = comboBreak;

        centerOfSinePosition = transform.position;

        animStartTime = Time.time;

        animate = true;
    }


    public void StartAnimating()
    {
        StartAnimating(false);
    }
}
