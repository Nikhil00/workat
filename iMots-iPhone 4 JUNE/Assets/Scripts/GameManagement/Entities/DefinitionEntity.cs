using UnityEngine;
using System.Collections;


public class DefinitionEntity : GridElementEntity
{

    public float yOffsetOnGrowing;

    public TextMesh textMeshSecondaryDef = null;
    public TextMesh textMeshSeparator = null;

    public AudioSource growAS, shrinkAS;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
    }


    protected override void Start()
    {
        base.Start();
    }


    public override void InitGridElement()
    {
        base.InitGridElement();

        name = name.Insert(0, "Definition");

        DefinitionTile myTile = (gameLogic.grid.tiles[(int)gridPosition.y, (int)gridPosition.x] as DefinitionTile);

        textMesh.text = myTile.definitions[0].text;

        // If tile with 2 defs
        if (myTile.definitions[1].defPos != Global.DefinitionPosition.Error)
        {
            // First we setup the positions of the different components (both defs and the separator)
            float yText = 0, yTextSecondary = 0, ySeparator = 0;

            if (myTile.definitions[0].defSize == Global.DefinitionSize.Half)
            {
                yText = 0.25f;
                yTextSecondary = -0.25f;
            }
            else if (myTile.definitions[0].defSize == Global.DefinitionSize.Quarter)
            {
                yText = 0.35f;
                yTextSecondary = -0.175f;
            }
            else if (myTile.definitions[0].defSize == Global.DefinitionSize.ThreeQuarters)
            {
                yText = 0.175f;
                yTextSecondary = -0.35f;
            }

            ySeparator = yText + yTextSecondary + 0.025f;

            textMesh.transform.localPosition = new Vector3(textMesh.transform.localPosition.x, textMesh.transform.localPosition.y, yText);

            // Handles the second def
            textMeshSecondaryDef = (Instantiate(transform.Find("Text").gameObject) as GameObject).GetComponentInChildren(typeof(TextMesh)) as TextMesh;
            textMeshSecondaryDef.name = "TextSecondDef";
            textMeshSecondaryDef.transform.parent = transform;

            textMeshSecondaryDef.text = myTile.definitions[1].text;

            textMeshSecondaryDef.transform.localPosition = new Vector3(textMesh.transform.localPosition.x, textMesh.transform.localPosition.y, yTextSecondary);
            textMeshSecondaryDef.transform.localScale = textMesh.transform.localScale;

            // Handles the separator
            textMeshSeparator = (Instantiate(Resources.Load("Prefabs/Ingame/DefinitionSeparator")) as GameObject).GetComponent(typeof(TextMesh)) as TextMesh;
            textMeshSeparator.transform.parent = transform;

            textMeshSeparator.transform.localPosition = new Vector3(textMesh.transform.localPosition.x, textMesh.transform.localPosition.y, ySeparator);
            textMeshSeparator.transform.localScale = textMesh.transform.localScale;
        }


        if (!Global.Global.iPadVersion)
        {
            textMesh.characterSize = 0.08f;

            if (textMeshSecondaryDef != null)
                textMeshSecondaryDef.characterSize = 0.08f;
        }
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    public void Grow()
    {
        StartAnimations();

        //		growAS.Play();

        // If there is already another def that is focused, make it go back to unfocused
        if (gameLogic.focusedDefinition != null && gameLogic.focusedDefinition != this)
        {
            gameLogic.focusedDefinition.StartAnimations();
            //			shrinkAS.Play();
            //			growAS.Play();
        }

        if (gameLogic.focusedDefinition == this)
        {
            gameLogic.focusedDefinition = null;
            gameLogic.DisplaySolutionToWordButton(false);
        }
        else
        {
            gameLogic.guiCam.StartTabsAnimation();
            gameLogic.focusedDefinition = this;
            growAS.Play();
        }
    }


    public void SetGrowTranslationToFitPanBorders()
    {
        GrowAnimation myGrowAnimation = GetComponent("GrowAnimation") as GrowAnimation;

        // Compute the position of the corners of this def once it will be grown
        float left = transform.position.x - myGrowAnimation.bValue / 2;
        float right = transform.position.x + myGrowAnimation.bValue / 2;
        float top = transform.position.z + myGrowAnimation.bValue / 2;
        float bot = transform.position.z - myGrowAnimation.bValue / 2;

        float xOffset = 0, zOffset = 0;
        Rect panBorders = gameLogic.cam.panBorders;

        // Check if this def will need to be moved when growing so that it still fits in the screen
        if (left < panBorders.x)
            xOffset = panBorders.x - left;
        else if (right > panBorders.width)
            xOffset = panBorders.width - right;
        if (bot < panBorders.y)
            zOffset = panBorders.y - bot;
        else if (top > panBorders.height)
            zOffset = panBorders.height - top;


        Translate3DAnimation myTranslate3DAnim = GetComponent("Translate3DAnimation") as Translate3DAnimation;
        myTranslate3DAnim.aValue = transform.position;
        myTranslate3DAnim.bValue = new Vector3(myTranslate3DAnim.aValue.x + xOffset, myTranslate3DAnim.aValue.y + yOffsetOnGrowing, myTranslate3DAnim.aValue.z + zOffset);
    }


    public override bool IsLetterEntity()
    {
        return false;
    }


    public override void AnimationStopped(string animation)
    {
        base.AnimationStopped(animation);

        if (animation == "GrowAnimation")
        {
            // If i'm the focused definition and I've finished growing
            if (gameLogic.focusedDefinition == this)
            {
                gameLogic.DisplaySolutionToWordButton(true);
            }
        }
    }


    #region Input Management


    protected override void ActionsOnTouchUpdateOnlyOnce(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdateOnlyOnce(touchPosition);

        //		if (gameLogic.focusedDefinition != this)
        //			Grow();
    }


    protected override void ActionsOnTouchUpdate(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdate(touchPosition);

        Pan(touchPosition);
    }


    protected override void ActionsOnTouchEnd(Vector2 touchPosition)
    {
        base.ActionsOnTouchEnd(touchPosition);

        if (gameLogic.focusedDefinition != this)
            Grow();


        if (gameLogic.gameMode == Global.GameMode.Keyboard)
        {
            //			gameLogic.SelectDefinition(this, true);

            if (Camera.main.WorldToScreenPoint(transform.position).y <= touchPosition.y)
            {
                Debug.Log("TOP");
                gameLogic.SelectDefinition(this, 0, true);
            }
            else
            {
                Debug.Log("BOT");
                gameLogic.SelectDefinition(this, 1, true);
            }
        }
    }


    #endregion
}
