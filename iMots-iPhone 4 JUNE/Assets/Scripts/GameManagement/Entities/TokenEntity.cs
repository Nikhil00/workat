using UnityEngine;
using System.Collections;


public class TokenEntity : LetterEntity
{

    [HideInInspector]
    public SpringJoint joint;

    Rigidbody rigidBody;

    GameObject jointAnchor;

    [HideInInspector]
    public bool isSwappingPosition = false;

    [HideInInspector]
    public bool isBeingDragged = false;

    [HideInInspector]
    public bool waitingForMovementEnd = false;

    public float tokenPickedColliderRadius;
    protected float tokenNormalColliderRadiusBackup;

    protected SphereCollider colliderAsSphere;

    // Security => in case tokens are suck
    protected const float durationBeforeResetPos = 1f;
    protected float waitBeforeResetPosStartTime;
    bool isWaitingBeforeResetPos = false;
    bool stillBeingTouched = false;
    float stillBeingTouchLastTime;

    bool waitingForBackToPosSound = false;

    protected GameObject ps;


    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();

        colliderAsSphere = (GetComponent<Collider>() as SphereCollider);
    }


    protected override void Start()
    {
        base.Start();

        tokenNormalColliderRadiusBackup = colliderAsSphere.radius;
    }


    public override void InitGridElement()
    {
        base.InitGridElement();

        name = "Token " + textMesh.text;

        CreateSpringJoint();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();


        if (waitingForMovementEnd)
        {
            if (gameLogic.GetGridPositionFromWorldPosition(transform.position) == gridPosition && GetComponent<Rigidbody>().velocity.magnitude <= 1)
            {
                Debug.Log("STOPPED TOKEN");
                transform.position = new Vector3(transform.position.x, 0.2f, transform.position.z);
                waitingForMovementEnd = false;
                GetComponent<Collider>().isTrigger = false;
            }
        }
        if (waitingForBackToPosSound)
        {
            if (gameLogic.GetGridPositionFromWorldPosition(transform.position) == gridPosition && GetComponent<Rigidbody>().velocity.magnitude <= 1)
            {
                gameLogic.tokenBackToPosAS.Play();

                waitingForBackToPosSound = false;
            }
        }

        if ((((joint == null || colliderAsSphere.radius <= 0 || ps != null || gameLogic.GetGridPositionFromWorldPosition(transform.position) != gridPosition && GetComponent<Rigidbody>().velocity.magnitude <= 1 && Input.touchCount == 0) && !isBeingDragged && !isSwappingPosition)
            || (stillBeingTouched && Time.time >= stillBeingTouchLastTime + 1))
            && gameLogic.endOfGridAnim == null)
        {
            if (!isWaitingBeforeResetPos)
            {
                Debug.Log("Detected potential unusual state for token " + name + ".");
                isWaitingBeforeResetPos = true;
                waitBeforeResetPosStartTime = Time.time;
            }
            else
            {
                if (Time.time >= waitBeforeResetPosStartTime + durationBeforeResetPos)
                {
                    Debug.Log("Confirmed unusual state for token " + name + ". => Reset");
                    isWaitingBeforeResetPos = false;
                    ResetPos();
                }
            }
        }
        else
        {
            if (isWaitingBeforeResetPos)
                isWaitingBeforeResetPos = false;
        }
    }


    public void CreateSpringJoint()
    {
        joint = gameObject.AddComponent(typeof(SpringJoint)) as SpringJoint;
        joint.anchor = new Vector3(0, 0.5f, 0);
        joint.spring = 250;
        joint.damper = 1;
        joint.maxDistance = 0f;
        joint.minDistance = 0f;

    }


    public void DestroyImmediateSpringJoint()
    {
        Object.DestroyImmediate(joint);

        Debug.Log(name + " -> Joint destroyed.");
    }


    public void DestroySpringJoint()
    {
        Object.Destroy(joint);

        Debug.Log(name + " -> Joint destroyed.");
    }


    public override void AnimationStopped(string animation)
    {
        base.AnimationStopped(animation);

        if (animation == "FadeAnimation")
        {
            if (isSwappingPosition || joint == null)
            {
                isSwappingPosition = false;

                //				Vector3 destPos = gameLogic.GetWorldPositionFromGridPosition(gridPosition);
                //				transform.position = new Vector3(destPos.x, transform.position.y, destPos.z);
                //				
                //				CreateSpringJoint();

                ResetPos();

                StartAnimation("FadeAnimation");

                if (!gameLogic.physicsEnabled)
                {
                    GetComponent<Collider>().isTrigger = false;
                }
            }

            SetProperMaterial();
        }
    }


    public override void UnlockRigidBodyConstraints(bool freeCompletely)
    {
        base.UnlockRigidBodyConstraints(freeCompletely);

        if (freeCompletely)
        {
            DestroySpringJoint();
        }
    }


    public void ResetPos()
    {
        DestroyImmediateSpringJoint();

        if (ps != null)
            Destroy(ps.gameObject);

        GetComponent<Rigidbody>().isKinematic = false;

        isBeingDragged = false;

        colliderAsSphere.radius = tokenNormalColliderRadiusBackup;

        Vector3 destPos = gameLogic.GetWorldPositionFromGridPosition(gridPosition);

        #region ORIGINAL_CODE
        // transform.position = new Vector3(destPos.x, transform.position.y, destPos.z);
        #endregion

        #region DEV_CODE
        //DEV_EDIT || 27 OCT || ORIGINAL GRID POSITION CODE 1
        transform.position = new Vector3(destPos.x, 0, destPos.z);
        #endregion

        CreateSpringJoint();

        stillBeingTouched = false;
    }


    protected override void ActionsOnTouchUpdateOnlyOnce(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdateOnlyOnce(touchPosition);

        UnfocusDefinition();
    }


    #region Input Management

    protected override void ActionsOnTouchUpdate(Vector2 touchPosition)
    {
        base.ActionsOnTouchUpdate(touchPosition);

        stillBeingTouched = true;

        // First time the touchUpdate is called for that touch
        if (!GetComponent<Rigidbody>().isKinematic && !isBeingDragged && !isSwappingPosition)
        {
            gameLogic.tokenDragAS.Play();

            //			Vector3 touchPosition3DTemp = Camera.mainCamera.ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, Camera.mainCamera.transform.position.y));
            //			transform.position = new Vector3(touchPosition3DTemp.x, transform.position.y,touchPosition3DTemp.z);

            GetComponent<Rigidbody>().isKinematic = true;
            transform.parent = logic.cam.transform;

            colliderAsSphere.radius = tokenPickedColliderRadius;

            isBeingDragged = true;

            // If the physics have been disabled
            if (!gameLogic.physicsEnabled)
            {
                transform.position = new Vector3(transform.position.x, 2, transform.position.z);
            }
            else
            {
                if (!isCorrect)
                {
                    //					ps = new GameObject("PS");
                    //					ps.transform.parent = transform;
                    //					ps.transform.localPosition = new Vector3(0, 0.05f, 0);
                    //					
                    //					Object subPSRessource = Resources.Load("Prefabs/Ingame/TokenSubPS");
                    //					GameObject tmpSubPS;
                    //					for (int i = 0; i < 360; i += 20)
                    //					{
                    //						tmpSubPS = Instantiate(subPSRessource) as GameObject;
                    //						tmpSubPS.transform.parent = ps.transform;
                    //						tmpSubPS.transform.localPosition = new Vector3(0, 0, 0);
                    //						tmpSubPS.transform.localEulerAngles = new Vector3(0, i, 0);
                    //					}
                    if (gameLogic.physicsEnabled)
                    {
                        ps = Instantiate(gameLogic.tokenPS) as GameObject;
                        ps.transform.parent = transform;
                        ps.transform.localPosition = new Vector3(0, 0.05f, 0);
                    }
                }
            }
        }

        if (touchPosition != lastTouchPosition && isBeingDragged)
        {
            lastTouchPosition3D = Camera.main.ScreenToWorldPoint(new Vector3(lastTouchPosition.x, lastTouchPosition.y, Camera.main.transform.position.y));
            touchPosition3D = Camera.main.ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, Camera.main.transform.position.y));
            Vector3 offset = touchPosition3D - lastTouchPosition3D;

            //			transform.position = new Vector3(transform.position.x + offset.x, transform.position.y, transform.position.z + offset.z);
            transform.position = new Vector3(touchPosition3D.x, transform.position.y, touchPosition3D.z);
        }

        logic.cam.ManageAutoPan(touchPosition);

        stillBeingTouchLastTime = Time.time;
    }


    protected override void ActionsOnTouchEnd(Vector2 touchPosition)
    {
        stillBeingTouched = false;
        if (gameLogic.endOfGridAnim != null && gameLogic.endOfGridAnim.playingEndAnims && Time.time >= gameLogic.endOfGridAnim.endAnimsStartTime + 0.5f)
        {
            gameLogic.endOfGridAnim.StopAnim();
        }

        if (!isCorrect)
        {

        }
        else
        {
            gameLogic.pickCorrectLetterAS.Play();
            base.ActionsOnTouchEnd(touchPosition);
            return;
        }

        if (ps != null)
            Destroy(ps.gameObject);

        colliderAsSphere.radius = tokenNormalColliderRadiusBackup;

        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().AddForce(new Vector3(0.01f, 0, 0)); // Adding an unsignificant force prevents the rigibody from not moving despite the joint

        GridElementEntity other = gameLogic.GetGridElementAtGridPosition(gameLogic.GetGridPositionFromWorldPosition(transform.position));

        // If other is a valid TokenEntity, swap tiles with it
        if (other != null && other.GetType() == typeof(TokenEntity) && other != this && !(other as TokenEntity).isCorrect && !(other as TokenEntity).isBeingDragged && !(other as TokenEntity).isSwappingPosition)
        {
            gameLogic.tokenDropAS.Play();

            TokenEntity otherToken = other as TokenEntity;

            // Destroy the joints
            DestroyImmediateSpringJoint();
            otherToken.DestroyImmediateSpringJoint();


            // Swap the grid positions
            Vector2 myOldPos = gridPosition;
            Vector2 myNewPos = otherToken.gridPosition;
            otherToken.gridPosition = new Vector2(-1, -1); // This is set to -1 temporarily to avoid conflicts from having the same pos for a little moment

            gameLogic.AssignPositionToLetterEntity(this, myNewPos, false);
            gameLogic.AssignPositionToLetterEntity(otherToken, myOldPos, false);

            bool tokenIsCorrect = (gameLogic.grid.tiles[(int)gridPosition.y, (int)gridPosition.x] as LetterTile).guessLetter == (gameLogic.grid.tiles[(int)gridPosition.y, (int)gridPosition.x] as LetterTile).answerLetter;
            bool otherTokenIsCorrect = (gameLogic.grid.tiles[(int)otherToken.gridPosition.y, (int)otherToken.gridPosition.x] as LetterTile).guessLetter == (gameLogic.grid.tiles[(int)otherToken.gridPosition.y, (int)otherToken.gridPosition.x] as LetterTile).answerLetter;

            // Check for combo if this token is correct (will increment) OR if both tokens are incorrect (will break the combo); if this token is incorrect but the other one isn't, don't break the combo
            if (tokenIsCorrect || !tokenIsCorrect && !otherTokenIsCorrect)
            {
                gameLogic.AssignPositionToLetterEntity(this, myNewPos, true);
            }
            // If the other token is correct, check for combo (will increment); if the other token is incorrect, don't break the combo (as it is probably not the one the player was aiming for)
            if (otherTokenIsCorrect)
            {
                gameLogic.AssignPositionToLetterEntity(otherToken, myOldPos, true);
            }

            // Change the position of this token
            Vector3 destPos = gameLogic.GetWorldPositionFromGridPosition(gridPosition);
            transform.position = new Vector3(destPos.x, transform.position.y, destPos.z);
            CreateSpringJoint();

            // Start the animation of the other token
            if (!otherToken.isSwappingPosition)
            {
                otherToken.isSwappingPosition = true;
                otherToken.StartAnimation("FadeAnimation");

                if (!gameLogic.physicsEnabled)
                {
                    otherToken.AnimationStopped("FadeAnimation");
                }
            }

            if (gameLogic.gameMode == Global.GameMode.DragNDrop && !alreadyMoved)
                SetAlreadyMoved(true);
        }
        else
        {
            waitingForBackToPosSound = true;
        }

        if (!gameLogic.physicsEnabled)
        {
            waitingForMovementEnd = true;
        }


        isBeingDragged = false;

        transform.parent = GameObject.Find("GridEntities").transform;



        base.ActionsOnTouchEnd(touchPosition);
    }

    #endregion
}
