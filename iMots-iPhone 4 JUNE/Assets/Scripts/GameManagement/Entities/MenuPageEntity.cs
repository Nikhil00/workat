using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MenuPageEntity : PanningEntity
{

    public Global.Difficulty difficulty;

    public Vector2 index { get; protected set; }

    protected List<string> grids;

    public float goToNextPagePanSpeedThreshold;

    protected MenuLogic menuLogic;

    [HideInInspector]
    public bool fullyInitialised = false;


    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();

        menuLogic = logic as MenuLogic;
    }


    protected override void Start()
    {
        base.Start();
    }

    public void Init(Vector2 index, List<string> grids, Global.Difficulty difficulty)
    {
        this.index = index;
        this.difficulty = difficulty;
        this.grids = grids;

        //		if (Application.platform == RuntimePlatform.OSXEditor)
        //DEV_EDIT || MAIN MENU || RENAME SPAWNED PAGE NAMES
        transform.parent.name = "Page [" + (int)index.x + "," + (int)index.y + "]";
        Vector2 fitToCamView = OrthoCam.GetFitToCamViewMultipliers(true);

        //DEV_EDIT || MAIN MENU || REMOVING SPAWNED PAGE'S SCALE AND PARENT CODE AFTER RENAMING
        // transform.localScale = new Vector3(fitToCamView.x, transform.localScale.y, fitToCamView.y);
        // transform.parent.position = new Vector3(transform.localScale.x * index.x, transform.position.y, -transform.localScale.z * index.y);

        // If the current page has not been set yet, search my grids if it contains the current grid
        if (!menuLogic.currentPageAtInitFound)
        {
            foreach (string grid in grids)
            {
                // If found, set me as the current page
                if (grid == PlayerPrefs.GetString("CurrentGrid"))
                {
                    menuLogic.currentPage = index;
                    menuLogic.currentPageAtInitFound = true;
                    break;
                }
            }
        }
    }


    public void Init()
    {
        //DEV_EDIT || ************************
        //Debug.Log("Fully Initialising Page " + transform.parent.name);

        //DEV_EDIT || MAIN MENU || HORIZONTAL AND VERTICAL GRID BUTTON'S CODE IS HERE

        Debug.Log("Fully initialising page " + transform.parent.name);

        // Set the difficultyString according to the difficulty of the grids
        string difficultyString = "";
        if (difficulty == Global.Difficulty.Easy)
        {
            difficultyString = "F";
        }
        else if (difficulty == Global.Difficulty.Medium)
        {
            difficultyString = "M";
        }
        else if (difficulty == Global.Difficulty.Hard)
        {
            difficultyString = "D";
        }
        else if (difficulty == Global.Difficulty.vEasy)
        {
            difficultyString = "F";
        }


        // Set the sizeString according to the size of the grids
        string sizeString = "";
        if (Global.Global.iPadVersion)
        {
            if (index.y == 0)
            {
                sizeString = "9x13";
            }
            else if (index.y == 1)
            {
                sizeString = "13x9";
            }
            else if (index.y == 2)
            {
                sizeString = "18x18";
            }
            //TODO || GRID 11
            // Dev || 06-06-2020 || Code Change
            else if (index.y == 3)
            {
                sizeString = "8X5";
            }
            else //if if (index.y == 4)
            {
                sizeString = "10X10";
            }
        }
        else
        {
            if (index.y == 0)
            {
                sizeString = "8x5";
            }
            else // if (index.y == 1)
            {
                sizeString = "10x10";
            }
        }

        Debug.Log("Nik - difficultyString : " + difficultyString + " || sizeString : " + sizeString);
        // Instantiate the appropriate title for this page
        GameObject titleGO = Instantiate(Resources.Load("Prefabs/Menu/Pages/Titles/Title" + difficultyString + sizeString)) as GameObject;
        titleGO.transform.parent = transform.parent;
        if (Global.Global.iPadVersion)
            titleGO.transform.localPosition = new Vector3(0.01f, titleGO.transform.position.y, -0.04f);
        else
            titleGO.transform.localPosition = titleGO.transform.position;

        #region DEV_CODE

        //DEV_EDIT || PAGES || RESETTING PAGE POSITION BASED ON CATEGORY
        if (titleGO.name.Contains("9x13"))
        {
            Dev_UIManager.instance.listOfSpawnedPages_9x13.Add(transform.parent.gameObject);
            //StartCoroutine(Dev_UIManager.instance.ResetSpawnedPagePositions(0));
        }
        else if (titleGO.name.Contains("13x9"))
        {
            Debug.Log("<color=green><b>ROW 13x9</b></color>");
            Dev_UIManager.instance.listOfSpawnedPages_13x9.Add(transform.parent.gameObject);
            //StartCoroutine(Dev_UIManager.instance.ResetSpawnedPagePositions(0));
        }
        else if (titleGO.name.Contains("18x18"))
        {
            Dev_UIManager.instance.listOfSpawnedPages_18x18.Add(transform.parent.gameObject);
            //StartCoroutine(Dev_UIManager.instance.ResetSpawnedPagePositions(0));
        }
        // Nik - Add Code
        else if (titleGO.name.Contains("8x5"))
        {
            Dev_UIManager.instance.listOfSpawnedPages_8X5.Add(transform.parent.gameObject);
            //StartCoroutine(Dev_UIManager.instance.ResetSpawnedPagePositions(0));
        }
        else if (titleGO.name.Contains("10x10"))
        {
            Dev_UIManager.instance.listOfSpawnedPages_10X10.Add(transform.parent.gameObject);
            //StartCoroutine(Dev_UIManager.instance.ResetSpawnedPagePositions(0));
        }
        Debug.Log("Nik - Filter level in Board Size");

        #endregion

        float nbLinesAsFloat = (grids.Count / menuLogic.maxGridsButtonPerPage.x);
        int nbCompleteLines = (int)nbLinesAsFloat;
        int nbElemsOnIncompleteLine = (int)((nbLinesAsFloat - nbCompleteLines) * menuLogic.maxGridsButtonPerPage.x);

        if (menuLogic.sizeOfAGridButton.x > menuLogic.spaceAllowedForAGridButton.x)
        {
            Debug.LogWarning("Grids buttons horizontal size is bigger than the space allowed for it");
            menuLogic.sizeOfAGridButton.x = menuLogic.spaceAllowedForAGridButton.x;
        }
        if (menuLogic.sizeOfAGridButton.y > menuLogic.spaceAllowedForAGridButton.y)
        {
            Debug.LogWarning("Grids buttons vertical size is bigger than the space allowed for it");
            menuLogic.sizeOfAGridButton.y = menuLogic.spaceAllowedForAGridButton.y;
        }

        int j = 0;
        int currentButtonIndex;

        string currentGrid = PlayerPrefs.GetString("CurrentGrid");

        Material newGridMaterial = Resources.Load("Materials/Menu/GridsButtons/StateIconNew" + difficultyString) as Material;
        Material startedGridMaterial = Resources.Load("Materials/Menu/GridsButtons/StateIconStarted" + difficultyString) as Material;
        Material completedGridMaterial = Resources.Load("Materials/Menu/GridsButtons/StateIconCompleted" + difficultyString) as Material;

        #region DEV_CODE

        string newLevelIconName;
        string startedLevelIconName;
        string completedLevelIconName;

        if (Dev_UIManager.instance.isUiElement)
        {
            newLevelIconName = "newGridSprite_" + difficultyString;
            startedLevelIconName = "startedGridSprite_" + difficultyString;
            completedLevelIconName = "completedGridSprite_" + difficultyString;
        }

        #endregion

        float timeReferenceForShiningIconsStart = Time.time;
        
        Debug.Log("Nik - Page in Level Spawn Here");
        // Grids buttons creation; handles only the COMPLETE LINES
        for (int i = 0; i < menuLogic.maxGridsButtonPerPage.x; i++)
        {
            for (j = 0; j < nbCompleteLines; j++)
            {   
                currentButtonIndex = (int)(j * menuLogic.maxGridsButtonPerPage.x + i);

                ButtonMenuGridInstance button = (Instantiate(menuLogic.buttonMenuGridInstancePrefabRessource) as GameObject).GetComponent("ButtonMenuGridInstance") as ButtonMenuGridInstance;
                button.name = "GridButton [" + i + "," + j + "]";

                #region ORIGINAL_CODE

                // button.transform.parent = transform.parent;
                // button.transform.localScale = new Vector3(menuLogic.sizeOfAGridButton.x, 1, menuLogic.sizeOfAGridButton.y);
                // button.transform.localPosition = new Vector3(menuLogic.buttonsPlacementZone.x + menuLogic.spaceAllowedForAGridButton.x * (i + 0.5f), button.transform.localPosition.y, menuLogic.buttonsPlacementZone.height - menuLogic.spaceAllowedForAGridButton.y * (j + 0.5f));
                #endregion

                #region DEV_CODE

                //DEV_EDIT || LEVEL BUTTON || REMOVING UNNECESSORY CODE HERE - ADDING UI PARENT FOR BUTTONS
                
                button.transform.parent = transform.parent.Find("ButtonsParent").transform;
                if (button.GetComponent<RectTransform>())
                {
                    button.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                }
                else
                {
                    Debug.Log("Something is null here");
                }

                #endregion

                button.myPage = this;
                button.associatedGrid = grids[currentButtonIndex];

                // If the currentGrid is associated to this button, then make this page as the currentPage
                //				if (button.associatedGrid == currentGrid)
                //				{
                //					menuLogic.currentPage = index;
                //				}

                #region ORIGINAL_CODE

                // if (PlayerPrefs.HasKey(button.associatedGrid + "_ScoreBest"))
                // {
                //     button.idText.text = PlayerPrefs.GetInt(button.associatedGrid + "_ScoreBest").ToString();
                //     button.ptsTextGO.GetComponent<Renderer>().enabled = true;
                //     button.ptsTextGO.transform.localPosition = new Vector3(button.idText.text.Length * 0.06f, button.ptsTextGO.transform.localPosition.y, button.ptsTextGO.transform.localPosition.z);
                // }
                // else
                // {
                //     button.idText.text = button.associatedGrid.Remove(0, logic.platform.Length + 5);
                // }

                #endregion

                #region DEV_CODE

                //DEV_EDIT || 12 OCT || GAME OVER SHOWING ERROR HERE
                if (PlayerPrefs.HasKey(button.associatedGrid + "_ScoreBest") && !FindObjectOfType<Dev_UIManager>().isUiElement)
                {
                    button.idText.text = PlayerPrefs.GetInt(button.associatedGrid + "_ScoreBest").ToString();
                    button.ptsTextGO.GetComponent<Renderer>().enabled = true;
                    button.ptsTextGO.transform.localPosition = new Vector3(button.idText.text.Length * 0.06f, button.ptsTextGO.transform.localPosition.y, button.ptsTextGO.transform.localPosition.z);
                }
                else if (PlayerPrefs.HasKey(button.associatedGrid + "_ScoreBest") && FindObjectOfType<Dev_UIManager>().isUiElement)
                {
                    //DEV_EDIT || LEVEL BUTTON || REMOVING BUTTON.IDTEXT LOGIC FOR TEMP
                    button.transform.Find("GridId").GetComponent<Text>().text = PlayerPrefs.GetInt(button.associatedGrid + "_ScoreBest").ToString();
                }
                else
                {
                    button.transform.Find("GridId").GetComponent<Text>().text = button.associatedGrid.Remove(0, logic.platform.Length + 5);
                    if (button.GetComponent<TextMesh>())
                    {
                        button.idText.text = button.associatedGrid.Remove(0, logic.platform.Length + 5);
                        //Dev_LogManager.instance.PrintLog("Button Id : " + button.idText.text, 2);
                    }
                    else
                    {
                        button.transform.Find("GridId").GetComponent<Text>().text = button.associatedGrid.Remove(0, logic.platform.Length + 5);
                        //Dev_LogManager.instance.PrintLog("Button Grid Id : " + button.transform.Find("GridId").GetComponent<Text>().text, 2);
                    }
                }

                #endregion

                Transform stateIconTransform = button.transform.Find("StateIcon");
                // This grid exists in the player prefs


                #region ORIGINAL_CODE

                // if (PlayerPrefs.HasKey(grids[currentButtonIndex]))
                // {
                //     int gridState = PlayerPrefs.GetInt(grids[currentButtonIndex]);

                //     if (gridState == (int)Global.GridState.Started)
                //     {
                //         stateIconTransform.GetComponent<Renderer>().material = startedGridMaterial;
                //         button.rotateIcon = true;
                //     }
                //     else if (gridState == (int)Global.GridState.Completed)
                //     {
                //         stateIconTransform.GetComponent<Renderer>().material = completedGridMaterial;
                //         stateIconTransform.localScale = new Vector3(0.5f, stateIconTransform.localScale.y, 0.5f);

                //         button.SetPendingStartShining((i + j) * 0.2f /*+ 0.5f*/, timeReferenceForShiningIconsStart);
                //     }
                //     else
                //     {
                //         stateIconTransform.GetComponent<Renderer>().material = newGridMaterial;
                //         stateIconTransform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
                //     }
                // }
                // // Else it's certain that it's empty
                // else
                // {
                //     stateIconTransform.GetComponent<Renderer>().material = newGridMaterial;
                //     stateIconTransform.transform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
                // }

                #endregion

                #region DEV_CODE

                if (PlayerPrefs.HasKey(grids[currentButtonIndex]))
                {
                    int gridState = PlayerPrefs.GetInt(grids[currentButtonIndex]);

                    if (gridState == (int)Global.GridState.Started)
                    {
                        if (stateIconTransform.GetComponent<Renderer>())
                        {
                            //ORIGINAL CODE
                            stateIconTransform.GetComponent<Renderer>().material = startedGridMaterial;
                            button.rotateIcon = true;
                        }
                        else
                        {
                            //DEVELOPER CODE
                            if (difficultyString == "F")
                            {
                                stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.startedGridSprite_F;
                                stateIconTransform.GetComponent<Dev_RotateStateIcon>().rotateState = true;
                            }
                            else if (difficultyString == "M")
                            {
                                stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.startedGridSprite_M;
                                stateIconTransform.GetComponent<Dev_RotateStateIcon>().rotateState = true;
                            }
                            else if (difficultyString == "D")
                            {
                                stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.startedGridSprite_D;
                                stateIconTransform.GetComponent<Dev_RotateStateIcon>().rotateState = true;
                            }
                        }
                    }
                    else if (gridState == (int)Global.GridState.Completed)
                    {
                        if (stateIconTransform.GetComponent<Renderer>())
                        {
                            //ORIGINAL CODE
                            stateIconTransform.GetComponent<Renderer>().material = completedGridMaterial;
                            stateIconTransform.localScale = new Vector3(0.5f, stateIconTransform.localScale.y, 0.5f);
                        }
                        else
                        {
                            //DEVELOPER CODE
                            if (difficultyString == "F")
                            {
                                stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.completedGridSprite_F;
                            }
                            else if (difficultyString == "M")
                            {
                                stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.completedGridSprite_M;
                            }
                            else if (difficultyString == "D")
                            {
                                stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.completedGridSprite_D;
                            }
                        }

                        button.SetPendingStartShining((i + j) * 0.2f /*+ 0.5f*/, timeReferenceForShiningIconsStart);
                    }
                    else
                    {
                        if (stateIconTransform.GetComponent<Renderer>())
                        {
                            //ORIGINAL CODE
                            stateIconTransform.GetComponent<Renderer>().material = newGridMaterial;
                            stateIconTransform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
                        }
                        else
                        {
                            //DEVELOPER CODE
                            if (difficultyString == "F")
                            {
                                stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_F;
                            }
                            else if (difficultyString == "M")
                            {
                                stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_M;
                            }
                            else if (difficultyString == "D")
                            {
                                stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_D;
                            }
                        }
                    }
                }
                // Else it's certain that it's empty
                else
                {
                    if (stateIconTransform.GetComponent<Renderer>())
                    {
                        //ORIGINAL CODE
                        stateIconTransform.GetComponent<Renderer>().material = newGridMaterial;
                        stateIconTransform.transform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
                    }
                    else
                    {
                        //DEVELOPER CODE
                        if (difficultyString == "F")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_F;
                        }
                        else if (difficultyString == "M")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_M;
                        }
                        else if (difficultyString == "D")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_D;
                        }
                    }
                }

                #endregion
            }
        }

        // Last pass for the remaining buttons (INCOMPLETE LINE)
        for (int i = 0; i < nbElemsOnIncompleteLine; i++)
        {
            currentButtonIndex = (int)(j * menuLogic.maxGridsButtonPerPage.x + i);

            ButtonMenuGridInstance button = (Instantiate(menuLogic.buttonMenuGridInstancePrefabRessource) as GameObject).GetComponent("ButtonMenuGridInstance") as ButtonMenuGridInstance;
            button.name = "GridButton [" + i + "," + j + "]";

            #region ORIGINAL_CODE

            // button.transform.parent = transform.parent;
            // button.transform.localScale = new Vector3(menuLogic.sizeOfAGridButton.x, 1, menuLogic.sizeOfAGridButton.y);
            // button.transform.localPosition = new Vector3(menuLogic.buttonsPlacementZone.x + menuLogic.spaceAllowedForAGridButton.x * (i + 0.5f), button.transform.localPosition.y, menuLogic.buttonsPlacementZone.height - menuLogic.spaceAllowedForAGridButton.y * (j + 0.5f));

            #endregion

            #region DEV_CODE

            //DEV_EDIT || LEVEL BUTTON || REMOVING UNNECESSORY CODE HERE - ADDING UI PARENT FOR BUTTONS
            button.transform.parent = transform.parent.Find("ButtonsParent").transform;
            button.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            #endregion

            button.myPage = this;
            button.associatedGrid = grids[(int)(j * menuLogic.maxGridsButtonPerPage.x + i)];

            // If the currentGrid is associated to this button, then make this page as the currentPage
            if (button.associatedGrid == currentGrid)
            {
                menuLogic.currentPage = index;
            }

            #region DEV_CODE

            //DEV_EDIT || LEVEL BUTTON || REMOVING BUTTON.IDTEXT LOGIC FOR TEMP
            button.transform.Find("GridId").GetComponent<Text>().text = button.associatedGrid.Remove(0, logic.platform.Length + 5);
            // button.idText.text = button.associatedGrid.Remove(0, logic.platform.Length + 5);

            #endregion

            Transform stateIconTransform = button.transform.Find("StateIcon");

            #region ORIGINAL_CODE

            // This grid exists in the player prefs
            // if (PlayerPrefs.HasKey(grids[currentButtonIndex]))
            // {
            //     int gridState = PlayerPrefs.GetInt(grids[currentButtonIndex]);

            //     if (gridState == (int)Global.GridState.Started)
            //     {
            //         stateIconTransform.GetComponent<Renderer>().material = startedGridMaterial;
            //         button.rotateIcon = true;
            //     }
            //     else if (gridState == (int)Global.GridState.Completed)
            //     {
            //         stateIconTransform.GetComponent<Renderer>().material = completedGridMaterial;
            //         stateIconTransform.localScale = new Vector3(0.5f, stateIconTransform.localScale.y, 0.5f);

            //         button.SetPendingStartShining((i + j) * 0.2f /*+ 0.5f*/, timeReferenceForShiningIconsStart);
            //     }
            //     else
            //     {
            //         stateIconTransform.GetComponent<Renderer>().material = newGridMaterial;
            //         stateIconTransform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
            //     }
            // }
            // // Else it's certain that it's empty
            // else
            // {
            //     stateIconTransform.GetComponent<Renderer>().material = newGridMaterial;
            //     stateIconTransform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
            // }

            #endregion

            #region DEV_CODE

            if (PlayerPrefs.HasKey(grids[currentButtonIndex]))
            {
                int gridState = PlayerPrefs.GetInt(grids[currentButtonIndex]);

                if (gridState == (int)Global.GridState.Started)
                {
                    if (stateIconTransform.GetComponent<Renderer>())
                    {
                        stateIconTransform.GetComponent<Renderer>().material = startedGridMaterial;
                        button.rotateIcon = true;
                    }
                    else
                    {
                        //DEVELOPER CODE
                        if (difficultyString == "F")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.startedGridSprite_F;
                            stateIconTransform.GetComponent<Dev_RotateStateIcon>().rotateState = true;
                        }
                        else if (difficultyString == "M")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.startedGridSprite_M;
                            stateIconTransform.GetComponent<Dev_RotateStateIcon>().rotateState = true;
                        }
                        else if (difficultyString == "D")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.startedGridSprite_D;
                            stateIconTransform.GetComponent<Dev_RotateStateIcon>().rotateState = true;
                        }
                    }
                }
                else if (gridState == (int)Global.GridState.Completed)
                {
                    if (stateIconTransform.GetComponent<Renderer>())
                    {
                        stateIconTransform.GetComponent<Renderer>().material = completedGridMaterial;
                        stateIconTransform.localScale = new Vector3(0.5f, stateIconTransform.localScale.y, 0.5f);
                    }
                    else
                    {
                        //DEVELOPER CODE
                        if (difficultyString == "F")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.completedGridSprite_F;
                        }
                        else if (difficultyString == "M")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.completedGridSprite_M;
                        }
                        else if (difficultyString == "D")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.completedGridSprite_D;
                        }
                    }

                    button.SetPendingStartShining((i + j) * 0.2f /*+ 0.5f*/, timeReferenceForShiningIconsStart);
                }
                else
                {
                    if (stateIconTransform.GetComponent<Renderer>())
                    {
                        stateIconTransform.GetComponent<Renderer>().material = newGridMaterial;
                        stateIconTransform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
                    }
                    else
                    {
                        //DEVELOPER CODE
                        if (difficultyString == "F")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_F;
                        }
                        else if (difficultyString == "M")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_M;
                        }
                        else if (difficultyString == "D")
                        {
                            stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_D;
                        }
                    }
                }
            }
            // Else it's certain that it's empty
            else
            {
                if (stateIconTransform.GetComponent<Renderer>())
                {
                    stateIconTransform.GetComponent<Renderer>().material = newGridMaterial;
                    stateIconTransform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
                }
                else
                {
                    //DEVELOPER CODE
                    if (difficultyString == "F")
                    {
                        stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_F;
                    }
                    else if (difficultyString == "M")
                    {
                        stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_M;
                    }
                    else if (difficultyString == "D")
                    {
                        stateIconTransform.GetComponent<Image>().sprite = Dev_UIManager.instance.newGridSprite_D;
                    }
                }
            }

            #endregion
        }

        fullyInitialised = true;
        Debug.Log("<color=yellow>All Complete</color>");
    }


    //	public void Init(Vector2 index, List<string> grids, Global.Difficulty difficulty)
    //	{
    //		this.index = index;
    //		this.difficulty = difficulty;
    //		
    //		if (Application.platform == RuntimePlatform.OSXEditor)
    //			transform.parent.name = "Page [" + (int)index.x + "," + (int)index.y + "]";
    //		
    //		
    //		// Set the difficultyString according to the difficulty of the grids
    //		string difficultyString = "";
    //		if (difficulty == Global.Difficulty.Easy)
    //		{
    //			difficultyString = "F";
    //		}
    //		else if (difficulty == Global.Difficulty.Medium)
    //		{
    //			difficultyString = "M";
    //		}
    //		else if (difficulty == Global.Difficulty.Hard)
    //		{
    //			difficultyString = "D";
    //		}
    //		
    //		
    //		// Set the sizeString according to the size of the grids
    //		string sizeString = "";
    //		if (Global.Global.iPadVersion)
    //		{
    //			if (index.y == 0)
    //			{
    //				sizeString = "9x13";
    //			}
    //			else if (index.y == 1)
    //			{
    //				sizeString = "13x9";
    //			}
    //			else // if (index.y == 2)
    //			{
    //				sizeString = "18x18";
    //			}
    //		}
    //		else
    //		{
    //			if (index.y == 0)
    //			{
    //				sizeString = "8x5";
    //			}
    //			else // if (index.y == 1)
    //			{
    //				sizeString = "10x10";
    //			}
    //		}
    //		
    //		Vector2 fitToCamView = OrthoCam.GetFitToCamViewMultipliers(true);
    //		
    //		transform.localScale = new Vector3(fitToCamView.x, transform.localScale.y, fitToCamView.y);
    //		transform.parent.position = new Vector3(transform.localScale.x * index.x, transform.position.y, -transform.localScale.z * index.y);
    //		
    //		
    //		// Instantiate the appropriate title for this page
    //		GameObject titleGO = Instantiate(Resources.Load("Prefabs/Menu/Pages/Titles/Title" + difficultyString + sizeString)) as GameObject;
    //		titleGO.transform.parent = transform.parent;
    //		if (Global.Global.iPadVersion)
    //			titleGO.transform.localPosition = new Vector3(0.01f, titleGO.transform.position.y, -0.04f);
    //		else
    //			titleGO.transform.localPosition = titleGO.transform.position;
    //			
    //		
    //		float nbLinesAsFloat = (grids.Count / menuLogic.maxGridsButtonPerPage.x);
    //		int nbCompleteLines = (int)nbLinesAsFloat;
    //		int nbElemsOnIncompleteLine = (int)((nbLinesAsFloat - nbCompleteLines) * menuLogic.maxGridsButtonPerPage.x);
    //
    //		if (menuLogic.sizeOfAGridButton.x > menuLogic.spaceAllowedForAGridButton.x)
    //		{
    //			Debug.LogWarning("Grids buttons horizontal size is bigger than the space allowed for it");
    //			menuLogic.sizeOfAGridButton.x = menuLogic.spaceAllowedForAGridButton.x;
    //		}
    //		if (menuLogic.sizeOfAGridButton.y > menuLogic.spaceAllowedForAGridButton.y)
    //		{
    //			Debug.LogWarning("Grids buttons vertical size is bigger than the space allowed for it");
    //			menuLogic.sizeOfAGridButton.y = menuLogic.spaceAllowedForAGridButton.y;
    //		}
    //		
    //		int j = 0;
    //		int currentButtonIndex;
    //		
    //		string currentGrid = PlayerPrefs.GetString("CurrentGrid");
    //		
    //		Material newGridMaterial = Resources.Load("Materials/Menu/GridsButtons/StateIconNew" + difficultyString) as Material;
    //		Material startedGridMaterial = Resources.Load("Materials/Menu/GridsButtons/StateIconStarted" + difficultyString) as Material;
    //		Material completedGridMaterial = Resources.Load("Materials/Menu/GridsButtons/StateIconCompleted" + difficultyString) as Material;
    //		
    //		float timeReferenceForShiningIconsStart = Time.time;
    //		
    //		// Grids buttons creation; handles only the COMPLETE LINES
    //		for (int i = 0; i < menuLogic.maxGridsButtonPerPage.x ; i++)
    //		{
    //			for (j = 0; j < nbCompleteLines; j++)
    //			{
    //				currentButtonIndex = (int)(j * menuLogic.maxGridsButtonPerPage.x + i);
    //					
    //				ButtonMenuGridInstance button = (Instantiate(menuLogic.buttonMenuGridInstancePrefabRessource) as GameObject).GetComponent("ButtonMenuGridInstance") as ButtonMenuGridInstance;
    //				button.name = "GridButton [" + i + "," + j + "]";
    //				
    //				button.transform.parent = transform.parent;
    //				button.transform.localScale = new Vector3(menuLogic.sizeOfAGridButton.x , 1, menuLogic.sizeOfAGridButton.y);
    //				button.transform.localPosition = new Vector3(menuLogic.buttonsPlacementZone.x + menuLogic.spaceAllowedForAGridButton.x * (i + 0.5f), button.transform.localPosition.y, menuLogic.buttonsPlacementZone.height - menuLogic.spaceAllowedForAGridButton.y * (j + 0.5f));
    //			
    //				button.myPage = this;
    //				button.associatedGrid = grids[currentButtonIndex];
    //				
    //				// If the currentGrid is associated to this button, then make this page as the currentPage
    //				if (button.associatedGrid == currentGrid)
    //				{
    //					menuLogic.currentPage = index;
    //				}
    //				
    //				if (PlayerPrefs.HasKey(button.associatedGrid +"_ScoreBest"))
    //				{
    //					button.idText.text = PlayerPrefs.GetInt(button.associatedGrid +"_ScoreBest").ToString();
    //					button.ptsTextGO.renderer.enabled = true;
    //					button.ptsTextGO.transform.localPosition = new Vector3(button.idText.text.Length * 0.06f, button.ptsTextGO.transform.localPosition.y, button.ptsTextGO.transform.localPosition.z);
    //				}
    //				else
    //				{
    //					button.idText.text = button.associatedGrid.Remove(0, logic.platform.Length + 5);
    //				}
    //
    //				Transform stateIconTransform = button.transform.Find("StateIcon");
    //				// This grid exists in the player prefs
    //				if (PlayerPrefs.HasKey(grids[currentButtonIndex]))
    //				{
    //					int gridState = PlayerPrefs.GetInt(grids[currentButtonIndex]);
    //					
    //					if (gridState == (int)Global.GridState.Started)
    //					{
    //						stateIconTransform.renderer.material = startedGridMaterial;
    //						button.rotateIcon = true;
    //					}
    //					else if (gridState == (int)Global.GridState.Completed)
    //					{
    //						stateIconTransform.renderer.material = completedGridMaterial;
    //						stateIconTransform.localScale = new Vector3(0.5f, stateIconTransform.localScale.y, 0.5f);
    //						
    //						button.SetPendingStartShining((i + j) * 0.2f /*+ 0.5f*/, timeReferenceForShiningIconsStart);
    //					}
    //					else
    //					{
    //						stateIconTransform.renderer.material = newGridMaterial;
    //						stateIconTransform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
    //					}
    //				}
    //				// Else it's certain that it's empty
    //				else
    //				{
    //					stateIconTransform.renderer.material = newGridMaterial;
    //					stateIconTransform.transform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
    //				}
    //			}
    //		}
    //		
    //		// Last pass for the remaining buttons (INCOMPLETE LINE)
    //		for (int i = 0; i < nbElemsOnIncompleteLine ; i++)
    //		{
    //			currentButtonIndex = (int)(j * menuLogic.maxGridsButtonPerPage.x + i);
    //			
    //			ButtonMenuGridInstance button = (Instantiate(menuLogic.buttonMenuGridInstancePrefabRessource) as GameObject).GetComponent("ButtonMenuGridInstance") as ButtonMenuGridInstance;
    //			button.name = "GridButton [" + i + "," + j + "]";
    //			
    //			button.transform.parent = transform.parent;
    //			button.transform.localScale = new Vector3(menuLogic.sizeOfAGridButton.x , 1, menuLogic.sizeOfAGridButton.y);
    //			button.transform.localPosition = new Vector3(menuLogic.buttonsPlacementZone.x + menuLogic.spaceAllowedForAGridButton.x * (i + 0.5f), button.transform.localPosition.y, menuLogic.buttonsPlacementZone.height - menuLogic.spaceAllowedForAGridButton.y * (j + 0.5f));
    //		
    //			button.myPage = this;
    //			button.associatedGrid = grids[(int)(j * menuLogic.maxGridsButtonPerPage.x + i)];
    //			
    //			// If the currentGrid is associated to this button, then make this page as the currentPage
    //			if (button.associatedGrid == currentGrid)
    //			{
    //				menuLogic.currentPage = index;
    //			}
    //				
    //			button.idText.text = button.associatedGrid.Remove(0, logic.platform.Length + 5);
    //
    //			Transform stateIconTransform = button.transform.Find("StateIcon");
    //			// This grid exists in the player prefs
    //			if (PlayerPrefs.HasKey(grids[currentButtonIndex]))
    //			{
    //				int gridState = PlayerPrefs.GetInt(grids[currentButtonIndex]);
    //				
    //				if (gridState == (int)Global.GridState.Started)
    //				{
    //					stateIconTransform.renderer.material = startedGridMaterial;
    //					button.rotateIcon = true;
    //				}
    //				else if (gridState == (int)Global.GridState.Completed)
    //				{
    //					stateIconTransform.renderer.material = completedGridMaterial;
    //					stateIconTransform.localScale = new Vector3(0.5f, stateIconTransform.localScale.y, 0.5f);
    //					
    //					button.SetPendingStartShining((i + j) * 0.2f /*+ 0.5f*/, timeReferenceForShiningIconsStart);
    //				}
    //				else
    //				{
    //					stateIconTransform.renderer.material = newGridMaterial;
    //					stateIconTransform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
    //				}
    //			}
    //			// Else it's certain that it's empty
    //			else
    //			{
    //				stateIconTransform.renderer.material = newGridMaterial;
    //				stateIconTransform.localScale = new Vector3(stateIconTransform.localScale.x * 0.5f, stateIconTransform.localScale.y, stateIconTransform.localScale.z * 0.5f);
    //			}
    //		}
    //		
    //		fullyInitialised = true;
    //	}


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    // USEFUL TO DEBUG POINTS IN 3D SPACE
    void OnDrawGizmosSelected()
    {

    }


    protected override void StartPan(Vector2 touchPosition)
    {
        base.StartPan(touchPosition);

        logic.cam.ComputePanBorders();

        // If the finger swipe is mostly horizontal => consider it as horizontal
        if (Mathf.Abs(touchPosition.x - initialTouchPosition.x) >= Mathf.Abs(touchPosition.y - initialTouchPosition.y))
        {
            logic.cam.disableVerticalPan = true;
        }
        // Else consider it as vertical
        else
        {
            logic.cam.disableHorizontalPan = true;
        }
    }


    #region Input Management

    //	public override void TouchBegin(Vector2 touchPosition)
    //	{		
    //		base.TouchBegin(touchPosition);
    //		
    //	}
    //	
    //	
    //	public override void TouchUpdate(Vector2 touchPosition)
    //	{
    //		base.TouchUpdate(touchPosition);
    //	}


    public override void TouchEnd(Vector2 touchPosition)
    {
        base.TouchEnd(touchPosition);

        Vector3 camCenter = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, Camera.main.transform.position.y));
        Vector3 currentPageCenter = menuLogic.GetCurrentPage().transform.position;

        // If the pan is a horizontal one => check for a horizontal quick swipe
        if (!logic.cam.disableHorizontalPan)
        {
            logic.cam.panVelocity = new Vector3(logic.cam.panVelocity.x, 0, 0);

            if (logic.cam.panVelocity.x >= goToNextPagePanSpeedThreshold && camCenter.x < currentPageCenter.x)
            {
                menuLogic.GoToPreviousPage();
                return;
            }
            else if (logic.cam.panVelocity.x <= -goToNextPagePanSpeedThreshold && camCenter.x > currentPageCenter.x)
            {
                menuLogic.GoToNextPage();
                return;
            }
        }
        // If the pan is a vertical one => check for a vertical quick swipe
        else if (!logic.cam.disableVerticalPan)
        {
            logic.cam.panVelocity = new Vector3(0, 0, logic.cam.panVelocity.z);

            if (logic.cam.panVelocity.z >= goToNextPagePanSpeedThreshold && camCenter.z < currentPageCenter.z)
            {
                menuLogic.GoToDownwardPage();
                return;
            }
            else if (logic.cam.panVelocity.z <= -goToNextPagePanSpeedThreshold && camCenter.z > currentPageCenter.z)
            {
                menuLogic.GoToUpwardPage();
                return;
            }
        }

        logic.cam.panVelocity = Vector3.zero;

        // This will be called if none of the above test was true => default behaviour = set the currently most visible page as the current one 
        menuLogic.SetCurrentPageBasedOnCamPosition();
    }

    #endregion
}
