using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StorePageEntity : PanningEntity
{

    public Global.Difficulty difficulty;

    public Vector2 index;

    public float goToNextPagePanSpeedThreshold;

    protected StoreLogic storeLogic;

    public bool notAccessible = false;


    //	// Grids buttons placement
    //	public Rect buttonsPlacementZone = new Rect(-0.5f, -0.42f, 0.5f, 0.38f);
    //	public float horizontalSpaceBetweenGridButtons = 0.05f;


    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();

        // iPhone version
        if (!Global.Global.iPadVersion)
        {
            if (index.y == 2) // size 2 => destroy the corresponding pages as there is only size 0 and 1 in iPhone version
            {
                DestroyImmediate(transform.parent.gameObject);
            }
            else if (index.y == 3) // Special => set as index 2
            {
                index = new Vector2(index.x, 2);
            }
        }

        storeLogic = logic as StoreLogic;
    }


    protected override void Start()
    {
        base.Start();
    }


    public void Init()
    {
        storeLogic.pages[(int)index.y][(int)index.x] = this;

        Vector2 fitToCamView = OrthoCam.GetFitToCamViewMultipliers(true);

        transform.localScale = new Vector3(fitToCamView.x * 0.5f, transform.localScale.y, fitToCamView.y * 0.5f);
        transform.parent.position = new Vector3(transform.localScale.x * index.x, transform.position.y, -transform.localScale.z * index.y);

        // Set the difficultyString according to the difficulty of the grids
        string difficultyString = "";
        if (difficulty == Global.Difficulty.Easy)
        {
            difficultyString = "F";
        }
        else if (difficulty == Global.Difficulty.Medium)
        {
            difficultyString = "M";
        }
        else if (difficulty == Global.Difficulty.Hard)
        {
            difficultyString = "D";
        }
        else if (difficulty == Global.Difficulty.All)
        {
            difficultyString = "T";
        }


        // Set the sizeString according to the size of the grids
        string sizeString = "";
        if (Global.Global.iPadVersion)
        {
            if (index.y == 0)
            {
                sizeString = "9x13";
            }
            else if (index.y == 1)
            {
                sizeString = "13x9";
            }
            else if (index.y == 2)
            {
                sizeString = "18x18";
            }
            // Dev || 06-06-2020 || Code Change
            // else if (index.y == 3)
            // {
            //     sizeString = "8X5";
            // }
            // else if (index.y == 4)
            // {
            //     sizeString = "10X10";
            // }
            else //if (index.y == 3)
            {
                sizeString = "Special";
            }
        }
        else
        {
            if (index.y == 0)
            {
                sizeString = "8x5";
            }
            else if (index.y == 1)
            {
                sizeString = "10x10";
            }
            else if (index.y == 2)
            {
                sizeString = "Special";
            }
        }
        //TODO || GRID 12


        // Instantiate the appropriate title for this page
        GameObject titleGO;
        if (sizeString == "Special")
        {
            titleGO = Instantiate(Resources.Load("Prefabs/Menu/Pages/Titles/Title" + sizeString)) as GameObject;
        }
        else
        {
            titleGO = Instantiate(Resources.Load("Prefabs/Menu/Pages/Titles/Title" + difficultyString + sizeString)) as GameObject;
        }

        titleGO.transform.parent = transform.parent;
        if (Global.Global.iPadVersion)
            titleGO.transform.localPosition = new Vector3(0.065f, titleGO.transform.position.y, -0.09f);
        else
            titleGO.transform.localPosition = new Vector3(0.05f, titleGO.transform.position.y, -0.1f);


        // Set the correct mat for the buttons
        for (int i = 0; i < transform.parent.childCount; i++)
        {
            Transform child = transform.parent.GetChild(i);
            ButtonStoreItem button = child.GetComponent("ButtonStoreItem") as ButtonStoreItem;

            if (button != null)
            {
                if (sizeString == "Special")
                {
                    button.transform.Find("StateIcon").GetComponent<Renderer>().material = Resources.Load("Materials/Store/Items/StateIcon" + sizeString) as Material;
                }
                else
                {
                    button.transform.Find("StateIcon").GetComponent<Renderer>().material = Resources.Load("Materials/Store/Items/StateIcon" + difficultyString) as Material;
                }
            }
        }


        if (notAccessible)
        {
            titleGO.SetActiveRecursively(false);
        }
    }


    //	public void Init(Vector2 index, List<string> grids, Global.Difficulty difficulty)
    //	{
    //		this.index = index;
    //		this.difficulty = difficulty;
    //		
    //		transform.parent.name = "Page [" + (int)index.x + "," + (int)index.y + "]";
    //		
    //		
    //		// Set the difficultyString according to the difficulty of the grids
    //		string difficultyString = "";
    //		if (difficulty == Global.Difficulty.Easy)
    //		{
    //			difficultyString = "F";
    //		}
    //		else if (difficulty == Global.Difficulty.Medium)
    //		{
    //			difficultyString = "M";
    //		}
    //		else if (difficulty == Global.Difficulty.Hard)
    //		{
    //			difficultyString = "D";
    //		}
    //		
    //		
    //		// Set the sizeString according to the size of the grids
    //		string sizeString = "";
    //		if (Global.Global.iPadVersion)
    //		{
    //			if (index.y == 0)
    //			{
    //				sizeString = "9x13";
    //			}
    //			else if (index.y == 1)
    //			{
    //				sizeString = "13x9";
    //			}
    //			else // if (index.y == 2)
    //			{
    //				sizeString = "18x18";
    //			}
    //		}
    //		else
    //		{
    //			if (index.y == 0)
    //			{
    //				sizeString = "8x5";
    //			}
    //			else // if (index.y == 1)
    //			{
    //				sizeString = "10x10";
    //			}
    //		}
    //		
    //		Vector2 fitToCamView = OrthoCam.GetFitToCamViewMultipliers(true);
    //		
    //		transform.localScale = new Vector3(fitToCamView.x, transform.localScale.y, fitToCamView.y);
    //		transform.parent.position = new Vector3(transform.localScale.x * index.x, transform.position.y, -transform.localScale.z * index.y);
    //		
    //		
    //		// Instantiate the appropriate title for this page
    //		GameObject titleGO = Instantiate(Resources.Load("Prefabs/Store/Pages/Titles/Title" + difficultyString + sizeString)) as GameObject;
    //		titleGO.transform.parent = transform.parent;
    //		if (Global.Global.iPadVersion)
    //			titleGO.transform.localPosition = new Vector3(0.01f, titleGO.transform.position.y, -0.04f);
    //		else
    //			titleGO.transform.localPosition = titleGO.transform.position;
    //			
    //		
    //		float nbLinesAsFloat = (grids.Count / storeLogic.maxGridsButtonPerPage.x);
    //		int nbCompleteLines = (int)nbLinesAsFloat;
    //		int nbElemsOnIncompleteLine = (int)((nbLinesAsFloat - nbCompleteLines) * storeLogic.maxGridsButtonPerPage.x);
    //		
    //		// Compute the size of the "slots" assigned for each button depending on the number of buttons and the buttonsPlacementZone
    //		Vector2 spaceAllowedForAGridButton = new Vector2((storeLogic.buttonsPlacementZone.width - storeLogic.buttonsPlacementZone.x) / storeLogic.maxGridsButtonPerPage.x, (storeLogic.buttonsPlacementZone.height - storeLogic.buttonsPlacementZone.y) / storeLogic.maxGridsButtonPerPage.y);
    //
    //		// Compute the actual size of the buttons depending on the size of their slots and the horizontal space between them
    //		Vector2 sizeOfAGridButton = new Vector2(spaceAllowedForAGridButton.x - storeLogic.horizontalSpaceBetweenGridButtons, (spaceAllowedForAGridButton.x - storeLogic.horizontalSpaceBetweenGridButtons));// * fitToCamView.x);
    //		if (sizeOfAGridButton.x > spaceAllowedForAGridButton.x)
    //		{
    //			Debug.LogWarning("Grids buttons horizontal size is bigger than the space allowed for it");
    //			sizeOfAGridButton.x = spaceAllowedForAGridButton.x;
    //		}
    //		if (sizeOfAGridButton.y > spaceAllowedForAGridButton.y)
    //		{
    //			Debug.LogWarning("Grids buttons vertical size is bigger than the space allowed for it");
    //			sizeOfAGridButton.y = spaceAllowedForAGridButton.y;
    //		}
    //		
    //		int j = 0;
    //		int currentButtonIndex;
    //		
    //		string currentGrid = PlayerPrefs.GetString("CurrentGrid");
    //		
    //		// Grids buttons creation; handles only the COMPLETE LINES
    //		for (int i = 0; i < storeLogic.maxGridsButtonPerPage.x ; i++)
    //		{
    //			for (j = 0; j < nbCompleteLines; j++)
    //			{
    //				currentButtonIndex = (int)(j * storeLogic.maxGridsButtonPerPage.x + i);
    //					
    //				ButtonStoreItem button = (Instantiate(Resources.Load("Prefabs/Buttons/ButtonStoreItem")) as GameObject).GetComponent("ButtonStoreItem") as ButtonStoreItem;
    //				button.name = "GridButton [" + i + "," + j + "]";
    //				
    //				button.transform.parent = transform.parent;
    //				button.transform.localScale = new Vector3(sizeOfAGridButton.x , 1, sizeOfAGridButton.y);
    //				button.transform.localPosition = new Vector3(storeLogic.buttonsPlacementZone.x + spaceAllowedForAGridButton.x * (i + 0.5f), button.transform.localPosition.y, storeLogic.buttonsPlacementZone.height - spaceAllowedForAGridButton.y * (j + 0.5f));
    //			
    //				button.myPage = this;
    //				button.associatedGrid = grids[currentButtonIndex];
    //				
    //				// If the currentGrid is associated to this button, then make this page as the currentPage
    //				if (button.associatedGrid == currentGrid)
    //				{
    //					storeLogic.currentPage = index;
    //				}
    //				
    //				button.idText.text = button.associatedGrid.Remove(0, logic.platform.Length + 5);
    //
    ////				int tintInt = 0;
    ////				// Vertical index of this page is even => "normal" tint pattern
    ////				if ((int)index.y % 2 == 0)
    ////				{
    ////					tintInt = (currentButtonIndex / 5) + 1;
    ////				}
    ////				// Vertical index of this page is odd => "mirror" tint pattern
    ////				else
    ////				{
    //////					tintInt = ((int)((storeLogic.maxGridsButtonPerPage.y - j) * storeLogic.maxGridsButtonPerPage.x - (storeLogic.maxGridsButtonPerPage.x - i)) / 5) + 1;
    ////					tintInt = ((int)((storeLogic.maxGridsButtonPerPage.y - j) * storeLogic.maxGridsButtonPerPage.x - i - 1) / 5) + 1;
    ////				}
    ////
    ////				button.normalMaterial = Resources.Load("Materials/Menu/GridsButtons/GridsButtonsNormal" + difficultyString + tintInt) as Material;
    ////				button.normalMaterial = Resources.Load("Materials/Menu/GridsButtons/GridsButtonsNormal" + difficultyString + tintInt) as Material;
    //				
    //				
    //				GameObject stateIconGO = button.transform.Find("StateIcon").gameObject;
    //				// This grid exists in the player prefs
    //				if (PlayerPrefs.HasKey(grids[currentButtonIndex]))
    //				{
    //					int gridState = PlayerPrefs.GetInt(grids[currentButtonIndex]);
    //					
    //					if (gridState == (int)Global.GridState.Started)
    //					{
    //						stateIconGO.renderer.material = Resources.Load("Materials/Menu/GridsButtons/StateIconStarted" + difficultyString) as Material;
    //					}
    //					else if (gridState == (int)Global.GridState.Completed)
    //					{
    //						stateIconGO.renderer.material = Resources.Load("Materials/Menu/GridsButtons/StateIconCompleted" + difficultyString) as Material;
    //					}
    //					else
    //					{
    //						stateIconGO.renderer.material = Resources.Load("Materials/Menu/GridsButtons/StateIconNew" + difficultyString) as Material;
    //					}
    //				}
    //				// Else it's certain that it's empty
    //				else
    //				{
    //					stateIconGO.renderer.material = Resources.Load("Materials/Menu/GridsButtons/StateIconNew" + difficultyString) as Material;
    //				}
    //			}
    //		}
    //		
    //		// Last pass for the remaining buttons (INCOMPLETE LINE)
    //		for (int i = 0; i < nbElemsOnIncompleteLine ; i++)
    //		{
    //			currentButtonIndex = (int)(j * storeLogic.maxGridsButtonPerPage.x + i);
    //			
    //			ButtonStoreItem button = (Instantiate(Resources.Load("Prefabs/Buttons/ButtonStoreItem")) as GameObject).GetComponent("ButtonStoreItem") as ButtonStoreItem;
    //			button.name = "GridButton [" + i + "," + j + "]";
    //			
    //			button.transform.parent = transform.parent;
    //			button.transform.localScale = new Vector3(sizeOfAGridButton.x , 1, sizeOfAGridButton.y);
    //			button.transform.localPosition = new Vector3(storeLogic.buttonsPlacementZone.x + spaceAllowedForAGridButton.x * (i + 0.5f), button.transform.localPosition.y, storeLogic.buttonsPlacementZone.height - spaceAllowedForAGridButton.y * (j + 0.5f));
    //		
    //			button.myPage = this;
    //			button.associatedGrid = grids[(int)(j * storeLogic.maxGridsButtonPerPage.x + i)];
    //			
    //			// If the currentGrid is associated to this button, then make this page as the currentPage
    //			if (button.associatedGrid == currentGrid)
    //			{
    //				storeLogic.currentPage = index;
    //			}
    //				
    //			button.idText.text = button.associatedGrid.Remove(0, logic.platform.Length + 5);
    //			
    ////			int tintInt = (currentButtonIndex / 5) + 1;
    ////			button.normalMaterial = Resources.Load("Materials/Menu/GridsButtons/GridsButtonsNormal" + difficultyString + tintInt) as Material;
    //			
    //			GameObject stateIconGO = button.transform.Find("StateIcon").gameObject;
    //			// This grid exists in the player prefs
    //			if (PlayerPrefs.HasKey(grids[currentButtonIndex]))
    //			{
    //				int gridState = PlayerPrefs.GetInt(grids[currentButtonIndex]);
    //				
    //				if (gridState == (int)Global.GridState.Started)
    //				{
    //					stateIconGO.renderer.material = Resources.Load("Materials/Menu/GridsButtons/StateIconStarted" + difficultyString) as Material;
    //				}
    //				else if (gridState == (int)Global.GridState.Completed)
    //				{
    //					stateIconGO.renderer.material = Resources.Load("Materials/Menu/GridsButtons/StateIconCompleted" + difficultyString) as Material;
    //				}
    //				else
    //				{
    //					stateIconGO.renderer.material = Resources.Load("Materials/Menu/GridsButtons/StateIconNew" + difficultyString) as Material;
    //				}
    //			}
    //			// Else it's certain that it's empty
    //			else
    //			{
    //				stateIconGO.renderer.material = Resources.Load("Materials/Menu/GridsButtons/StateIconNew" + difficultyString) as Material;
    //			}
    //		}
    //		
    //	}


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    // USEFUL TO DEBUG POINTS IN 3D SPACE
    void OnDrawGizmosSelected()
    {

    }


    protected override void StartPan(Vector2 touchPosition)
    {
        base.StartPan(touchPosition);

        logic.cam.ComputePanBorders();

        // If the finger swipe is mostly horizontal => consider it as horizontal
        if (Mathf.Abs(touchPosition.x - initialTouchPosition.x) >= Mathf.Abs(touchPosition.y - initialTouchPosition.y))
        {
            logic.cam.disableVerticalPan = true;
        }
        // Else consider it as vertical
        else
        {
            logic.cam.disableHorizontalPan = true;
        }
    }


    #region Input Management

    //	public override void TouchBegin(Vector2 touchPosition)
    //	{		
    //		base.TouchBegin(touchPosition);
    //		
    //	}
    //	
    //	
    //	public override void TouchUpdate(Vector2 touchPosition)
    //	{
    //		base.TouchUpdate(touchPosition);
    //	}


    public override void TouchEnd(Vector2 touchPosition)
    {
        base.TouchEnd(touchPosition);

        Vector3 camCenter = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, Camera.main.transform.position.y));
        Vector3 currentPageCenter = storeLogic.GetCurrentPage().transform.position;

        // If the pan is a horizontal one => check for a horizontal quick swipe
        if (!logic.cam.disableHorizontalPan)
        {
            logic.cam.panVelocity = new Vector3(logic.cam.panVelocity.x, 0, 0);

            if (logic.cam.panVelocity.x >= goToNextPagePanSpeedThreshold && camCenter.x < currentPageCenter.x)
            {
                storeLogic.GoToPreviousPage();
                return;
            }
            else if (logic.cam.panVelocity.x <= -goToNextPagePanSpeedThreshold && camCenter.x > currentPageCenter.x)
            {
                storeLogic.GoToNextPage();
                return;
            }
        }
        // If the pan is a vertical one => check for a vertical quick swipe
        else if (!logic.cam.disableVerticalPan)
        {
            logic.cam.panVelocity = new Vector3(0, 0, logic.cam.panVelocity.z);

            if (logic.cam.panVelocity.z >= goToNextPagePanSpeedThreshold && camCenter.z < currentPageCenter.z)
            {
                storeLogic.GoToDownwardPage();
                return;
            }
            else if (logic.cam.panVelocity.z <= -goToNextPagePanSpeedThreshold && camCenter.z > currentPageCenter.z)
            {
                storeLogic.GoToUpwardPage();
                return;
            }
        }

        logic.cam.panVelocity = Vector3.zero;

        // This will be called if none of the above test was true => default behaviour = set the currently most visible page as the current one 
        storeLogic.SetCurrentPageBasedOnCamPosition();
    }

    #endregion
}
