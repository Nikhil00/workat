using UnityEngine;
using System.Collections;

public class PanningEntity : BaseEntity
{


    protected Vector3 initialTouchPosition3D, lastTouchPosition3D, touchPosition3D;

    protected bool isPanning = false;

    public int panActivationDistance;
    public float panDeactivationMinimumVelocity = 0.01f;
    public float panUsePreviousFrameThreshold;

    public float pickAutoActivationDelay;

    public bool alwaysPan = false;


    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
    }


    protected override void Start()
    {
        base.Start();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }


    // USEFUL TO DEBUG POINTS IN 3D SPACE
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(initialTouchPosition3D, 0.1F);

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(lastTouchPosition3D, 0.1F);

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(touchPosition3D, 0.1F);
    }


    protected void Pan(Vector2 touchPosition)
    {
        //		Debug.Log(initialTouchPosition + " - " + touchPosition);

        // Activate panning if the touch drag exceeded panActivationDistance
        if ((!logic.cam.isPanning) && Vector3.Distance(initialTouchPosition, touchPosition) > panActivationDistance && !(logic.cam.disableVerticalPan && logic.cam.disableHorizontalPan) && Input.touchCount < 2)
        {
            Debug.Log("PAN -> " + Vector3.Distance(initialTouchPosition, touchPosition) + " > " + panActivationDistance);
            StartPan(touchPosition);
        }

        if (logic.cam.isPanning)
        {
            if (touchPosition != lastTouchPosition)
            {
                logic.cam.panPreviousVelocity = logic.cam.panVelocity;

                lastTouchPosition3D = Camera.main.ScreenToWorldPoint(new Vector3(lastTouchPosition.x, lastTouchPosition.y, Camera.main.transform.position.y));
                touchPosition3D = Camera.main.ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, Camera.main.transform.position.y));
                logic.cam.panVelocity = touchPosition3D - lastTouchPosition3D;
                logic.cam.Move();
            }

#if UNITY_ANDROID
#else
            else
            {
                logic.cam.panVelocity = Vector3.zero;
            }
#endif
        }
    }


    protected virtual void StartPan(Vector2 touchPosition)
    {
        logic.cam.isPanning = true;
        isPanning = true;

        logic.cam.panPreviousVelocity = Vector3.zero;
        logic.cam.panVelocity = Vector3.zero;
    }


    protected virtual void StopPan()
    {
        //		Debug.Log("velocity: " + logic.cam.panVelocity.x);
        //		Debug.Log("previous velocity: " + logic.cam.panPreviousVelocity.x);

        if (Mathf.Abs(logic.cam.panVelocity.x) <= panDeactivationMinimumVelocity && Mathf.Abs(logic.cam.panVelocity.z) <= panDeactivationMinimumVelocity)
        {
            // If previous frame was fast => use the previous frame velocity, it's probably an error from the input
            if (Mathf.Abs(logic.cam.panPreviousVelocity.x) - Mathf.Abs(logic.cam.panVelocity.x) >= panUsePreviousFrameThreshold || Mathf.Abs(logic.cam.panPreviousVelocity.z) - Mathf.Abs(logic.cam.panVelocity.z) >= panUsePreviousFrameThreshold)
            {
                Debug.Log("Pan ALMOST deactivated: velocity too low but used the previous frame's one");
                Debug.Log("Previous frame velocity: " + logic.cam.panPreviousVelocity);
                logic.cam.panVelocity = logic.cam.panPreviousVelocity;
            }
            else
            {
                Debug.Log("Pan deactivated: velocity too low at touch end");
                logic.cam.panVelocity = Vector3.zero;
            }
        }

        isPanning = false;
        logic.cam.isPanning = false;
        logic.cam.panVelocity *= logic.cam.panSpeedInertiaMultiplier;

        return;
    }


    #region Input Management

    public override void TouchBegin(Vector2 touchPosition)
    {
        base.TouchBegin(touchPosition);

        //		if (logic.isGameLogic() && (logic as GameLogic).guiCam.isMenuOpen)
        if (logic.GetLogicType() == Global.LogicType.GameLogic && (logic as GameLogic).guiCam.isMenuOpen)
        {
            (logic as GameLogic).guiCam.StartMenuPanelAnimation();
        }

        logic.cam.panPreviousVelocity = Vector3.zero;
        logic.cam.panVelocity = Vector3.zero;

        initialTouchPosition3D = Camera.main.ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, Camera.main.transform.position.y));
    }


    public override void TouchUpdate(Vector2 touchPosition)
    {
        if (logic.inputBlocked || dontAcceptInputs)
            return;

        if (logic.cam.isPanning && !isPanning)
        {
            return;
        }

        // If the time frame allowed to enable the pan isn't over, check if the user wants to pan and if so do it
        if (alwaysPan || (!logic.cam.disableHorizontalPan || !logic.cam.disableVerticalPan) && (touchStartTime + pickAutoActivationDelay > Time.time || isPanning))
        {
            Pan(touchPosition);
        }
        // If the time frame allowed to enable the pan is over, enable the focus on the definition
        else
        {
            base.TouchUpdate(touchPosition);
        }


        lastTouchPosition = touchPosition;
    }


    public override void TouchEnd(Vector2 touchPosition)
    {
        if (logic.inputBlocked || dontAcceptInputs)
        {
            return;
        }

        if (isPanning)
        {
            StopPan();
        }
        else
        {
            base.TouchEnd(touchPosition);
        }
    }

    #endregion
}
