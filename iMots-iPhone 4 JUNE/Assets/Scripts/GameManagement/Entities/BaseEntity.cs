using UnityEngine;
using System.Collections;
using System;

//DEV_EDIT || BASE ENTITY || REMOVING REQUIRED COMPONENT CODE
//[RequireComponent (typeof (Collider))]

public class BaseEntity : MonoBehaviour
{

    protected Logic logic;

    public Vector2 lastTouchPosition = Vector2.zero, initialTouchPosition;

    protected float touchStartTime = 0f;

    public bool isAnchoredToTouch = true; // If false, when a tracker is created and linked to this object, it will not be anchored to this object

    protected bool dontAcceptInputs = false;

    protected bool triggerInputAlreadyExecuted = false;   // Used only when touches are supposed to act as triggers (they are taken into account only once during the TouchUpdates());
                                                          // true means that the input has already been executed and thus won't be executed again before a TouchEnd() occurs

    // Events
    public event Action<string> startAnimation;


    // Use this for initialization
    protected virtual void Awake()
    {
        logic = (Logic)FindObjectOfType(typeof(Logic));
    }


    protected virtual void Start()
    {
    }


    // Update is called once per frame
    protected virtual void Update()
    {

    }


    // USEFUL TO DEBUG POINTS IN 3D SPACE
    void OnDrawGizmosSelected()
    {

    }


    // Take care of unfocusing the currently focused definition if there is one
    protected virtual void UnfocusDefinition()
    {
        if ((logic as GameLogic).focusedDefinition != null && (logic as GameLogic).focusedDefinition != this)
        {
            (logic as GameLogic).focusedDefinition.Grow();
            (logic as GameLogic).guiCam.StartTabsAnimation();
        }
    }


    #region Input Management

    public virtual void TouchBegin(Vector2 touchPosition)
    {
        if (logic.inputBlocked)
        {
            dontAcceptInputs = true;
            return;
        }

        dontAcceptInputs = false;
        lastTouchPosition = touchPosition;
        initialTouchPosition = touchPosition;

        touchStartTime = Time.time;
    }


    public virtual void TouchUpdate(Vector2 touchPosition)
    {
        if (logic.inputBlocked || dontAcceptInputs)
            return;

        ActionsOnTouchUpdate(touchPosition);

        lastTouchPosition = touchPosition;
    }


    public virtual void TouchEnd(Vector2 touchPosition)
    {
        if (logic.inputBlocked || dontAcceptInputs)
        {
            return;
        }

        ActionsOnTouchEnd(touchPosition);
    }


    public virtual void MultiTouchBegin(Vector2[] touchPositions)
    {

    }


    public virtual void MultiTouchUpdate(Vector2[] touchPositions)
    {

    }


    public virtual void MultiTouchEnd(Vector2[] touchPositions)
    {

    }


    public virtual void DoubleTapBegin(Vector2 touchPosition)
    {

    }


    public virtual void DoubleTapEnd(Vector2 touchPosition)
    {
    }


    public virtual void DoubleTapUpdate(Vector2 touchPosition)
    {
    }


    public virtual void TouchEndNotAnchored(Vector2 touchPosition)
    {
    }


    public virtual void TouchEndBecauseOfMultitouch(Vector2 touchPosition)
    {
    }


    protected virtual void ActionsOnTouchUpdate(Vector2 touchPosition)
    {
        if (!triggerInputAlreadyExecuted)
        {
            ActionsOnTouchUpdateOnlyOnce(touchPosition);
        }
    }


    protected virtual void ActionsOnTouchUpdateOnlyOnce(Vector2 touchPosition)
    {
        //		if (logic.GetLogicType() == Global.LogicType.GameLogic)
        //		{
        //			UnfocusDefinition();
        //		}
        triggerInputAlreadyExecuted = true;

        if (logic.GetLogicType() == Global.LogicType.GameLogic)
        {
            GameLogic gameLogic = logic as GameLogic;

            if (gameLogic.waitingForEOGA)
            {
                gameLogic.StartEndOfGridAnim();
            }
        }
    }


    protected virtual void ActionsOnTouchEnd(Vector2 touchPosition)
    {
        // If !triggerInputAlreadyExecuted, this means that the touch has been released too quickly to be able to give the Grow a chance to be executed => execute it now
        if (!triggerInputAlreadyExecuted)
            ActionsOnTouchUpdateOnlyOnce(touchPosition);

        triggerInputAlreadyExecuted = false;

        if (logic.GetLogicType() == Global.LogicType.GameLogic)
        {
            GameLogic gameLogic = logic as GameLogic;

            if (gameLogic.endOfGridAnim != null && gameLogic.endOfGridAnim.playingEndAnims && Time.time >= gameLogic.endOfGridAnim.endAnimsStartTime + 0.5f)
            {
                gameLogic.endOfGridAnim.StopAnim();
            }

            UnfocusDefinition();
        }
    }

    #endregion


    #region Callbacks

    public virtual void StartAnimations()
    {
        if (startAnimation != null)
            startAnimation("");
    }


    public virtual void StartAnimation(string animationToPlay)
    {
        if (startAnimation != null)
            startAnimation(animationToPlay);
    }


    public virtual void AnimationStopped(string animation)
    {

    }

    #endregion
}
