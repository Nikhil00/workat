using UnityEngine;
using System.Collections;


public abstract class LetterEntity : GridElementEntity
{

    // Materials
    public Material normalMat, movedMat, correctMat, blinkingMat;
    public Material textMat;

    public bool isCorrect = false;

    protected bool alreadyMoved = false;

    // Blink properties
    public bool isBlinking = false;
    private float blinkStartTime;
    private const float blinkDuration = 0.35f;
    public int nbBlinks = 9;
    private int currentNbBlink;
    protected Material blinkBackupMat;

    public bool isBeingDestroyed = false;
    protected float fadeOutStartValue, fadeOutTextStartValue;
    public bool isBeingCreated = false;
    protected float fadeInEndValue, fadeInTextEndValue;

    // End of grid anims \\
    // Grenade
    [HideInInspector]
    protected bool isExploding = false;
    protected float explosionStartTime;
    [HideInInspector]
    public float explosionDuration, explosionColliderSizeMultiplier;
    protected float colliderStartSize, colliderEndSize;
    // RotateZ
    [HideInInspector]
    protected bool isRotatingZ = false;
    [HideInInspector]
    protected float rotateZSpeed;
    // Grow
    protected bool isGrowing = false, isActuallyGrowing = true;
    protected float growStartTime, growDuration = 1, growStartSize, growEndSize;


    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
    }


    protected override void Start()
    {
        base.Start();
    }


    public override void InitGridElement()
    {
        base.InitGridElement();
        LetterTile myTile = (gameLogic.grid.tiles[(int)gridPosition.y, (int)gridPosition.x] as LetterTile);
        textMesh.text = myTile.guessLetter;

        alreadyMoved = myTile.alreadyMoved;

        SetProperMaterial();
    }


    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (isBlinking)
        {
            if (Time.time >= blinkStartTime + blinkDuration * nbBlinks)
            {
                isBlinking = false;
                SetProperMaterial();
            }
            else
            {
                if (Time.time >= blinkStartTime + blinkDuration * currentNbBlink)
                {
                    if (currentNbBlink % 2 == 0)
                    {
                        GetComponent<Renderer>().material = blinkingMat;
                    }
                    else
                    {
                        GetComponent<Renderer>().material = blinkBackupMat;
                    }

                    currentNbBlink++;
                }
            }
        }

        if (isBeingDestroyed)
        {
            if (gameLogic.LetterEntitiesFadeStartTime + gameLogic.LetterEntitiesFadeDuration >= Time.time)
            {
                float lerpValue = (gameLogic.LetterEntitiesFadeDuration - (gameLogic.LetterEntitiesFadeStartTime + gameLogic.LetterEntitiesFadeDuration - Time.time)) / gameLogic.LetterEntitiesFadeDuration;
                GetComponent<Renderer>().material.color = new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, Mathf.Lerp(fadeOutStartValue, 0, lerpValue));
                textMesh.GetComponent<Renderer>().material.color = new Color(textMesh.GetComponent<Renderer>().material.color.r, textMesh.GetComponent<Renderer>().material.color.g, textMesh.GetComponent<Renderer>().material.color.b, Mathf.Lerp(fadeOutTextStartValue, 0, lerpValue));
            }
            else
            {
                Destroy(gameObject);
            }
        }
        else if (isBeingCreated)
        {
            if (gameLogic.LetterEntitiesFadeStartTime + gameLogic.LetterEntitiesFadeDuration >= Time.time)
            {
                float lerpValue = (gameLogic.LetterEntitiesFadeDuration - (gameLogic.LetterEntitiesFadeStartTime + gameLogic.LetterEntitiesFadeDuration - Time.time)) / gameLogic.LetterEntitiesFadeDuration;
                GetComponent<Renderer>().material.color = new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, Mathf.Lerp(0, fadeInEndValue, lerpValue));
                textMesh.GetComponent<Renderer>().material.color = new Color(textMesh.GetComponent<Renderer>().material.color.r, textMesh.GetComponent<Renderer>().material.color.g, textMesh.GetComponent<Renderer>().material.color.b, Mathf.Lerp(0, fadeInTextEndValue, lerpValue));
            }
            else
            {
                isBeingCreated = false;

                SetProperMaterial();
                textMesh.GetComponent<Renderer>().material = textMat;
            }
        }


        // End of grid anims
        if (isExploding)
        {
            if (explosionStartTime + explosionDuration >= Time.time)
            {
                float lerpValue = (explosionDuration - (explosionStartTime + explosionDuration - Time.time)) / explosionDuration;
                (GetComponent<Collider>() as SphereCollider).radius = Mathf.Lerp(colliderStartSize, colliderEndSize, lerpValue);
            }
            else
            {
                isExploding = false;
            }
        }

        if (isRotatingZ)
        {
            transform.RotateAroundLocal(Vector3.back, rotateZSpeed * Time.deltaTime);
        }

        if (isGrowing)
        {
            if (growStartTime + growDuration >= Time.time)
            {
                float lerpValue = (growDuration - (growStartTime + growDuration - Time.time)) / growDuration;
                float lerpOutput;

                if (isActuallyGrowing)
                {
                    lerpOutput = Mathf.Lerp(growStartSize, growEndSize, lerpValue);
                }
                else
                {
                    lerpOutput = Mathf.Lerp(growEndSize, growStartSize, lerpValue);
                }

                transform.localScale = new Vector3(lerpOutput, 1, lerpOutput);
            }
            else
            {
                isActuallyGrowing = !isActuallyGrowing;
                growStartTime = Time.time;
            }

        }
    }


    public void StartBlinking()
    {
        isBlinking = true;
        blinkStartTime = Time.time;
        currentNbBlink = 0;
        SetProperMaterial();

        blinkBackupMat = GetComponent<Renderer>().material;
    }


    #region End Of Grid Anims

    public void StartExploding()
    {
        isExploding = true;

        explosionStartTime = Time.time;
        colliderStartSize = (GetComponent<Collider>() as SphereCollider).radius;
        colliderEndSize = colliderStartSize * explosionColliderSizeMultiplier;
    }


    public void StartRotatingZ()
    {
        isRotatingZ = true;

        rotateZSpeed = Random.Range(5, 15);
        int direction = Random.Range(0, 2);
        if (direction == 0)
            rotateZSpeed *= -1;

        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        GetComponent<Rigidbody>().isKinematic = true;
    }


    public void StartGrow()
    {
        isGrowing = true;
        isActuallyGrowing = true;

        growDuration = Random.Range(0.25f, 1.5f);

        growStartSize = transform.localScale.x;
        growEndSize = growStartSize * Random.Range(1.5f, 3f);

        growStartTime = Time.time;
    }

    #endregion


    // Set the given letter as the guessLetter and check if correct
    public void SetGuess(string letter, bool playSounds, bool checkForCombo)
    {
        textMesh.text = letter;

        (gameLogic.grid.tiles[(int)gridPosition.y, (int)gridPosition.x] as LetterTile).guessLetter = letter;

        if (checkForCombo)
        {
            Debug.Log("SETGUESS");
            if (PlayerPrefs.HasKey(gameLogic.grid.name) && PlayerPrefs.GetInt(gameLogic.grid.name) == (int)Global.GridState.Empty)
                PlayerPrefs.SetInt(gameLogic.grid.name, (int)Global.GridState.Started);
            gameLogic.CheckForWordsCombo(this);
        }

        gameLogic.CheckForCorrectGuess(this, true, playSounds);
    }


    // Set the entity state as correct
    public virtual void SetCorrect(bool correct, bool delayed)
    {
        bool resetDisplaySolutionToWordButton = false;
        if (isCorrect != correct)
            resetDisplaySolutionToWordButton = true;

        isCorrect = correct;

        alwaysPan = isCorrect;

        if (!delayed)
        {
            SetProperMaterial();
        }

        // If there is a focused def, this is a cheat to check if the word(s) associated to it are copletely correct; if so, the solution button will be hidden 
        if (correct && gameLogic.focusedDefinition != null && resetDisplaySolutionToWordButton)
        {
            gameLogic.DisplaySolutionToWordButton(false);
            gameLogic.DisplaySolutionToWordButton(true);
        }
    }


    // Set the entity state as alreadyMoved
    public void SetAlreadyMoved(bool didMove)
    {
        alreadyMoved = didMove;

        (gameLogic.grid.tiles[(int)gridPosition.y, (int)gridPosition.x] as LetterTile).alreadyMoved = alreadyMoved;

        if (gameLogic.gameMode == Global.GameMode.DragNDrop && gameLogic.gameDifficulty == Global.Difficulty.Hard)
            SetProperMaterial();
    }


    // Set the correct material for this entity according to its state (moved? correct?)
    public void SetProperMaterial()
    {
        if (isCorrect)
        {
            GetComponent<Renderer>().material = correctMat;

            FindObjectOfType<Dev_GameUIManager>().ResetGridPositions(gameObject);
            //DEV_EDIT || 27 OCT || GRID CORRECT CODE
        }
        else
        {
            if (gameLogic.gameMode == Global.GameMode.DragNDrop && gameLogic.gameDifficulty == Global.Difficulty.Hard && alreadyMoved)
                GetComponent<Renderer>().material = movedMat;
            else
                GetComponent<Renderer>().material = normalMat;
        }
    }


    // Take care of fading the entity out before destroying it
    public void FadeAndDestroy()
    {
        isBeingDestroyed = true;

        fadeOutStartValue = GetComponent<Renderer>().material.color.a;

        fadeOutTextStartValue = textMesh.GetComponent<Renderer>().material.color.a;

        GetComponent<Collider>().enabled = false;
    }


    // Take care of fading the entity in when changing mode
    public void FadeIn()
    {
        isBeingCreated = true;

        fadeInEndValue = GetComponent<Renderer>().material.color.a;

        fadeInTextEndValue = textMesh.GetComponent<Renderer>().material.color.a;

        GetComponent<Renderer>().material.color = new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, 0);
        textMesh.GetComponent<Renderer>().material.color = new Color(textMesh.GetComponent<Renderer>().material.color.r, textMesh.GetComponent<Renderer>().material.color.g, textMesh.GetComponent<Renderer>().material.color.b, 0);
    }


    // Unlock most of the rigidbody's constraints. Mostly used by the EndOfGridAnims to be able to play around with them
    public virtual void UnlockRigidBodyConstraints(bool freeCompletely)
    {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().isKinematic = false;
    }


    public override bool IsLetterEntity()
    {
        return true;
    }


    #region Collisions

    void OnCollisionEnter(Collision collision)
    {
        if (gameLogic.endOfGridAnim != null && gameLogic.endOfGridAnim.playingEndAnims)
        {
            LetterEntity hitLetterEntity = collision.gameObject.GetComponent("LetterEntity") as LetterEntity;

            switch (gameLogic.endOfGridAnim.endOfGridAnimType)
            {
                case Global.EndOfGridAnim.Waterfall:
                    if (hitLetterEntity != null)
                    {
                        hitLetterEntity.UnlockRigidBodyConstraints(false);
                    }
                    break;

                case Global.EndOfGridAnim.Helium:
                    if (hitLetterEntity != null && hitLetterEntity.GetComponent<ConstantForce>() == null)
                    {
                        hitLetterEntity.UnlockRigidBodyConstraints(false);
                    }
                    break;

                case Global.EndOfGridAnim.Grenade:
                    ;
                    if (hitLetterEntity != null)
                    {
                        hitLetterEntity.UnlockRigidBodyConstraints(false);
                        (gameLogic.endOfGridAnim as EndOfGridAnimGrenade).targetEntities.Remove(this);
                    }
                    break;

                case Global.EndOfGridAnim.Grow:
                    break;
            }
        }
    }

    #endregion


    #region Input Management

    protected override void ActionsOnTouchEnd(Vector2 touchPosition)
    {
        base.ActionsOnTouchEnd(touchPosition);
    }

    #endregion


    #region Callbacks


    public override void AnimationStopped(string animation)
    {
        base.AnimationStopped(animation);
    }

    #endregion
}
