using UnityEngine;
using System.Collections;

public class OrthoCam : MonoBehaviour
{

    #region Vars

    Logic logic;

    Camera myCam;

    // Screen rotations
    [HideInInspector]
    public bool changingOrientation = false;
    public bool autorotateToPortrait { get; protected set; }
    public bool autorotateToPortraitUpsideDown { get; protected set; }
    public bool autorotateToLandscapeLeft { get; protected set; }
    public bool autorotateToLandscapeRight { get; protected set; }


    // Pan properties
    public bool isPanning = false; // True when the player is actually panning, but false otherwise (even when there is some panning done by the application, like the comeback)
    public float panFriction = 0.99f;
    public float panSpeedInertiaMultiplier = 0.5f; // the velocity of the pan is multiplied by this when the player ends his pan
    public float panDeactivationSpeedThreshold;
    public Vector3 panVelocity = new Vector2(), panPreviousVelocity;
    public Rect panBorders = new Rect();
    public Rect panBordersOffset;
    public float panOutsideBorderMaxDistance = 5;
    public float panComebackSpeed = 0.7f; // Used when outside of the borderPans to reduce the panVelocity
    public float panComebackAnimationDuration = 0.3f;
    float xComebackVelocity = 0, yComebackVelocity = 0; // Used when the pan is actually coming back
    public bool disableHorizontalPan = false, disableVerticalPan = false;
    public Rect autoPanZoneLengthAsPercentage;
    public Rect autoPanZoneLength { get; protected set; }
    public int autoPanSpeed;
    public bool disablePan = false; // Override the others pan enabling/disabling; used mainly for temporary disabling of the panning

    // 3D helpers
    Vector3 leftBottomCorner, rightTopCorner;


    [HideInInspector]
    public int previousScreenHeight = 0;

    // Zoom properties
    public bool isZooming = false; // True when the player is actually zooming, but false otherwise (even when there is some zooming done by the application, like the comeback)
    public bool disableZoom = false;
    public float zoomClosest = 2, zoomFarthest = 10;
    public float zoomSpeed = 0.01f;
    public float defaultOrthoSize = 7;


    // Ads
#if UNITY_IOS
    // UnityEngine.iOS.ADBannerView adBannerView;// = new ADBannerView();
#elif UNITY_ANDROID
    public GameObject adMobAdapter;
#endif
    bool wantToDisplayAd = false;
    public bool pendingSetBannerView = false;
    bool waitedForOneUpdateSetBannerView = false;
    float bannerViewCheckLoadLastTime;
    const float bannerViewCheckLoadTimestep = 1;
    //	public GameObject buyPopupGO;

    #endregion


    void Awake()
    {
        logic = (Logic)FindObjectOfType(typeof(Logic));

        myCam = GetComponent<Camera>();

        //		myCam.orthographicSize = 0;

        if (Global.Global.iPadVersion)
        {
            // Nik || 04-06-2020 || Code Change
            // Debug.Log("Nik - Code change Add");
            // defaultOrthoSize = 10;
            defaultOrthoSize = 7;
        }
        else
            defaultOrthoSize = 2.85f;

        if (Global.Global.adEnabled)
        {
#if UNITY_IOS
            //			adBannerView = new UnityEngine.iOS.ADBannerView();
            //						adBannerView.position =  ADBannerView.Layout..Manual;
#endif
        }
    }


    // Use this for initialization
    void Start()
    {
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;

        if (logic.GetLogicType() == Global.LogicType.GameLogic)
        {
            GameLogic gameLogic = logic as GameLogic;
            //  Nik - Add Code
            // if ((Global.Global.iPadVersion && gameLogic.grid.size.size == Global.Size.Big) || (!Global.Global.iPadVersion && gameLogic.grid.size.size == Global.Size.Medium))
            // if ((Global.Global.iPadVersion && gameLogic.grid.size.size == Global.Size.Big) || (Global.Global.iPadVersion && gameLogic.grid.size.size == Global.Size.Medium))
            // {
                disableZoom = false;
            // }
            // else
            // {
            //     disableZoom = true;
            // }
        }
    }

    // Update is called once per frame
    void Update()
    {
        // ADBanner management
        if (Global.Global.adEnabled)
        {
            // Manages the ad orientation
            if (pendingSetBannerView)
            {
                if (waitedForOneUpdateSetBannerView)
                {
                    Debug.Log("SET BANNER VIEW");
#if UNITY_ANDROID
                    if (adMobAdapter != null)
                    {
                        //TODO || DEV2
                        /*AdMobAndroid.destroyBanner();
                        AdMobAndroid.createBanner(AdMobAndroidAd.smartBanner, 0, 1);*/
                    }
#endif
                    //					if (logic.GetLogicType() == Global.LogicType.GameLogic) // Only create the banner if we're ingame
                    //					{
                    //DEV_CODE || ADMOB || SET BANNER VIEW
                    //SetBannerView(); 
                    //					}

                    SetOrthoSize();
                    (logic as GameLogic).guiCam.SetOrthoSize();
                    pendingSetBannerView = false;
                    waitedForOneUpdateSetBannerView = false;
                }
                else
                {
                    Debug.Log("WAIT");
                    waitedForOneUpdateSetBannerView = true;
                }
            }

            if (wantToDisplayAd)
            {
#if UNITY_IOS
                //Manages the displaying of the ads
                //				if (!adBannerView.visible && Time.time >= bannerViewCheckLoadLastTime + bannerViewCheckLoadTimestep)
                //				{
                //					if (adBannerView.loaded && adBannerView.error == null)
                //					{
                ////						if (buyPopupGO.active)
                ////						{
                ////							Debug.Log("ADBanner loaded but not displaying because buyPopup is displayed");
                ////						}
                ////						else
                ////						{
                //							Debug.Log("ADBanner showing");
                //							adBannerView.Show();
                ////						}
                //					}
                //					else if (adBannerView.loaded && adBannerView.error != null)
                //					{
                //						Debug.Log("ADBanner error: " + adBannerView.error);
                //					}
                //					else 
                //					{
                //						Debug.Log("ADBanner not loaded");
                //					}
                //					
                //					bannerViewCheckLoadLastTime = Time.time;
                //				}
                //				else
                //				{
                //					if (adBannerView.error != null)
                //					{
                //						Debug.Log("ADBanner error while visible: " + adBannerView.error.code + "\n" + adBannerView.error.description + "\n" + adBannerView.error.reason);
                //						HideBannerView();
                ////						Destroy(adBannerView);
                ////						adBannerView = new ADBannerView();
                //// 	 					adBannerView.autoPosition = ADPosition.Manual;
                //						
                //						wantToDisplayAd = true;
                //					}
                //				}
#endif
            }
        }
    }


    // USEFUL TO DEBUG POINTS IN 3D SPACE
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(leftBottomCorner, 0.1F);
        Gizmos.DrawSphere(rightTopCorner, 0.1F);


        Gizmos.color = Color.red;
        Gizmos.DrawSphere(new Vector3(panBorders.x, 0, panBorders.y), 0.1F);
        Gizmos.DrawSphere(new Vector3(panBorders.width, 0, panBorders.height), 0.1F);
    }


    void FixedUpdate()
    {
        // Take care of handling screen rotations according to device rotation
        if (changingOrientation && previousScreenHeight != Screen.height)
        {
            changingOrientation = false;
            SetOrthoSize();

            // If we're ingame
            if (logic.GetLogicType() == Global.LogicType.GameLogic)
            {
                Debug.Log("FCKING CHANGING ORIENTATION");
                (logic as GameLogic).guiCam.SetOrthoSize();

                if (Global.Global.adEnabled)
                {
                    pendingSetBannerView = true;
                }
            }
            else if (logic.GetLogicType() == Global.LogicType.StoreLogic)
            {
                (logic as StoreLogic).inAppPopupMask.transform.localScale = new Vector3(7.68f * Camera.main.aspect, 1, 7.68f);
            }
        }

        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft && Screen.orientation != ScreenOrientation.LandscapeLeft && autorotateToLandscapeLeft)
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            changingOrientation = true;
            previousScreenHeight = Screen.height;
            Debug.Log("LandscapeLeft");
        }

        else if (Input.deviceOrientation == DeviceOrientation.LandscapeRight && Screen.orientation != ScreenOrientation.LandscapeRight && autorotateToLandscapeRight)
        {
            Screen.orientation = ScreenOrientation.LandscapeRight;
            changingOrientation = true;
            previousScreenHeight = Screen.height;
            Debug.Log("LandscapeRight");
        }

        else if (Input.deviceOrientation == DeviceOrientation.Portrait && Screen.orientation != ScreenOrientation.Portrait && autorotateToPortrait)
        {
            Screen.orientation = ScreenOrientation.Portrait;
            changingOrientation = true;
            previousScreenHeight = Screen.height;
            Debug.Log("Portrait");
        }

        else if (Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown && Screen.orientation != ScreenOrientation.PortraitUpsideDown && autorotateToPortraitUpsideDown)
        {
            Screen.orientation = ScreenOrientation.PortraitUpsideDown;
            changingOrientation = true;
            previousScreenHeight = Screen.height;
            Debug.Log("PortraitUpsideDown");
        }


        // This will be done only when the player releases his pan input
        if (!isPanning)
        {
            float panSpeed = panComebackSpeed;

            if (!disableHorizontalPan)
            {
                // If pan is out of horizontal panBorders, apply the come back force on x axis
                if (leftBottomCorner.x < panBorders.x)
                {
                    if (panVelocity.x >= -0.2)
                    {
                        panVelocity.x = 0;

                        transform.position = new Vector3(Mathf.SmoothDamp(transform.position.x, panBorders.x + (rightTopCorner.x - leftBottomCorner.x) * 0.5f, ref xComebackVelocity, panComebackAnimationDuration), transform.position.y, transform.position.z);

                        if (xComebackVelocity <= 0.01f)
                        {
                            transform.position = new Vector3(panBorders.x + (rightTopCorner.x - leftBottomCorner.x) * 0.5f, transform.position.y, transform.position.z);


                            if (xComebackVelocity == 0f)
                            {
                                if (logic.GetLogicType() == Global.LogicType.MenuLogic)
                                {
                                    MenuLogic menuLogic = (logic as MenuLogic);
                                    if (menuLogic.needToUpdateActivePagesAfterHorizontalMove)
                                        menuLogic.SetPendingManageActivePages();
                                }
                            }
                        }
                    }
                    else
                    {
                        panVelocity.x *= panSpeed;
                    }
                }
                else if (rightTopCorner.x > panBorders.width)
                {
                    if (panVelocity.x <= 0.2)
                    {
                        panVelocity.x = 0;

                        transform.position = new Vector3(Mathf.SmoothDamp(transform.position.x, panBorders.width - (rightTopCorner.x - leftBottomCorner.x) * 0.5f, ref xComebackVelocity, panComebackAnimationDuration), transform.position.y, transform.position.z);

                        if (xComebackVelocity >= -0.01f)
                        {
                            transform.position = new Vector3(panBorders.width - (rightTopCorner.x - leftBottomCorner.x) * 0.5f, transform.position.y, transform.position.z);

                            //							if (logic.GetLogicType() == Global.LogicType.MenuLogic)
                            //							{
                            //								MenuLogic menuLogic = (logic as MenuLogic);
                            //								if (menuLogic.needToUpdateActivePages)
                            //									menuLogic.SetPendingManageActivePages();
                            //							}

                            if (xComebackVelocity == 0f)
                            {
                                if (logic.GetLogicType() == Global.LogicType.MenuLogic)
                                {
                                    MenuLogic menuLogic = (logic as MenuLogic);
                                    if (menuLogic.needToUpdateActivePagesAfterHorizontalMove)
                                        menuLogic.SetPendingManageActivePages();
                                }
                            }
                        }
                    }
                    else
                    {
                        panVelocity.x *= panSpeed;
                    }
                }
                else
                {
                    //					if (logic.GetLogicType() == Global.LogicType.MenuLogic)
                    //					{
                    //						Debug.Log("WTF");
                    //						MenuLogic menuLogic = (logic as MenuLogic);
                    //						if (menuLogic.needToUpdateActivePagesAfterHorizontalMove)
                    //						{
                    //							Debug.Log("HEYO");
                    //							menuLogic.SetPendingManageActivePages();
                    //						}
                    //					}
                }
            }
            else
            {
                panVelocity.x = 0;
            }

            if (!disableVerticalPan)
            {
                // If pan is out of vertical panBorders, apply the come back force on y axis
                if (leftBottomCorner.z < panBorders.y)
                {
                    if (panVelocity.z >= -0.2)
                    {
                        panVelocity.z = 0;

                        transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.SmoothDamp(transform.position.z, panBorders.y + (rightTopCorner.z - leftBottomCorner.z) * 0.5f, ref yComebackVelocity, panComebackAnimationDuration));

                        if (yComebackVelocity <= 0.01f)
                        {
                            transform.position = new Vector3(transform.position.x, transform.position.y, panBorders.y + (rightTopCorner.z - leftBottomCorner.z) * 0.5f);

                            if (yComebackVelocity == 0f)
                            {
                                if (logic.GetLogicType() == Global.LogicType.MenuLogic)
                                {
                                    MenuLogic menuLogic = (logic as MenuLogic);
                                    if (menuLogic.needToUpdateActivePagesAfterVerticalMove)
                                        menuLogic.SetPendingManageActivePages();
                                }
                            }
                        }
                    }
                    else
                    {
                        panVelocity.z *= panSpeed;
                    }
                }
                else if (rightTopCorner.z > panBorders.height)
                {
                    if (panVelocity.z <= 0.2)
                    {
                        panVelocity.z = 0;

                        transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.SmoothDamp(transform.position.z, panBorders.height - (rightTopCorner.z - leftBottomCorner.z) * 0.5f, ref yComebackVelocity, panComebackAnimationDuration));

                        if (yComebackVelocity >= -0.01f)
                        {
                            transform.position = new Vector3(transform.position.x, transform.position.y, panBorders.height - (rightTopCorner.z - leftBottomCorner.z) * 0.5f);

                            if (yComebackVelocity == 0f)
                            {
                                if (logic.GetLogicType() == Global.LogicType.MenuLogic)
                                {
                                    MenuLogic menuLogic = (logic as MenuLogic);
                                    if (menuLogic.needToUpdateActivePagesAfterVerticalMove)
                                        menuLogic.SetPendingManageActivePages();
                                }
                            }
                        }
                    }
                    else
                    {
                        panVelocity.z *= panSpeed;
                    }
                }
                else
                {

                    //					if (logic.GetLogicType() == Global.LogicType.MenuLogic)
                    //					{
                    //						MenuLogic menuLogic = (logic as MenuLogic);
                    //						if (menuLogic.needToUpdateActivePagesAfterVerticalMove)
                    //						{
                    //							Debug.Log("HEYA");
                    //							menuLogic.SetPendingManageActivePages();
                    //						}
                    //					}
                }
            }
            else
            {
                panVelocity.z = 0;
            }
            // Apply the velocity to the gameOjects
            if (Mathf.Abs(panVelocity.x) > panDeactivationSpeedThreshold || Mathf.Abs(panVelocity.z) > panDeactivationSpeedThreshold)
            {

                Move();
                panVelocity *= panFriction;
            }
            else
            {
                //				if (logic.GetLogicType() == Global.LogicType.MenuLogic)
                //				{
                //					if (panVelocity.x != 0)
                //						Debug.Log(panVelocity.x);
                //					MenuLogic menuLogic = (logic as MenuLogic);
                //					if (menuLogic.needToUpdateActivePages)
                //						menuLogic.SetPendingManageActivePages();
                //				}
            }
        }
        else
        {
            yComebackVelocity = 0;
            xComebackVelocity = 0;
        }



    }


    public void Zoom(float offset)
    {
        if (disableZoom)
            return;


        float newSize = myCam.orthographicSize + offset * zoomSpeed;


        if (newSize < zoomClosest)
        {
            newSize = zoomClosest;
        }
        else if (newSize > zoomFarthest)
        {
            newSize = zoomFarthest;
        }

        myCam.orthographicSize = newSize;

        Debug.Log("ZOOM: ortho size now is " + newSize);

        SetCamCorners();
        ComputePanBorders();
    }


    #region Movement

    public void FocusViewOnTile(GridElementEntity tile)
    {
        Vector3 tilePos = tile.transform.position;
        Vector3 camDestPos = new Vector3(tilePos.x, transform.position.y, tilePos.z - tile.transform.localScale.z * 1.5f);

        MoveTo(camDestPos);
    }


    public void MoveTo(Vector3 newPos)
    {
        Rect isWithinPanBorders = IsWithinPanBorders(new Vector2(newPos.x, newPos.z));

        if (disableHorizontalPan)
        {
            newPos.x = transform.position.x;
        }
        else
        {
            if (isWithinPanBorders.x == 0)
            {
                newPos.x = panBorders.x + (rightTopCorner.x - leftBottomCorner.x) * 0.5f;
            }
            else if (isWithinPanBorders.width == 0)
            {
                newPos.x = panBorders.width - (rightTopCorner.x - leftBottomCorner.x) * 0.5f;
            }
        }

        if (disableVerticalPan)
        {
            newPos.z = transform.position.z;
        }
        else
        {
            if (isWithinPanBorders.y == 0)
            {
                newPos.z = panBorders.y + (rightTopCorner.z - leftBottomCorner.z) * 0.5f;
            }
            else if (isWithinPanBorders.height == 0)
            {
                newPos.z = panBorders.height - (rightTopCorner.z - leftBottomCorner.z) * 0.5f;
            }
        }

        transform.position = newPos;

        SetCamCorners();
    }


    public void Move()
    {
        Move(-panVelocity);
    }


    // Pan
    public void Move(Vector3 offset)
    {
        if (disablePan)
            return;

        if (disableHorizontalPan)
            offset.x = 0;
        if (disableVerticalPan)
            offset.z = 0;

        float reductionMultiplier;

        Rect isWithinPanBorders = IsWithinPanBorders(new Vector2(transform.position.x + offset.x, transform.position.z + offset.z));

        if (isWithinPanBorders.y == 0)
        {
            float dist = 1 / ((-panOutsideBorderMaxDistance) / (transform.position.z + offset.z - (panBorders.y + (rightTopCorner.z - leftBottomCorner.z) * 0.5f)));
            reductionMultiplier = 1 - dist;

            if (reductionMultiplier < 0)
                reductionMultiplier = 0;

            offset.z *= reductionMultiplier;
        }
        else if (isWithinPanBorders.height == 0)
        {
            float dist = 1 / ((panOutsideBorderMaxDistance) / (transform.position.z + offset.z - (panBorders.height - (rightTopCorner.z - leftBottomCorner.z) * 0.5f)));
            reductionMultiplier = 1 - dist;

            if (reductionMultiplier < 0)
                reductionMultiplier = 0;

            offset.z *= reductionMultiplier;
        }

        if (isWithinPanBorders.x == 0)
        {
            float dist = 1 / ((-panOutsideBorderMaxDistance) / (transform.position.x + offset.x - (panBorders.x + (rightTopCorner.x - leftBottomCorner.x) * 0.5f)));
            reductionMultiplier = 1 - dist;

            if (reductionMultiplier < 0)
                reductionMultiplier = 0;

            offset.x *= reductionMultiplier;
        }
        else if (isWithinPanBorders.width == 0)
        {
            float dist = 1 / ((panOutsideBorderMaxDistance) / (transform.position.x + offset.x - (panBorders.width - (rightTopCorner.x - leftBottomCorner.x) * 0.5f)));
            reductionMultiplier = 1 - dist;

            if (reductionMultiplier < 0)
                reductionMultiplier = 0;

            offset.x *= reductionMultiplier;
        }

        transform.position = new Vector3(transform.position.x + offset.x, transform.position.y, transform.position.z + offset.z);


        SetCamCorners();
    }


    // Checks if the given pos is within the autoPanZone and if so, autoPan in the according direction
    public void ManageAutoPan(Vector2 pos)
    {
        // Check if the camera is currently within the pan borders (otherwise we won't autoPan anyways)
        Rect isWithinPanBorders = IsWithinPanBorders(new Vector2(transform.position.x, transform.position.z));

        if (pos.x > Screen.width - autoPanZoneLength.width && isWithinPanBorders.width == 1)
        {
            Move(new Vector3(autoPanSpeed * Time.deltaTime, 0, 0));
        }
        else if (pos.x < autoPanZoneLength.x && isWithinPanBorders.x == 1)
        {
            Move(new Vector3(-autoPanSpeed * Time.deltaTime, 0, 0));
        }

        if (pos.y > Screen.height - autoPanZoneLength.height && isWithinPanBorders.height == 1)
        {
            Move(new Vector3(0, 0, autoPanSpeed) * Time.deltaTime);
        }
        else if (pos.y < autoPanZoneLength.y && isWithinPanBorders.y == 1)
        {
            Move(new Vector3(0, 0, -autoPanSpeed) * Time.deltaTime);
        }
    }

    #endregion


    #region Covered Zones Management


    // Compute the 3D position of the bottom left and top right corners of the cam
    void SetCamCorners()
    {
        leftBottomCorner = myCam.ScreenToWorldPoint(new Vector3(0, 0, myCam.transform.position.y));
        rightTopCorner = myCam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, myCam.transform.position.y));
    }


    // Compute the boundaries of the camera pan
    public void ComputePanBorders()
    {
        float additionalBottomPanBorderOffset = 0;

        //		if (logic.isGameLogic())
        if (logic.GetLogicType() == Global.LogicType.GameLogic)
        {
            GameLogic gameLogic = logic as GameLogic;



            if (gameLogic.gameMode == Global.GameMode.Keyboard)
            {
                additionalBottomPanBorderOffset = (gameLogic.guiCam.vKeyboardBgGO.transform.localScale.z / gameLogic.guiCam.GetComponent<Camera>().orthographicSize) * GetComponent<Camera>().orthographicSize;
            }
            else if (Global.Global.adEnabled) // DnD mode + ad enabled => set the additionalBottomPanBorderOffset so that the grid can be fully visible even with the ad showing
            {
#if UNITY_IOS
                //				additionalBottomPanBorderOffset = (UnityEngine.iOS.ADBannerView.GetSizeFromSizeIdentifier(adBannerView.currentSizeIdentifier).y / gameLogic.guiCam.GetComponent<Camera>().orthographicSize) * GetComponent<Camera>().orthographicSize;
                // iPad 3
                if (Global.Global.iPadVersion && Screen.width > 1024 && Screen.height > 1024)
                    additionalBottomPanBorderOffset *= 2;

                //				Debug.Log("additionalBottomPanBorderOffset for " + adBannerView.currentSizeIdentifier + " ADBanner orientation: " + additionalBottomPanBorderOffset);
#elif UNITY_ANDROID
                if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
                {
                    additionalBottomPanBorderOffset = (32f / gameLogic.guiCam.GetComponent<Camera>().orthographicSize) * GetComponent<Camera>().orthographicSize;
                    Debug.Log("additionalBottomPanBorderOffset for Landscape AdMob banner orientation: " + additionalBottomPanBorderOffset);
                }
                else
                {
                    additionalBottomPanBorderOffset = (50f / gameLogic.guiCam.GetComponent<Camera>().orthographicSize) * GetComponent<Camera>().orthographicSize;
                    Debug.Log("additionalBottomPanBorderOffset for Portrait AdMob banner orientation: " + additionalBottomPanBorderOffset);
                }

#endif

            }

            #region DEV_CODE
            //panBorders = new Rect(-gameLogic.gridGO.transform.localScale.x * gameLogic.grid.size.xSize * 0.5f + panBordersOffset.x, -gameLogic.gridGO.transform.localScale.z * gameLogic.grid.size.ySize * 0.5f + panBordersOffset.y - additionalBottomPanBorderOffset, gameLogic.gridGO.transform.localScale.x * gameLogic.grid.size.xSize * 0.5f + panBordersOffset.width, gameLogic.gridGO.transform.localScale.z * gameLogic.grid.size.ySize * 0.5f + panBordersOffset.height);

            //DEV_EDIT || GAME OVER ERROR
            if (gameLogic.gridGO)
            {
                panBorders = new Rect(-gameLogic.gridGO.transform.localScale.x * gameLogic.grid.size.xSize * 0.5f + panBordersOffset.x, -gameLogic.gridGO.transform.localScale.z * gameLogic.grid.size.ySize * 0.5f + panBordersOffset.y - additionalBottomPanBorderOffset, gameLogic.gridGO.transform.localScale.x * gameLogic.grid.size.xSize * 0.5f + panBordersOffset.width, gameLogic.gridGO.transform.localScale.z * gameLogic.grid.size.ySize * 0.5f + panBordersOffset.height);
            }
            else
            {
                Debug.Log("Object is null");
            }
            #endregion
        }
        else if (logic.GetLogicType() == Global.LogicType.MenuLogic)
        {
            MenuLogic menulogic = logic as MenuLogic;
            MenuPageEntity currentPage = menulogic.GetCurrentPage();

            if (isPanning)
            {
                panBorders = new Rect(-currentPage.transform.localScale.x * 0.5f + menulogic.pages[0][0].transform.position.x + panBordersOffset.x, -currentPage.transform.localScale.z * 0.5f + menulogic.pages[menulogic.pages.Length - 1][0].transform.position.z + panBordersOffset.y, currentPage.transform.localScale.x * 0.5f + menulogic.pages[0][menulogic.pages[0].Count - 1].transform.position.x + panBordersOffset.width, currentPage.transform.localScale.z * 0.5f + menulogic.pages[0][0].transform.position.z + panBordersOffset.height);
            }
            else
            {
                panBorders = new Rect(-currentPage.transform.localScale.x * 0.5f + currentPage.transform.position.x + panBordersOffset.x, -currentPage.transform.localScale.z * 0.5f + currentPage.transform.position.z + panBordersOffset.y, currentPage.transform.localScale.x * 0.5f + currentPage.transform.position.x + panBordersOffset.width, currentPage.transform.localScale.z * 0.5f + currentPage.transform.position.z + panBordersOffset.height);
            }
        }
        else// if (logic.GetLogicType() == Global.LogicType.StoreLogic)
        {
            StoreLogic storelogic = logic as StoreLogic;
            StorePageEntity currentPage = storelogic.GetCurrentPage();

            if (isPanning)
            {
                if (!Dev_StoreUIManager.instance.isUiElement) panBorders = new Rect(-currentPage.transform.localScale.x * 0.5f + storelogic.pages[0][0].transform.position.x + panBordersOffset.x, -currentPage.transform.localScale.z * 0.5f + storelogic.pages[storelogic.pages.Length - 1][0].transform.position.z + panBordersOffset.y, currentPage.transform.localScale.x * 0.5f + storelogic.pages[0][storelogic.pages[0].Count - 1].transform.position.x + panBordersOffset.width, currentPage.transform.localScale.z * 0.5f + storelogic.pages[0][0].transform.position.z + panBordersOffset.height);
            }
            else
            {
                if (!Dev_StoreUIManager.instance.isUiElement) panBorders = new Rect(-currentPage.transform.localScale.x * 0.5f + currentPage.transform.position.x + panBordersOffset.x, -currentPage.transform.localScale.z * 0.5f + currentPage.transform.position.z + panBordersOffset.y, currentPage.transform.localScale.x * 0.5f + currentPage.transform.position.x + panBordersOffset.width, currentPage.transform.localScale.z * 0.5f + currentPage.transform.position.z + panBordersOffset.height);
            }
        }

        if (rightTopCorner.x - leftBottomCorner.x > panBorders.width - panBorders.x)
        {
            //			Debug.Log("Horizontal pan disabled");
            disableHorizontalPan = true;
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
        }
        else
        {
            disableHorizontalPan = false;
        }
        if (rightTopCorner.z - leftBottomCorner.z > panBorders.height - panBorders.y)
        {
            //			Debug.Log("Vertical pan disabled");
            //			Debug.Log((panBorders.height + panBorders.y));
            disableVerticalPan = true;
            transform.position = new Vector3(transform.position.x, transform.position.y, (panBorders.height + panBorders.y) * 0.5f);
        }
        else
        {

            disableVerticalPan = false;
        }
    }


    // Return a rect containing the info about whether the given pos is within the panBorders; 0 => outside and 1 => within
    Rect IsWithinPanBorders(Vector2 pos)
    {
        int answerX = 0, answerY = 0, answerWidth = 0, answerHeight = 0;

        if (pos.y > panBorders.y + (rightTopCorner.z - leftBottomCorner.z) * 0.5f)
            answerY = 1;
        if (pos.y < panBorders.height - (rightTopCorner.z - leftBottomCorner.z) * 0.5f)
            answerHeight = 1;
        if (pos.x > panBorders.x + (rightTopCorner.x - leftBottomCorner.x) * 0.5f)
            answerX = 1;
        if (pos.x < panBorders.width - (rightTopCorner.x - leftBottomCorner.x) * 0.5f)
            answerWidth = 1;

        return new Rect(answerX, answerY, answerWidth, answerHeight);
    }


    // Return true if the given pos is within the cam view frustum
    public bool IsWithinViewFrustum(Vector2 pos)
    {
        if (pos.x < leftBottomCorner.x || pos.x > rightTopCorner.x || pos.y < leftBottomCorner.z || pos.y > rightTopCorner.z)
        {
            Debug.Log("OUTSIDE CAM VIEW");
            return false;
        }

        return true;
    }

    #endregion


    public static Vector2 GetFitToCamViewMultipliers(bool fitToLandscape)
    {
        // Take care of the aspect ratio
        float aspectMultiplier;
        if (fitToLandscape && Application.platform != RuntimePlatform.OSXEditor && Screen.height > Screen.width)//(Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown))
            aspectMultiplier = 1f / Camera.main.aspect;
        else
            aspectMultiplier = Camera.main.aspect;

        return new Vector2(Camera.main.orthographicSize * 2 * aspectMultiplier, Camera.main.orthographicSize * 2);
    }


    // Set the orthographic size of the camera and enable/disable the pan depending on the size of the grid (fully visible grid on any axis will disable the corresponding pan)
    public void SetOrthoSize()
    {
        float size = 0;

        if (logic.GetLogicType() == Global.LogicType.GameLogic)
        {
            if (Global.Global.iPadVersion)
            {
                size = defaultOrthoSize;

                zoomClosest = defaultOrthoSize;
                zoomFarthest = defaultOrthoSize * 1.8f;

                if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
                {
                    FitToRatio(ref size);
                    FitToRatio(ref zoomClosest);
                    FitToRatio(ref zoomFarthest);
                }
            }
            else
            {
                //				size = 2.85f;

                size = defaultOrthoSize;
                //				size = zoomFarthest;

                zoomClosest = defaultOrthoSize;
                zoomFarthest = defaultOrthoSize * 1.9f;

                if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
                {
                    FitToRatio(ref size);
                    FitToRatio(ref zoomClosest);
                    FitToRatio(ref zoomFarthest);
                }
            }
        }
        else
        {
            size = 0.5f;
        }


        myCam.orthographicSize = size;


        if (logic.GetLogicType() == Global.LogicType.GameLogic)
        {
            Zoom(0); // Called only to clamp values if needed according to new orthographic size
        }

        SetCamCorners();

        logic.SetupCamDependantObjects(false);

        autoPanZoneLength = new Rect(autoPanZoneLengthAsPercentage.x * Screen.width, autoPanZoneLengthAsPercentage.y * Screen.height, autoPanZoneLengthAsPercentage.width * Screen.width, autoPanZoneLengthAsPercentage.height * Screen.height);
    }


    void FitToRatio(ref float size)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        {
            Debug.Log("PORTRAIT");
            size /= myCam.aspect;
        }
    }


    // Setup the allowed autorotations
    public void AllowOrientations(bool portrait, bool landscape)
    {
        autorotateToPortrait = false;
        autorotateToPortraitUpsideDown = false;
        autorotateToLandscapeLeft = false;
        autorotateToLandscapeRight = false;

        if (portrait)
        {
            autorotateToPortrait = true;
            autorotateToPortraitUpsideDown = true;
        }

        if (landscape)
        {
            autorotateToLandscapeLeft = true;
            autorotateToLandscapeRight = true;
        }

        // If we are in a case where one of the orientation is disabled and the screen is currently in that orientation, force it to the other one
        if (portrait && !landscape && Screen.orientation != ScreenOrientation.PortraitUpsideDown)
        {
            previousScreenHeight = Screen.height;
            Screen.orientation = ScreenOrientation.Portrait;
        }
        else if (!portrait && landscape && Screen.orientation != ScreenOrientation.LandscapeRight)
        {
            previousScreenHeight = Screen.height;
            Screen.orientation = ScreenOrientation.LandscapeLeft;
        }

        changingOrientation = true;
    }


    #region Ads

    public void HideBannerView()
    {
        if (!Global.Global.adEnabled)
            return;

        wantToDisplayAd = false;

#if UNITY_IOS
        //		if (adBannerView.loaded && adBannerView.error == null)
        //		{
        //			adBannerView.Hide();
        //			Debug.Log("ADBanner hidden");
        //		}
#elif UNITY_ANDROID
        if (adMobAdapter != null)
        {
            //TODO || DEV22
            //AdMobAndroid.hideBanner(true);
        }
#endif
    }


    public void SetBannerView()
    {
        if (!Global.Global.adEnabled)
            return;

        if (logic.GetLogicType() != Global.LogicType.GameLogic) // Only create the banner if we're ingame
            return;

        GameLogic gameLogic = logic as GameLogic;
        if (gameLogic.guiCam.isHelpMenuOpen || gameLogic.guiCam.vKeyboardBgGO.GetComponent<Collider>().enabled)
        {
            return;
        }

        wantToDisplayAd = true;

#if UNITY_IOS
        int screenSizeY;

        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
        {

            //			adBannerView.currentSizeIdentifier = ADSizeIdentifier.Landscape;

            // We have to hardcode the screen sizes, as it uses the same screen size (non retina) even for the retina displays
            if (Global.Global.iPadVersion)
            {
                screenSizeY = 768;
            }
            else
            {
                screenSizeY = 320;
            }
        }
        else
        {
            //			adBannerView.currentSizeIdentifier = ADSizeIdentifier.Portrait;


            // We have to hardcode the screen sizes, as it uses the same screen size (non retina) even for the retina displays
            if (Global.Global.iPadVersion)
            {
                screenSizeY = 1024;
            }
            else
            {
                screenSizeY = 480;
            }
        }


        //		adBannerView.position = new Vector2(0, screenSizeY - UnityEngine.iOS.ADBannerView.GetSizeFromSizeIdentifier(adBannerView.currentSizeIdentifier).y);

#elif UNITY_ANDROID
        if (adMobAdapter == null)
        {
            Debug.Log("___SET BANNERVIEW ORTHOCAM___");
            adMobAdapter = Instantiate(Resources.Load("Prefabs/Ingame/AdMobAndroidBanner")) as GameObject;
        }

        //TODO || DEV23
        //AdMobAndroid.hideBanner(false);
#endif

        bannerViewCheckLoadLastTime = Time.time - bannerViewCheckLoadTimestep;
    }

    #endregion
}
