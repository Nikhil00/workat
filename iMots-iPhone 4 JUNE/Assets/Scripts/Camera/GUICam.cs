using UnityEngine;
using System.Collections;

public class GUICam : MonoBehaviour
{

    #region Vars

    GameLogic gameLogic;

    Camera myCam;

    // GUI Objects
    public GameObject ingameMenuBgGO, ingameMenuOpenButtonGO, helpBgGO, ingameMenuClockGO, ingameMenuClockBGGO, ingameMenuClockTextGO, ingameMenuClockTextShadowGO,
            ingameMenuClockBestGO, ingameMenuClockBestBGGO, ingameMenuClockBestTextGO, ingameMenuClockBestTextShadowGO, scoreMenuGO, GCButtonGO;
    public GameObject[] ingameMenuButtons;
    public Material[] ingameMenuClockBGiPadMaterials = new Material[2];
    public GameObject vKeyboardGO, vKeyboardBgGO;
    public GameObject[] vKeysGO;
    public Material[] vKeyHideMaterials = new Material[4];
    public Material[] vKeyBackMaterials = new Material[4];


    // GameMode button handling
    public Material dragNDropMat, dragNDropPressedMat, keyboardMat, keyboardPressedMat;

    // Panel animation
    public GameObject menuPanelGO;
    bool menuPanelAnimating = false;
    public bool isMenuOpen = false;
    float menuPanelAnimationPercentage;
    float menuPanelAnimationStartTime, menuPanelAnimationPercentageAtStart, menuPanelAnimationSpeed = 5f;
    public AudioSource openAS, closeAS;

    // Tabs animation
    public GameObject tabsGO;
    bool tabsAnimating = false;
    public bool isTabsOpen = false;
    float tabsAnimationPercentage;
    float tabsAnimationStartTime, tabsAnimationPercentageAtStart, tabsAnimationSpeed = 3f;

    // Options menu
    bool optionsMenuAnimating = false;
    public bool isOptionsMenuOpen = false;
    float optionsMenuAnimationPercentage;
    float optionsMenuAnimationStartTime, optionsMenuAnimationPercentageAtStart, optionsMenuAnimationSpeed = 3f;
    public string[] difficultyExplanations;
    public GameObject difficultyExplanationGO;
    protected bool portraitOrientationAllowedBeforeOptions, landscapeOrientationAllowedBeforeOptions;

    // Help menu
    bool helpMenuAnimating = false;
    public bool isHelpMenuOpen = false;
    float helpMenuAnimationPercentage;
    float helpMenuAnimationStartTime, helpMenuAnimationPercentageAtStart, helpMenuAnimationSpeed = 3f;

    // Score menu
    bool scoreMenuAnimating = false;
    public bool isScoreMenuOpen = false;
    float scoreMenuAnimationPercentage;
    float scoreMenuAnimationStartTime, scoreMenuAnimationPercentageAtStart, scoreMenuAnimationSpeed = 1.5f;
    public GameObject[] scoreMenuValuesGOs = new GameObject[3];
    // Holds the texts objects for the values (time, count, etc.) from top to bottom of the score popup
    public GameObject[] scoreMenuScoreGOs = new GameObject[5];
    // Holds the texts objects for the SCORE values (points) from top to bottom of the score popup
    [HideInInspector]
    public TextMesh[] scoreMenuValuesTexts;
    [HideInInspector]
    public TextMesh[] scoreMenuScoreTexts;
    public GameObject scoreMenuRankGO;
    [HideInInspector]
    public TextMesh scoreMenuRankText;
    public GameObject scoreMenuRankEvolutionGO;
    [HideInInspector]
    public TextMesh scoreMenuSubScoreRecordText;
    public GameObject scoreMenuSubScoreRecordGO;

    // VKeyboard
    public float spaceBetweenVKeys;
    public Global.KeyboardState vKeyboardState = Global.KeyboardState.Off;
    protected bool vKeyboardFading = false;
    protected float vKeyboardFadeStartValue, vKeyboardFadeEndValue;
    public float vKeyboardUnfocusedAlphaValue;
    protected float vKeyboardFadeStartTime;
    protected float vKeyboardFadeDuration;
    public float vKeyboardFullFadeDuration;
    public float vKeyboardPartialFadeDuration;
    public VKey[] vKeys;

    // Options menu
    public GameObject optionsMenuGO;

    // Ingame TapToContinue
    public GameObject tapToContinueGO;
    float tapToContinueStartTime = 0, tapToContinueStepDuration = 0.75f;
    bool tapToContinueFadingIn = true;


    float guiSizeMultiplier = 1;

    #endregion


    void Awake()
    {
        gameLogic = (GameLogic)FindObjectOfType(typeof(GameLogic));

        myCam = GetComponent<Camera>();

#if UNITY_IOS
        //				if(UnityEngine.iOS.Device)
        if (Screen.width < 640 && Screen.height < 640)
        { // Small screens
          //			guiSizeMultiplier = 0.5f;
            guiSizeMultiplier = 2f;
        }
        // TEST IPAD 3
        else if (Screen.width > 768 && Screen.height > 768)
        { // Xtra Big screens
            guiSizeMultiplier = 2f;
        }

#elif UNITY_ANDROID
		Vector3 androidGuiScale = new Vector3(960f / (Screen.width>Screen.height?Screen.width:Screen.height), 0, 640f / (Screen.width>Screen.height?Screen.height:Screen.width));
		Vector3 androidGuiSize = new Vector3(1024f / (960f / (Screen.width>Screen.height?Screen.width:Screen.height)), 0, 1024f / (640f / (Screen.width>Screen.height?Screen.height:Screen.width)));
#endif

        // Sliding Panel
        //				ingameMenuBgGO.transform.localScale *= guiSizeMultiplier * 2f;
        ingameMenuBgGO.transform.localScale = new Vector3(ingameMenuBgGO.transform.localScale.x * guiSizeMultiplier * 2f, 1f, 128f);
        ingameMenuOpenButtonGO.transform.localScale *= guiSizeMultiplier;
        ingameMenuClockGO.transform.localScale *= guiSizeMultiplier;
        ingameMenuClockBestGO.transform.localScale *= guiSizeMultiplier;

#if UNITY_ANDROID
		ingameMenuBgGO.transform.localScale = new Vector3(ingameMenuBgGO.transform.localScale.x / androidGuiScale.x, 1, ingameMenuBgGO.transform.localScale.z / androidGuiScale.z);
		ingameMenuOpenButtonGO.transform.localScale = new Vector3(ingameMenuOpenButtonGO.transform.localScale.x / androidGuiScale.x, 1, ingameMenuOpenButtonGO.transform.localScale.z / androidGuiScale.z);
		ingameMenuClockGO.transform.localScale = new Vector3(ingameMenuClockGO.transform.localScale.x / androidGuiScale.x, 1, ingameMenuClockGO.transform.localScale.z / androidGuiScale.z);
		ingameMenuClockBestGO.transform.localScale = new Vector3(ingameMenuClockBestGO.transform.localScale.x / androidGuiScale.x, 1, ingameMenuClockBestGO.transform.localScale.z / androidGuiScale.z);
#endif

        for (int i = 0; i < ingameMenuButtons.Length; i++)
        {
            ingameMenuButtons[i].transform.localScale *= guiSizeMultiplier;

#if UNITY_ANDROID
			ingameMenuButtons[i].transform.localScale = new Vector3(ingameMenuButtons[i].transform.localScale.x / androidGuiScale.x, 1, ingameMenuButtons[i].transform.localScale.z / androidGuiScale.z);
#endif
        }


        // VKeyboard
        vKeyboardBgGO.transform.localScale *= guiSizeMultiplier * 2f;
        spaceBetweenVKeys = spaceBetweenVKeys * guiSizeMultiplier;

        vKeys = new VKey[vKeysGO.Length];
        for (int i = 0; i < vKeysGO.Length; i++)
        {
            vKeys[i] = vKeysGO[i].transform.Find("BG").GetComponent("VKey") as VKey;
        }

        // Score
        scoreMenuValuesTexts = new TextMesh[scoreMenuValuesGOs.Length];
        for (int i = 0; i < scoreMenuValuesGOs.Length; i++)
        {
            scoreMenuValuesTexts[i] = scoreMenuValuesGOs[i].GetComponent("TextMesh") as TextMesh;
        }
        scoreMenuScoreTexts = new TextMesh[scoreMenuScoreGOs.Length];
        for (int i = 0; i < scoreMenuScoreGOs.Length; i++)
        {
            scoreMenuScoreTexts[i] = scoreMenuScoreGOs[i].GetComponent("TextMesh") as TextMesh;
        }

        scoreMenuRankText = scoreMenuRankGO.GetComponent("TextMesh") as TextMesh;

        scoreMenuSubScoreRecordText = scoreMenuSubScoreRecordGO.GetComponent("TextMesh") as TextMesh;


        // Options menu
        optionsMenuGO.transform.localScale *= guiSizeMultiplier;
#if UNITY_ANDROID
		optionsMenuGO.transform.localScale = new Vector3(optionsMenuGO.transform.localScale.x / androidGuiScale.x, 1, optionsMenuGO.transform.localScale.z / androidGuiScale.z);
#endif

        // Help menu
        helpBgGO.transform.localScale *= guiSizeMultiplier * 1.2f;
        //				helpBgGO.transform.localScale = new Vector3 (Screen.width, 1, Screen.height);

#if UNITY_ANDROID
		helpBgGO.transform.localScale = androidGuiSize;
#endif

        // Score menu
        scoreMenuGO.transform.localScale *= guiSizeMultiplier;
#if UNITY_ANDROID
		scoreMenuGO.transform.localScale = new Vector3(scoreMenuGO.transform.localScale.x / androidGuiScale.x, 1, scoreMenuGO.transform.localScale.z / androidGuiScale.z);
#endif

        // GC Button
        GCButtonGO.transform.localScale *= guiSizeMultiplier;
#if UNITY_ANDROID
		GCButtonGO.active = false;
#endif

        //		SetKeyboardState(Global.KeyboardState.Off, true);

    }


    // Use this for initialization
    void Start()
    {
        SetGameModeButtonCorrectMaterial();

        if (gameLogic.gameMode == Global.GameMode.Keyboard)
        {
            vKeyboardGO.SetActiveRecursively(true);
        }

        if (isMenuOpen)
            menuPanelAnimationPercentage = 0;
        else
            menuPanelAnimationPercentage = 1;

        SetMenuPanelPosition();


        if (isOptionsMenuOpen)
            optionsMenuAnimationPercentage = 1;
        else
            optionsMenuAnimationPercentage = 0;

        SetOptionsMenuAlpha();


        if (isHelpMenuOpen)
            helpMenuAnimationPercentage = 1;
        else
            helpMenuAnimationPercentage = 0;

        SetHelpMenuAlpha();


        if (isScoreMenuOpen)
            scoreMenuAnimationPercentage = 1;
        else
            scoreMenuAnimationPercentage = 0;

        SetScoreMenuAlpha();

        if (isTabsOpen)
            tabsAnimationPercentage = 1;
        else
            tabsAnimationPercentage = 0;

        SetTabsMenuAlpha();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
            StartOptionsMenuAnimation();

        // Menu panel sliding animation handling
        if (menuPanelAnimating)
        {
            if (!isMenuOpen)
            {
                menuPanelAnimationPercentage = Mathf.Lerp(menuPanelAnimationPercentageAtStart, 1, (Time.time - menuPanelAnimationStartTime) * menuPanelAnimationSpeed);

                if (menuPanelAnimationPercentage >= 1)
                {
                    StopMenuPanelAnimation();
                }
            }
            else
            {
                menuPanelAnimationPercentage = Mathf.Lerp(menuPanelAnimationPercentageAtStart, 0, (Time.time - menuPanelAnimationStartTime) * menuPanelAnimationSpeed);

                if (menuPanelAnimationPercentage <= 0)
                {
                    StopMenuPanelAnimation();
                }
            }

            SetMenuPanelPosition();
        }
        // Else => because we want the menu panel to finish its animaton before starting the animation of the options menu
        else if (optionsMenuAnimating)
        {
            if (!tabsAnimating)
            {
                StartTabsAnimation();
            }

            if (isOptionsMenuOpen)
            {
                optionsMenuAnimationPercentage = Mathf.Lerp(optionsMenuAnimationPercentageAtStart, 1, (Time.time - optionsMenuAnimationStartTime) * optionsMenuAnimationSpeed);

                if (optionsMenuAnimationPercentage >= 1)
                {
                    StopOptionsMenuAnimation();
                }
            }
            else
            {
                optionsMenuAnimationPercentage = Mathf.Lerp(optionsMenuAnimationPercentageAtStart, 0, (Time.time - optionsMenuAnimationStartTime) * optionsMenuAnimationSpeed);

                if (optionsMenuAnimationPercentage <= 0)
                {
                    StopOptionsMenuAnimation();
                }
            }

            SetOptionsMenuAlpha();
        }

        if (tabsAnimating)
        {
            if (isTabsOpen)
            {
                tabsAnimationPercentage = Mathf.Lerp(tabsAnimationPercentageAtStart, 1, (Time.time - tabsAnimationStartTime) * tabsAnimationSpeed);

                if (tabsAnimationPercentage >= 1)
                {
                    StopTabsMenuAnimation();
                }
            }
            else
            {
                tabsAnimationPercentage = Mathf.Lerp(tabsAnimationPercentageAtStart, 0, (Time.time - tabsAnimationStartTime) * tabsAnimationSpeed);

                if (tabsAnimationPercentage <= 0)
                {
                    StopTabsMenuAnimation();
                }
            }

            SetTabsMenuAlpha();
        }

        if (scoreMenuAnimating)
        {
            if (isScoreMenuOpen)
            {
                scoreMenuAnimationPercentage = Mathf.Lerp(scoreMenuAnimationPercentageAtStart, 1, (Time.time - scoreMenuAnimationStartTime) * scoreMenuAnimationSpeed);

                if (scoreMenuAnimationPercentage >= 1)
                {
                    StopScoreMenuAnimation();
                }
            }
            else
            {
                scoreMenuAnimationPercentage = Mathf.Lerp(scoreMenuAnimationPercentageAtStart, 0, (Time.time - scoreMenuAnimationStartTime) * scoreMenuAnimationSpeed);

                if (scoreMenuAnimationPercentage <= 0)
                {
                    StopScoreMenuAnimation();
                }
            }

            SetScoreMenuAlpha();
        }

        if (helpMenuAnimating)
        {
            if (isHelpMenuOpen)
            {
                helpMenuAnimationPercentage = Mathf.Lerp(helpMenuAnimationPercentageAtStart, 1, (Time.time - helpMenuAnimationStartTime) * helpMenuAnimationSpeed);

                if (helpMenuAnimationPercentage >= 1)
                {
                    StopHelpMenuAnimation();
                }
            }
            else
            {
                helpMenuAnimationPercentage = Mathf.Lerp(helpMenuAnimationPercentageAtStart, 0, (Time.time - helpMenuAnimationStartTime) * helpMenuAnimationSpeed);

                if (helpMenuAnimationPercentage <= 0)
                {
                    StopHelpMenuAnimation();
                }
            }

            SetHelpMenuAlpha();
        }

        // Keyboard fade handling
        if (vKeyboardFading && vKeyboardGO.active)
        {
            float lerpInputValue, lerpOutputValue;

            if (vKeyboardFadeStartTime + vKeyboardFadeDuration >= Time.time)
            {
                lerpInputValue = (vKeyboardFadeDuration - (vKeyboardFadeStartTime + vKeyboardFadeDuration - Time.time)) / vKeyboardFadeDuration;
            }
            else
            {
                vKeyboardFading = false;
                lerpInputValue = 1;
            }

            lerpOutputValue = Mathf.Lerp(vKeyboardFadeStartValue, vKeyboardFadeEndValue, lerpInputValue);
            Color vKeyCurrentBGColor = vKeys[0].GetComponent<Renderer>().material.color;
            Color vKeyNewBGColor = new Color(vKeyCurrentBGColor.r, vKeyCurrentBGColor.g, vKeyCurrentBGColor.b, lerpOutputValue);
            Color vKeyCurrentTextColor = vKeys[0].textMesh.GetComponent<MeshRenderer>().material.color;
            Color vKeyNewTextColor = new Color(vKeyCurrentTextColor.r, vKeyCurrentTextColor.g, vKeyCurrentTextColor.b, lerpOutputValue);

            foreach (VKey vKey in vKeys)
            {
                vKey.GetComponent<Renderer>().material.color = vKeyNewBGColor;
                vKey.textMesh.GetComponent<Renderer>().material.color = vKeyNewTextColor;
            }

            Color vKeyboardCurrentBGColor = vKeyboardBgGO.GetComponent<Renderer>().material.color;
            Color vKeyboardNewBGColor = new Color(vKeyboardCurrentBGColor.r, vKeyboardCurrentBGColor.g, vKeyboardCurrentBGColor.b, lerpOutputValue);
            vKeyboardBgGO.GetComponent<Renderer>().material.color = vKeyboardNewBGColor;


            for (int i = 0; i < 4; i++)
            {
                vKeyHideMaterials[i].color = new Color(vKeyHideMaterials[i].color.r, vKeyHideMaterials[i].color.g, vKeyHideMaterials[i].color.b, lerpOutputValue);
                vKeyBackMaterials[i].color = new Color(vKeyBackMaterials[i].color.r, vKeyBackMaterials[i].color.g, vKeyBackMaterials[i].color.b, lerpOutputValue);
            }
        }

        if (gameLogic.waitingForEOGA)
        {
            // Handles the blinking "tap to continue"
            Color tapToContinueColor = tapToContinueGO.GetComponent<Renderer>().material.color;
            if (tapToContinueFadingIn)
                tapToContinueColor = new Color(tapToContinueColor.r, tapToContinueColor.g, tapToContinueColor.b, Mathf.Lerp(0.25f, 1f, (Time.time - tapToContinueStartTime) / tapToContinueStepDuration));
            else
                tapToContinueColor = new Color(tapToContinueColor.r, tapToContinueColor.g, tapToContinueColor.b, Mathf.Lerp(1f, 0.25f, (Time.time - tapToContinueStartTime) / tapToContinueStepDuration));
            tapToContinueGO.GetComponent<Renderer>().material.color = tapToContinueColor;

            if (Time.time >= tapToContinueStartTime + tapToContinueStepDuration)
            {
                tapToContinueFadingIn = !tapToContinueFadingIn;
                tapToContinueStartTime = Time.time;
            }
        }
    }


    // USEFUL TO DEBUG POINTS IN 3D SPACE
    void OnDrawGizmosSelected()
    {
    }


    void FixedUpdate()
    {
        if (ingameMenuBgGO.transform.localPosition.x != -myCam.orthographicSize * myCam.aspect + ingameMenuBgGO.transform.localScale.x * 0.5f)
        {
            //						Debug.Log ("GUI PROBLEM => RESET GUI");

            SetOrthoSize();
        }
    }


    // Set the orthographic size of the camera and enable/disable the pan depending on the size of the grid (fully visible grid on any axis will disable the corresponding pan)
    public void SetOrthoSize()
    {
        float size = 0;

        size = Screen.height / 2;

        GetComponent<Camera>().orthographicSize = size;

        SetupGUI();
    }


    // Setup the GUI objects according to the size of the GUI cam
    protected void SetupGUI()
    {
        // Options menu
        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight || Application.platform == RuntimePlatform.OSXEditor)
            optionsMenuGO.transform.localRotation = Quaternion.Euler(-90, 0, 0);
        else
            optionsMenuGO.transform.localRotation = Quaternion.Euler(0, 90, 270);

        // Help menu
        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight || Application.platform == RuntimePlatform.OSXEditor)
            helpBgGO.transform.parent.localRotation = Quaternion.Euler(-90, 0, 0);
        else
            helpBgGO.transform.parent.localRotation = Quaternion.Euler(0, 90, 270);

        // Score menu
        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
            scoreMenuGO.transform.localRotation = Quaternion.Euler(-90, 0, 0);
        else
            scoreMenuGO.transform.localRotation = Quaternion.Euler(0, 90, 270);

        // Tap to continue text
        if (Global.Global.iPadVersion)
        {
            //			if (Global.Global.adEnabled)
            //			{
            //				tapToContinueGO.transform.localPosition = new Vector3(0, -myCam.orthographicSize + 82 * guiSizeMultiplier, tapToContinueGO.transform.localPosition.z);
            //			}
            //			else
            //			{
            tapToContinueGO.transform.localPosition = new Vector3(0, -myCam.orthographicSize + 16 * guiSizeMultiplier, tapToContinueGO.transform.localPosition.z);
            //			}
        }
        else
        {
            //			if (Global.Global.adEnabled)
            //			{
            //				if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
            //					tapToContinueGO.transform.localPosition = new Vector3(0, -myCam.orthographicSize + 48 * guiSizeMultiplier, tapToContinueGO.transform.localPosition.z);
            //				else
            //					tapToContinueGO.transform.localPosition = new Vector3(0, -myCam.orthographicSize + 66 * guiSizeMultiplier, tapToContinueGO.transform.localPosition.z);
            //			}
            //			else
            //			{
            tapToContinueGO.transform.localPosition = new Vector3(0, -myCam.orthographicSize + 16 * guiSizeMultiplier, tapToContinueGO.transform.localPosition.z);
            //			}
        }

        // GC Button
        if (Global.Global.iPadVersion)
        {
            if (Global.Global.adEnabled)
            {
                GCButtonGO.transform.localPosition = new Vector3(myCam.orthographicSize * myCam.aspect - 72 * guiSizeMultiplier, -myCam.orthographicSize + 120 * guiSizeMultiplier, GCButtonGO.transform.localPosition.z);
            }
            else
            {
                GCButtonGO.transform.localPosition = new Vector3(myCam.orthographicSize * myCam.aspect - 72 * guiSizeMultiplier, -myCam.orthographicSize + 90 * guiSizeMultiplier, GCButtonGO.transform.localPosition.z);
            }
        }
        else
        {
            if (Global.Global.adEnabled)
            {
                GCButtonGO.transform.localPosition = new Vector3(myCam.orthographicSize * myCam.aspect - 87 * guiSizeMultiplier, -myCam.orthographicSize + 150 * guiSizeMultiplier, GCButtonGO.transform.localPosition.z);
            }
            else
            {
                GCButtonGO.transform.localPosition = new Vector3(myCam.orthographicSize * myCam.aspect - 87 * guiSizeMultiplier, -myCam.orthographicSize + 96 * guiSizeMultiplier, GCButtonGO.transform.localPosition.z);
            }
        }

        // Sliding Panel
        ingameMenuBgGO.transform.localPosition = new Vector3(-myCam.orthographicSize * myCam.aspect + ingameMenuBgGO.transform.localScale.x * 0.5f, ingameMenuBgGO.transform.localPosition.y, myCam.orthographicSize - ingameMenuBgGO.transform.localScale.z * 0.5f);
        ingameMenuOpenButtonGO.transform.localPosition = new Vector3(-myCam.orthographicSize * myCam.aspect + ingameMenuOpenButtonGO.transform.localScale.x * 0.5f, ingameMenuOpenButtonGO.transform.localPosition.y, myCam.orthographicSize - ingameMenuOpenButtonGO.transform.localScale.z * 0.5f - ingameMenuBgGO.transform.localScale.z);
        ingameMenuClockGO.transform.localPosition = new Vector3(myCam.orthographicSize * myCam.aspect - ingameMenuClockGO.transform.localScale.x * 0.5f, ingameMenuClockGO.transform.localPosition.y, myCam.orthographicSize - ingameMenuClockGO.transform.localScale.z * 0.5f - ingameMenuBgGO.transform.localScale.z);
        ingameMenuClockBestGO.transform.localPosition = new Vector3(myCam.orthographicSize * myCam.aspect - ingameMenuClockBestGO.transform.localScale.x * 1.51f, ingameMenuClockBestGO.transform.localPosition.y, myCam.orthographicSize - ingameMenuClockBestGO.transform.localScale.z * 0.5f - ingameMenuBgGO.transform.localScale.z);

        for (int i = 0; i < ingameMenuButtons.Length; i++)
        {
            ingameMenuButtons[i].transform.localPosition = new Vector3(-Screen.width * 0.5f + (Screen.width / ingameMenuButtons.Length) * (i + 0.5f), ingameMenuButtons[i].transform.localPosition.y, ingameMenuBgGO.transform.localPosition.z);
        }


        // VKeyboard
        float vKeySize = (Screen.width - spaceBetweenVKeys * 10) / 10;
        float vKeySizeMultiplierY = 1;

        // On iPhones, make the VKeys bigger vertically when in portrait because otherwise they are too small
        if (Application.platform != RuntimePlatform.OSXEditor)
        {
            if (!Global.Global.iPadVersion && Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
            {
                vKeySizeMultiplierY = 1.5f;
            }
        }

        vKeyboardBgGO.transform.localScale = new Vector3(vKeyboardBgGO.transform.localScale.x, vKeyboardBgGO.transform.localScale.y, (vKeySize * vKeySizeMultiplierY + spaceBetweenVKeys) * 3);
        vKeyboardBgGO.transform.localPosition = new Vector3(-myCam.orthographicSize * myCam.aspect + vKeyboardBgGO.transform.localScale.x * 0.5f, vKeyboardBgGO.transform.localPosition.y, -myCam.orthographicSize + vKeyboardBgGO.transform.localScale.z * 0.5f);


        for (int i = 0; i < vKeysGO.Length; i++)
        {
            float xOffset = 0;

            vKeysGO[i].transform.localScale = new Vector3(vKeySize, 1, vKeySize);


            if (i >= 20)
            { // Handles the last line, which is pretty different than the rest of the keyboard
                if (i == 20)
                { // Handles the hide keyboard key
                    xOffset = 0.675f;

                    // Portrait => use the stretched texture
                    if (vKeySizeMultiplierY == 1.5f)
                    {
                        vKeys[i].SetNormalMaterial(vKeyHideMaterials[2]);
                        vKeys[i].SetPressedMaterial(vKeyHideMaterials[3]);
                    }
                    else
                    {
                        vKeys[i].SetNormalMaterial(vKeyHideMaterials[0]);
                        vKeys[i].SetPressedMaterial(vKeyHideMaterials[1]);
                    }
                }
                else if (i == 27)
                { // Handles the back key
                    xOffset = 1.325f;

                    // Portrait => use the stretched texture
                    if (vKeySizeMultiplierY == 1.5f)
                    {
                        vKeys[i].SetNormalMaterial(vKeyBackMaterials[2]);
                        vKeys[i].SetPressedMaterial(vKeyBackMaterials[3]);
                    }
                    else
                    {
                        vKeys[i].SetNormalMaterial(vKeyBackMaterials[0]);
                        vKeys[i].SetPressedMaterial(vKeyBackMaterials[1]);
                    }
                }
                else
                { // Handles the rest of the last line
                    xOffset = 1;
                    vKeys[i].transform.localScale = new Vector3(vKeys[i].transform.localScale.x, 1, vKeySizeMultiplierY);
                }
            }
            else
            {
                vKeys[i].transform.localScale = new Vector3(vKeys[i].transform.localScale.x, 1, vKeySizeMultiplierY);
            }

            vKeysGO[i].transform.localPosition = new Vector3(-Screen.width * 0.5f + (vKeySize + spaceBetweenVKeys) * (i % 10 + 0.5f + xOffset), vKeysGO[i].transform.localPosition.y, vKeyboardBgGO.transform.localPosition.z - ((int)(i / 10) - 1) * (vKeysGO[i].transform.localScale.z * vKeySizeMultiplierY + spaceBetweenVKeys));
        }

        //		SetKeyboardState(vKeyboardState);


        gameLogic.cam.ComputePanBorders();


        // iPad only => we have to take care of the clock BG material, as it has to be changed in portrait/landscape in orfer to have the stripes correct
        if (Global.Global.iPadVersion)
        {
            if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight || Application.platform == RuntimePlatform.OSXEditor)
            {
                ingameMenuClockBGGO.GetComponent<Renderer>().material = ingameMenuClockBGiPadMaterials[1];
                ingameMenuClockBestBGGO.GetComponent<Renderer>().material = ingameMenuClockBGiPadMaterials[1];
            }
            else
            {
                ingameMenuClockBGGO.GetComponent<Renderer>().material = ingameMenuClockBGiPadMaterials[0];
                ingameMenuClockBestBGGO.GetComponent<Renderer>().material = ingameMenuClockBGiPadMaterials[0];
            }

            SetOptionsMenuAlpha();
        }
    }


    //	protected void OnApplicationPause(bool pause)
    //	{
    //		// App pausing
    //		if(pause)
    //		{
    //			if (Global.Global.adEnabled)
    //			{
    //				gameLogic.cam.HideBannerView();
    //			}
    //		}
    //		// App resuming
    //		else
    //		{
    //			if (Global.Global.adEnabled)
    //			{
    //				if (gameLogic != null && gameLogic.cam != null)
    //					gameLogic.cam.SetBannerView();
    //			}
    //		}
    //	}


    #region Menu Panel

    // Starts animating the menu panel
    public void StartMenuPanelAnimation()
    {
        menuPanelAnimating = true;

        isMenuOpen = !isMenuOpen;

        menuPanelAnimationPercentageAtStart = menuPanelAnimationPercentage;

        menuPanelAnimationStartTime = Time.time;

        // If we want to close the menu panel => disable the input on tge GC button right away
        if (!isMenuOpen)
        {
            GCButtonGO.GetComponent<Collider>().enabled = false;

            if (closeAS != null)
                closeAS.Play();
        }
        else
        {
            if (openAS != null)
                openAS.Play();
        }
    }


    // Stops animating the menu panel
    protected void StopMenuPanelAnimation()
    {
        menuPanelAnimating = false;

        if (!isMenuOpen)
        {
            menuPanelAnimationPercentage = 1;
        }
        else
        {
            menuPanelAnimationPercentage = 0;

            // If we just opened the menu panel => enable the input on GC button now that the menu is fully visible
            GCButtonGO.GetComponent<Collider>().enabled = true;
        }

        // If the menu panel is being closed by the options menu being opened, set the options menu animation start time as now
        if (isOptionsMenuOpen)
        {
            optionsMenuAnimationStartTime = Time.time;
        }
    }

    // Wrapper for setting the position of the menu panel according to the menuPanelAnimationPercentage
    protected void SetMenuPanelPosition()
    {
        menuPanelGO.transform.localPosition = new Vector3(menuPanelGO.transform.localPosition.x, ingameMenuBgGO.transform.localScale.z * menuPanelAnimationPercentage, menuPanelGO.transform.localPosition.z);

        SetGCAlpha();
    }

    #endregion


    #region GC

    // Wrapper for setting the alpha of the GC Button according to the menuPanelAnimationPercentage
    protected void SetGCAlpha()
    {
        Material GCButtonGOMaterial = GCButtonGO.GetComponent<Renderer>().material;
        GCButtonGOMaterial.color = new Color(GCButtonGOMaterial.color.r, GCButtonGOMaterial.color.g, GCButtonGOMaterial.color.b, 1 - menuPanelAnimationPercentage);
    }


    // Enable/disable the GC Button taking input
    public void EnableGCInput(bool enable)
    {
        Debug.Log("EnableGCInput " + enable);

        GCButtonGO.GetComponent<Collider>().enabled = enable;
    }

    #endregion


    #region Tabs

    // Starts animating the Tabs
    public void StartTabsAnimation()
    {
        tabsAnimating = true;

        isTabsOpen = !isTabsOpen;

        tabsAnimationPercentageAtStart = tabsAnimationPercentage;

        tabsAnimationStartTime = Time.time;

        // If we want to close the tabs menu => disable the input on it right away
        if (!isTabsOpen)
        {
            EnableTabsInput(false);
        }
    }


    // Stops animating the Tabs
    protected void StopTabsMenuAnimation()
    {
        tabsAnimating = false;

        if (!isTabsOpen)
        {
            tabsAnimationPercentage = 0;
        }
        else
        {
            tabsAnimationPercentage = 1;

            // If we just opened the tabs menu => enable the input now that the tabs are fully visible
            EnableTabsInput(true);
        }
    }


    // Wrapper for setting the alpha of the Tabs according to the tabsAnimationPercentage
    protected void SetTabsMenuAlpha()
    {
        Material ingameMenuOpenButtonGOMaterial = ingameMenuOpenButtonGO.GetComponent<Renderer>().material;
        ingameMenuOpenButtonGOMaterial.color = new Color(ingameMenuOpenButtonGOMaterial.color.r, ingameMenuOpenButtonGOMaterial.color.g, ingameMenuOpenButtonGOMaterial.color.b, tabsAnimationPercentage);

        Material ingameMenuClockBGGOMaterial = ingameMenuClockBGGO.GetComponent<Renderer>().material;
        ingameMenuClockBGGOMaterial.color = new Color(ingameMenuClockBGGOMaterial.color.r, ingameMenuClockBGGOMaterial.color.g, ingameMenuClockBGGOMaterial.color.b, tabsAnimationPercentage);
        Material ingameMenuClockTextGOMaterial = ingameMenuClockTextGO.GetComponent<Renderer>().material;
        ingameMenuClockTextGOMaterial.color = new Color(ingameMenuClockTextGOMaterial.color.r, ingameMenuClockTextGOMaterial.color.g, ingameMenuClockTextGOMaterial.color.b, tabsAnimationPercentage);
        Material ingameMenuClockTextShadowGOMaterial = ingameMenuClockTextShadowGO.GetComponent<Renderer>().material;
        ingameMenuClockTextShadowGOMaterial.color = new Color(ingameMenuClockTextShadowGOMaterial.color.r, ingameMenuClockTextShadowGOMaterial.color.g, ingameMenuClockTextShadowGOMaterial.color.b, tabsAnimationPercentage);
        Material ingameMenuClockBestBGGOMaterial = ingameMenuClockBestBGGO.GetComponent<Renderer>().material;
        ingameMenuClockBestBGGOMaterial.color = new Color(ingameMenuClockBestBGGOMaterial.color.r, ingameMenuClockBestBGGOMaterial.color.g, ingameMenuClockBestBGGOMaterial.color.b, tabsAnimationPercentage);
        Material ingameMenuClockBestTextGOMaterial = ingameMenuClockBestTextGO.GetComponent<Renderer>().material;
        ingameMenuClockBestTextGOMaterial.color = new Color(ingameMenuClockBestTextGOMaterial.color.r, ingameMenuClockBestTextGOMaterial.color.g, ingameMenuClockBestTextGOMaterial.color.b, tabsAnimationPercentage);
        Material ingameMenuClockBestTextShadowGOMaterial = ingameMenuClockBestTextShadowGO.GetComponent<Renderer>().material;
        ingameMenuClockBestTextShadowGOMaterial.color = new Color(ingameMenuClockBestTextShadowGOMaterial.color.r, ingameMenuClockBestTextShadowGOMaterial.color.g, ingameMenuClockBestTextShadowGOMaterial.color.b, tabsAnimationPercentage);
    }


    // Enable/disable the Tabs taking input
    public void EnableTabsInput(bool enable)
    {
        Debug.Log("EnableTabsInput " + enable);

        ingameMenuOpenButtonGO.GetComponent<Collider>().enabled = enable;
    }

    #endregion


    #region Options Menu

    // Starts animating the options menu
    public void StartOptionsMenuAnimation()
    {
        optionsMenuAnimating = true;

        isOptionsMenuOpen = !isOptionsMenuOpen;

        optionsMenuAnimationPercentageAtStart = optionsMenuAnimationPercentage;

        optionsMenuAnimationStartTime = Time.time;

        // If we want to close the options menu => disable the input on it right away
        if (!isOptionsMenuOpen)
        {
            EnableOptionsMenuInput(false);

            gameLogic.cam.AllowOrientations(portraitOrientationAllowedBeforeOptions, landscapeOrientationAllowedBeforeOptions);
        }
        else
        { // Else if we want to open the optioens menu => close the menu panel
            StartMenuPanelAnimation();
        }
    }


    // Stops animating the options menu
    protected void StopOptionsMenuAnimation()
    {
        optionsMenuAnimating = false;

        if (!isOptionsMenuOpen)
        {
            optionsMenuAnimationPercentage = 0;
        }
        else
        {
            optionsMenuAnimationPercentage = 1;

            // If we just opened the options menu => enable the input now that the menu is fully visible
            EnableOptionsMenuInput(true);

            portraitOrientationAllowedBeforeOptions = gameLogic.cam.autorotateToPortrait;
            landscapeOrientationAllowedBeforeOptions = gameLogic.cam.autorotateToLandscapeRight;

            gameLogic.cam.AllowOrientations(false, true);
        }
    }


    // Wrapper for setting the alpha of the options menu according to the optionsMenuAnimationPercentage
    protected void SetOptionsMenuAlpha()
    {
        for (int i = 0; i < optionsMenuGO.transform.childCount; i++)
        {
            Material childMaterial = optionsMenuGO.transform.GetChild(i).GetComponent<Renderer>().material;
            childMaterial.color = new Color(childMaterial.color.r, childMaterial.color.g, childMaterial.color.b, optionsMenuAnimationPercentage);
        }

        //		Material ingameMenuOpenButtonGOMaterial = ingameMenuOpenButtonGO.renderer.material;
        //		ingameMenuOpenButtonGOMaterial.color = new Color(ingameMenuOpenButtonGOMaterial.color.r, ingameMenuOpenButtonGOMaterial.color.g, ingameMenuOpenButtonGOMaterial.color.b, 1 - optionsMenuAnimationPercentage);
        //		
        //		Material ingameMenuClockBGGOMaterial = ingameMenuClockBGGO.renderer.material;
        //		ingameMenuClockBGGOMaterial.color = new Color(ingameMenuClockBGGOMaterial.color.r, ingameMenuClockBGGOMaterial.color.g, ingameMenuClockBGGOMaterial.color.b, 1 - optionsMenuAnimationPercentage);
        //		Material ingameMenuClockTextGOMaterial = ingameMenuClockTextGO.renderer.material;
        //		ingameMenuClockTextGOMaterial.color = new Color(ingameMenuClockTextGOMaterial.color.r, ingameMenuClockTextGOMaterial.color.g, ingameMenuClockTextGOMaterial.color.b, 1 - optionsMenuAnimationPercentage);
        //		Material ingameMenuClockTextShadowGOMaterial = ingameMenuClockTextShadowGO.renderer.material;
        //		ingameMenuClockTextShadowGOMaterial.color = new Color(ingameMenuClockTextShadowGOMaterial.color.r, ingameMenuClockTextShadowGOMaterial.color.g, ingameMenuClockTextShadowGOMaterial.color.b, 1 - optionsMenuAnimationPercentage);
        //		Material ingameMenuClockBestBGGOMaterial = ingameMenuClockBestBGGO.renderer.material;
        //		ingameMenuClockBestBGGOMaterial.color = new Color(ingameMenuClockBestBGGOMaterial.color.r, ingameMenuClockBestBGGOMaterial.color.g, ingameMenuClockBestBGGOMaterial.color.b, 1 - optionsMenuAnimationPercentage);
        //		Material ingameMenuClockBestTextGOMaterial = ingameMenuClockBestTextGO.renderer.material;
        //		ingameMenuClockBestTextGOMaterial.color = new Color(ingameMenuClockBestTextGOMaterial.color.r, ingameMenuClockBestTextGOMaterial.color.g, ingameMenuClockBestTextGOMaterial.color.b, 1 - optionsMenuAnimationPercentage);
        //		Material ingameMenuClockBestTextShadowGOMaterial = ingameMenuClockBestTextShadowGO.renderer.material;
        //		ingameMenuClockBestTextShadowGOMaterial.color = new Color(ingameMenuClockBestTextShadowGOMaterial.color.r, ingameMenuClockBestTextShadowGOMaterial.color.g, ingameMenuClockBestTextShadowGOMaterial.color.b, 1 - optionsMenuAnimationPercentage);

    }


    // Enable/disable the options menu taking input
    public void EnableOptionsMenuInput(bool enable)
    {
        Debug.Log("EnableOptionsMenuInput " + enable);

        for (int i = 0; i < optionsMenuGO.transform.childCount; i++)
        {
            Collider childCollider = optionsMenuGO.transform.GetChild(i).GetComponent<Collider>();
            if (childCollider != null)
                childCollider.enabled = enable;
        }
    }

    #endregion


    #region Help Menu

    // Starts animating the help menu
    public void StartHelpMenuAnimation()
    {
        helpMenuAnimating = true;

        isHelpMenuOpen = !isHelpMenuOpen;

        helpMenuAnimationPercentageAtStart = helpMenuAnimationPercentage;

        helpMenuAnimationStartTime = Time.time;

        // If we want to close the help menu => disable the input on it right away
        if (!isHelpMenuOpen)
        {
            helpBgGO.GetComponent<Collider>().enabled = false;
        }
        else
        {
            if (Global.Global.adEnabled)
            {
                //DEV_CODE || ADMOB || HIDE BANNER
                //gameLogic.cam.HideBannerView();
            }
        }
    }


    // Stops animating the help menu
    protected void StopHelpMenuAnimation()
    {
        helpMenuAnimating = false;

        if (!isHelpMenuOpen)
        {
            helpMenuAnimationPercentage = 0;

            if (Global.Global.adEnabled)
            {
                //DEV_CODE || ADMOB || SET BANNER VIEW
                //gameLogic.cam.SetBannerView();
            }
        }
        else
        {
            helpMenuAnimationPercentage = 1;

            // If we just opened the options menu => enable the input now that the menu is fully visible
            helpBgGO.GetComponent<Collider>().enabled = true;
        }
    }


    // Wrapper for setting the alpha of the options help according to the optionsMenuAnimationPercentage
    protected void SetHelpMenuAlpha()
    {
        Material helpBgGOMaterial = helpBgGO.GetComponent<Renderer>().material;
        helpBgGO.GetComponent<Renderer>().material.color = new Color(helpBgGOMaterial.color.r, helpBgGOMaterial.color.g, helpBgGOMaterial.color.b, helpMenuAnimationPercentage);
    }

    #endregion


    #region Score Menu

    // Starts animating the score menu
    public void StartScoreMenuAnimation()
    {
        scoreMenuAnimating = true;

        isScoreMenuOpen = !isScoreMenuOpen;

        scoreMenuAnimationPercentageAtStart = scoreMenuAnimationPercentage;

        scoreMenuAnimationStartTime = Time.time;

        // If we want to close the help menu => disable the input on it right away
        if (!isScoreMenuOpen)
        {
            EnableScoreMenuInput(false);
        }
    }


    // Stops animating the score menu
    protected void StopScoreMenuAnimation()
    {
        scoreMenuAnimating = false;

        if (!isScoreMenuOpen)
        {
            scoreMenuAnimationPercentage = 0;
        }
        else
        {
            scoreMenuAnimationPercentage = 1;

            gameLogic.scoreMenuButton.StartScoresAnim();

            // If we just opened the options menu => enable the input now that the menu is fully visible
            EnableScoreMenuInput(true);

            gameLogic.DestroyGameElements();
        }
    }


    // Wrapper for setting the alpha of the options score according to the scoreMenuAnimationPercentage
    protected void SetScoreMenuAlpha()
    {
        //		Material scoreBgGOMaterial = scoreBgGO.renderer.material;
        //		scoreBgGO.renderer.material.color = new Color(scoreBgGOMaterial.color.r, scoreBgGOMaterial.color.g, scoreBgGOMaterial.color.b, scoreMenuAnimationPercentage);

        for (int i = 0; i < scoreMenuGO.transform.childCount; i++)
        {
            Material childMaterial = scoreMenuGO.transform.GetChild(i).GetComponent<Renderer>().material;
            childMaterial.color = new Color(childMaterial.color.r, childMaterial.color.g, childMaterial.color.b, scoreMenuAnimationPercentage);
        }
    }


    // Enable/disable the score menu taking input
    public void EnableScoreMenuInput(bool enable)
    {
        Debug.Log("EnableScoreMenuInput " + enable);

        for (int i = 0; i < scoreMenuGO.transform.childCount; i++)
        {
            Collider childCollider = scoreMenuGO.transform.GetChild(i).GetComponent<Collider>();
            if (childCollider != null)
                childCollider.enabled = enable;
        }
    }

    #endregion


    #region Wrappers


    // Set the correct material for the game mode button according to the actual game mode
    public void SetGameModeButtonCorrectMaterial()
    {
        Material newNormalMaterial, newPressedMat;
        if (gameLogic.gameMode == Global.GameMode.DragNDrop)
        {
            newNormalMaterial = keyboardMat;
            newPressedMat = keyboardPressedMat;
        }
        else
        {
            newNormalMaterial = dragNDropMat;
            newPressedMat = dragNDropPressedMat;
        }

        ButtonIngameMenuGameMode gameModeButton = (ingameMenuButtons[1].GetComponent("ButtonIngameMenuGameMode") as ButtonIngameMenuGameMode);
        gameModeButton.SetNormalMaterial(newNormalMaterial);
        gameModeButton.SetPressedMaterial(newPressedMat);
    }

    #endregion


    #region VKeyboard

    // Enable/disable the keyboard taking input
    public void EnableKeyboardInput(bool enable)
    {
        // No need to enable/disable the keyboard taking input if it was already respectively enabled/disabled
        if (enable && vKeyboardState == Global.KeyboardState.Focused || !enable && vKeyboardState != Global.KeyboardState.Focused)
        {
            Debug.Log("VKeyboard already in the given state CONCERNING COLLIDERS");
            return;
        }

        Debug.Log("EnableKeyboardInput " + enable);

        foreach (VKey vKey in vKeys)
        {
            vKey.GetComponent<Collider>().enabled = enable;
        }

        vKeyboardBgGO.GetComponent<Collider>().enabled = enable;

        if (Global.Global.adEnabled)
        {
            if (enable)
            {
                //DEV_CODE || ADMOB || HIDE BANNER VIEW
                //gameLogic.cam.HideBannerView();
            }
            else
            {
                //DEV_CODE || ADMOB || SET BANNER VIEW
                //gameLogic.cam.SetBannerView();
            }
        }

    }


    // Change the state of the VKeyboard to the given one
    public void SetKeyboardState(Global.KeyboardState keyboardState, bool instantaneously)
    {
        //		// If the vKeyboard is already in that state, do nothing
        //		if (vKeyboardState == keyboardState)
        //			return;

        switch (keyboardState)
        {
            case Global.KeyboardState.Off:
                EnableKeyboardInput(false);
                vKeyboardFadeEndValue = 0;

                if (gameLogic.gameMode == Global.GameMode.Keyboard)
                    vKeyboardFadeDuration = vKeyboardFullFadeDuration;
                else
                    vKeyboardFadeDuration = vKeyboardPartialFadeDuration;
                break;

            case Global.KeyboardState.Unfocused:
                EnableKeyboardInput(false);
                vKeyboardFadeEndValue = vKeyboardUnfocusedAlphaValue;

                if (vKeyboardState == Global.KeyboardState.Off && gameLogic.gameMode == Global.GameMode.Keyboard)
                {
                    vKeyboardFadeDuration = vKeyboardFullFadeDuration;
                }
                else
                {
                    vKeyboardFadeDuration = vKeyboardPartialFadeDuration;
                }
                break;

            case Global.KeyboardState.Focused:
                EnableKeyboardInput(true);
                vKeyboardFadeEndValue = 1;

                if (vKeyboardState == Global.KeyboardState.Off && gameLogic.gameMode == Global.GameMode.Keyboard)
                {
                    vKeyboardFadeDuration = vKeyboardFullFadeDuration;
                }
                else
                {
                    vKeyboardFadeDuration = vKeyboardPartialFadeDuration;
                }
                break;
        }

        vKeyboardFadeStartValue = vKeys[0].GetComponent<Renderer>().material.color.a;
        vKeyboardFading = true;
        vKeyboardFadeStartTime = Time.time;

        vKeyboardState = keyboardState;


        if (instantaneously)
            vKeyboardFadeDuration = 0;
    }

    #endregion

}
