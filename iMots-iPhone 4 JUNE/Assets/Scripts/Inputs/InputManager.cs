using UnityEngine;
using System.Collections;
//using System;

public class InputManager : MonoBehaviour {
	
	#region Variables
	
	// all of our active trackers
	protected ArrayList		m_Trackers;

	// index is finger id, value is a TouchTracker
	protected Hashtable		m_TrackerLookup;
		
	protected TouchTracker	m_Touch;
	
	public int				m_iMaxTouch = 10;
	
	public bool				multiTouchBehavesDifferently = false;
	
	private float 			doubleTapActivationTime = 0.2f;
	private float 			doubleTapCheckStartTime = 0;
	private Vector2 		doubleTapFirstTapPosition;
	private const int 		doubleTapMaxDistanceBetweenTaps = 80;
	
	private float			disabledInputsAfterMultiTouchDuration = 0.25f;
	private float			disabledInputsAfterMultiTouchStartTime = 0;
	
	private float			pinchActivationTime = 0.25f;
	private float			pinchActivationCheckStartTime = 0;
	private bool			isPinching = false;
	
	#endregion
	
	// Use this for initialization
	void Start () {
		//Init the Input
		m_Trackers = new ArrayList();
		m_TrackerLookup = new Hashtable();
	}
	
	
	// USEFUL TO DEBUG POINTS IN 3D SPACE
//	 void OnDrawGizmosSelected() {
//		if (m_Touch == null)
//			return;
//		
//        Vector3 p = Camera.mainCamera.ScreenToWorldPoint(new Vector3(m_Touch.m_Position.x, m_Touch.m_Position.y, Camera.mainCamera.transform.position.y));
//        Gizmos.color = Color.yellow;
//        Gizmos.DrawSphere(p, 0.1F);
//    }
	
	
	// Update is called once per frame
	void Update () {
		if (disabledInputsAfterMultiTouchStartTime != 0)
		{
			if (Time.time <= disabledInputsAfterMultiTouchStartTime + disabledInputsAfterMultiTouchDuration)
				return;
			else
				disabledInputsAfterMultiTouchStartTime = 0;
		}
		
		//update input if on a mobile platform
		if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer){
			InputUpdate();
		}
		//else to play it with the mouse on Unity
		else{
			InputUpdateMouse();
		}
	}
	
	
	public virtual void InputUpdate(){
		
		// clean all touches (so they know if they aren't updated after we pull info)
		foreach(TouchTracker LoopTracker in m_Trackers){
			LoopTracker.Clean();
		}
		
		// If the application behaves differently with the several touches and with one touch
		if (multiTouchBehavesDifferently)
		{
			// There is currently no touch => do nothing
			if (Input.touchCount == 0)
			{
			}
			
			// There is one touch
			else if (Input.touchCount == 1)
			{
				Touch touch = Input.GetTouch(0);
				
				// try to get our tracker for this finger id
				TouchTracker tracker = (TouchTracker)m_TrackerLookup[touch.fingerId];
				
				//if not null update it
				if(tracker != null)
				{
					// If the tracker is one touch update it
					if (!tracker.isMultiTouch)
						tracker.UpdateTouch(touch.position);
					// If the tracker is multiTouch
					else
					{
						// If the tracker is already active => end it properly
						if (tracker.isActive)
						{
//							Debug.Log("ending multitouch tracker properly");
							EndTracking(tracker);
						}
						// If the tracker is not active yet => just delete it
						else
						{
//							Debug.Log("ending multitouch tracker brutally");
							m_Trackers.Remove(tracker);
							m_TrackerLookup[tracker.fingerId] = null;
						}
						
						// Create a new one single touch
						tracker = BeginTracking(touch);
					}
						
				}
				else
				{
					//else start tracking it if the number of max touch hasn't been reached
					if(m_Trackers.Count < m_iMaxTouch)
					{
						tracker = BeginTracking(touch);
					}
				}
			}
			
			// There are several touches
			else
			{
				Vector2[] touchesPosition = new Vector2[Input.touchCount];
				
				for (int i = 0; i < touchesPosition.Length; i++)
					touchesPosition[i] = Input.GetTouch(i).position;
				
				// try to get our tracker for this finger id
				TouchTracker tracker = (TouchTracker)m_TrackerLookup[Input.GetTouch(0).fingerId];
				
				//if not null update it
				if(tracker != null)
				{
					// If the tracker is already multiTouch update it
					if (tracker.isMultiTouch)
						tracker.UpdateMultiTouch(touchesPosition);
					// If the tracker is one touch only
					else
					{						
						// If the tracker is already active => end it properly
						if (tracker.isActive)
						{
							tracker.prematureEndBecauseOfMultitouch = true;
//							Debug.Log("ending one touch tracker properly");
							EndTracking(tracker);
						}
						// If the tracker is not active yet => just delete it
						else
						{
//							Debug.Log("ending one touch tracker brutally");
							m_Trackers.Remove(tracker);
							m_TrackerLookup[tracker.fingerId] = null;
						}
						
						// Create a new one multitouch
						tracker = BeginMultiTouchTracking(Input.touches);
					}
				}
				else
				{
					//else start tracking it if the number of max touch hasn't been reached
					if(m_Trackers.Count < m_iMaxTouch)
					{
						tracker = BeginMultiTouchTracking(Input.touches);
					}
				}
			}
				
		}
		
		// The application behaves the same way with several touches and with one touch
		else
		{
			// When pinching, behaviour is different (probably zoom)
			if (isPinching)
			{				
				// Try to get our tracker
				TouchTracker tracker = (TouchTracker)m_TrackerLookup[0];
				
				// If not null update it
				if (tracker != null)
				{
					if (Input.touches.Length >= 2 && Input.touches[0].position != null && Input.touches[1].position != null)
						tracker.UpdatePinch(Input.touches[0].position, Input.touches[1].position);
					else
					{
						Debug.Log("Need 2 touches to pinch => pinch is over");
					}
				}
				else
				{
					Debug.Log("Pinch tracker is null");
				}
			}
			// Usual behaviour
			else
			{
				// process our touches
				foreach(Touch touch in Input.touches)
				{
					// try to get our tracker for this finger id
					TouchTracker tracker = (TouchTracker)m_TrackerLookup[touch.fingerId];
					
					//if not null update it
					if(tracker != null)
					{
						tracker.UpdateTouch(touch.position);
					}
					else
					{
						// Else start tracking it if the number of max touch hasn't been reached
						if(m_Trackers.Count < m_iMaxTouch)
						{
							// Check for pinch (= only 2 touches, very close one to another in time)
							if (Input.touches.Length == 2 && pinchActivationCheckStartTime != 0 && Time.time <= pinchActivationCheckStartTime + pinchActivationTime)
							{
								isPinching = true;
//								((TouchTracker)m_TrackerLookup[0]).isPinching = true;
								TouchTracker touchTracker = ((TouchTracker)m_TrackerLookup[0]);
								touchTracker.End(); // When starting a pinch, we must first End() the tracker that will be used for the pinch, because we started it as a non pinch tracker and thus triggered its normal behaviour
								if (Input.touches[0].position != null && Input.touches[1].position != null)
									touchTracker.m_Positions = new Vector2[2] {Input.touches[0].position, Input.touches[1].position};
								Debug.Log("Pinching enabled");
							}
							else
							{
								tracker = BeginTracking(touch);
							}
						}
						else
						{
							Debug.Log("Too many trackers");
						}
					}
				}
			}
		}
		
		
		if (doubleTapCheckStartTime != 0 && Time.time > doubleTapCheckStartTime + doubleTapActivationTime)
			doubleTapCheckStartTime = 0;
		
		if (pinchActivationCheckStartTime != 0 && Time.time > pinchActivationCheckStartTime + pinchActivationTime)
			pinchActivationCheckStartTime = 0;
		
		// track which events vanished (without using iPhoneTouchPhase.Ended)
		ArrayList EndEvent = new ArrayList();
		
		// use an intermediate list because EndTracking removes from trackers arraylist
		foreach(TouchTracker LoopTracker in m_Trackers){
			if(!LoopTracker.isDirty){
				EndEvent.Add(LoopTracker);
			}
		}
		//End all ended touches
		foreach(TouchTracker LoopTracker in EndEvent){
			EndTracking(LoopTracker);	
		}
	}
	
	
	public virtual void InputUpdateMouse(){
		
		// clean all touches (so they know if they aren't updated after we pull info)
		foreach(TouchTracker LoopTracker in m_Trackers){
			LoopTracker.Clean();
		}
		
		if(Input.GetMouseButtonDown(0)){
			m_Touch = BeginTrackingMouse(Input.mousePosition);
		}
		
		if(Input.GetMouseButtonUp(0)){
			EndTrackingMouse(m_Touch);
		}
		
		if(m_Touch != null){
			
			m_Touch.UpdateTouch(Input.mousePosition);	
		}
	}
	
	
	//Begin Tracking
	public virtual TouchTracker BeginTracking(Touch touch)
	{
		//Get all the HUD element
//		GameObject[] HUDElements = GameObject.FindGameObjectsWithTag("HUDElement") as GameObject[];
	
		
//		//Loop Trough the HUD elements to see if one of them has been touch
//		foreach(PanningEntity PanningEntity in interactive2DEntities){
//			if((PanningEntity.gameObject.guiTexture.enabled && PanningEntity.gameObject.guiTexture.color.a > 0) && PanningEntity.gameObject.guiTexture.HitTest(touch.position)){
//				//create the tracker
//				TouchTracker tracker = new TouchTracker(touch, PanningEntity.gameObject);
//				
//				if (doubleTapCheckStartTime != 0 && Time.time <= doubleTapCheckStartTime + doubleTapActivationTime && Vector2.Distance(touch.position, doubleTapFirstTapPosition) <= doubleTapMaxDistanceBetweenTaps)
//				{
//					tracker.isDoubleTap = true;
//				}
//				
////				Debug.Log("BeginTracking => " + PanningEntity.transform.parent.name);
//				
//				// remember our tracker
//				m_Trackers.Add(tracker);
//				m_TrackerLookup[touch.fingerId] = tracker;
//				
//				return tracker;
//			}
//		}

		
		// 3D OBJECTS (THEY NEED A COLLIDER)
		// We need to do this for every camera in the scene
		Camera[] cameras = Camera.allCameras;
		
		for (int i = cameras.Length - 1; i > -1; i--)
		{
//			Debug.Log(cameras[i].tag);
			
			//Need to project the touch to see what the user touches in the game
			//create a ray 	to project the x and y touch position to the world
			Ray ray = cameras[i].ScreenPointToRay(new Vector2(touch.position.x,touch.position.y));
			RaycastHit hitInfo;
			
			LayerMask layerMask;
			if (cameras[i].tag == "GUICamera")
				layerMask = 1 << 31;
			else
				layerMask = (1 << 0) | (1 << 8);
			
			//if the Ray hit something create a new touch
			if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
			{
				
				//create the tracker
				TouchTracker tracker = new TouchTracker(touch,hitInfo.transform.gameObject);
				
				// remember our tracker
				m_Trackers.Add(tracker);
				m_TrackerLookup[touch.fingerId] = tracker;
				
				pinchActivationCheckStartTime = Time.time;
				
				return tracker;
			}
//			else
//			{
//				Debug.Log("InputManager => RAYCAST FAILED TO HIT AN OBJECT");
//			}
		}
		
		return null;
	}
	

	
	public virtual TouchTracker BeginMultiTouchTracking(Touch[] touches)
	{
		//Get all the HUD element
//		GameObject[] HUDElements = GameObject.FindGameObjectsWithTag("HUDElement") as GameObject[];
	
//		//Loop Trough the HUD elements to see if one of them has been touch
//		foreach(PanningEntity PanningEntity in interactive2DEntities){
//			
//			if((PanningEntity.gameObject.guiTexture.enabled && PanningEntity.gameObject.guiTexture.color.a > 0) && PanningEntity.gameObject.guiTexture.HitTest(touches[0].position)){
//				//create the tracker
//				TouchTracker tracker = new TouchTracker(touches, PanningEntity.gameObject);
//				
////				Debug.Log("BeginMultiTouchTracking => " + PanningEntity.transform.parent.name);
//				
//				// remember our tracker
//				m_Trackers.Add(tracker);
//				m_TrackerLookup[touches[0].fingerId] = tracker;
//				return tracker;
//			}
//		}
		
		
		
		// 3D OBJECTS (THEY NEED A COLLIDER)
		// We need to do this for every camera in the scene
		Camera[] cameras = Camera.allCameras;
		
		for (int i = cameras.Length - 1; i > -1; i--)
		{
//			Debug.Log(cameras[i].tag);
			
			//Need to project the touch to see what the user touches in the game
			//create a ray to project the x and y touch position to the world
			Ray ray = cameras[i].ScreenPointToRay(new Vector2(touches[0].position.x,touches[0].position.y));
			RaycastHit hitInfo;
			
			LayerMask layerMask;
			if (cameras[i].tag == "GUICamera")
				layerMask = 1 << 31;
			else
				layerMask = (1 << 0) | (1 << 8);
			
			//if the Ray hit something create a new touch
			if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask)){
				
				//create the tracker
				TouchTracker tracker = new TouchTracker(touches, hitInfo.transform.gameObject);
				
				// remember our tracker
				m_Trackers.Add(tracker);
				m_TrackerLookup[touches[0].fingerId] = tracker;
				
				return tracker;
			}
		}
		
		return null;
	}
	
	
	//Begin Tracking
	public virtual TouchTracker BeginTrackingMouse(Vector2 Position)
	{	
		//Get all the HUD element
//		GameObject[] HUDElements = GameObject.FindGameObjectsWithTag("HUDElement") as GameObject[];
	
		
//		//Loop Trough the HUD elements to see if one of them has been touch
//		foreach(PanningEntity PanningEntity in interactive2DEntities){
//			if((PanningEntity.gameObject.guiTexture.enabled && PanningEntity.gameObject.guiTexture.color.a > 0) && PanningEntity.gameObject.guiTexture.HitTest(Position)){
//				//create the tracker
//				TouchTracker tracker = new TouchTracker(Position, PanningEntity.gameObject);
//				
//				if (doubleTapCheckStartTime != 0 && Time.time <= doubleTapCheckStartTime + doubleTapActivationTime && Vector2.Distance(Position, doubleTapFirstTapPosition) <= 40)
//				{
//					tracker.isDoubleTap = true;
//				}
//				
////				Debug.Log("BeginTrackingMouse => " + PanningEntity.transform.parent.name);
//				
//				return tracker;
//			}
//		}
		
		
		
		
		// 3D OBJECTS (THEY NEED A COLLIDER)
		// We need to do this for every camera in the scene
		Camera[] cameras = Camera.allCameras;
		
		for (int i = cameras.Length - 1; i > -1; i--)
		{
			Debug.Log(cameras[i].tag);
		
			//Need to project the touch to see what the user touches in the game
			//create a ray to project the x and y touch position to the world
			Ray ray = cameras[i].ScreenPointToRay(Position);
			RaycastHit hitInfo;
			
			LayerMask layerMask;
			if (cameras[i].tag == "GUICamera")
				layerMask = 1 << 31;
			else
				layerMask = (1 << 0) | (1 << 8);
			
			//if the Ray hit something create a new touch
			if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
			{
				//create the tracker
				TouchTracker tracker = new TouchTracker(Position,hitInfo.transform.gameObject);
	//			Debug.Log(hitInfo.transform.gameObject.name);
				
				return tracker;
			}
		}
		
		return null;
	}
	
	
	//Clear a tracker from being updated, tell it to stop
	public virtual void EndTracking(TouchTracker tracker){
		if (tracker.isMultiTouch)
		{
			if (tracker.isActive)
			{
				tracker.EndMultiTouch();
			}
			disabledInputsAfterMultiTouchStartTime = Time.time;
		}
		else
		{
			if (isPinching)
			{
				isPinching = false;
				pinchActivationCheckStartTime = 0;
				
				Debug.Log("Pinching disabled");
			}
			else
			{
				if (tracker.isActive)
				{
					tracker.End();
				}
			}
			
			if (!tracker.isDoubleTap && tracker.isCandidateForDoubleTap)
			{
				doubleTapCheckStartTime = Time.time;
				doubleTapFirstTapPosition = tracker.m_Position;
			}
		}
		
		m_Trackers.Remove(tracker);
		m_TrackerLookup[tracker.fingerId] = null;
	}
	
	
	//Clear a tracker from being updated, tell it to stop
	public virtual void EndTrackingMouse(TouchTracker tracker){
		if(m_Touch != null)
		{
			if (m_Touch.isActive)
			{
				m_Touch.End();
			}
			
			if (!m_Touch.isDoubleTap && m_Touch.isCandidateForDoubleTap)
			{
				doubleTapCheckStartTime = Time.time;
				doubleTapFirstTapPosition = tracker.m_Position;
			}
		}
		
		m_Touch = null;
	}
	
	
//	//Clear all the trackers from being updated, tell them to stop
//	public virtual void EndEveryTracking(){
//		if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
//			foreach(TouchTracker LoopTracker in m_Trackers)
//			{
//				EndTracking(LoopTracker);
//			}
//		else
//			EndTrackingMouse(m_Touch);
//		
//	}

}
