using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using System;

public class TouchTracker
{

    #region Variables

    private Logic logic;

    // expose our finger id for convenience
    private int m_FingerId;

    //	// our last data from Unity
    //	private	Touch 		m_Touch;

    // have we been updated this frame?
    private bool m_IsDirty = false;

    // track how long we've been active
    private float m_TotalTime = 0.0f;

    //GameObject which will be updated by the touch (unless pinching)
    private GameObject m_UpdatedObject;


    public Vector2 m_Position { get; private set; }

    public Vector2[] m_Positions;

    private Touch[] m_Touches;

    public bool isMultiTouch; // If true, will behave differently than usual (this tracker is associated to a multitouch touch, not a normal one)

    public bool isActive { get; private set; }

    private float activationTime = 0.00f; // Duration before this tracker becomes active (allows to prevent unintentional touch before a multitouch tracker)

    public bool isDoubleTap = false; // If true, will behave differently than usual (this tracker is associated to a double tap touch, not a normal one)

    public bool isAnchored = true; // If false, m_UpdatedObject will change when the tracked finger moves onto another object

    public bool isCandidateForDoubleTap = true; // If true, this touch will activate the input manager's doubleTapCheckTime on End()
    public float maxTimeForCandidateToDoubleTap = 0.25f;

    public bool prematureEndBecauseOfMultitouch = false;

    // Pinching
    //	public bool 		isPinching = false;

    #endregion


    // Classic tracker constructor
    public TouchTracker(Touch firstTouch, GameObject objectTouched)
    {
        m_FingerId = firstTouch.fingerId;
        //		m_Touch = firstTouch;
        m_UpdatedObject = objectTouched;
        m_Position = firstTouch.position;

        isMultiTouch = false;
        isActive = false;

        Begin();
    }


    // Constructor for mouse tracker
    public TouchTracker(Vector2 firstTouchposition, GameObject objectTouched)
    {
        //m_FingerId = firstTouch.fingerId;
        //m_Touch = firstTouch;
        m_UpdatedObject = objectTouched;

        isMultiTouch = false;
        isActive = false;

        //		activationTime = 0.07f;

        Begin();
    }


    // Constructor for multitouch tracker
    public TouchTracker(Touch[] firstTouches, GameObject objectTouched)
    {
        m_FingerId = firstTouches[0].fingerId;
        //		m_Touch = firstTouch;
        m_UpdatedObject = objectTouched;
        //		m_Position = firstTouch.position;

        m_Touches = firstTouches;

        m_Positions = new Vector2[firstTouches.Length];
        for (int i = 0; i < m_Touches.Length; i++)
            m_Positions[i] = m_Touches[i].position;

        isMultiTouch = true;
        isActive = false;

        Begin();
    }


    // Update is called once per frame
    public void UpdateTouch(Vector2 touchPosition)
    {
        if (!isActive && (m_TotalTime >= activationTime || isDoubleTap))
        {
            isActive = true;
            // Execute the TouchBegin action
            if (m_UpdatedObject && m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity)
            {
                if (isDoubleTap)
                    (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).DoubleTapBegin(touchPosition);
                else
                    (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).TouchBegin(touchPosition);
            }
        }

        else if (isActive)
        {
            if (isCandidateForDoubleTap && m_TotalTime > maxTimeForCandidateToDoubleTap)
            {
                isCandidateForDoubleTap = false;
            }
            // If this tracker is not anchored to its m_UpdatedObject, get the object currently being hovered on by this tracker
            if (!isAnchored)
            {
                ChangeUpdatedObject(touchPosition);
            }

            // Execute the TouchUpdate action
            if (m_UpdatedObject && m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity)
            {
                if (isDoubleTap)
                    (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).DoubleTapUpdate(touchPosition);
                else
                    (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).TouchUpdate(touchPosition);
            }
        }

        m_Position = touchPosition;

        m_TotalTime += Time.deltaTime;
        //indicate that the input is still running
        m_IsDirty = true;
    }


    public void UpdatePinch(Vector2 touch1Position, Vector2 touch2Position)
    {
        Debug.Log("Pinching");

        logic.cam.Zoom(-(Vector2.Distance(touch1Position, touch2Position) - Vector2.Distance(m_Positions[0], m_Positions[1])));

        m_Positions = new Vector2[2] { touch1Position, touch2Position };

        m_TotalTime += Time.deltaTime;
        //indicate that the input is still running
        m_IsDirty = true;
    }


    // Update is called once per frame
    public void UpdateMultiTouch(Vector2[] touchPositions)
    {
        if (!isActive && m_TotalTime >= activationTime)
        {
            isActive = true;

            // Execute the TouchBegin action
            if (m_UpdatedObject && m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity)
                (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).MultiTouchBegin(touchPositions);
        }

        else if (isActive)
        {
            // Execute the TouchUpdate action
            if (m_UpdatedObject && m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity)
                (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).MultiTouchUpdate(touchPositions);
        }
        //		else{
        //			(m_UpdatedObject.GetComponent(typeof(DragNDropButton)) as DragNDropButton).TouchUpdate(touchPosition);
        //		}

        m_Positions = touchPositions;

        m_TotalTime += Time.deltaTime;
        //indicate that the input is still running
        m_IsDirty = true;
    }


    // Update the m_UpdatedObject
    public void ChangeUpdatedObject(Vector2 touchPosition)
    {


        //Get all the HUD element
        BaseEntity[] interactive2DEntities = GameObject.FindObjectsOfType(typeof(BaseEntity)) as BaseEntity[];
        //		System.Array.Sort(interactive2DEntities, new Interactive2DEntitiesZComparer());

        foreach (BaseEntity BaseEntity in interactive2DEntities)
        {
            // If this VKey is still under the touch at TouchEnd time

            //TODO || DEV21

#if UNITY_5
            if (BaseEntity.gameObject.GetComponent<GUITexture>().enabled && BaseEntity.gameObject.GetComponent<GUITexture>().HitTest(touchPosition))
            {

                //			if (hitInfo.transform.gameObject && hitInfo.transform.gameObject.GetComponent(typeof(BaseEntity)) as BaseEntity && !(hitInfo.transform.gameObject.GetComponent(typeof(BaseEntity)) as BaseEntity).isAnchoredToTouch)
                if (!BaseEntity.isAnchoredToTouch)
                {
                    m_UpdatedObject = BaseEntity.transform.gameObject;
                }
                else
                {
                    if (m_UpdatedObject && m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity)
                        (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).TouchEndNotAnchored(touchPosition);
                    m_UpdatedObject = null;
                }

                break;
            }
#endif
        }
    }


    /**
	* Called before our manager updates everything
	*/
    public void Clean()
    {
        m_IsDirty = false;
    }


    public bool isDirty
    {
        // Only declare readonly property by omitting 'set'.
        get
        {
            return m_IsDirty;
        }
    }


    public int fingerId
    {
        // Only declare readonly property by omitting 'set'.
        get
        {
            return m_FingerId;
        }
    }


    /**
	* Callback when we start
	*/
    public void Begin()
    {
        logic = GameObject.FindObjectOfType(typeof(Logic)) as Logic;

        m_IsDirty = true;
        isActive = false;
        //		Debug.Log("started tracking finger " + m_FingerId);

        if (m_UpdatedObject && m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity)
        {
            isAnchored = (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).isAnchoredToTouch;
        }
    }


    /**
	* Clean things up
	*/
    public void End()
    {
        //		Debug.Log("ended tracking finger " + m_FingerId);
        //		Debug.Log("End => " + m_UpdatedObject.transform.parent.name);

        if (m_UpdatedObject && m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity)
        {
            if (prematureEndBecauseOfMultitouch)
                (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).TouchEndBecauseOfMultitouch(m_Position);
            else if (isDoubleTap)
                (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).DoubleTapEnd(m_Position);
            else
            {
                (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).TouchEnd(m_Position);
            }
        }
    }


    public void EndMultiTouch()
    {
        //		Debug.Log("ended multitouch tracking finger " + m_FingerId);
        //		Debug.Log("EndMultiTouch => " + m_UpdatedObject.transform.parent.name);

        if (m_UpdatedObject && m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity)
        {
            (m_UpdatedObject.GetComponent(typeof(BaseEntity)) as BaseEntity).MultiTouchEnd(m_Positions);
        }
    }
}