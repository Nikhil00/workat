using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EndOfGridAnimGrenade : EndOfGridAnim
{

    public List<LetterEntity> targetEntities = new List<LetterEntity>();

    public int nbTargets;
    public float explosionDuration, explosionColliderSizeMultiplier;

    protected float delayBetweenTargetsExplode;
    protected float lastExplosionTime;


    // Use this for initialization
    public override void Init()
    {
        base.Init();

        endOfGridAnimType = Global.EndOfGridAnim.Grenade;

        LetterEntity[] letterEntities = GameObject.FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];


        foreach (LetterEntity letterEntity in letterEntities)
        {
            targetEntities.Add(letterEntity);
        }

        delayBetweenTargetsExplode = endAnimsDuration / (nbTargets + 3);
        lastExplosionTime = -delayBetweenTargetsExplode;
    }



    public override void StartAnim()
    {
        base.StartAnim();

        LetterEntity[] letterEntities = GameObject.FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];

        foreach (LetterEntity letterEntity in letterEntities)
        {
            letterEntity.GetComponent<Rigidbody>().mass = 10f;
            letterEntity.GetComponent<Rigidbody>().drag = 0f;
            letterEntity.GetComponent<Collider>().isTrigger = false;

            if (gameLogic.gameMode == Global.GameMode.DragNDrop)
            {
                if ((letterEntity as TokenEntity).joint != null)
                    (letterEntity as TokenEntity).joint.breakForce = 0.8f;
            }
        }
    }


    protected override void UpdateAnim()
    {
        if (Time.time >= lastExplosionTime + delayBetweenTargetsExplode)
        {
            if (targetEntities.Count > 0)
            {
                int index = Random.Range(0, targetEntities.Count);

                LetterEntity target = targetEntities[index];

                target.UnlockRigidBodyConstraints(true);
                target.GetComponent<Rigidbody>().mass = 50;

                targetEntities.RemoveAt(index);

                lastExplosionTime = Time.time;
                target.explosionDuration = explosionDuration;
                target.explosionColliderSizeMultiplier = explosionColliderSizeMultiplier;
                target.StartExploding();

                Debug.Log("EXPLODE");

                targetEntities.Remove(target);
            }
        }


    }



}
