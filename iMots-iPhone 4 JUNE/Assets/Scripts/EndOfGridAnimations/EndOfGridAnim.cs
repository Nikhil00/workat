using UnityEngine;
using System.Collections;

public abstract class EndOfGridAnim : MonoBehaviour
{

    public bool playingEndAnims = false;
    public float endAnimsDuration = 10;
    public float endAnimsStartTime;
    public Global.EndOfGridAnim endOfGridAnimType = Global.EndOfGridAnim.Error;

    protected GameLogic gameLogic;

    // Use this for initialization
    public virtual void Init()
    {
        gameLogic = (GameLogic)GameObject.FindObjectOfType(typeof(GameLogic));
    }

    // Update is called once per frame
    void Update()
    {
        if (playingEndAnims)
        {
            if (Time.time >= endAnimsStartTime + endAnimsDuration)
            {
                StopAnim();
            }
            else
            {
                UpdateAnim();
            }
        }
    }


    public virtual void StartAnim()
    {
        playingEndAnims = true;
        endAnimsStartTime = Time.time;
    }


    public virtual void StopAnim()
    {
        playingEndAnims = false;

        gameLogic.EndOfGridAnimFinished();
    }


    protected abstract void UpdateAnim();
}
