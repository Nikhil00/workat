using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EndOfGridAnimRotateZ : EndOfGridAnim
{

    // Use this for initialization
    public override void Init()
    {
        base.Init();

        endOfGridAnimType = Global.EndOfGridAnim.RotateZ;

    }


    public override void StartAnim()
    {
        base.StartAnim();

        LetterEntity[] letterEntities = GameObject.FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];

        foreach (LetterEntity letterEntity in letterEntities)
        {
            letterEntity.StartRotatingZ();
        }
    }


    protected override void UpdateAnim()
    {

    }



}
