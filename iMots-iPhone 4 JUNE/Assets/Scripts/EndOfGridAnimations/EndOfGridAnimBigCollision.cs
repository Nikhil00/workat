using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EndOfGridAnimBigCollision : EndOfGridAnim
{


    // Use this for initialization
    public override void Init()
    {
        base.Init();

        endOfGridAnimType = Global.EndOfGridAnim.BigCollision;

        transform.position = new Vector3(gameLogic.gridGO.transform.position.x, gameLogic.gridGO.transform.position.y, gameLogic.gridGO.transform.position.z - gameLogic.grid.tiles.GetLength(0) * 0.5f);
    }


    public override void StartAnim()
    {
        base.StartAnim();

        LetterEntity[] letterEntities = GameObject.FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];

        foreach (LetterEntity letterEntity in letterEntities)
        {
            letterEntity.GetComponent<Collider>().isTrigger = false;
            letterEntity.GetComponent<Rigidbody>().isKinematic = false;
        }
    }


    protected override void UpdateAnim()
    {
        (GetComponent<Collider>() as CapsuleCollider).radius *= 1.01f;
    }



}
