using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EndOfGridAnimHelium : EndOfGridAnim
{

    protected List<LetterEntity> targetEntities = new List<LetterEntity>();

    protected float delayBetweenTargetsRises;
    protected float lastRiseTime;

    // Delayed impulse
    bool impulseToBeApplied = false;
    LetterEntity impulseTarget;

    // Use this for initialization
    public override void Init()
    {
        base.Init();

        endOfGridAnimType = Global.EndOfGridAnim.Helium;

        GridElementEntity elem;

        int y = gameLogic.grid.tiles.GetLength(0) - 1;

        for (int i = 0; i < gameLogic.grid.tiles.GetLength(1); i++)
        {
            elem = gameLogic.GetGridElementAtGridPosition(new Vector2(i, y));

            if (elem.IsLetterEntity())
            {
                targetEntities.Add(elem as LetterEntity);
                elem.gameObject.AddComponent(typeof(ConstantForce));
            }

        }

        delayBetweenTargetsRises = endAnimsDuration / (targetEntities.Count + 3);
        lastRiseTime = -delayBetweenTargetsRises;

    }


    public override void StartAnim()
    {
        base.StartAnim();

        LetterEntity[] letterEntities = GameObject.FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];

        foreach (LetterEntity letterEntity in letterEntities)
        {
            letterEntity.GetComponent<Rigidbody>().mass = 10f;
            letterEntity.GetComponent<Rigidbody>().drag = 0f;
            letterEntity.GetComponent<Collider>().isTrigger = false;

            if (gameLogic.gameMode == Global.GameMode.DragNDrop)
            {
                if ((letterEntity as TokenEntity).joint)
                    (letterEntity as TokenEntity).joint.breakForce = 0.8f;
            }
        }
    }


    protected override void UpdateAnim()
    {
        if (impulseToBeApplied)
        {
            impulseToBeApplied = false;

            Vector2 impulse = new Vector2(Random.Range(-10, 10), 10);
            impulseTarget.GetComponent<Rigidbody>().AddForce(new Vector3(impulse.x, 0, impulse.y), ForceMode.Impulse);

            float torque = Random.Range(-1.0f, 1.0f);
            Debug.Log(torque);
            impulseTarget.GetComponent<Rigidbody>().AddTorque(new Vector3(0, torque, 0), ForceMode.Impulse);

            Debug.Log("IMPULSE");
        }

        if (Time.time >= lastRiseTime + delayBetweenTargetsRises)
        {
            if (targetEntities.Count > 0)
            {
                int index = Random.Range(0, targetEntities.Count);

                LetterEntity target = targetEntities[index];

                target.UnlockRigidBodyConstraints(true);

                target.GetComponent<Rigidbody>().useGravity = false;
                //				target.rigidbody.AddForce( 0, 0, 9.81f, 
                //				target.gameObject.AddComponent(typeof(ConstantForce));
                target.GetComponent<ConstantForce>().force = new Vector3(0, 0, 100f);

                targetEntities.RemoveAt(index);

                lastRiseTime = Time.time;

                impulseToBeApplied = true;
                impulseTarget = target;

                Debug.Log("UNLOCK");
            }
        }


    }



}
