using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EndOfGridAnimGrow : EndOfGridAnim
{

    protected List<LetterEntity> targetEntities = new List<LetterEntity>();

    public int nbTargets;

    protected float delayBetweenTargetsExplode;
    protected float lastExplosionTime;


    // Use this for initialization
    public override void Init()
    {
        base.Init();

        if (Global.Global.iPadVersion)
        {
            float nbTarg = nbTargets * 1.5f;
            nbTargets = Mathf.RoundToInt(nbTarg);
        }

        endOfGridAnimType = Global.EndOfGridAnim.Grow;

        LetterEntity elem;

        LetterEntity[] letterEntities = GameObject.FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];

        for (int i = 0; i < nbTargets; i++)
        {
            int index = Random.Range(0, letterEntities.Length - 1);
            elem = letterEntities[index];

            if (!targetEntities.Contains(elem))
                targetEntities.Add(elem);
            else
                i--;
        }

        delayBetweenTargetsExplode = endAnimsDuration / (targetEntities.Count + 3);
        lastExplosionTime = -delayBetweenTargetsExplode;
    }


    public override void StartAnim()
    {
        base.StartAnim();

        LetterEntity[] letterEntities = GameObject.FindObjectsOfType(typeof(LetterEntity)) as LetterEntity[];

        foreach (LetterEntity letterEntity in letterEntities)
        {
            letterEntity.GetComponent<Rigidbody>().mass = 10f;
            letterEntity.GetComponent<Rigidbody>().drag = 0f;
            letterEntity.GetComponent<Collider>().isTrigger = false;
            (letterEntity.GetComponent<Collider>() as SphereCollider).radius *= 1.25f;
            letterEntity.UnlockRigidBodyConstraints(false);
        }


        foreach (LetterEntity letterEntity in targetEntities)
        {
            letterEntity.StartGrow();
        }
    }


    protected override void UpdateAnim()
    {

    }



}
