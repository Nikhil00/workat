using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;


public class XMLManager
{

    public static string gameStateFilename = Application.persistentDataPath + "/GameState.xml";
    public static string _xmlDatabasePath = "XMLs/XMLDatabase";

    public static bool dataCorrupted = false;

    //	// Loads the given xml file and returns the corresponding XmlDocument
    //	private static XmlDocument LoadFile(string filename, bool isTextAsset)
    //    {
    //        XmlDocument xmlDoc = new XmlDocument();
    //
    //
    //
    //        try
    //        {
    //			if(isTextAsset)
    //				xmlDoc.LoadXml((Resources.Load(filename, typeof(TextAsset)) as TextAsset).text);
    //			else
    //            	xmlDoc.Load(filename);
    //		}
    //        catch (System.Exception e)
    //        {
    //			Debug.LogError("Couldn't load XML file " + filename + ".\nException raised: " + e);
    //            return null;
    //        }
    //
    //		return xmlDoc;
    //    }


    // Loads the given xml file and returns the corresponding XmlDocument
    private static XmlDocument LoadFile(string filename, bool isTextAsset)
    {
        XmlDocument xmlDoc = new XmlDocument();
        Debug.Log("Nik - LoadFile In Enter Filename : " + filename + " is bool is : " + isTextAsset);
        try
        {
            if (isTextAsset)
            {
                string[] splittedId = filename.Split('-');

                if (splittedId.Length != 4)
                {
                    Debug.LogWarning("Be careful, grid ID " + filename + " doesn't match the intended format");
                    return null;
                }

                int gridSize = int.Parse(splittedId[1]);
                // WORKAROUND => in THIS version of the game, the first 50 grids (except from ipad's 18x18) XMLs are in a different enconding, and thus we don't need to use the following BOM workaround...
                if (gridSize == 2 || gridSize < 2 && int.Parse(splittedId[3]) > 50)
                {
                    System.IO.StringReader stringReader = new System.IO.StringReader((Resources.Load(filename, typeof(TextAsset)) as TextAsset).text);
                    //										stringReader.Read (); // WORKAROUND to pass through the file's BOM, which is making Unity parser have an error
                    xmlDoc.LoadXml(stringReader.ReadToEnd());
                }
                else
                {
                    xmlDoc.LoadXml((Resources.Load(filename, typeof(TextAsset)) as TextAsset).text);
                }
            }
            else
                xmlDoc.Load(filename);
        }
        catch (System.Exception e)
        {
            Debug.LogError("Couldn't load XML file " + filename + ".\nException raised: " + e);
            return null;
        }

        return xmlDoc;
    }


    // Loads the given grid file into memory
    public static Grid LoadGridFile(string filename)
    {
        Grid loadedGrid;

        XmlDocument xmlDoc = LoadFile(filename, true);

        if (xmlDoc != null)
        {
            XmlNodeList xmlGrid = xmlDoc.GetElementsByTagName("ArrowWords");
            loadedGrid = new Grid(xmlGrid[0].Attributes["GameId"].Value);
            Debug.Log("Nik - LoadGridFile name  : " + loadedGrid.name  + " || Size : " + loadedGrid.size + " || Tiles : " + loadedGrid.tiles + " || State : " +loadedGrid.state);

            XmlNodeList xmlItems = xmlDoc.GetElementsByTagName("Item");

            foreach (XmlNode item in xmlItems)
            {
                loadedGrid.CreateDefinitionTile(int.Parse(item.FirstChild.Attributes["x"].Value) - 1, int.Parse(item.FirstChild.Attributes["y"].Value) - 1,                 // Position of the def tile
                        item.FirstChild.InnerText.Trim(), item.FirstChild.Attributes["size"].Value, item.FirstChild.Attributes["position"].Value,   // Values concerning the def
                        Regex.Replace(item.LastChild.InnerText, @"\s", ""), item.LastChild.Attributes["direction"].Value,                           // Values concerning the ans
                        int.Parse(item.LastChild.Attributes["x"].Value) - 1, int.Parse(item.LastChild.Attributes["y"].Value) - 1,                   // Position of the ans start tile
                        item.LastChild.Attributes["hyphen"].Value);                                                                                 // Values concerning the hyphen
            }

            loadedGrid.FillRemainingWithLetterTiles();


            return loadedGrid;
        }

        return null;
    }


    // Loads all the grid files located in the given folder into memory
    public static List<Grid> LoadGridFilesInFolder(string folder)
    {
        List<Grid> grids = new List<Grid>();
        string[] files = Directory.GetFiles(folder, "*.xml");//, System.IO.SearchOption.AllDirectories);

        for (int i = 0; i < files.Length; i++)
        {
            Grid grid = XMLManager.LoadGridFile(files[i].Substring(17, files[i].Length - 17 - 4));
            grids.Add(grid);
        }

        return grids;
    }



    // Loads the given grid file's header into memory ("surface" loading, not in depth => used to init the PlayerPrefs)
    public static string LightLoadGridFile(TextAsset file)
    {
        XmlDocument xmlDoc = new XmlDocument();

        try
        {
            xmlDoc.LoadXml(file.text);
        }
        catch (System.Exception e)
        {
            Debug.LogError("Couldn't light load TextAsset " + file.name + ".\nException raised: " + e);
            return null;
        }


        if (xmlDoc != null)
        {
            XmlNodeList xmlGrid = xmlDoc.GetElementsByTagName("ArrowWords");

            Debug.Log("XMLManager light loaded " + file.name);

            return xmlGrid[0].Attributes["GameId"].Value;
        }

        return null;
    }


    // Loads all the grid files located in the given folder into memory ("surface" loading, not in depth => used to init the PlayerPrefs)
    public static List<string> LightLoadGridFilesInFolder(string folder)
    {
        List<string> headers = new List<string>();
        Debug.Log("Before LoadAll in folder " + folder);
        UnityEngine.Object[] files = Resources.LoadAll(folder, typeof(TextAsset));
        Debug.Log("After LoadAll");

        for (int i = 0; i < files.Length; i++)
        {
            string header = files[i].name;
            headers.Add(header);
        }

        return headers;
    }



    // Loads all the grid files headers located in the given folder into memory ("surface" loading, not in depth => used to init the PlayerPrefs)
    public static List<string> LoadGridFilesHeaders()
    {
        List<string> headers = new List<string>();

        string platform;
        if (Global.Global.iPadVersion)
            platform = "-iPad";
        else
            platform = "-iPhone";

        string line;

        try
        {
            System.IO.StringReader stringReader = new System.IO.StringReader((Resources.Load(_xmlDatabasePath + platform) as TextAsset).text);
            int counter = 0;
            while ((line = stringReader.ReadLine()) != null)
            {
                //								counter++;
                //								Debug.Log ("Line " + line + " == counter " + counter);
                headers.Add(line);
                // Debug.Log("Nik - Headers : " + line);
            }

            stringReader.Close();
        }
        catch (System.Exception e)
        {
            Debug.LogError("Couldn't load file " + _xmlDatabasePath + platform + ".\nException raised: " + e);
            return null;
        }

        headers.Sort();
        Debug.Log("Nik - Headers : " + headers);
        return headers;
    }



    // Loads the given gameState file into memory
    public static void LoadGameStateFile(ref Grid grid)
    {
        if (grid == null)
        {
            Debug.LogError("Couldn't load game state file " + gameStateFilename + " because there is no corresponding grid loaded.");
            return;
        }

        XmlDocument xmlDoc = LoadFile(gameStateFilename, false);

        if (xmlDoc != null)
        {

            XmlNodeList xmlGrids = xmlDoc.GetElementsByTagName("Grid");
            XmlElement xmlGrid = null;

            // Search for the grid matching the given grid id	
            bool found = false;

            for (int i = 0; i < xmlGrids.Count; i++)
            {
                xmlGrid = xmlGrids[i] as XmlElement;
                if (xmlGrid.Attributes["id"].Value == grid.name)
                {
                    found = true;
                    break;
                }
            }

            if (xmlGrid == null || !found)
            {
                Debug.LogError("Couldn't load game state file " + gameStateFilename + " because no grid matching the given grid id " + grid.name + " was found in that file.");
                return;
            }

            if (xmlGrid.Attributes["gameMode"].Value == Global.GameMode.DragNDrop.ToString())
                grid.gameMode = Global.GameMode.DragNDrop;
            else if (xmlGrid.Attributes["gameMode"].Value == Global.GameMode.Keyboard.ToString())
                grid.gameMode = Global.GameMode.Keyboard;
            else
            {
                Debug.LogError("Couldn't load game state file " + gameStateFilename + " because the game mode of the grid " + grid.name + " is not valid.");
                return;
            }

            XmlNodeList xmlLetterTiles = xmlGrid.ChildNodes;

            foreach (XmlNode letterTile in xmlLetterTiles)
            {
                int x = int.Parse(letterTile.Attributes["x"].Value);
                int y = int.Parse(letterTile.Attributes["y"].Value);

                // Security check
                if (x >= 0 && x < grid.tiles.GetLength(1) && y >= 0 && y < grid.tiles.GetLength(0) && grid.tiles[y, x].GetTileType() == Global.TileType.Letter)
                {
                    (grid.tiles[y, x] as LetterTile).guessLetter = letterTile.Attributes["guess"].Value;
                    //					Debug.Log(letterTile.Attributes["alreadyMoved"].Value);
                    if (letterTile.Attributes["alreadyMoved"] != null && letterTile.Attributes["alreadyMoved"].Value == "True")
                        (grid.tiles[y, x] as LetterTile).alreadyMoved = true;
                    else
                        (grid.tiles[y, x] as LetterTile).alreadyMoved = false;
                }
            }
        }
        else
        {
            dataCorrupted = true;
        }

    }


    // Save the given XmlDocument in the given filename
    private static void SaveFile(XmlDocument xmlDoc, string filename)
    {
        // Check if the given directory exists
        string directoryPath = filename.Substring(0, filename.LastIndexOf('/'));
        if (!Directory.Exists(directoryPath))
        {
            try
            {
                Directory.CreateDirectory(directoryPath);

                Debug.Log("XMLManager created the folder " + directoryPath);
            }
            catch (System.Exception e)
            {
                Debug.LogError("Couldn't create new folder to save XML file " + filename + ".\nException raised: " + e);
            }
        }

        try
        {
            xmlDoc.Save(filename);

            Debug.Log("XMLManager saved the file to " + filename);
        }
        catch (System.Exception e)
        {
            Debug.LogError("Couldn't save XML file " + filename + ".\nException raised: " + e);
        }
    }


    // Save the given gameState in the given filename
    public static void SaveGameStateFile(Grid grid)
    {
        XmlDocument xmlDoc = new XmlDocument();
        XmlElement xmlGrid = null, xmlElem, xmlRoot = null;

        // If file exist look if the grid already exist => overwrite
        if (File.Exists(gameStateFilename))
        {
            xmlDoc = LoadFile(gameStateFilename, false);
            Debug.Log("Game state file " + gameStateFilename + " already exists, now looking if the grid " + grid.name + " already exist witin it.");

            if (xmlDoc != null)
            {
                xmlRoot = xmlDoc.GetElementsByTagName("Root")[0] as XmlElement;
                XmlNodeList xmlGrids = xmlDoc.GetElementsByTagName("Grid");

                // Search for the grid matching the given grid id	
                bool found = false;

                for (int i = 0; i < xmlGrids.Count; i++)
                {
                    xmlGrid = xmlGrids[i] as XmlElement;
                    if (xmlGrid.Attributes["id"].Value == grid.name)
                    {
                        found = true;
                        break;
                    }
                }

                // The given grid entry is not found => create it from scratch
                if (xmlGrid == null || !found)
                {
                    Debug.Log("The grid " + grid.name + " doesn't exist yet within the game state file, now creating it.");

                    xmlGrid = xmlDoc.CreateElement("Grid");
                }
                // The given grid entry is found => overwrite it
                else
                {
                    Debug.Log("The grid " + grid.name + " already exist within the game state file, now overwriting it.");
                    xmlGrid.RemoveAll();
                }
            }
        }

        // File doesn't exist => create one from scratch
        else
        {
            // Add the XML Declaration and the root at the very top
            xmlDoc.AppendChild(xmlDoc.CreateNode(XmlNodeType.XmlDeclaration, "", ""));
            xmlRoot = xmlDoc.CreateElement("Root");
            xmlDoc.AppendChild(xmlRoot);

            // Add the given grid
            xmlGrid = xmlDoc.CreateElement("Grid");
        }



        xmlGrid.SetAttribute("id", grid.name);
        xmlGrid.SetAttribute("gameMode", grid.gameMode.ToString());
        xmlRoot.AppendChild(xmlGrid);


        for (int i = 0; i < grid.tiles.GetLength(1); i++)
        {
            for (int j = 0; j < grid.tiles.GetLength(0); j++)
            {
                if (grid.tiles[j, i].GetTileType() == Global.TileType.Letter)
                {
                    xmlElem = xmlDoc.CreateElement("LetterTile");
                    xmlElem.SetAttribute("x", i.ToString());
                    xmlElem.SetAttribute("y", j.ToString());
                    xmlElem.SetAttribute("guess", (grid.tiles[j, i] as LetterTile).guessLetter);
                    xmlElem.SetAttribute("alreadyMoved", (grid.tiles[j, i] as LetterTile).alreadyMoved.ToString());
                    xmlGrid.AppendChild(xmlElem);
                }
            }
        }

        SaveFile(xmlDoc, gameStateFilename);
    }


    // Remove the given grid from the gameState file
    public static void RemoveFromGameStateFile(string gridID)
    {
        XmlDocument xmlDoc = new XmlDocument();
        XmlElement xmlGrid = null, xmlRoot;

        // If file exist look if the grid already exist => overwrite
        if (File.Exists(gameStateFilename))
        {
            xmlDoc = LoadFile(gameStateFilename, false);

            if (xmlDoc != null)
            {
                xmlRoot = xmlDoc.GetElementsByTagName("Root")[0] as XmlElement;

                XmlNodeList xmlGrids = xmlDoc.GetElementsByTagName("Grid");

                // Search for the grid matching the given grid id	
                bool found = false;

                for (int i = 0; i < xmlGrids.Count; i++)
                {
                    xmlGrid = xmlGrids[i] as XmlElement;
                    if (xmlGrid.Attributes["id"].Value == gridID)
                    {
                        found = true;
                        break;
                    }
                }

                // The given grid entry is not found => return
                if (xmlGrid == null || !found)
                {
                    Debug.Log("The grid " + gridID + " doesn't exist within the game state file, thus cannot remove it.");
                    return;
                }
                // The given grid entry is found => remove it
                else
                {
                    Debug.Log("The grid " + gridID + " exists within the game state file, now removing it.");
                    xmlRoot.RemoveChild(xmlGrid);
                }

                SaveFile(xmlDoc, gameStateFilename);
            }
        }
        else
        {
            Debug.Log("GameState file doesn't exist, thus there is nothing to remove.");
        }
        //		SaveFile(xmlDoc, gameStateFilename); // TO BE REMOVED - Used to test the corrupted game data (to do so, uncomment it, delete the game state file, start a new grid and try to reset it twice)
    }


    // Delete all saved progress
    public static void ResetAllSavedData(List<string> headers)
    {
        foreach (string header in headers)
        {
            if (PlayerPrefs.HasKey(header))
                PlayerPrefs.SetInt(header, (int)Global.GridState.Empty);
            if (PlayerPrefs.HasKey(header + "_Timer"))
                PlayerPrefs.SetFloat(header + "_Timer", 0);
            if (PlayerPrefs.HasKey(header + "_Combo"))
                PlayerPrefs.SetInt(header + "_Combo", 0);
        }

        if (File.Exists(gameStateFilename))
        {
            Debug.Log("Deleting GameState file!");
            File.Delete(gameStateFilename);

            dataCorrupted = false;
        }
    }
}
