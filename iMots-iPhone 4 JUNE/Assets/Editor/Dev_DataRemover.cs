﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Dev_DataRemover : MonoBehaviour
{
    [MenuItem("Dev/Remove Data")]
    public static void RemoveData()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("<b><color=red>DATA REMOVED!</color></b>");
    }
}
